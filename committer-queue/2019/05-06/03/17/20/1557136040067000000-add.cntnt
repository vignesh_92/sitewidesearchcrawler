

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Explains: Role of Inflation in Financial Planning

            		November 27, 2017 . Mutual Fund Research Desk
                    

	
                    
                                    Simply Important
                    
		
            	

            	If you have ever spoken to a financial advisor, they might have asked you about your financial goals. Thinking of your investments in terms of goals requires you to estimate the amount of money you will need at a future date. People often take current costs and use that as an estimate of their requirements. For instance, if a person is saving for his child’s education, and an engineering degree currently costs  Rs. 10 Lakh, the investor might state that he/she would like to save an arbitrary amount of say Rs 12-15 Lakh for their child who is now 3 years old.

How much impact does inflation have

This kind of estimation is unrealistic. Estimates should consider the expected inflation rates for the entire duration of the goal. For instance, if a goal is 15 years away, an inflation rate of 7% will lead to prices multiplying  2.8 times. This means that a course which costs Rs. 10 Lakhs today will cost Rs. 28 Lakhs 15 years from now.

The effect is more pronounced while estimating monthly requirements well into the future. At times I have been asked, “how can I earn Rs. 1 Lakh every month after I retire.” The assumption here is that the person needs Rs. 1 Lakh per month at the time of retirement. However, what is not being considered is that Rs. 1 Lakh per month is not going to be enough forever.

Think of it this way. How quickly does the cost of rice rise? According to data released by Ministry of Statistics and Program Implementation (MOSPI), the same quantity of rice which would have cost you Rs. 100 in January 2014 (2014 was taken as it comes with a name base for inflation), would cost you Rs. 114 in October 2017. This is an increase of 3.6% per year.

Medical costs rise faster than food prices. According to the same MOSPI data, during the period of January 2014 to October 2017, hospital and nursing home charges grew by 6.8% per year whereas doctor’s/surgeon’s fees grew by 7.8% per year.

Consider a 50-year old man whose current monthly expense is Rs. 50,000. He wants to retire at the age of 60. Even if we were to assume a modest rate of inflation of 4%, he would need Rs. 74,000 per month at the time of retirement, Rs. 1,10,000 per month when he is 70-years old and Rs. 1,62,000 per month when he is 80-years old. And this is considering no change in lifestyle. An improvement in living standards will increase the required amount and a higher need for medical attention in old age will definitely inflate this further.

This is what makes accounting for inflation so important when you are calculating your goal amount. Ignore inflation and you will fall short of your target. For a realistic estimation of your target amount, you need to consider inflation at a reasonable rate.

Historically, India has been a country with a relatively high rate of inflation, going into double digits. It’s only recently that inflation rates have come down below 4%. So next time you are estimating your monetary needs in the future, don’t forget to take inflation into account.

How to beat inflation

People in India traditionally prefer FD and PPF for long term investments. However, FDs have time and again failed to beat inflation on a post-tax basis. Here is a comparison of post-tax returns from 3-year fixed deposits and inflation over the period at various points:


	Start Month

	Post-tax FD return	Inflation over next 3 years
	April 2011

	6.5%	9.4%
	April 2012	6.3%	8%
	April 2013	6.4%	6.5%




Hence it is important to invest in instruments that provide returns higher than the prevailing inflation. Equities, as an asset class, have consistently beaten inflation in India. Even debt funds have mostly managed provide inflation-beating returns on a post-tax basis.

During the period from January 2011 to October 2017, a monthly SIP in a large cap equity fund would have given you a return of 15.35% per year. At the same time, an SIP in income funds would have given you a return of 8.55% per year.

Here are different scenarios showing how much an SIP in a weighted portfolio of debt and equity funds would have returned over January 2011 to October 2017:


	Equity:Debt	Portfolio return	Average Inflation	Real Returns
	50:50	12.0%	6.4%	5.6%
	60:40	12.6%	6.4%	6.2%
	70:30	13.3%	6.4%	6.9%




The returns shown above are the category average returns considering large-cap equity funds for equity and income funds for debt, as classified by Fundsindia. Real returns represent the return on your portfolio less inflation. You can achieve higher returns with good fund selection.

A correct estimation of your target amount, good mix of asset classes, along  with good choice of funds will help you reach your goals without hitches.

 

Other articles you may like
	FundsIndia explains: Investing in gold
	FundsIndia Explains – Thematic and Sector funds
	Don’t actively manage your fund portfolio based on market themes
	Simply Important: Your new capital gains report at FundsIndia





			
			Post Views: 
			1,587
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    financial planning. goal-based investing
                

			

            	


			
			3 thoughts on “FundsIndia Explains: Role of Inflation in Financial Planning”		


		
			
			
				
					
												Vandhi says:					


					
						
							
								November 29, 2017 at 10:22 am							
						
											


									

				
					What is the inflation to be considered for following goals

Retirement (20 Years from now )

Child education(14 years)

Marriage(25 years)

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								January 7, 2018 at 4:43 pm							
						
											


									

				
					CPI is the inflation to consider. Long Term CPI has been 6-7%. Having said that, it is also important to know the real inflation in education sector or with marriage expenses. Often times, it is in double digits. Plan accordingly. thanks, Vidya

				


				Reply
			




	
			
				
					
												Shweta says:					


					
						
							
								June 4, 2018 at 11:07 am							
						
											


									

				
					Very informative Vidya, thanks 🙂

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


