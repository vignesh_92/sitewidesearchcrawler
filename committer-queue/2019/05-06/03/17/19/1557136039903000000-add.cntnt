

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Explains: Rebalancing a Mutual Fund Portfolio

            		February 12, 2018 . Mutual Fund Research Desk
                    

	
                    
                                    Simply Important
                    
		
            	

            	When you start to invest, you build a diversified, asset-allocated portfolio. This allocation is based on your goal timeline, your risk appetite, and other factors. That is, based on what you’re aiming for, this is the equity-debt proportion that works best for you.

As markets move, your asset allocation proportion changes. Your equity allocation could go up if markets rally for a long time, or it could go down when markets correct. So you need to rebalance it to bring it back in line with the original. But what is rebalancing and what does it entail? Read on to find out.

What is Portfolio rebalancing?

Portfolio rebalancing is the process of bringing a portfolio back to its original asset allocation. In an asset allocated portfolio, the target proportion of debt and equity is determined based on your time frame and risk profile. Over time, if one asset class moves more than another, the allocation between them may diverge from the original. The skewed allocation will affect the portfolio’s returns and the risk you are taking.

Take this example for instance. Over the past couple of years, equity has seen an unprecedented rally, whereas long term debt has underperformed over the last year. In such a scenario, the equity component of a portfolio will grow more than debt. This will lead to your portfolio having more equity by value than originally intended. In such a case, you would need to reduce your equity exposure to restore the original allocation.

 Open  a FREE Account Now! 

Why do you need to rebalance?

There are two main reasons why you should rebalance your portfolio. First, is to keep the risk you are taking within sustainable limits. Based on your inputs, say you had a portfolio made up of 60% equity and 40% debt. This proportion is ideal within the limits set by your risk appetite and your timeframe. However, if the equity component goes up to 65%, then you are taking more risk than you originally intended and what was prudent given your timeframe. Hence restoring the original allocation will keep the risk within your tolerance limit.

Second is to take advantage of a rising asset class and investing in a cheaper valued one. This also helps avoid having to time the market. If the equity component of your portfolio has grown more sharply compared to debt, it is usually because stock markets have gained handsomely. When you rebalance, you would actually be booking profits in the asset class that has rallied (equity) and getting into the relatively undervalued one (debt). Rebalancing when equity-debt gets out of sync is better than trying to redeem based on market conditions. In trying to time markets,you may end up redeeming before you have sufficient gains, whether in debt or in equity. Instead, if you focus on your own portfolio and rebalance when called for, it will ensure you book your profits in a timely manner.

When should you rebalance?

The easiest and most practical way to rebalance is by reviewing your asset-allocation on a yearly basis. As soon as the asset allocation diverges by a certain percentage, you can rebalance it at that point.

The normally recommended divergence is 5%. So if your target equity allocation was 60% and the current allocation is 55% or 65%, you should rebalance your portfolio. This is only a recommended threshold and you are free to determine your own limits. 5% is a level which does not lead to frequent rebalancing and yet ensures that in times of sustained rally or underperformance, you take corrective action on time.

How should you rebalance?

This is perhaps the most important question of all. There are three ways to rebalance your portfolio:

	Redemption: In this method, you redeem from the asset class which has grown more, thereby bringing down its value. Suppose you invested Rs. 60,000 in equity and Rs. 40,000 in debt. Over the years, equity grew to Rs. 1,50,000 while debt grew to Rs. 80,000. The allocation has been skewed to 65:35. To restore the original allocation, you can redeem Rs. 30,000 from the equity component. 

However, this method is not recommended, as it reduces the value of the entire portfolio, making it difficult to reach your goal. It also brings up the question of deploying the redeemed amount – when would you invest it back into equity? It would amount to timing entry and exit, exactly what you want to avoid. Besides, if you keep that money in your bank account, it would be earning sub-optimal returns.
	Fresh investment: In this method, you bring in fresh money to the asset class which has lagged behind. This increases the value of this asset class and restores its original allocation. Taking the above example, you can invest Rs. 20,000 in debt to restore the original allocation.

This method may pose practical difficulties at the time of execution depending on the amount which needs to be brought in. As the years go by, your portfolio value would also be rising and therefore, amounts needed to be brought in may consequently be high. However, if you have investible surplus, this method is recommended as it is the easiest.
	Switch: In this method, you switch from the asset class where the allocation has gone up disproportionately into the asset class where the allocation is lagging behind. Once again, carrying forward the above example, to restore the original allocation, you can switch Rs. 12,000 from your equity fund to your debt fund. You will end up with Rs. 1,38,000 in equity and Rs. 92,000 in debt, which is in the proportion of 60:40. This may be slightly trickier to execute and may involve tax incidences, but it would help where bringing in fresh money as explained in the previous point is nonviable.


Where should I redeem from and invest into?

Each of the approaches listed above involves either redemption or investment or both. In an asset allocated portfolio, you will have several funds. How do you decide where to redeem from and where to invest the money? There are no hard and fast rules for this. However, you can take the following points into account while making your decision.

 Open  a FREE Account Now! 

If you need to redeem: 

	Choose a fund which has been consistently underperforming. 
	Prioritise funds where you do not have running SIPs and redeem from those. For instance, if you had invested in a fund initially, where you stopped the SIP later to invest in a better fund, you should redeem from that fund first. It can also be easier to account for exit loads and holding periods for funds where SIPs have stopped.
	Use category allocation within an asset class as a guideline. For instance, if your midcap funds have run up a lot more than large cap funds and your mid-cap allocation is much higher than what you started out with, prioritise redemptions from midcap funds. This will also help you restore the original category-wise allocation within that asset class.


If you need to invest:

	This is easier – simply choose the funds which are consistently doing well. You do not need to add new funds every time you rebalance. 
	If you have funds where you have running SIPs, consider further investments in these rather than the ones where you have stopped SIPs due to underperformance. 
	Again, go by category allocation. If there is a category where the allocation is lower than your original portfolio – say income funds in long-term debt, add fresh money here to bring it back in line.


When you’re bringing in money to any fund, ensure that  the underperformance was because of market factors rather than the fund’s own underperformance.

Of course, rebalancing is possible only if you have a properly built asset-allocated and category-allocated portfolio to begin with. For any goal, a balanced portfolio is always the best way to match risk, time-frame, and returns. It ensures that you don’t go overboard on any asset class or stay under-invested in one. If you haven’t got an asset-allocated portfolio yet, review your portfolio to bring it in.

Other articles you may like
	FundsIndia explains: What is compounding?
	FundsIndia explains: What are debt funds?
	FundsIndia explains: Closed-ended Fixed Income Mutual Funds
	Why SIPs work better: The science behind cost averaging





			
			Post Views: 
			2,441
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Mutual funds. Personal Finance. portfolio. rebalancing
                

			

            	


	
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


