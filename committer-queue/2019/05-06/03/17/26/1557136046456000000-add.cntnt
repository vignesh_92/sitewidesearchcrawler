

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		Your investment options

            		February 18, 2019 . Mutual Fund Research Desk
                    

	
                    
                                    Simply Important
                    
		
            	

            	When you first start out to plan your finances, it is likely that you are overwhelmed with the options there are. There are a number of instruments that will help you grow your money, but how do you choose which ones are appropriate for your needs? We’ll give you a brief on the popular investment avenues and tips on how you can make your decision.

To understand this, the following will help you put investments in perspective. Investments are all split into “asset classes” – primarily these are equity, debt, gold, and real estate.

Equity refers to the stock markets. Debt refers to an investment that is linked to fixed returns for a particular maturity period.

Now, on to the main investment product options available and what they are:

	PPF


Public Provident Fund (PPF) comes with a fixed interest rate for each year, which is pegged to prevailing government bond yields. With an upper limit of Rs.1,50,000 per year, it is meant to inculcate small saving habits. The investment amount is locked-in for 15 years and is eligible for tax saving deduction under 80C. Current rate of interest on PPF is 8% per annum. EPF is similar to PPF.

The positives: With a long lock-in period, PPF ensures that you put aside some money for the long term. Apart from being eligible for tax deductions, the interest on PPF is also tax-free.

The negatives: Being a debt product, its returns are limited and even investing the maximum amount each year may not be enough for your retirement corpus. 

	Fixed Deposits


FDs issued by banks are more common, but corporates issue FDs as well and often with higher interest rates. FDs have fixed maturity and can range from 7 days to even 10 years. A few bank FDs have become more flexible with premature exit options but largely they are locked in for the maturity period. Prevailing interest rates in popular banks is around 6.5%-7% per annum while the higher risk counterpart corporate deposits offer around 8.5%-9%.

The positives: FDs are low-risk, offering a guaranteed return. Setting up recurring deposits and using your bank’s sweep facilities are a great way to build up investments. FDs ensure that your money is not idling in savings account earning minimal interest.

The negatives: For longer time periods, FDs aren’t great. Historically, they haven’t given inflation beating returns. Interest income is also taxed at your tax slab. Depending on FDs alone for all your needs means you need to save up a high sum or you may miss your target.

	Insurance


Insurance in its true sense is a risk cover. But some life insurance policies like endowment and money-back policies place themselves as investments. This is because they offer some benefits on maturity even on survival. ULIP is a hybrid product where a part of the amount gets invested and a part goes for the insurance cover. ULIPs have different schemes which invests in equity and debt market.

The positives: Insurance provides a protection. Premium payments qualify for tax deductions.

The negatives: They are not investments. Variants which give lumpsum amounts at the time of maturity may look attractive, but the returns are poor given the time you stayed invested. ULIPs give you a life cover and returns from your investment. Unfortunately, the life cover you get for the premium paid is minimal, when compared to plain term insurance. In a ULIP, you get neither adequate insurance nor investment. Liquidity, transparency, and comparisons of ULIPs are also poor. 

	National Pension Scheme (NPS)


NPS is a pension scheme which invests in a combination of equity, corporate bonds and government securities. You can decide the allocation between each yourself, or you can allow the system to choose it for you. The scheme matures when you turn 60 or 10 years from the time of account opening, whichever is earlier. At that time, up to 60% of the amount can be withdrawn and the rest is compulsorily invested in annuity. Even though premature withdrawal is possible for a certain amount, it comes with a lot of rigid clauses.

The positives: NPS gives an additional tax deduction of Rs.50,000 under 80CCD. Similar to PPF, the long lock-in period ensures that there is a retirement fund is in place. The addition of equity helps improve returns compared to PPF or EPF.

The negatives: For disciplined investors who don’t need such constraints, NPS is a bit too rigid. Compulsory conversion of a large portion of the end corpus into an annuity is not tax efficient nor attractive in terms of returns.  

	Gold


Gold has traditionally been used as a savings avenue. But physical gold is not an investment because of challenges arising in purity, storage costs, wastage costs, and liquidity. Financial gold is more effective in capturing movement in gold prices without having to deal with physical gold. These include Gold ETF and gold mutual funds. Gold sovereign bonds issued by the government are also another way of investing in gold, and comes additionally with a small interest component and taxation benefits.

The positives: Its low correlation with stock prices and its counter to inflation makes it a good hedge against market risk.

The negatives: Gold as an investment does little to your overall portfolio. Their prices move solely based on global equity market sentiments and the exchange rate. Gold prices can and have stayed stagnant for long periods of times. 

	Stocks


By investing in shares (equity) of a company, you become a shareholder. The returns you reap will come from the growth of the company which will reflect in the price of the stock. There is no cap to these returns and stock prices can rise multi-fold. A small amount, therefore, can grow in very large sums. You need a trading and demat account to transact in stocks.

The positives: Equity has historically been the highest returning asset class. Higher returns mean you can invest lower amounts – so a large corpus target can be achievable even if your savings capacity is lower.   

The negatives: Picking stocks comes with high risk. It requires market expertise. Stocks can be very volatile and therefore require both risk appetite to stomach rapid change in values as well as conviction to hold on to the stock. While volatility evens out over the long term, it still needs time. It can be safely said that equity investing is not for all and you should refrain from investing directly in stocks if you don’t understand the equity market well.

	Mutual funds


A mutual fund is a pooled investment vehicle which invests in equity, debt, or gold and is managed by investment professionals. Choice of investments and their management is made by fund managers. Mutual funds charge a fee for such management. Eventual returns will be decided by which asset class you invested in. Broadly, returns from equity mutual funds will be market linked whereas debt oriented mutual funds will be a notch or two higher than that of fixed deposits.

The positives: Mutual funds give you professional management of both equity and debt markets without you having to make choices yourself. The variety of mutual funds means that you can find a fund that matches all your requirements. Open-ended funds can be exited anytime, giving you flexibility to structure your investments around your needs. Debt mutual funds have lower taxes than bank or corporate FDs.     

The negatives: The same flexibility can turn harmful if not used prudently. Easy-exit and no lock-in feature of mutual funds can hamper long term wealth building if not used for the right purpose. You will also need to track your mutual funds from time to time to ensure that they continue to be good performers.  

What you should consider

Now that you understood features of different products, you need to see which ones suit you. For that, you need to consider two important things – your needs and your risk appetite.

First, let’s understand what risk here means. It is the uncertainty in returns and probability of losses. For example, returns from any equity product is volatile and cannot be predicted beforehand. Whereas debt-oriented products, like PPF and fixed deposits, are more predictable and low risk. The more you’re able to withstand volatility in returns and losses, the more risk you can take and vice versa. But why should you take higher risk? The trade-off here is higher returns. So, while equity (stocks and equity mutual funds) is risky, returns over time compensate for this. While debt (FDs, PPF, debt mutual funds) is low-risk, so are its returns.

Second, your needs. You can think of your needs as the purpose for which you need money. Split this into what you would need in the short term versus money that you can put away for your future. To keep it simple, call anything longer than 7 years as long term. If you don’t have a well-defined goal and you just want to save up your surplus, consider it to be long-term as you have no definite use for it.

How to make the choice

Two simple rules. One – longer the timeframe, higher the risk you can take and vice versa. Two – the more risk you can take, the more equity investments you can make and vice versa.

Here are a few pointers. For short term needs and contingency (emergency) funds, you’d need a stable source of returns, easy conversion into money, and low risk. You can consider bank FDs or debt mutual funds for that purpose. PPF is unsuitable because of its long lock-in. Don’t use equity-oriented products for short-term goals, no matter how attractive the 1-year or 3-year returns may look.

For long term wealth building or a timeframe of 5 years and longer, include equity as it delivers superior return and you can build more wealth. Be it through NPS or mutual funds or direct stocks, up your equity exposure longer the time frame and the more comfortable with the market. You can include an exposure of 60%-80% in equity for a period more than 5 years, depending on your risk level. Put the remaining in debt-oriented products like FDs or debt funds. Don’t put all your investments into equity – that’s just exposing your entire portfolio to high risk with no means to limit volatility.

Insurance products for the purpose of investment can be written off the list. Simply hold adequate term insurance (thumb rule is your sum assured should be 10 times your annual income) and medical insurance.

If you get confused by the array of products out there and whether you should invest in those, just do this – look at how it gets its returns, the risk level it involves, how long you can give that investment the time to perform, and how easily to can liquidate it either to meet your needs or if it is not performing well. Then blend these products to create a portfolio that suits different aspects of your needs.

Other articles you may like
	FundsIndia explains: What is NAV?
	FundsIndia explains: What to do if  your AMC shuts shop
	FundsIndia explains: When is it a good time to invest?
	Rising rate scenario favors FMPs





			
			Post Views: 
			1,251
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    FD. Gold. insurance. investment. Mutual fund. NPS. stocks. ulip
                

			

            	


			
			1 thought on “Your investment options”		


		
			
			
				
					
												RAVEE S says:					


					
						
							
								February 19, 2019 at 9:30 am							
						
											


									

				
					A Very simple and informative article. Thanks for the writings and expects like this, more. Advance Wishes.

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


