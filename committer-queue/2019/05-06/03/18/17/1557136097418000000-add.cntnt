

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		To be or not to be in the market?

            		June 14, 2013 . Vidya Bala
                    

	
                    
                                    Advisory . Equity Research . Mutual funds
                    
		
            	

            	To be in equities now would amount to being on the right side – perhaps a bit early. But then, it is the early bird that catches the worm!

The S&P BSE Sensex fell 3% in the last one month and as if that weren’t enough, bond yields have been moving up since May 22, causing a fall in the bond prices and a consequent dip in the NAVs of many of your medium to long-term debt funds.

Quite a few of you have been calling in to ask us the reason for the fall and whether you should move out of equity /move out of debt, at least temporarily, given the fall in both asset classes.


Yes, you are right in being confused. So are we. Let’s take solace from Warren Buffet’s words ” “if you’re not confused, you don’t understand things very well”.

But we still hope we will answer some of your questions in this article or at least put them in perspective for you. 

1. Why did the markets fall?

Okay, the obvious answer, as mentioned enough in the media is the rupee’s fall against the dollar and how it spooked the FIIs in to exiting the market.

Partly true, but let’s admit, this is not the first fall in 2013. We have already had 2 rallies in the space of less than 6 months and both have petered out. Post September 2012, when a stream of measures were announced, there were enough indications of things getting better. Improvements by way of mellowing inflation and fiscal deficit, lower commodity prices and optimism over recovery in GDP as well as earnings growth, later in the year, sent the market rallying.

But then, actual GDP data for the just ended fiscal year sent the market’s optimism for a toss. The current account deficit and earnings numbers too did enough to pull the markets down every time they rallied.

Ok, we’re listing this simply to remind that the recent rupee fall perhaps accentuated fears but there were enough factors in play in the last few months to keep the markets at bay.

Now moving to the rupee’s weakness against the dollar, it is noteworthy that the Indian markets are not alone in their fall. A number of emerging market currencies and their markets in general have had a bad May themselves, what with a possible ‘Quantitative easing’ tapering in large markets like the US and Japan sending fear signals.

When the monetary shots are withdrawn by such large markets, there is a fear of liquidity withdrawal in emerging markets with institutions showing interest in markets such as US and Japan. Simply put, there could be a flight to safety.

But before you conclude, just keep in mind a few key factors about the Indian markets:

One, there has not been any terrible news/data internally to cause panic.

Two, the government is actually looking at more reforms especially in areas such as FDI to boost foreign inflows.

Three, a gaining US dollar means flat-to-low commodity prices. That means some relief to the raw material costs and therefore operating earnings of companies. That also means oil, which is a key dent on our current account, may actually not play spoil sport. Yes, while, weak rupee would makes imports costlier, to some extent, exports too would gain from a higher dollar.

Four, in normal conditions, a depreciating rupee could spell a hike in interest rates. But given the poor economic growth internally, this will be spared. Yes, rate cuts may not happen as fast as we all anticipated; yet, the 50-75 basis point cut will remain impending over the course of the year.

Five, a fall in markets due to external factors, would mean fall in valuations, what with limited impact on the already tepid earnings growth. That means Indian markets are becoming more attractive.

Now, if you ask me if I expect a recovery now, honestly I have no answer. But if you are asking with a long-term view then my answer would be: to be in equities now would amount to being on the right side – perhaps a bit early. But then, it is the early bird that catches the worm!

2. If the US markets are looking up and FIIs are moving there, should I invest there?

Investment in international markets could be a good diversification strategy (at about 10-15% of one’s portfolio) but if you wish to move your existing domestic allocation to international markets now, you’re probably not doing the best thing for your portfolio.

By moving now, you are shifting from a cheaper market to a rather expensive market that has rallied quite a bit.

The BSE S&P 500 is at 16.6 times its trailing earnings now; and guess where the S&P 500 (US) is at? It is 18.4 times its trailing earnings and at forward 12-month estimates of 15 times, would remain expensive than Indian markets.

 And remember, earnings growth in emerging markets like India can far outpace developed markets in the long run.

Also, by moving to an international market like US now, you are exposing yourself to a very expensive currency. Any fall in the dollar will pose you enough currency risks.

3. Why are bond funds falling? 

It is the same currency effect that has spooked the market. A falling currency would mean that the RBI may not cut rates in its upcoming monetary policy early next week. That has caused a rally in the yield of long-term gilts and bonds, resulting in a fall in their price. This led to a slip in most of your NAVs since May 22. The currency fall has also triggered FII selling in debt.

But according to a Credit Suisse report, the fears of FII selling in Indian debt may be rather exaggerated. While the selling led to about 17 basis point increase in Indian 10-year government securities, the rise in yields in similar instruments elsewhere in emerging markets (Indonesia, Turkey, Brazil, Russia, South Africa) was to the tune of 60-115 basis points suggesting that the damage in India was minimal.

Also, India has a relatively closed debt market as there are caps on FII holding limits (less than 5% of the bonds in India are foreign-owned according to Credit Suisse), the impact of FII selling on the yields is not high.

The fall in your fund NAVs between May 22 till date would be anywhere between 0.01% to 1.25%, for most income funds and long-term gilt funds.

4. Should I move out of my bond funds?

The same principle that applies for equities applies here as well. By moving out of your debt funds now, you will be moving out at a cheaper price and probably come in again when yields have fallen (and when NAVs have rallied). Its not easy to time your entry into debt based on macro factors. If you have a 2-3 year view, its best to stay invested.

But if your goal is a couple of months away, then you should probably be moving to liquid funds as any disappointment in the RBI policy stance would only irritate the markets more.

5. Should I invest now?

If you are starting to invest now for a long-term goal, its probably a good time, both in equities and debt. But this, only if you are game to invest more systematically through SIPs. With election year coming up, you can expect more volatility in the coming months and such skittish markets are best managed through the SIP tool.

In debt too, if you are investing in income funds with a 2-3 year view, use an SIP for the course of 6 months to 1 year at least. 

If you are already running SIPs, make sure you are not tempted to stop them. If you are serious about your long-term investment, the only way you are going to lower your cost is by buying in a down market; which is what we are going through. Stopping SIPs and starting later would simply mean a lost opportunity to average.

Other articles you may like
	Mutual fund meet: what we would have said
	ITC LTD.
	Rate cut to provide benefit to your loans and your investments
	L&T Finance Holdings Ltd.





			
			Post Views: 
			3,816
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    equity market. Mutual funds
                

			

            	


			
			17 thoughts on “To be or not to be in the market?”		


		
			
			
				
					
												e m sivasankaran says:					


					
						
							
								June 15, 2013 at 9:54 am							
						
											


									

				
					the article is too good to be ignored.thanks.

				


				Reply
			

	
			
				
					
												Aditya Karnik says:					


					
						
							
								June 15, 2013 at 11:02 am							
						
											


									

				
					Incisive article. Your views and analysis on debt funds is immensely useful. 

Thanks and Regards,

Aditya.

				


				Reply
			

	
			
				
					
												U.Kumar says:					


					
						
							
								June 17, 2013 at 10:49 am							
						
											


									

				
					Vidya Balan, as always, is spot on. Very good read, very educative. Sensible advice for wise investors.

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								June 17, 2013 at 10:54 am							
						
											


									

				
					Hello Sir,

Thanks!

Vidya

				


				Reply
			




	
			
				
					
												Sunil says:					


					
						
							
								June 17, 2013 at 11:32 am							
						
											


									

				
					Dear Vidya,

How would you term the impact on Dynamic Bond Funds?.

For my goal (2-3 yrs) i have invested lumpsum.

I hope Dynamic Bond Funds manage (as far as i understand) will adjust these factors into the Fund.

Please correct if my understanding is correct.

FYI : I have invested in Birla Sun Life Dynamic Bond Fund.

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								June 17, 2013 at 11:36 am							
						
											


									

				
					Hello sir,

The rates cuts remain due, although not happening now. To this extent, gains from dynamic bond funds remain impending. Funds like Birla Sun Life Dynamic do not take long maturity calls and are therefore well placed to face the volatility over the next several weeks compared with some riskier peers. Over a 2-3 year time frame, the outlook remains unchanged. Tks, Vidya

				


				Reply
			




	
			
				
					
												Saurav says:					


					
						
							
								June 17, 2013 at 11:33 am							
						
											


									

				
					Very well written Vidya!! I have started following your articles recently and I must admit they are really informative. More importantly they are not running commentaries like so many ‘so called’ experts!!  You take a very holistic approach and that’s the best part of your write-ups.

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								June 17, 2013 at 11:36 am							
						
											


									

				
					Hello sir,

Thanks. Hope you continue to find them useful. Rgds, Vidya

				


				Reply
			




	
			
				
					
												Ajith says:					


					
						
							
								June 17, 2013 at 1:30 pm							
						
											


									

				
					I have an SIP in Gilt fund. Should I co ntinue it or shift to an dynamic bond fund. I don’t mind running it for another 2 years. fund

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								June 17, 2013 at 1:35 pm							
						
											


									

				
					Hello Ajith,

If you have held the gilt fund for at least 1 year and made some profits then consider shifting to an income fund. But even in the latter, expect some near term volatility and dip (as RBI has not cut rates today). If you are willing to hold with a 2-3 year time frame, such volatility should not worry you. Thanks, Vidya

				


				Reply
			




	
			
				
					
												sunil agrawal says:					


					
						
							
								June 17, 2013 at 3:39 pm							
						
											


									

				
					Hi Vidya,

I am looking to invest in debt instruments through sip route with an investment horizon of 3-4 years. After some reading I think I should go for dynamic bond funds. Can you suggest 2-3 funds and whether I need to look beyond this category of dwbt instruments? What annualized returns can I expect from dynamic funds?

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								June 17, 2013 at 7:55 pm							
						
											


									

				
					Hello Sunil,

Income funds will fit your choice. Income funds is a broad category and will include dynamic bond funds. You may choose 1-2 funds in the ‘Debt funds long-term’ category in this link: http://www.fundsindia.com/select-funds.  You may expect 9-9.5% annually based on performance thus far.

 If you are game for some equity too with a majority in debt, then you can consider at least 1 fund from ‘hybrid fund low risk’ category in the same link. These debt-oriented funds will suit a 3+ year time frame and can be expected to deliver slightly higher returns than pure debt.

If you wish to have fund-specific advice, kindly use the ‘Ask Advisor’ Feature in your FundsIndia account (click help tab) and our advisor will help choose the funds. The feature will help us track your queries and our responses instead of it becoming adhoc in the blog. Thanks, Vidya

				


				Reply
			
	
			
				
					
												sunil agrawal says:					


					
						
							
								June 26, 2013 at 2:33 pm							
						
											


									

				
					Thanks a lot Vidya

Regards

Sunil

				


				Reply
			







	
			
				
					
												Satish says:					


					
						
							
								June 24, 2013 at 3:08 pm							
						
											


									

				
					Hello Vidya,

The article is really a worth for any investor.  I have one question to you I have been investing in some diversified mutual funds since 2007 through growth options via SIP and had gained some profit on it.  Can I redeem only the profits out of those mutual funds and invest same in good Bank fixed deposits so that I can save the profits which i had gained investing in mutual funds and stay invested in the funds with the rest of cost amounts? Or does redeeming the profits at this point makes any huge difference in the long term.  Pls advise

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								June 24, 2013 at 3:14 pm							
						
											


									

				
					Hello Satish,

Thanks!

Redemptions can be made in a phased manner as you mentioned if your goal (for which the portfolio was built) is 1-2 years away, to ensure you move to safer asset classes closer to goal. If your goal is farther away, then by removing profits now, you will not allow the principle of compounding to work well. Thanks, Vidya

				


				Reply
			




	
			
				
					
												Satish says:					


					
						
							
								June 26, 2013 at 11:59 am							
						
											


									

				
					Hello Vidya,

I really appreciate for my prior query answered.  I have some more questions to you. As I told you I had been investing in some equity oriented mf  since 2007 in growth option via SIP (Large cap: FT india bluechip fund, DSPBR Top 100) (Large and mid cap: L&T Equity, Quantum LTE) (Midcap; IDFC pre eq plan A and SBI Emerging business)  for my retirement savings which is for long term(25 yrs from now). Do you find my portfolio has good funds to create good corpus and reach my retirement goal in the longterm.

And also am planning to invest through SIP in some good balanced funds for my childrens eduction time horizon (10-15 yrs from now) the reason I choose this is because this provides the stable return in volatile market like we have now which should not hamper the goals for childrens education.   Please suggest me some good balanced funds I can look for the said period.  Please do comment on my entire portfolio.

Thanks and looking for your advise

Satish

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								June 26, 2013 at 12:05 pm							
						
											


									

				
					Hello Satish,

Thanks for writing in. Our portfolio review and advisory services are available for funds held with us. Investors can choose the ‘Ask Advisor’ feature in their FundsIndia account (by clicking help tab) and avail our services free of cost. Thanks, Vidya

				


				Reply
			







		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


