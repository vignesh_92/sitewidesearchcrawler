

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Views: Why you can go wrong with point-to-point returns

            		December 13, 2017 . Bhavana Acharya
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Research Desk . Views
                    
		
            	

            	

Synopsis

	Point-to-point returns do not show whether the fund is able to consistently do well
	Point-to-point returns do not show volatility in returns or performance in different market cycles
	It is not possible to understand fund strategy and thus suitability from point-to-point returns
	Return expectations based on 1-3-5 year returns would be erroneous 




Today, the 1-year return of BNP Paribas Midcap looks to be among the best at 40%. L&T India Value sports a 1-year return of 31.7%. On a 3-year basis too, BNP Paribas Midcap has higher returns than L&T India Value. So is BNP Paribas Midcap a better fund than L&T India Value? The instinctive answer would be yes, for most investors.

Consider the 1-year returns for the two funds on 12th December 2016. BNP Paribas Midcap’s return was 6.4% while L&T India Value sported a 17% 1-year return. On a 3-year return basis, BNP Paribas Midcap undershot L&T India Value’s return by 5 percentage points. Or take the 5-year returns today. BNP Paribas Midcap is marginally lower than L&T India Value. Would you revisit the answer to the question above?

The answer is that you do not use the 1, 3, and 5-year returns alone to base decisions. These returns neither offer sufficient information for you to choose funds nor are they the right benchmark to set your future return expectations. These returns, when displayed anywhere, are point-to-point – that is, they are returns from a single day to a single day. Looking at just the 1, 3, and 5-year return gives a limited idea of a fund’s performance. There are four aspects to these point-to-point returns.

One, returns change on a daily basis. If markets – whether debt or equity – change every day, so too would the NAV. Therefore, fund returns would also keep changing. 1-year return for HDFC Midcap Opportunities, for example, leaped from 32.4% on 20th November 2017 to 37.2% the next day. ICICI Prudential Balanced went from a 1-year return of 25.4% on 24th November 2017 to 20.8% just five sessions later.

In a rising market phase, the trend of returns would be upward and vice versa. Most mid-cap/ small-cap funds for example were clocking losses or low single-digit returns on a 3-year basis throughout 2013. In the rally the following year, average 3-year returns were upwards of 15% by mid-2014 and rose beyond 25% towards the end. Similarly, average 5-year returns for large-cap and diversified funds crashed from 50% plus at the start of 2008 to less than 15% by the end of 2008 after stock markets sank that year.

Besides, it is not just market movements that affect fund NAVs. Fund portfolios also play a role. In the short term, stocks move higher or lower depending on news flows. The impact these movements have on NAVs depends on the extent to which they hold that stock. Funds holding a heavier weight in Infosys such as HDFC Top 200, for example, felt the 13% plunge in the share’s price on the day its CEO resigned in August more than those that held a smaller weight. In dynamic bond funds, those that are more aggressive on duration, such as Aditya Birla Sun Life Dynamic Bond are sporting much lower returns today than those that are either less aggressive, such as UTI Dynamic Bond, or have aligned their portfolios towards an accrual strategy, such as Kotak Flexi Debt.

Two, returns can be influenced by what takes place either on the start date or the end date. If markets were down on the start date, the returns may look higher and vice versa. Dynamic bond funds are an example. With gilt yields falling, especially towards the last few months of 2016, the bond price rally sent the 1-year returns of these funds as high as 15-16%. Gilt funds had even higher returns. But concerns over inflation, exchange rates, and sell-offs, bond yields have been creeping up. Therefore, the 1-year return today for these funds is just about 3%. A sharp fall in bond prices today when taken against a high bond price last year was partly responsible for the stark differential in returns.

This base effect holds true for funds that have begun to slide in performance (which pulls returns lower) or begun to improve performance (which pushes returns much higher). For example, among the best-performing mid-cap funds this year with a 51% 1-year return is IDFC Sterling Equity. The fund had been languishing in the lower quartiles for years before a fund manager and strategy change in April 2016 turned things around.

Three, point-to-point returns ignore events that transpired between the two dates. In other words, the returns don’t show what happened over a period of time. They only show what happened on a particular day. For longer timeframes such as 5 years or even 3 years, when especially equity markets go through phases, the return does not show the returns volatility in the years between the two dates considered. The current 5-year return for equity funds does not show that markets spent 2013 first correcting and then moving sideways, does not show that large-cap indices and funds corrected in 2015, does not show that there were some months in 2016 and 2017 where almost all funds clocked losses on a 1-year basis.

Four, the returns of different periods are not comparable. It is not possible to compare the returns of different periods with each other and draw any sort of conclusion. It would be wrong to look at equity fund returns today and conclude that a 1 year holding period would yield more than a 3 year or 5 year period. Because returns change each day and they are influenced by what was happening both on the two point-to-point dates, the 1 year return may sometimes be higher than the 3 year or the 5 year return may be higher than the 1 year and so on.

For large-cap equity funds, at this time in 2016, the 1-year return was lower by around 6 percentage points lower than the 3-year return which was in turn equal to the 5-year return. The picture is the opposite today. For dynamic bond funds, the 1-year return is around 1-3% today while the 3 and 5 year returns are around 8-9%. Last year, the 1-year return was the highest at about 13-14% which was much better than the 10-12% of the 3 year and 5 year periods.

What we are trying to say? That merely looking at the 1 year, 3 year, and 5 year returns will give you the wrong picture of a fund.

	It does not show whether the fund is able to consistently do well. For a fund to be good, the trend of indentifying securities that deliver a higher return than the benchmark (and peers) should be sustained. This understanding will not come through by looking at 1, 3, and 5 year returns in isolation – it does not show whether the fund has done well on any day in any year instead of just that single day. What are the chances that you invested in the fund on that one day it did well? What would have happened if you had invested on other days? As investing in a fund can be done on any day, you would need that fund to deliver market and category beating returns no matter when you invested. That comes about only if a fund is consistent which cannot be judged by just the 1-3-5 year returns.IDFC Sterling Equity, for example, looks strong today but it has begun to do well only this year. In earlier years, the fund lagged both the Nifty Midcap 100 index and the peer average; even as recent as January 2017, the fund trailed the index on a 1-year return. Similarly, the 1-3-5 year returns may look like a fund is not doing well now, but the fund could in fact be a good and consistent performer in earlier years. Though this could mean something is wrong with the fund, oftentimes it is the strategy that can cause shorter term slips. In such a case, it would be premature to exit the fund based on relatively lower 1 or 3 year returns. ICICI Prudential Value Discovery is a good example here. The fund’s move into contrarian sectors and stocks which are out of favour now pulled down returns. But the fund’s strategy is to discover under-valued stocks in order to gain from a rerating later and thus it is actually doing its job. Looking at its returns in earlier years will tell you that it has, in fact, usually been able to deliver returns above category and benchmark.
The 1-3-5 year returns also do not tell whether the fund is volatile – that is, are its returns prone to swinging widely higher or lower or are they steadier? A more volatile fund requires a higher risk appetite. Funds with lower volatility are also those that eventually deliver better returns over the longer term.



	It does not show fund strategy. And this strategy may require a different risk level from you as an investor. As seen with ICICI Prudential Value Discovery, the 1-3-5 year returns do not show what kind of strategy the fund follows. A fund following a growth-oriented strategy would today show better returns than one with a value-driven one. A value fund requires a longer holding period and a higher risk level. A fund with a higher mid-cap allocation may be doing better today than one with a lower mid-cap allocation, but such a fund may not be a good fit for all.It holds true for debt funds too. Franklin India Ultra Short-term Bond shows returns of 8-9% across the three timeframes, but this does not show that the higher returns are because its portfolio is made up far riskier instruments than a fund like Aditya Birla SL FRF or ICICI Prudential Flexible Income Plan. An income fund such as HDFC Medium Term Opportunities may seem sedate in terms of returns but the fund’s steady strategy of investing only in top-rated instruments may be the best option for those simply looking for FD-plus returns.


	It is not a return indicator. What often happens is that recent returns are taken to as return expectations. As explained above, given that returns change every day, basing expectations on past returns is hardly right. Low returns may give way to higher returns or vice versa. Using past returns are especially dangerous for sector funds as high 1-year returns usually means that a good part of the sector uptrend is already captured. 


So how do you get around the bias of 1-3-5 year point to point returns? It can only be done by looking at these returns over a period of time – across market phases, in different quarters, in different calendar years. Only then would there be a more balanced view of a fund’s performance.

How we use returns

At research, we don’t look at the point-to-point returns. What we use are rolling returns – i.e., we take returns of different periods ( 1 year, 3 years) and roll them every day for a number of years. Pitching these returns against the benchmark indices or peers will give the answer to a fund’s consistency in delivering higher returns. Because fund strategies play a key role in returns, we may be willing to let near term underperformance slide if the strategy is such that it can play out well and if the fund has earlier been consistent. In the same vein, a fund that has just started outperforming may require some more time to judge sustenance of its performance.

We average out these rolling returns so that we’re able to arrive at a more realistic return figure that considers both market phases and the effects of a fund’s strategy. So a fund with a higher average return would place better. We also use returns to look at volatility, incidences of losses, and the ability of the fund to keep them in check because recovering from steeper losses takes more effort. We see returns in different market cycles (whether in debt or in equity) to judge its ability to perform no matter what is happening in the markets.

Given the ongoing uncertainty and valuation worries in equity markets and on the interest rate front for debt markets, the 1-3-5 year returns in each category can be quite divergent. It’s important not to get unduly influenced by the simple returns and pick a fund that may not suit your appetite or purposes. This is why some of the funds we pick at research may not be the 1-3-5 year return chart busters.

FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis for investment decisions. To know how to read our weekly fund reviews, please click here. 

Other articles you may like
	Capital Gains report - new and improved
	When should you invest in Fixed Maturity Plans?
	FAQs on investing in the current volatile market
	FundsIndia Strategies: Debt Funds to Gain from High Interest Rates





			
			Post Views: 
			1,232
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Advisory. Investments. Mutual fund. Personal Finance. returns
                

			

            	


	
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


