

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Views: Reset your return expectations

            		August 16, 2017 . Vidya Bala
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Research Desk . Views
                    
		
            	

            	Short on time? Listen to a brief overview of this week’s review.



	Structural shifts in the economy may mean longer period of low inflation
	This calls for toning down return expectations over the next 1-2 years 




With the markets delivering close to 20% returns on a year-to-date basis, many of you may set this as a benchmark for future returns. We will have to caution you that such expectations may not be realistic. If the present low inflation and low rate scenario coupled with earnings and valuations tell us something, it is that the high absolute returns you have seen since 2013 may come down and you may have to recalibrate your expectations accordingly.

Please note that this does not mean that the ‘real returns’ from your investment would come down. Real returns are what you get from your investment over and above inflation. So, even if absolute returns trend down, it would still likely be above the inflation rate, delivering healthy real returns to your portfolio.

Here’s why you should reset your return expectations for the next couple of years and not merely go by past returns.

 Open  a FREE Account Now! 

New inflation

It appears that the RBI may have achieved its inflation target of 4%. While the 1.54% low in June 2017 quickly moved up to 2.36% in July, we think the high inflation scenario between 2008-13 may not be repeated any time soon. There are a few reasons why we may not go back to high inflation of yester years:

First, global commodity prices have stabilised and all indications suggest subdued future prices with marginal ups and downs. For India, the primary driver of inflation on the commodity side is oil and oil has seen a significant let up in prices since 2014. Oil is an important inflation driver simply because everything that you consume right from shampoos to paint to fuel are powered by oil and its by-products.

The supply economics of oil has undergone structural shifts since 2014. The US has almost doubled its oil production in the past few years and produces enough to even export. The oil that it imported earlier must therefore find other takers in the global market and that hasn’t happened. Lifting of the sanction on Iran brought it into the international oil market, even as Canada and Iraq increased supplies. While the supply scenario came to a state of glut, the demand did not keep pace. China went into a slowdown while Europe started investing more in clean energy. All these meant that this was not just a temporary supply demand mismatch. In fact, the International Energy Agency has stated that oil supply in 2018 will outstrip demand.



Second, food inflation is showing signs of taming due to structural shifts. Bumper crop on the back of good monsoons and destocking (GST) have kept inflation low, but these are temporary factors. On a more sustained basis, the minimum support price has been kept within a tight range over the past couple of years, thus breaking the usual accusation that MSPs are inflationary. In 2016 for instance, even while MSP for pulses were hiked sharply, it remained modest for paddy. This encouraged production where it mattered, to keep supply healthy and prices under check. For 2017-18 too, the MSPs have been in the range of 5-8% for kharif crops, not too high nor too low.  It appears that fiscal prudence has scored over short-term income enhancing measures. The Government has instead decided to focus on long term income development measures such as unified agriculture products market, expansion of storage capacity, irrigation fund, new crop insurance and rural development.

 Open  a FREE Account Now! 

Third, and most importantly, adoption of an inflation-targeted policy means identifying early signs of shift in inflation trend and being pro-active in keeping inflation stable. According to a recent article in the media, an economist stated that emerging countries that have adopted inflation targeted policies have managed to achieve their target around the 4th year and that happens to be 2017 for India.

What to expect?

Now what do these mean for the economy and for you? It simply means that the economy must (and probably is) readjust itself to a low inflation and low rate scenario. A low inflation is not the best for generating high nominal GDP growth. The second volume of the Indian Economic Survey has recently given a 6.75%-7.5% range for GDP growth this fiscal. With an inflation of around 4%, a 11-12% earnings growth for companies seems realistic. To peg your own return expectations to that level would be more prudent. This is because, given the current capacity levels in most sectors, it is unlikely that a situation of excess demand will occur. That means a need for private investment may not arise any time soon.  And for earnings to grow faster than that, companies need to invest more. As can be seen at present, no amount of rate cuts has revived corporate credit growth, suggesting reluctance to invest.

If you are trying to conclude that returns from equity would be low – not really. Your real returns would still likely be comparable to earlier years. Let us take an example: In FY-12, GDP growth was 6.2% and inflation was 9%. Your return expectation of 15% was not unreasonable for the next 3 years and markets did deliver those returns between FY12-15.

But with inflation at 4% and an expected GDP of 6.75%-7.5%, your return expectation cannot be far higher than 11%-12%. This is what we mean when we say temper your return expectation. Your real return in both the cases would still be 6-7% unless there is a significant slowdown in economic growth. It is the real return that you need to aim to keep intact and equities as an asset class seeks to do that.

If that were on the equity side, on the debt side, a stable inflation means that we may be at the fag-end of rate cuts. The problem with Indian FD investors is that they have never really seen a period of prolonged low rates. There have seen rate cuts like in 2001-03 but quickly followed by upward movement of rates. While shift out of FDs may become inevitable, investors too need to temper return expectations from debt options such as debt funds. Rallies in debt market may soon be done with. The double digit returns from duration funds would become more normal – to high single digit returns, albeit comfortably delivering higher ‘real returns’ than FDs.



What you should do

If you have entered mutual funds in the post-2013 period this is what you need to know:


	In different markets, macro-economic changes influence the returns delivered by various asset classes.
	Setting realistic expectations is necessary not only from an investment perspective but from a goal setting perspective.



 Open  a FREE Account Now! 

	We are at one such structural shift now, driven by low inflation. It is therefore imperative that you keep your return expectations moderate from a 1-2-year perspective.


For those with long-term goals of say 10-15 years, nobody really knows whether a 4% inflation or an 8% inflation would prevail then. It is also a different matter that ‘real inflation’ – the cost of education, rent, medical bills may move much faster. For these, the only way out is to save more, not expect higher returns! As long as you are invested in an asset class that generates sufficient ‘real returns’ for you, you should worry less on returns and focus more on saving and investing. Looking at exit points to book profits or trying to re-enter at lower levels are sub-optimal methods to invest in any scenario.

Mutual fund investments are subject to market risks. Read all scheme related documents carefully. Past performance is not indicative of future returns.

Other articles you may like
	FundsIndia Recommends: Birla Sun Life Frontline Equity
	FundsIndia Recommends: Canara Robeco Large Cap+
	FundsIndia Views: Should you worry about debt fund declines?
	Why your bank deposit rates won’t go up much





			
			Post Views: 
			2,555
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Advisory. CPI. FD. fixed deposits. fund recommendation. inflation. Investments. Mutual funds. Personal Finance. returns
                

			

            	


			
			1 thought on “FundsIndia Views: Reset your return expectations”		


		
			
			
				
					
												sbsivakumar says:					


					
						
							
								August 16, 2017 at 3:15 pm							
						
											


									

				
					Dear vidya madam,

.   It is really a good article and it is a reality also

Thanks

Sbsivakumar

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


