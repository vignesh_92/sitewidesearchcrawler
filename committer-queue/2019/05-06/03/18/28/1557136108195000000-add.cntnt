

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Recommends: HDFC Small Cap

            		October 31, 2018 . Bhavana Acharya
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Personal Finance . Recommendations . Research Desk . Research desk featured
                    
		
            	

            	
What
	Equity fund investing predominantly in small-cap stocks


Why

	Promise in small-cap segment post market correction and new categorization
	Maintains a low portfolio churn and contains downsides well


Who

	High-risk investors with a minimum 5-year perspective 




HDFC Small Cap is a recent addition to our Select Funds list. With a 1-year loss of 0.5%, the fund is among the few small-caps whose 1-year returns aren’t deep into the negative zone.

The Nifty Smallcap 100 index has lost 28% in the past year. From the January 2018 high, the index is down 38%. This is a far cry from the phenomenal 61% gain in 2017. Even considering that HDFC Small Cap has held up in this correction, why are we recommending a small-cap fund at this point?

Small-cap potential

We had so far refrained from stepping up small-cap recommendations even with the rally. The heady rise in small-cap stocks over 2016 and 2017 even when larger stocks were correcting and without matching underlying growth meant that the segment was overheated.

With the segment simply spoiling for a correction, it was more sensible not to go overboard on small-caps. Recoveries by small-caps after a market decline also take longer than large-caps. Liquidity risks prevailed in small-cap stocks and falls would mean that funds find it that much harder to sell off holdings without impacting returns.

We also felt that the limited exposure in this market-cap segment through multi-cap and mid-cap funds would suffice.

Now, however, the small-cap segment has more positives. First, the sharp correction in the stocks has brought prices and valuations to a more realistic zone. The average fall from the 52-week high for the Nifty Smallcap 100 index constituents is about 46%. This provides buying opportunities for funds and for investors in turn. We do not know if this is the bottom, since several broad uncertainties still prevail. But this is a good time for phased investments (or through SIPs) in small-caps.

Second, after the SEBI-imposed categorization, the small-cap space has expanded. Earlier, we used to base small-cap cut-offs on the market capitalisation of the index constituents. In December 2017, for example, stocks with a market cap of Rs 5,000 crore or lower were small-caps. With the new SEBI definition, small-caps are those with a marketcap of Rs 10,000 crore or below. This definition also means that the small-cap space will now include relatively less risky stocks – i.e., what were last year’s mid-caps are today redefined as small-caps.

Third, the changed definition reduces the scope of mid-cap funds, since they have a much smaller universe to deal with. Though mid-cap funds can include about 30% of small-cap stocks, their investible universe will still be largely restricted to 150 stocks.

In a nutshell, the correction in the segment has brought some rationality to prices and the new definition of the segment means that small-cap funds have greater performance potential now than other fund categories and with marginally lower risk than before.

In this space, HDFC Small Cap is a good option. The fund requires a high risk appetite and a long-term horizon of 5 years or more, since small-caps can take time from now to recover and deliver.

Fund performance and strategy

HDFC Small Cap was acquired from Morgan Stanley in 2014. The fund under the earlier management was a multi-cap fund. It was then repositioned into a mid-and-smallcap fund and further as a small-cap fund in end-2016. Therefore, the fund’s relevant performance is just since mid-2014.

In this period, the fund has been a middle-of-the-road performer. Transitioning of the portfolio into a mid-cap fund could have weighed on returns. Against small-cap peers, the fund began picking up performance from early 2017 onwards. One, the fund had time to gain from its repositioning.

Two, it cut back on portfolio drags. Pharma stocks, for example, made up the highest sector weight for much of 2016. The fund then exited most of these, including IPCA Labs, Alkem Labs, Cadila Healthcare, Glenmark Pharma and such which had been weighing on returns. The portfolio’s stock exits were much more from mid to end 2016 than in other periods.

It added NBFC and banking stocks such as City Union Bank and Indian Bank which returned well. Similar pay-offs were in auto ancillary additions, including Balkrishna Industries and Swaraj Engines and niche engineering and capital goods stocks. Sundaram Fasteners, Kalpataru Transmission, LG Balakrishnan were all strong 2017 performers. This helped the fund move slowly up the small-cap chart, going from a third-quartile performer at the start to 2016 to the top quartile by the end of 2017.

The fund continues to be in the top quartile. Apart from the performance turnaround, profit booking in several stocks protected the fund once the correction firmly set in. For example, gains made in stocks such as Balkrishna Industries, City Union Bank, Avanti Feeds, Dilip Buildcon and IFB were locked into within the first few months of this year. Additions in software and some pharma stocks, the lone gainers in this year, also helped.

HDFC Small Cap has held up to 20% of its portfolio in cash since the start of this year, both on account of strong inflows and profit-booking. The high cash position gives the fund the means to pick up beaten down stocks without affecting returns. Both factors are unique to this fund. Peers do not have cash positions to give them firing power; selling off stocks now would further pressure returns.

For this reason, HDFC Small Cap appears to be in a better spot to capture recovery compared to other small-cap funds. HDFC Small Cap has already averaged out several of its stocks in the past few months and has picked up new ones.


The past few years do not lend to observing downside containment. However, considering all 1-month periods since 2014 where the Nifty Smallcap 100 TRI has dropped, HDFC Small Cap contains losses better than the average for the category. Peers such as Reliance Smallcap and DSP Smallcap fell more. HDFC Smallcap is also above average on risk-adjusted returns and is less volatile as well.

Strategy and portfolio

The fund tends towards a buy-and-hold approach and keeps its portfolio churn low compared to peers such as Reliance Small Cap. It looks for stocks with lower valuations and which are niche players or leaders in their markets. The fund is slightly less aggressive than peers, holding a slightly lower share of small-cap stocks than DSP Smallcap, Reliance Smallcap or SBI Smallcap. Its marketcap break-up is similar to Franklin India Smaller Companies.


It holds a large portfolio of around 50-70 stocks with low individual stock weights. Top holdings are not concentrated, which mitigates the risk of sharp losses in stocks. The top ten stocks generally making up a third or less of the portfolio. The fund’s portfolio shares very little overlap with either the Nifty Smallcap 100 or peer funds.

The fund has an AUM of Rs 4,948 crore. Chirag Setalvad is the fund’s manager.

FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis for investment decisions. To know how to read our weekly fund reviews, please click here.

Other articles you may like
	Where safety comes at a price
	FundsIndia Strategies: What the ‘rate cut pause’ means to you
	FundsIndia Views: Election uncertainty removed; what next for the markets?
	FundsIndia explains: Should you invest in mid-cap funds?





			
			Post Views: 
			3,949
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Advisory. equity market. fund recommendation. Mutual fund. Mutual funds. Personal Finance
                

			

            	


	
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


