

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Views: Indian economy – are experts’ concerns overblown?

            		September 27, 2017 . Vidya Bala
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Research Desk . Views
                    
		
            	

            	
Synopsis: Markets (and economists) are reacting to a slowdown in GDP growth for the June 2017 quarter. The call for a fiscal stimulus to revive the economy is also growing loud. The question is whether a large fiscal stimulus will shake the economic stability that has been painstakingly built. In this article, we

	explore when the slowdown really started
	argue why there is no cause for panic
	opine why limited, targeted spending rather than a largesse is sufficient to tide over this difficult phase.


Any substantial stock market correction as a reaction to the slowdown will do you good and prevent further froth in the market. It will also actually provide opportunities to invest well.




Distress calls by a large section of the media and by economists on feeble GDP growth of 5.7% for the quarter ending June 2017 has spilled over into stock markets. It has also prompted the Government to wear its thinking cap to come up with ‘stimulus’ packages for the economy. No surprise there – who would be more worried about a slowing economy than a ruling Government that is going into elections in less than 2 years!

 Open  a FREE Account Now! 

Hard-earned economic stability

Let’s pause for a moment to review the economic journey over the past few years. From double-digit inflation, high current account (CAD) and fiscal deficit and a currency that was losing ground 4 years ago, India became a haven of stability in 2016-17. Falling oil prices contributed to a lower CAD. Subsidies were scaled back. There was a prudent use of minimum support prices for agricultural commodities along with a management of food imports and an improved supply chain. Monetary policy shifted to maintaining a target rate of inflation.

In the wake of all this, along with consumer price inflation contained at less than 5% for over 3 years now, the stability that a steady inflation brings to the economy is visible. A stable currency, lower interest rates, higher FDI and higher FPI and DII money all followed stability. And yet, here we are with a quarterly GDP growth at a 13-quarter low. Why?

Slowdown was already underway

Whether it was poor monsoons in FY-16, structural transformations such as Aadhar push, the bankruptcy law, the Real Estate (Regulation & Development) Act 2016, demonetisation, or GST, to name just a few, the economy has had to deal with continuous jolts over the past several quarters. Cumulatively taken, these are among the reasons that are stalling growth. And, yes, the government can be blamed for executing these without sufficient measures to tackle their impact. However, these are more transitional than long-term issues that will, hopefully, fade over time and bring in the benefits they are supposed to.

But were these the only reasons for the slowdown? Not really – it appears that slowdown had already started before the impact of these major changes could be felt.

First, let us take the supply side of the economy measured through GVA. GVA, or gross value added, is the total of output and income in the economy. It does not exclude subsidies and does not include taxes. That means it is a better gauge of real production in the economy, without being distorted by taxes and subsidies, like GDP. India adopted GVA a couple of years ago, to represent the supply side (production) of the economy. This number at 5.6% for the quarter ending June-17 (over June 2016) was the same as March-17 quarter. But let’s look at core GVA. The core GVA, stripped of agriculture and government spending, captures the private sector’s production. The graph below will tell you that the slowdown there started right after March 2016.



Second, let us take the demand side of the GDP. India’s 9% GDP growth periods in the pre-2008 era was fuelled by double-digit export growth. If we take the last 12 quarters average exports FELL by 0.5%. For a demand-side factor that contributes over 20% to the GDP, a growth at this rate can only pull down the economy.

Still the growth engine held until 2015-16. However, the fact is the 2015-16 GDP was substantially boosted by oil’s freefall. India ran out of that luck with imports picking up once again.

So, what kept the growth engine going in 2016-17? It was government spending of course! The average growth of government spending for 16 quarters ending FY-16 was 4%. For the next 4 quarters of FY-17, the average stood at 21.5%! That has now dropped to 17.2%. What does that mean? The government has been spending and spending enough – and that is indeed fiscal in nature.  Whether the government should once again raise this rate of spending at the cost of expanding the deficit, rocking rates, rupee and inflation is the big question.


	 	2016-17

(Growth %)	2017-18

Growth (%)
		Q1	Q2	Q3	Q4	Q1
	Govt. Final Consumption Exp.	16.6	16.5	21.0	31.9	17.2
	Private final Consumption Exp.	8.4	7.9	11.1	7.3	6.7
	Gross Fixed Capital Formation	7.4	3.0	1.7	-2.1	1.6
	Source: MOSPI




 Open  a FREE Account Now! 

Growth without rocking stability 

The above data tells us that the slowdown has occurred despite government spending. That means the populist demand of large-scale spending to drive growth may not even yield results – at least not in the short term. More, such spending may rock the boat of stability, which has been hard earned over several years through the work of policymakers.

This is more so with tax revenues expected to be lower than budgeted (because of operational issues with GST) and non-tax revenues (divestments and dividends) remaining uncertain. Reviving investment spending by the private sector is also a tall order when capacity utilisation is at a little over 70% and no corporate entity would want to invest in a phase of structural shifts (like GST).

Selective spending to revive growth

If we remove all the panic created by the slowdown and look at the numbers, they are not terrible. If we take electricity and other utilities, construction, trade, financial, and other services, these have remained strong, or are showing signs of revival (see table below). These components account for 54% of the GVA and can support growth.


	 	2016-17

(Growth %)	2017-18

Growth (%)
		Q1	Q2	Q3	Q4	Q1
	Agriculture, forestry & fishing	2.5	4.1	6.9	5.2	2.3
	Mining & quarrying	-0.9	-1.3	1.9	6.4	-0.7
	Manufacturing	10.7	7.7	8.2	5.3	1.2
	Electricity, gas, water supply & other utility services	10.3	5.1	7.4	6.1	7.0
	Construction	3.1	4.3	3.4	-3.7	2.0
	Trade, hotels, transport, communication and services related to broadcasting 	8.9	7.7	8.3	6.5	11.1
	Financial, real estate & professional services 	9.4	7.0	3.3	2.2	6.4
	Public administration, defence and Other Services	8.6	9.5	10.3	17.0	9.5
	GVA at basic price	7.6	6.8	6.7	5.6	5.6
	Core GVA at basic price*	8.4	6.7	5.9	3.8	5.5
	Source: MOSPI					




The solution, as some economists also opine, may lie in reviving demand of products both locally and through exports. The government has already started announcing relief measures for exporters, especially on the tax front. Similar relief may also be warranted for small and medium enterprises, even if temporarily.

Similarly, targeted spending that can create employment and boost consumption would also help. On this front, the suggestions from former Chief Economic Advisor Kaushik Basu and a few other economists to spend in social sectors of healthcare and education can result in employment as well as boost income, especially in rural areas.  A marginal digression in fiscal spending may be tolerated in these areas given that the probability of spurring consumption demand is higher. A report by Kotak Institutional Securities based on actual figures in 2015 and estimates by UN and other social development focused organisations talk of at least 20 million government jobs in social sectors of education (teachers), healthcare (nurses and doctors) and public security (police) alone, in the next 5 years. Even a small boost on this count can lead to a sharp pickup in consumption.

Another area that has a ripple effect on the economy is housing. Towards this front, interest rates on home loan have fallen from 10.2% to 8.6% (between 2015 and now) now causing at least a 10% lower EMI. The Centre has also recently announced extending the Interest Subsidy Scheme (under Pradhan Mantri Awas Yojana (Urban) on home loans for middle-income groups by another 15 months; subsidy of about Rs 2.6 lakh will now be available until March 2019. These factors with stable to marginally lower prices of houses prevailing now can boost the sector. Further spending on mass housing schemes by the government could also help.

In other words, selective spending to revive consumption/export demand and not large-scale spending could be the way forward without impacting economic stability. Any other populist measures, in our opinion, would not only be imprudent but also neutralise any future benefits from all the structural reforms built over the last 3 years.

What if markets react?

Panic is the best friend for your investments. Markets have reacted and may continue to react. We have been hinting that this market is being driven by liquidity and not earnings growth. At 25 times trailing P/E for the Nifty, FPIs have been pulling out for more attractively valued emerging markets (valued at 15-20 times P/E) although domestic investors continue to invest. The FPI pull out, coupled with earnings disappointment already underway, will simply afford an opportunity for domestic investors to average stocks of companies with sound financials and business. For example, with consumption slowing, the consumption proxy plays picked by funds could well take a beating. This may provide good entry points into such stocks. Hence instead of panicking, gear up for volatility over the next couple of quarters.

If you invested any time after June 2017, you may probably be in the red by the end of this calendar year. It simply means you have bargaining opportunities. Instead of panicking, run systematic transfer plans or value averaging plans to tide over the volatility. Get in touch with your advisors to help you average. We will also sound you off on good opportunities that corrections can provide.

Other articles you may like
	Mutual fund labeling: What colour is your fund?
	"Value investing is a far bigger opportunity in India now"
	FundsIndia Recommends: Invesco India Growth
	Why SWPs are great for regular income - Part I





			
			Post Views: 
			2,039
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    economy. growth. Investments. Mutual funds. slowdown. view
                

			

            	


			
			1 thought on “FundsIndia Views: Indian economy – are experts’ concerns overblown?”		


		
			
			
				
					
												Kaustuh says:					


					
						
							
								September 28, 2017 at 7:48 pm							
						
											


									

				
					Thanks for sharing useful article!!

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


