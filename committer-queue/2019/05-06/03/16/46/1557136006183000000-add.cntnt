

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia explains: When is it a good time to invest?

            		July 17, 2017 . Mutual Fund Research Desk
                    

	
                    
                                    Back to Basics . General . Simply Important
                    
		
            	

            	When is it a good time to invest? But that’s a loaded question at any time, and not just in the current market mood. The answer to whether the time is right depends on three factors – the purpose for which you’re investing, the number of months or years you will be investing for, and the mode in which you’re investing. Answering this needs answering a couple other questions first.

What is your goal?

If it to save taxes through tax-saving funds, then there is no correct time to invest. One, you need to do it every year. Two, you need to hold this for three years or more and equities usually deliver well over the long term. If it is for your retirement several years down the line or you simply want your money to grow over time, even then the timing does not matter. This is because, over the long term, equity delivers inflation-beating returns and you would always get superior returns. However, if you are saving up to buy a car or to meet the down payment for a home, then your horizon is likely to be shorter. It makes timing more important then.

What is your investment horizon?

Once your goal is decided, then your horizon is decided. In other words, how long do you plan to hold your investments? If your holding period is less than five years, then it is not a good time to begin investing in equity funds. Considering the Nifty 50 returns over the past ten years rolled daily, there was a 20 per cent chance that you would wind up with losses if you had pulled out your money in one or two years. Such a holding period is good timing for debt funds. If you have a horizon of 2-3 years, then a short-term debt fund is good. If it is even shorter at 1 year, go for ultra short-term funds. 

If your horizon is five or more years, then it is always a good time to invest in equity mutual funds whether the market at the moment is in a bullish mood or bearish. Rolling the Nifty 50’s five-year returns daily over the past ten years throws up only a 2 per cent chance of actually incurring losses at the end. The odds are even better on investing in quality equity funds. Likewise, there is no perfect timing for long-term income funds and dynamic bond funds if you have a time horizon of 3 years and more. 

However, timing is essential if you’re considering sector-specific funds those on themes, such as manufacturing, services, or consumption. Themes and sectors are not perennial and can completely go bust. You need to catch the theme around the time it is gathering momentum, and get out when the tide turns. For example, 2009 to 2014 was good for pharmaceuticals, but no longer. 2015 was a bad year for bank stocks, while 2016 was a good one. The timing problem doesn’t arise in non-themed equity funds as the fund manger actively switches between sectors. Similarly, pure gilt funds need a timed entry when yields are high and an exit when they bottom out. Other debt funds don’t have such timing issues. Even dynamic bond funds would book profits on gilts as the cycle bottoms and move into accrual, so the timing problem is solved.

How do you solve the timing problem?

Even if you have a long-term horizon, you may be worried about investing at market peaks in equity funds – and rightly so. The answer to this concern is systematic plans. If you do not have a lump sum to invest, then simply start a SIP in your equity funds for whatever your monthly amount is. Should the market correct, continuing your SIPs through this will allow you to buy at lows. SIPs anyway need to be run for three years at least. If you do have a large sum to invest, decide the equity funds. for each of those equity funds, pick the liquid fund from the same fund house. Then set a systematic transfer plan from those liquid funds into the equity funds for 8-10 months. This will ensure that you do not put all your money in at peaks and allow you to capture any corrections if they occur. 

Further, adding debt funds to your portfolio along with equity will temper your portfolio’s risk level. So even if stocks do correct, the debt funds will help contain your portfolio’s losses. The extent to which you include debt depends on your timeframe and risk levels.

Other articles you may like
	Nifty Weekly Outlook - June 3 - 7
	FundsIndia Features: Group to grow!
	Is waiting to pay off all the loans before investing a good strategy?
	Investment Wisdom from Rajnikanth





			
			Post Views: 
			2,825
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Mutual funds. timing
                

			

            	


			
			2 thoughts on “FundsIndia explains: When is it a good time to invest?”		


		
			
			
				
					
												Dhruv says:					


					
						
							
								July 27, 2017 at 9:20 pm							
						
											


									

				
					Could you explain why “Even dynamic bond funds would book profits on gilts as the cycle bottoms and move into accrual, so the timing problem is solved”? As when interest rates are high, all debt instruments will suffer losses and vice versa. Hence, GILTs and dynamic bond funds would behave more or less same. If you look at 3-years or 5-years SIP return for both, you feel observe the same.

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								August 3, 2017 at 4:54 pm							
						
											


									

				
					Hi Dhruv, 

Dynamic bond funds – if you look at the past 3 years or even 5 years – would have been similar to gilts because rate cut cycle and the expectations surrounding it have been around for nearly that long. 2013 was a brief period where rates were hiked suddenly against the expectations. So if the rate cycle were to pause now, dynamic bond funds would book the profits on the gilts, reduce their gilt exposures, and redeploy to accrual. So when bond prices correct, they wouldn’t suffer. Or if rates rise, they will stick to accrual and earn the higher yields from there. Several dynamic bond funds in fact have reduced their duration calls quite a bit compared to previous years. They did increase it slightly in the past few months as rate cut expectations surfaced again, but they are not as aggressive as they were before. So what will happen is that the superlative returns normalise post the move to accrual. A gilt fund wouldn’t be able to move to accrual, and returns will consequently suffer especially if rates rose. Therefore, a gilt fund requires timing. 

Thanks,

Bhavana

				


				Reply
			







		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


