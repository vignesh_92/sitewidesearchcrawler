

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		It’s the time in the market that matters, not timing the market

            		October 9, 2014 . Vidya Bala
                    

	
                    
                                    Equity Research . General . Mutual funds
                    
		
            	

            	Aashish P. Somaiyya, Managing Directors & CEO of Motilal Oswal Asset Management Company does not need an introduction. If you have not heard him speak on stage, you would probably have read his columns both in the print and digital media. 

A good speaker, voracious writer and a forceful debater, Aashish is your true argumentative Indian, arguing the cause of Indian equities and their potential to build your wealth.

Aashish’s penchant for investments stems from his earlier stint in ICICI Pru AMC where he spent close to 13 years understanding and dealing with different investor segments and products. In this article, Aashish abandons the need to talk with numbers and uses logic to explain why you have to stay invested in equities for ‘long’ to build wealth.

 Buy  24 Karat Gold Online 

[hr]

I am sure anyone who has ever read anything about investing into equities has read the clichéd “equities are for long term” statement a zillion times. Most of the time these statements are also backed by a lot of statistics which prove that if you try to time the market for its bottoms and peaks and by mistake or by a stroke of bad luck, if you missed the best 10 days then your returns would be quite a few percentage points lower.

Equally, on the other hand, you will find a lot of articles and data which will tell you that when there is profit to be had, take it off the table. There is no definition of long term and if you invested exactly on August 31, 1994 and withdrew 10 years later on August 31, 2004 you wouldn’t even beat PPF returns.

I have no love for this kind of analysis because I have one of those rare habits where I read the preface, introduction, foreword, afterword everything in a book before I actually read the book.

First year of management studies I read a book called “Statistics for Management” written by Levin-Rubin. The introduction to this book says that there are three types of people in this world – liars, damned liars and then there are statisticians. So much for using numbers to prove a point in favour or against long term investing. I believe in logic. And hence, I will spare you the horror of reading graphs, charts and tables.

What’s long term?

I do believe that while investing in equities one must invest for the long term and really one has no idea when the market will do what you have been waiting for it to do. I read a famous statement by John Maynard Keynes which said that “The market can remain irrational longer than you can remain solvent” and then there’s another equally apt one which goes something like “Markets go up till the last person has bought and markets keep going down till the last person has sold”.

These statements mean that if you have a definite time horizon to your investment which is not sufficiently long term in nature, then forget fundamentals, your future will depend on gyrations of the stock market. So what is the definition of long term?

 Buy  24 Karat Gold Online 

Our industry started off by saying that long term means 3 years; if you go by taxation policies for investing, long term means one year, after a real bad fall in markets sometimes people tend to say long term means five years!

Look, let me be honest, there is no fixed definition of long term. And only put that money into equities, which you are OK to forget about for next few years. Money, which will be needed in a visible time frame, should never be allocated to equities. After hearing this kind of statement from me, a lot of times people talk about investing in equities as if they have alternative options.

If you have savings of Rs 5 lakh right now or if you get an inflow of Rs 5 lakh right now and you need it to be Rs 10 lakh in the next 5 years, you basically need somewhere in range of 15% compounded returns to meet that goal.

There is no asset class other than equities which will absorb a sum like Rs 5 lakh and still leave you the full potential to reach your goal. So where is the choice? And what is the point thinking that if I keep the money in a bank it will at least be safe? Yes, it will be safe, but the goal will remain a goal and not a score.


Long term to me is all about identifying good quality companies and participating in their entire growth cycle such that we don’t just get couple of percentage points of extra return; we should aim for multiplication and wealth creation.

Rome was not built in a day

The best of companies are in business for decades. It takes decades to implement a business plan, exploit a market opportunity to the fullest and to build a scale business. If you think the new kid on the block Flipkart is new think again – they started in 2006!

L&T started in 1946, HDFC in 1970, ICICI in 1955, Infosys in 1981, TCS in 1971 and so on and so forth. There are many examples of long-term investment in companies that has generated outstanding results.

 Buy  24 Karat Gold Online 

If we take a call on the businesses by way of buy, hold and sell recommendations every quarter we will always miss the wood for the trees. When people send messages on emails, FB and whatsapp they come and talk about Wipro and Maruti stories but then want do they do in real life?

I recently read on FB that Rs 10,000 invested in Wipro stock in 1980 if held all through would have been valued around Rs 700 crore today. Wow, nice. Shouldn’t we practice this?

So long term to me is all  about identifying good quality companies and participating in their entire growth cycle such that we don’t just get couple of percentage points of extra return; we should aim for multiplication and wealth creation.

After reading that Wipro example, I thought that I must go back and ask my father what he was doing when he could have bought Rs 10,00 worth of Wipro and saved me all the hard work. Can I ask that question? And what can he answer? I am sure he would have said that there was no Economic Times, there was no CNBC TV 18, and all said and done Azim Premji’s company only made oil back then and not software!

He is absolutely right in saying this and then I think what will I answer if my son asks me 25 years later than when TCS market capitalization was barely Rs 5 lakh crore what were you doing? What do you think I will answer?


And long term investing has to be preceded by buying the right stocks or the right funds. The issue is not that most of us don’t buy the right stocks or the right funds. The issue is that whenever we buy the right fund or right stock, we book profits, we sell.

Making money or creating wealth?

The question still remains – are we here for getting few percentage points more return or are we here for creating wealth? If you wish to create wealth by way of multiplication, you need to remain invested in equities absolutely like those FB and whatsapp messages seem to suggest.

 Buy  24 Karat Gold Online 

And also think, do the promoters of these much FBed and whatsapped companies go to office and work after checking the share price?

As a company at Motilal Oswal Mutual Fund we manage only equity mutual funds and we advertise ‘BUY RIGHT : SIT TIGHT’. I had a funny experience because recently at a conference one of the gentlemen told me what is so special you have said, we are “SIT TIGHT” since last 7 years since we invested in the peak of 2007!

The entire audience burst into peals of laughter leaving me to point out rather meekly that the SIT TIGHT has to be preceded by BUY RIGHT.

And long term investing has to be preceded by buying the right stocks or the right funds.

The issue is not that most of us don’t buy the right stocks or the right funds. The issue is that whenever we buy the right fund or right stock, we book profits, we sell.

And whenever we buy a bad stock or a bad fund, how can we sell at a loss? So we hold on. Portfolios are more often than not riddled with profits from good investments being successively ploughed into bad investments in the name of booking profits and averaging losses.

At least a long term investing orientation, holding on to good quality mutual fund schemes; more time spent in the market will ensure you hold on to winners. So the next time you buy a stock or an equity mutual fund, please visualise you are buying a business which will grow over the years, the owners will become wealthier and you will be one of them!

Other articles you may like
	Use RSI More Effectively - Part II
	FundsIndia Strategies: Funds for risk-taking debt investors
	Kotak Mahindra Bank: A Prime Candidate For Core Portfolio
	FundsIndia Recommends: Franklin India Smaller Companies





			
			Post Views: 
			2,114
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    equity market. Mutual fund
                

			

            	


	
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


