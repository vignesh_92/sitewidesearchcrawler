

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia View: The story behind how your wealth is created

            		December 23, 2015 . Vidya Bala
                    

	
                    
                                    Mutual funds . Personal Finance . Recommendations
                    
		
            	

            	Motilal Oswal has come out with its 20th Annual Wealth Creation Study for five years between 2010 and 2015. We only thought it apt to not just showcase the results of that report, but to also let you know how these findings play a vital role in your own mutual fund portfolios and investing habits as well. After all, your equity mutual fund portfolios are nothing but a basket of stocks, meticulously picked by fund managers, in the hope that they build your wealth. 

The report splits its findings into various segments. Of these, we will discuss the ‘biggest wealth creators’ and the ‘fastest wealth creators’. Wealth creation is defined by the addition (in value) to market cap in the last 5 years. Market cap growth and price returns are for the 5 years ending March 2015. 

 Open  a FREE Account Now! 

Elephants too dance

The table below showcases the top 10 wealth creators. Needless to say, they are large-sized companies with significant growth in their market cap in the last 5 years. Do note that it is not just the increase in market cap; the returns (column called ‘Price CAGR %’) is by no means small. And do note that markets, over the long term, are slaves only to earnings growth. You will notice that these companies had double-digit earnings growth delivered over this period. This is no mean task, given that these are large companies with an already high earnings base. 

Takeaway: Of course, this is not to say that you should expect  equity funds to deliver likewise. They likely won’t. Do not forget that a fund basket is a mix of good and mediocre picks; only perhaps better than a portfolio that a novice investor would construct. 

The point here is that large-cap stocks too deliver, and more importantly, deliver consistently. In this list, for instance, the top 3 stocks were consistent wealth creators in Motilal Oswal’s past reports as well; that means they generated wealth in the previous 3 blocks of 5 years. Large-cap stocks, with their dominant position, are able to not only withstand slow down, but are also ready to take off when the revival happens. This reflects clearly in their earnings growth.


 

The low base effect

Needless to say, small companies have managed to grow their market cap at a much faster clip, generating superlative returns in the process. It is a small wonder that you see your mid-cap and small-cap funds deliver high returns. However, the key here is to be able to catch them all at the right time; and this is no easy task.

Besides, the ability of these stocks to deliver consistently is also low. For instance, according to the report, in the table below (top 10 fastest wealth creators), only Eicher Motors and Page Industries managed to appear in this list in the past 3 years (for every block of 5 years). That means, there are always new companies coming up.


 

Just take a look at the data below to see the 5-year fastest wealth creators in different years. That means, unless you had entered these stocks in the right years, you may not land up with a multi-bagger. Not an easy task even for a fund manager, right? And the larger risk is that smaller companies’ stocks are prone to high mortality. Take the case of Sanwaria Agro (wealth creator for 5 years ending March 2011). From its rise between 2006 and 2011, the stock plummeted 85 per cent for 5 years ending March 2015.





 

Takeaway: Mid and small-caps are necessary to provide pace to your wealth creation. However, their high mortality, and need to time their entry and exit proves it extremely difficult to enjoy their growth story in a sustained way, save for a few stocks.

Why is this so? According to the report, this segment of market cap needs a ‘Lollapalooza effect’ to occur. This term, used by Charlie Munger of Bershire Hathaway and partner of Warren Buffett, simply means that real big outcomes arise when multiple factors are at play. For instance, size, quality, growth, longevity, and price may have to act together for a stock to raise to meteoric proportions. There are also some fundamental factors that become essential. 

For instance, many of the companies in the fastest wealth creator list were in large sectors such as finance, autos, or pharma, or in scalable niche businesses such as air coolers (Symphony), or inner wear (Page Industries). These, combined with sound management and proper handling of financial resources, can lead to high pace of earnings growth, and in turn, drive evaluations. 

That’s a lot to deal with isn’t it? Some of you may be enamoured by the returns generated by mid and small cap funds (and their stocks), and you may enter them after a huge run up. Do remember that the run up simply implies that a good number of stocks in that segment may be highly valued. 

Take a look at the table (Top 10 fastest wealth creators) and you will see that the price earnings ratio of these stocks are at a premium that makes it hard for earnings growth to move at the same pace. Hence, stagnation, lower returns, or sometimes, even a price fall cannot be ruled out. As opposed to this, you will see better parity in the valuation of larger companies (first table) when compared with their earnings growth. 

This is why we think it is good to have mid-caps, but not too much. Large and emerging large caps, through large-cap and diversified funds, should provide the necessary stability; even as mid-cap funds generate a few winners.

 Open  a FREE Account Now! 

Other takeaways

The report has plenty of other takeaways, a few of which we enlist:

– Wealth is created through buying and holding – good opportunities that is. This holds true for mutual funds as well.

– However, buy and hold has an expiry date too. To be able to identify when a stock is nearing its high-growth expiry date, or a fund is slipping in performance requires periodic monitoring and review.

– Chasing chart busters seldom yields results.

– It is hard for mid-cap stocks, and therefore, mid-cap funds to sustain performance. While stocks may have to be exited at the right time (even if early), mid-cap funds have to deftly rejig their portfolio to identify new opportunities. If not, they fall by the way side in the performance chart.

– Short term wealth destruction cannot be prevented in downturns; such times can, however, be well used to average costs in otherwise sound stocks/funds.

FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis of investment decisions. To know how to read our weekly fund reviews, please click here.

Other articles you may like
	Beware of the yield claims on tax-saving deposits
	Should you have sector funds in your portfolio?
	FundsIndia Strategies: Challenge fund managers with this election portfolio
	FundsIndia Views: 2018 – taking stock of markets and economy





			
			Post Views: 
			2,026
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Investments. Mutual funds. wealth
                

			

            	


			
			2 thoughts on “FundsIndia View: The story behind how your wealth is created”		


		
			
			
				
					
												paresh says:					


					
						
							
								December 23, 2015 at 8:03 pm							
						
											


									

				
					but real concern for investor is – this growth isn’t reflecting in mutual fund returns.

What is avg CAGR of large cap funds for last 05 yrs : around 7%.

Thanks.

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								December 30, 2015 at 10:15 pm							
						
											


									

				
					Hello Sir, the 5-year return you are talking of is a mere point to point market return of Sensex. Good funds have delivered anywhere between 12-20% annual returns over 5 years. And SIPs have delivered even higher as a result of cost averaging. If it is not reflecting in your mutual fund then either the fund has to be reviewed or the strategy (lump sum or SIP) has to be. True, mutual funds cannot deliver as well s individual stock winners. Afterall, nobody can pick a portfolio of 30/30 or 40/40 winning stocks! thanks, Vidya

				


				Reply
			







		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


