

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		When to redeem funds and when not to

            		December 18, 2017 . Ashwini Arulrajhan
                    

	
                    
                                    Simply Important
                    
		
            	

            	A lot of deliberation goes into investment decisions. When to invest, what funds to invest in, how to go about investing in them and a number of other factors are given a thought to before making an investment. But in contrast, when you redeem money, you often do so in haste. Only when you redeem, you convert your gains into real money. This makes redemption a process that requires thought and consideration. Redeeming in haste can result in your foregoing optimal returns on your investment.

When should you redeem

First, let us discuss when you should redeem and which is the best way to redeem before getting to common misconceptions investors have about the same.

When you need money

You might need some money for an unplanned event that cropped up even when your goal is far from close. Ideally, during these times you should be dipping into your contingency fund. But when that is not the case, you end up considering your mutual fund investments for liquidation. To know more on which funds to redeem from during such times, read here. Other things to keep in mind would be the tax implications and exit loads applicable. Redeeming from the wrong fund can not only result in loads or taxes but also result in missed opportunities to build wealth.

When your goal is nearing

In case of equity investments planned for specific goals, it is important how you redeem them. When your goal is one year away or so, plan for an STP to a debt fund or SWP to your bank account, so that you can avoid volatility during the penultimate stage of your goal. This gives the same benefit a SIP gives to your investment. You are booking your gains in portions to reduce the impact of fluctuations that occur in an equity fund.

When you have to rebalance your portfolio

Your asset allocation of the portfolio has to be revisited once every year to make sure it still matches your risk appetite and goal requirements. If the divergence from your original asset allocation is more than say 5%, then you can consider rebalancing it by redeeming some part of your investment from the inflated asset class and reinvesting it into the other asset class that is lower. This typically happens when equities have run up to a great extent and you have excessive exposure in that asset class or sometimes when equities have seen a significant fall, as was the case by end 2008.

 Open  a FREE Account Now! 

Reasons for which you should not redeem

According to data from AMFI, only 34.2% of equity assets have been held for more than 3 years as of Oct 2017. At a time when it is quite established that equity funds are meant for long term, it is alarming that investors hesitate to stay put. This reveals the attitude among investors when it comes to investing for the required time period. Lets us look at other reasons that prompts investors to redeem and why that may not be the smartest idea.

My fund is underperforming

There can be two scenarios when this thought might occur to you. One is when you think your fund is not doing well and the second is when you think there are funds that are doing better than your fund.

Let’s take a real-time example:

Say, you invested Rs.100,000 in Franklin India Bluechip on 24th September, 2013. A year later, you happen to check the returns. Your money has grown to Rs.1,38,622 giving you a return of 38.6%  in one year and you are happy with this. However, you wish to compare your fund and look at HDFC Equity fund. Your happiness quotient dips as you see the latter’s past year return at 69.3%. You feel you cannot let this fund go and invest in HDFC Equity fund. You investment would have a value of Rs. 1,84,399 (as of 24th Sept 2017) giving you an overall returns of 16.5% per annum from the first time you started investment. HDFC Equity on the other hand, delivered 22.5% over the 4 years ending September 2017. Unfortunately, you were not invested in the fund when it made maximum gains. Hence, the benefit of superior returns from the fund did not convert into returns for you. On the other hand, if you had held on to Franklin India Bluechip, you would have had Rs.1,91,614 with a return of 17.65%. Which means you would have ended with a larger corpus by not switching!

Of course, it could have taken the other course as well. You could have ended up benefitting because of your decision to switch. The point here is that, you don’t exactly know how the fund is going to perform in the future based on past returns alone. In that case, choosing to switch would simply be playing a game of luck. This is precisely why we don’t encourage shuffling your portfolio unnecessarily. This does not mean you have to stick with the fund you first invested in, no matter how poor the returns seem. Spotting true underperformance is the key here.

Leaving the returns differential, comparing Franklin India Bluechip, which is a pure large-cap fund, with HDFC Equity, which has some midcaps as well, may not even be the correct thing to do. In other words, inappropriate comparisons may result in making wrong redemption calls. And the large-cap fund was probably added to your portfolio for a reason. You would be changing that premise by moving to a differently-styled fund.

Comparing funds across categories often happen in debt. Debt funds, for instance, generate optimal returns when held for the respective time periods for the category.



If you happen to compare a long term debt fund with liquid funds in the shorter period, it might be possible that the returns look dismal. This has especially been the case with dynamic bond funds recently as they have been experiencing short term volatility. Therefore, concluding that a fund is underperforming by looking at returns for a period that is not meant for the category can be misleading.

Enough thought should go into if at all is it necessary to switch out of a fund.  Take the help of your advisor to decide if a fund should be a part of your portfolio or not.

Market related concerns

When market is peaking, you might think you should be booking your profits as you will not see gains like this again. Similarly, when the market is falling, you might think you should curtail your losses by getting out as soon as possible. Both these strategies may be flawed. Why? Because your fund manager is already booking profits and cutting losses for you. Unless you are talking about sector or theme funds where exit strategy is important, it is not necessary for you to act on market movements.

Booking profits

If you are trying to book profits, your next concern will be where to deploy the money. We can take the same example that we took before. Investment in Franklin India Bluechip gave a 1-year return of around 40% in 2014. After a year like 2014 where the market had a good rally, you may have wanted to book your gain. With the FD rates also at a high then, it may have seemed like a good idea to move the fund to a 3 year FD at 8.5% interest. If you had done that, your overall return from the investment made in 2013 would be 15.3% (September 2017) versus the 17.6% return you would have got if you had held onto the fund. Prematurely withdrawing from an investment because you see gains will not be the best thing to do. However, if you really wish to book profits, the best way to go about doing it is doing an asset rebalancing once a year. When you are cutting down excessive equity exposure, you are essentially booking profits and redeploying it in debt and vice versa. But like we said earlier, rebalancing is needed only after your allocation crosses a certain threshold from your original allocation – by say plus or minus 5% or more.

Curtailing losses

When you see a fund in the negative territory, your instinctive action is to get out of it. ‘The market is not doing well, I need to get out before it gets worse’. If you redeem when the market is not doing well, you will only be booking losses. By sticking to the time period planned for your investment, you give time for the market to recover; for the fund to recover. And if you are systematically investing, staying invested during a downturn will do you more good as you will get a chance to buy on dips which will give you better returns.

 Open  a FREE Account Now! 

Summary

Unless you need the money right away or you are nearing your goal, there is no real need for redemption. Even if you decide to redeem for causes such as underperformance make sure the money is redeployed. Money not redeployed is money lost.

Other articles you may like
	FundsIndia Explains - Why there is no fixed return in mutual funds
	FundsIndia Explains : How mutual funds pay dividends
	FundsIndia Explains - Scheme Information Document (SID)
	FundsIndia Explains: Rolling Returns - What is it and how it is used





			
			Post Views: 
			2,443
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    rebalancing. Redemption. underperforming
                

			

            	


			
			2 thoughts on “When to redeem funds and when not to”		


		
			
			
				
					
												Kumar says:					


					
						
							
								January 11, 2018 at 8:37 pm							
						
											


									

				
					Hi,

Its a nice article. However I am also having similar stuff in my mind, can you please suggest.

I am investing in HFDC Equity and SBI Emerging fund since 2013 but later in 2015 realized that fund is not performing so stopped further SIP and invested in other funds but didn’t redeemed the units.

Recently I noticed that fund is performing well and giving good profit however I was planning to redeem the unit and invest into my other existing fund via STP mode. Is it right time to redeem the units or should I wait for some more time since its performing well now (though these are in-active funds in my portfolio).

Please suggest.

Thanks,

Kumar

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								March 8, 2018 at 11:12 am							
						
											


									

				
					Hello,

The call on whether or not to redeem depends on other funds you have in your portfolio, the amount invested in those funds and so on. Please route such portfolio-specific queries to your advisor. If you’re a FundsIndia investor, you can use the Talk to your Advisor feature on your dashboard to get your portfolio reviewed. 

Thanks,

Bhavana

				


				Reply
			







		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


