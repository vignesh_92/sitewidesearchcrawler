

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		How long should you run your SIP?

            		May 14, 2018 . Mutual Fund Research Desk
                    

	
                    
                                    Simply Important
                    
		
            	

            	Last week I was talking to a friend from another profession.  He wanted some financial advice from me. I asked him all the usual questions an advisor would ask to know the purpose of investment but he did not have any specific goal in mind. All that he could say was but he was willing to do ‘SIP’. I said fine, I’d give him mutual funds and I asked him how long he could stay invested. He said he could invest for another 3 years. I said they are not the same. I again asked him when he needed this money. Does he need it for any purpose? He said, no, I don’t really need it until retirement. Great, then you can stay invested until retirement. He was confused at this stage.

He had many questions starting from whether I was going to suggest him SIPs at all (or just mutual funds) and about the period of SIP and what I meant by investment period. 

Here is a summary of the responses I gave for the above questions.

SIP is not an instrument

Systematic investment plan or SIP is not an instrument of investment. It is just a method of investing into mutual funds. You can invest in mutual funds periodically or regularly (typically every month). Investing regularly helps navigate the volatility in the equity market and sometimes even in the debt market. The idea of a SIP is to buy through up and downs in the market so that you don’t time your entry wrong. The longer you do this, the higher the chances that you get averaging opportunities – that is – buy more in market dips. Hence, a SIP, especially in an equity fund is best done over at least 3-5 year period. 

SIP period and investment holding period need not be the same 

Most investors think you need to take out your money once the SIP instalment is done. This comes from their understanding of FD and RD. In a fixed deposit, you deposit the money once and hold it for a certain period of time. In a recurring deposit, you deposit regularly over a fixed period of time, and withdraw at the end of this time period. The time-frame, in such cases, is not decided based on your goals, but based on deposit tenure and the interest rates over such tenures offered by the banks. So, with this background you often tend to make two mistakes: one, you choose an arbitrary period of 3 years or 5 years to invest, relating it to a typically FD/RD lock-in period. This should not be done. Your savings and goal period should determine how long you need to run a SIP. Second, when your SIP instalments are done, you think it is time to withdraw. In case you had just a 1-year SIP period, you might think you should withdraw at the end of the 1st year. This is not true. 

The fact is, with mutual funds, you can withdraw anytime. You can withdraw the balance while your SIP is running 0. You can hold your investments well after your SIP is stopped; as long as you wish to hold or until your goal nears. In other words, you can hold your investments long after your SIPs stop. 

Having said that, when you are investing towards a goal, you need to make sure you save up enough for the goal. That means your tenure of the SIP must not be way away from your goal. For example, if you are investing for retirement, then you need to make sure you save for long enough a period so that you do not fall short of corpus when you retire. 

What should be your SIP tenure

How long you need to invest depend on how much you can invest and what is your asset allocation and time frame. If you have just 2 years to go for your goal and cannot spare much sums, you may have to invest regularly through these 2 years. For example, if someone is investing Rs 1 lakh a month in an asset allocated portfolio that will fetch them 10% annually and wants Rs 1 crore in 10 years, then they can stop investing (or shift to safer options) even before the end of the 8th year since  he would have reached the goal. However, somebody investing Rs 50,000 may have to stay invested over 10 years to reach there. This is the ideal scenario. But for those without goals, they should seek to keep their investment going as SIP is basically meant to provide discipline to your savings habit.

What happens when you choose short SIP tenures?

There are 2 pitfalls when you choose SIPs, especially in equity funds for short periods of say 1 year or less. First, you will not effectively average in the equity market, unless you are lucky to be investing through a falling market. You need to give time for SIPs to work. Second, while your intention would be to continue based on performance, there is a good chance (as seen by us) that you allow your SIP to expire and never restart. 

What if the fund does not do well? 

SIP is only a means to invest in mutual funds. So that means, you can change funds in which you SIP. You need not shorten the time frame of your investing for this purpose. For example, if you find your fund underperforming in the 2nd year of your holding it, you can seek advice to stop SIP in it and choose a better fund to start SIPs. The important thing is for the process of SIP to continue, so that you do not fall short of money when you near your goal. In other words, change funds in which you do a SIP if your fund is underperforming. Do not simply stop them or take a shorter time frame to test performance. 

Other articles you may like
	FundsIndia explains: What are debt funds?
	What is portfolio turnover ratio?
	FundsIndia explains : What are liquid funds
	FundsIndia Explains: What it means to own equity funds





			
			Post Views: 
			4,826
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                                    

			

            	


			
			2 thoughts on “How long should you run your SIP?”		


		
			
			
				
					
												Jignesh Shah says:					


					
						
							
								December 28, 2018 at 3:13 pm							
						
											


									

				
					Generally, holding period of the SIP investment is of 3 years. But, my financial advisers recommend me to invest in the SIP plan for a longer period (minimum of 5 years). While invested for the long term, SIP will work a lot of wonders for you.

				


				Reply
			

	
			
				
					
												seema prasad says:					


					
						
							
								February 28, 2019 at 5:14 pm							
						
											


									

				
					Well, SIP needs to be invested for longer period like 15 to 20 years. In this article you have explained so well about SIP mutual funds. Every person should compare the plans and go for SIP mutual funds.  It is basically synchronizes your financial decision towards achieving your life goals with available resources.

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


