

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Views: Why markets fell and what you should do

            		February 7, 2018 . Bhavana Acharya
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Research Desk . Views
                    
		
            	

            	On Tuesday morning, stock market investors woke up to a sea of red. The Sensex plunged 1000 points in a matter of minutes. The Nifty 50 was down 3%. The more opportunistic among you deployed more into markets. The more wary worried. And most wondered if this was a good time to invest and whether more fall was in store. In the year to date, the Nifty 100 index is down 1.2%, the Nifty Free Float Mid-cap 100 is down 8.2% and the Nifty Free Float Smallcap index is down 11.1%. So what drove the fall?

Global factors spark the fall

Though yesterday’s 1.6% was a sharp drop, markets had been on a downward phase for the past week. The Sensex and Nifty 50 hit a high on January 29th. On February 2nd, markets dipped 2.3% and followed it up with a 0.9% dip on Monday. The popular perception was that this was the fallout of the budget proposals. Little attention at the time was paid to what was happening in global markets.

However, the market correction is a result of global selling and not a domestic one. Across markets, 2017 and the start of 2018 was exuberant. But markets began correcting last week, with the US markets at the centre of the storm. US indices corrected around 4% last week, main European indices were down 3-4%, Asian indices were down 1-2%, and our markets were down 2.4%.

That sell-off deepened into a 1,175 point rout in the Dow Jones Industrial Average on Monday, the steepest fall since 2011. This sparked off a decline in Asian markets the following day. Japan’s Nikkei and Hong Kong’s Hang Seng both lost over 4% yesterday. Two trends led to the fall.

	Rising US Treasury yields: US yields have been rising from around September last year, on rate hikes by the US Federal Reserve and expectations of rate hikes. 10-year US treasury yields spiked to 2.75% by February from around 2.05% in September last year. Rate hike expectations beyond indicated hikes intensified last week, as US jobs data showed wage growth rising at the fastest pace since 2009 causing worry that it would lead to higher inflation and faster rate hikes.
	Rising stock markets: Across markets, 2017 was a great year. The Nasdaq, the S&P 500 and the Down Jones Industrial Average were up 20-28%, Asian markets were up a similar 20-36% through indices such as the Hang Seng, Nikkei, Kospi and the Nifty 50. Rapidly rising stock markets meant that valuations were getting expensive, though there was some earnings growth. Markets were also factoring in good growth with steady inflation.


The gap between stock and bond yields narrowed; last week’s concerns over inflation, and further and faster resultant rate hikes were the catalyst for selling in equities. Higher yields make bonds more attractive and equities lose their sheen. Rate hikes in US not only impact that market there but also institutional flows across markets. This is because the arbitrage opportunity from borrowing in US (at higher rates) and investing elsewhere narrows.

Domestic picture holds

With global selling, it is inevitable that it has an impact on our markets too. However, domestic flows provide some balance to FII flows, as has been seen over 2017. DIIs are also opportunistic, using FII selling to accumulate stocks and using FII buying to offload them. January 2018, in fact, saw FIIs buy a net Rs 9,568 crore while DIIs bought a net Rs 398 crore. In the past two days, DIIs have bought Rs 2,863 crore against an FII sale of Rs 3,589 crore.

When it comes to domestic market drivers, nothing has materially changed between the January market high and today. The budget did fulfil market expectations of a higher fiscal deficit and fiscal slippage. However, this higher spending has been mostly directed at productive spending – such as strengthening rural infrastructure and agricultural markets, healthcare and social security. To some extent, these are not simply giveaways and work towards strengthening the economy over time. The budget proposals did not have much to offer to or take away from corporate growth prospects, and for the market to react to. Our take on it is here.

Further, corporate earnings are showing improving signs of recovery. We already wrote about it a few months ago based on September 2017 quarter results. Going by the December 2017 quarter results available so far, corporate earnings growth has sustained. Aggregate profit for about 1000 companies has grown 25% for the December 2017 quarter over the year ago. Close to half the universe clocked double-digit earnings growth. Revenue growth was also in double-digits, at 14%. Earnings growth can continue to recover as, for one, the GST and demonetisation had delayed recovery and two, a lower base will help. Over the longer term, therefore, growth drivers as we detailed in our 2018 equity outlook continue to hold.

Correction needed

A marked feature of 2017 markets was the complete lack of volatility. Price-earnings multiples were also pushed to levels close to that of the 2007 market. We had flagged this in our 2018 market outlook. Given the lack of market fears and the stock market run up ahead of fundamentals, bouts of correction provide a welcome opportunity to get stock valuations into more reasonable zones.

Between end-December 2017 and now, the Nifty 50 PE multiple has dropped from 26.9 times to 25.4 times. Funds had a hard time in 2017; based on metrics such as valuations, growth trajectory, visibility of growth, and so on, there were fewer stock opportunities. Mid-cap and small-cap funds were hit especially hard on this front, with less than a quarter of the category managing to deliver returns better than the mid-cap and small-cap indices. The chart shows the movement of the Nifty 50 index and PEs. You can see how the correction has led to better PEs.



A market correction coupled with a recovery in earnings, and a broad-basing of growth can bring valuations in line. It will also serve to calm the fever pace of the market which saw an addition of 2,226 points to the Sensex in the span of a month. Corrections, therefore, are not to be feared especially at this point. Apart from global factors, events such as monetary policy, assembly elections and anticipation of the general elections, trajectory of inflation and interest rates could keep volatility up.

Strategy to follow

For those of you who ask us whether there will be more correction ahead, our answer is: it isn’t possible to predict how far down the market can go; markets have already moved back into positive territory on global cues. As we said before, while long-term drivers are intact, a part of market returns was front-loaded in 2017 and volatility can sustain through 2018 in reaction. A 1000-plus point single-day Sensex fall may be rare, but volatility can provide buying opportunities for investors with a long-term perspective. Here’s what to do when markets correct:

	If you have STPs running from liquid into equity funds already: Use market corrections like the one yesterday to switch an additional sum into equity from the liquid funds. This switch should be made apart from your regular STP. This can help buy more on steeper dips and effect better averaging.
	If you have a large fresh investment to make: Do not invest it at one go because markets look like they are correcting, nor should you put it off waiting for markets to correct some more. There is no predicting the market. The easiest thing to do is to start an STP and run this over the next 10-12 months and add to it as mentioned in the previous point if markets correct sharply. If you have the ability to track markets, you can also consider splitting this lump sum amount into smaller sums and wait for steep corrections to deploy it.
	If you have SIPs running: Continue to run these and do not stop SIPs because markets look to be sliding. With the lack of any significant correction in the past three years, any market decline will help bring down costs. You can add to your equity funds outside of the SIPs on steep market corrections, if you have small surpluses.


When you’re deploying the additional amounts, know that there is no necessity to add new funds each time. When markets correct, make additional investments in your existing equity funds.

	If your portfolio mid-cap allocation is already at 25% or thereabouts, refrain from stepping up investing here even if mid-caps are correcting more sharply. Mid-caps and small-caps have had a much longer and higher run than large-caps. Earnings recovery and valuations, at this time, are more attractive in larger companies than the smaller ones. Mid-caps need the steeper correction to bring them back in line. Hence, to try and average mid-caps at this point may be a slippery slope.
	Add to holdings in large-cap and diversified funds, especially if your mid-cap exposure is on the higher side. This will help reduce overall portfolio volatility and bring in diversification. You can also add to balanced funds, especially if you don’t have a debt component to your portfolio.
	While you add equities, don’t lose sight of the fact that the increasing yields have made accrual opportunities more attractive. When you are adding equities, do not ignore asset allocation with some debt funds in the income accrual space.


FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis for investment decisions. To know how to read our weekly fund reviews, please click here.

Other articles you may like
	FundsIndia Recommends: Invesco India Growth
	FundsIndia Views: Recapitalisation – Impact beyond the banking sector
	The impact of Budget 2018 on your investments
	Auto time the equity market with FundsIndia SmartSIP





			
			Post Views: 
			1,492
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    debt markets. equity market. Investments. Mutual funds. Personal Finance. SIP. stock markets
                

			

            	


	
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


