

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Views: SEBI’s new categories – what’s changing in your equity funds

            		May 9, 2018 . Vidya Bala
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Research Desk . Views
                    
		
            	

            	A month ago, we wrote about what SEBI’s scheme re-categorization means for you. Now, with a good chunk of the changes announced by AMCs, we’d like to get into what the changes broadly are with examples and let you know what you should watch out for and what you can ignore.

This week, we’ll discuss about equity and hybrid balanced categories and next week we’ll look at debt and debt-oriented hybrid categories.

How they realign

When fund houses mapped their current funds to the new categories, one of the following happened:

	A fund could fit into a category neatly without calling for any change. In these cases, just a category change and sometimes a name change was required. In some cases there was no change at all. For example, a Franklin India Bluechip fitting into the largecap category or ICICI Pru Value Discovery being classified as a value fund required no change. There was no exit period since no fundamental attributes changed. As of the first week of May, about 50% of the over 400 funds (compiled from addendums) in equity and hybrid categories came under this.
	A fund may have been given an exit period by the AMC due to changes in its attributes. This could have been done for two reasons: one, there would have been a small change in the scheme information document (SID) although nothing in terms of strategy changes. For example, Kotak Balance will remain a balanced fund (called aggressive hybrid). Since the new category allows it to go up to 80% into equities a change in its SID was warranted. This required providing an exit period. Two, there could have been a change in strategy itself. For example, Aditya Birla Sun life Top 100 becoming a focused fund is change in strategy although not very significant. On the other hand, SBI Magnum Equity shifting into a thematic fund is significant. Overall, about 50% of the about 400 funds had/have an exit period. Of this, just 12 founds saw a merger. Of the funds with an exit period, a good 20% or 1 in 5 funds, saw a ‘significant change’ that requires watch. 


What is a significant change

We have discussed in our earlier article that a change, in our opinion, is significant only in the following cases for equity funds:

	The fund must meaningfully increase or decrease large/midcap/small-cap stocks in order to comply with the norms laid out in the new category
	The fund must change its asset allocation (in equity/debt/gold) or bring in derivatives
	The fund must reduce the number of stocks it holds (focused)
	The fund must change its strategy in terms of picking stocks (example: contra, value, dividend yield) or in terms of concentrated sector exposures
	The fund becomes a thematic fund


Since all the above can change the risk-return equation of a fund, we consider them significant.

Now, let’s get into the various categories into which funds were classified earlier to see where changes are significant. The categories we mention here as sub-heads are all ‘old categories’ thus far used by FundsIndia based on earlier market-cap segments. We will be transitioning to the new categories once all the changes are done.

Large-cap funds

SEBI requires this category of funds to hold at least 80% in large-cap stocks. Large-cap is defined as the first 100 stocks in terms of market cap.

A little over 40% of the large-cap funds for which we have data have remained in the large-cap category. A few have categorized themselves as focused (which they already were) and the rest in large & midcap/multi-cap/contra.

The ones that have moved to different categories have lesser restrictions in their investment universe. For example, Invesco India Growth and ICICI Pru Top 100 are moving to the large and mid-cap segment. That means they will have more leeway to invest in midcaps. Hence, while risk may go up a bit, their return potential too can be expected to increase, subject to good performance. On the other hand, a Reliance large cap (earlier called Reliance Top 200) which had an average 70% in large caps 9as defined by SEBI) will have to now hold at least 80% in large-cap stocks. Thus, the new norm becomes marginally more restrictive for the fund.

The table below summarises our take on this:


	Old Category	New Category to which they shifted	Return Impact	Risk Impact
	Large cap	Large cap	Neutral or negative	Neutral or lower risk
	Other equity categories	Neutral or positive	Neutral or marginally higher risk
	*risk and return impacts are our estimates and are yet to transpire




What we will watch for: Our task, where funds remain in the large-cap category, would be to see if return potential is diminished. This is because of the definition of large-cap by SEBI. Currently the 100th stock is a little over Rs 29,000 crore in marketcap. Earlier funds defined even a stock with Rs 25,000 marketcap as largecap. By way of SEBI’s definition, the universe of investible stocks has shrunk.  We will watch to see whether fund managers are able to keep up performance by active stock/sector exposure compared with benchmark, or by using the small leeway they have to invest in stocks other than large-cap stocks.

Diversified/multi-cap funds

About a third of the funds have been categorized as large & midcap or multi-cap. The others have gone into value/contra/dividend yield. In our opinion, there is not much distinction between the two. Having said that, large and midcap as a category is required to hold at least 35% in large caps and at least 35% in midcaps. No such restriction exists with multi-caps. A few funds such as Franklin India Flexicap or DSP BR Equity Opportunities though, will start holding higher midcaps as a result. Others such as SBI Magnum Multiplier would not need any such tweak.


	Old Category	New Category to which they shifted	Return Impact	Risk Impact
	Diversified/multi-cap	Large & midcap or multi-cap	Neutral or positive	Neutral or marginally higher risk
	Other equity categories	Neutral	Neutral or marginally higher risk
	*risk and return impacts are our estimates and are yet to transpire




What we will watch for: We will not be worried about any significant change in performance in these funds as they have a higher flexibility in stock and marketcap allocations. But comparing these funds with new peers entering the category is a different challenge to tackle. We will discuss this later. What we will caution you about is to avoid comparing peers in the new large & midcap, value, or multi-cap category for some time. These peers come from different market-cap segments and have different historical performance which is not comparable. 

Midcap funds

SEBI requires a mid-cap fund to hold at least 65% of its assets in mid-cap stocks. Midcap is defined as the 101st to 250th stock in terms of marketcap.  A little over 40% of the midcap funds in our compilation have remained midcaps. The others have moved to large and midcap/multicap/smallcap/focused and so on. The prominent ones to move to large and mid-cap category would be Mirae Asset Emerging Bluechip, Principal Emerging Bluechip and Canara Robeco Emerging Equities. Invesco India Mid N Small Cap fund would become a multi-cap fund, which means it need not do anything but can also do anything!


	Old Category	New Category to which they shifted	Return Impact	Risk Impact
	Midcap	Midcap	Neutral or negative	Neutral or lower risk
	Other equity categories	Neutral	Neutral or marginally lower risk
	*risk and return impacts are our estimates and are yet to transpire




What we will watch for: In this category, again, we are not too worried about funds that shifted to large & midcap or multicap. For example, the Mirae fund always had about 35% in large-cap and will require only that level of holding to qualify into large & midcap. We will however be concerned about two issues: first, the restriction in terms of market-cap for midcap funds, as defined by SEBI. The 101st to 250th stock in terms of market capitalization is considered as midcap.

The universe of 101st to 250th stock means, at present, stocks between Rs 8,500 crore and Rs 29,000 crore would only be midcap stocks. However, earlier even a Rs 5,000 crore stock would have qualified as midcap in most funds’ portfolios. We hope funds make use of the leeway provided (at least 65% should be in midcap stocks and rest can be invested anywhere) to invest in small-cap stocks too to maintain performance. We will hence keenly watch for performance on this front.

Our second concern would be about the present midcap funds moving into large & midcap or multicap and dominating this category because of aggressive past performance. For example, a star midcap performer like Mirae Asset Emerging Bluechip would be placed in the same bucket as DSP BR Equity Opportunities (which has a predominant large-cap history) and Aditya Birla Sunlife Advantage.

What hasn’t changed much

The following categories have not seen too many funds shift out. Even if they did, the changes are not significant.


	Old Category	New Category to which they shifted	Return Impact	Risk Impact
	Arbitrage funds	Arbitrage funds	Neutral	Neutral
	Index funds	Index funds	Neutral	Neutral
	International funds	FoF - overseas or Sector/ thematic fund	Neutral	Neutral
	Sector funds/Thematic funds	Sector funds/Thematic funds	Neutral	Neutral
	Hybrid balanced funds	Aggressive hybrid equity/ balanced advantage/ multi asset allocation/ dynamic asset allocation/ solution-oriented funds/ FoF domestic	Neutral	Neutral to marginally higher risk
	Small-cap funds	Small cap or midcap	Neutral 	Neutral or lower risk
	Equity savings funds	Equity savings funds	Neutral	Neutral
	ELSS	ELSS	Neutral	Neutral
	*risk and return impacts are our estimates and are yet to transpire




What you should do

	If funds that you hold fall under the old large-cap or mid-cap category and continue to stay there, you can check with your advisor a year from now on how these funds are faring.
	If you hold large-cap or midcap funds which have shifted categories now, check whether your allocation needs to be changed to maintain your originally intended allocation. However, it will take us at least 1 quarter to assess how the fund portfolio has changed to adapt to its new category. Hence, get this review after September 2018. One tip here: if you have a large-cap fund that has shifted to other categories, do not consider upping large-cap exposure. It is perfectly fine to be holding a large & midcap or a multicap fund so long as their large-cap holding is higher than their midcap holding.
	If you held a diversified/multi-cap fund, you don’t need to act as there is unlikely to be any significant impact.
	You can continue your SIPs and investments in your funds unless you hear from research & advisory on why you should move out of a fund to another.


What you need to avoid

As mentioned earlier, avoid doing peer comparison for at least 3-4 quarters, specifically in segments where funds have come from different categories. Large & midcap and multicap categories are key categories where the return differential between funds will be wide given their historical portfolio and marketcap orientations.

For example, comparing a large & midcap fund that moved from midcap with another large & midcap fund that moved from either large or diversified would clearly put the erstwhile midcap fund in an advantageous position. Such anomalies are best avoided.

We will be normalizing past performance of funds that have shifted from different categories to ensure we do not compare a high past performer from an aggressive category with a regular performer from a sedate category. As always, rest assured that FundsIndia’s research will keep watch over the strategy changes and the consequent performance change, if any. We have always gone beyond merely looking at returns into looking at portfolio quality and strategy. We will continue to do so with heightened vigilance now.

With LTCG tax also introduced, we would want to avoid unnecessary churn to your portfolio. If an exit or change is recommended, we will do it after giving the fund manager a chance to rejig, settle and perform. Please note that this SEBI categorization is easier on paper but requires a lot of work from the fund house.

FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis for investment decisions. To know how to read our weekly fund reviews, please click here.

Other articles you may like
	Don't make resolutions; make habits
	Introducing – The FundsIndia Advisory Review Board
	FundsIndia Recommends: Motilal Oswal Long Term Equity
	First quarter earnings show broad-based growth





			
			Post Views: 
			1,570
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Advisory. equity funds. Investments. Mutual funds. Personal Finance
                

			

            	


	
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


