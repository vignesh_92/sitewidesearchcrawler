
 

 

 

 

. 

 

. 

 

 

 

 

 

 

 

 

Rating   BUY 
CMP (Rs.)   415 

Target (Rs.)   468 
Potential Upside    13% 

Duration   Long Term 

Face Value (Rs.)   10 

52 week H/L (Rs.)   463/279 

Adj. all time High (Rs.)   463 

Decline from 52WH (%)   10.4 

Rise from 52WL (%)   48.7 

Beta   0.3 

Mkt. Cap (Rs.Cr)   201,569 

 

Market Data 

 

September 18, 2017 

 

    BSE Code: 530965  NSE Code: IOC    Reuters Code: IOC.NS  Bloomberg Code: IOCL:IN 

Fiscal Year Ended 

For private circulation only 

Y/E FY16 FY17 FY18E FY19E 

Revenue (Rs.Cr)  347,176  359,873 384,552 399,017 

Adj. profit 
(Rs.Cr) 

 9,878   19,106   19,275   20,346  

Adj. EPS (Rs.)  41.7   40.3   40.7   42.9  

P/E (x) 10.0 10.3 10.2 9.7 

P/BV (x) 1.1 2.0 1.8 1.7 

ROE (%) 12.7 20.3 18.6 18.3 

  

Paradip refinery to drive better profitability 
 

Indian Oil Corporation Ltd. (IOCL) has a strong presence in refining, marketing, 

pipelines networks and petrochemicals. It is the largest oil marketing company 

in India with a ~43% market share. Further, it has the largest network of retail 

outlets across the country (more than 26,000 outlets). IOCL also has minor 

interest in the upstream segment. 
 

Investment Rationale 

  Largest operator of crude and product pipelines: IOCL has the largest 

network of cross‐country pipelines in India with a total length of 12,848 

kilometers (kms) and capacity of 103.19 million metric tonnes per annum 

(mmtpa). Of its total pipeline infrastructure, 5,217 kms is used to transport 

crude oil with a capacity of 48.6 mmtpa while 7,491 kms is a product pipeline 

with a capacity of 45.1 mmtpa. 
 

  Most extensive marketing network: The company has an extensive 

marketing network with a market share of 42.9% as of FY17. It markets various 

brands of petrol, diesel, aviation fuel, lubricants and LPG. It has 26,212 retail 

outlets across India and reaches 114.4mn households with its cooking gas 

brand, Indane. Further, IOCL continued to maintain its dominant position in 

the Indian downstream sector with total petroleum product sales of 74.1mn 

tonnes in FY17. 
 

  Core GRM improving steadily: IOCL’s gross refining margin (GRM) declined 

and stood at USD4.3/bbl (USD10.0/bbl in Q1FY17 and USD8.9/bbl in Q4FY17) 

mainly due to a large inventory loss of USD3.3/bbl in Q1FY18. However, core 

GRM (excluding inventory gain or loss) improved and stood at USD7.6/bbl 

(premium of $1.2/bbl to Singapore Complex GRM) as compared to USD3.6/bbl 

in Q1FY17 and USD6.9/bbl in Q4FY17. 
 

  Paradip refinery to drive earnings in FY19E: Paradip refinery started 

operating at 100+% rate since May 2017. The Paradip refinery (15 MMTPA), on 

full capacity utilization, can consistently generate GRMs which will be at a 

premium to Singapore benchmarks given its high complexity adding to IOCL’s 

refining margins. Hence, we expect Paradip refinery to drive the company’s 

profitability over FY17-19E and project EBITDA to grow at a CAGR of 8%. 
 

Outlook and Valuation:  

Within state oil marketing companies (OMCs), IOCL is our top pick given its 

diversified business model, further ramp-up of Paradeep refinery and potential 

upside in marketing segment. Moreover, IOCL also offers an attractive 

dividend yield of ~4.6%. Hence, we continue to maintain BUY rating to the 

stock with a target price (TP) of Rs. 468 using sum of the part (SOTP) valuation 

methodology wherein we value its standalone business at Rs. 427 (P/E of 10.0x 

for FY19E) and investments at Rs. 41. 

 

 

Shareholding Pattern 

 

Jun-17 Mar-17 Chg. 

 Promoters 57.3 57.3 - 

FII’s 6.5 5.4 1.1 

MFs/Insti 10.8 11.7 (0.9) 

Public 25.4 25.6 (0.2) 

Others - - - 

 

 
Source: Company, In-house research 

0

100

200

300

400

500

Se
p

-1
6

O
ct

-1
6

N
o

v-
1

6

D
ec

-1
6

Ja
n

-1
7

Fe
b

-1
7

M
ar

-1
7

A
p

r-
1

7

M
ay

-1
7

Ju
n

-1
7

Ju
l-

1
7

A
u

g-
1

7

Se
p

-1
7

IOC Sensex (rebased)

One year Price Chart 

 

 

Volume No. I Issue No. 137 Indian Oil Corporation Ltd. 

. 



 

 

 

 

 
 

Indian Oil Corporation Limited: Business overview 
 

Indian Oil Corporation Ltd. (IOCL) was formed in 1964, following the merger of Indian 

Refineries with Indian Oil Company. Presently, it is the largest oil marketing company in India 

with a ~43% market share. It also has the largest network of retail outlets across the country 

(more than 26,000 outlets). Further, it has the largest refining capacity in the country at 80.7 

million metric tonnes per annum (mmtpa). It has commissioned its 15.0 mmtpa refinery at 

Paradip which is the most complex PSU refinery. Moreover, IOCL has a large presence in 

pipelines and chemicals. Stable earnings from pipelines cushion it from the volatility of the 

refining and marketing segments. Besides, IOCL enjoys a first mover advantage and strong 

presence in the high entry barrier rural areas (~26%) which will enable it to ride the robust 

rural demand growth. 
 

 

Refineries controlled 
 

Refineries Installed Capacities (mmtpa) 

Koyali 13.7 

Panipat 15.0 

Mathura 8.0 

Barauni 6.0 

Haldia 7.5 

Paradip 15.0 

Bongaigaon 2.4 

Guwahati 1.0 

Digboi 0.7 

Standalone IOCL 69.2 

CPCL, Chennai 10.5 

CPCL, Narimanam 1.0 

Subsidiaries 11.5 

Group Total 80.7 
 

Source: Company, In-house research 

 

 

Pipeline network 
 

Operating Pipelines Length (km) Capacities (mmtpa) 

Crude Oil 5,217 48.60 

Product 7,491 45.09 

Gas 140 9.50 

Total 12,848 - 
 

Source: Company, In-house research 
 

 

IOCL also forayed into the Exploration & Production segment by acquiring participating 

interests in several domestic as well as overseas blocks. The company’s present domestic 

Exploration & Production portfolio comprises eight domestic blocks (including two Coal Bed 

Methane blocks) and 9 overseas blocks, with participating interest ranging from 3.5% to 50%. 

Out of the 17 blocks, 5 are under production (all overseas), 4 are under development (1 

overseas & 3 domestic), 3 are under appraisal (all domestic), 3 are under discovery (2 

overseas & 1 domestic) and 2 are under exploration phase (1 overseas & 1 domestic). The 

overseas blocks are located in 8 countries, namely, Canada, Gabon, Libya, Nigeria, Russia, 

USA, Venezuela and Yemen. 

 

For private circulation only 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 



 

 

 

 

  
Core GRM improving steadily 
 

IOCL’s gross refining margin (GRM) declined and stood at USD4.3/bbl (USD10.0/bbl in Q1FY17 

and USD8.9/bbl in Q4FY17) mainly due to a large inventory loss of USD3.3/bbl in Q1FY18. 

However, core GRM (excluding inventory gain or loss) improved and stood at USD7.6/bbl 

(premium of $1.2/bbl to Singapore Complex GRM) as compared to USD3.6/bbl in Q1FY17 and 

USD6.9/bbl in Q4FY17. 
 

Paradip refinery to drive earnings in FY19E 
 

Paradip refinery operated at 88% utilization during Q1FY18 as compared to 82% in Q4FY17 

and 32% in Q1FY17. Notably, this number is skewed by low operating rate in April 2017 and 

the refinery operated at 100+% rate during May and June 2017. The Paradip refinery (15 

MMTPA), on full capacity utilization, will consistently generate GRMs which will be at a 

premium to Singapore benchmarks given its high complexity adding to IOCL’s refining margins. 

Hence, we expect Paradip refinery to generate core GRM of USD10-12/bbl from FY19E. 
 

Inventory loss shadows strong operational performance 

IOCL’s net revenue increased by 22.5% YoY (in line with expectation) due to higher refining 

and sales volume. Crude throughput increased by 8.8% YoY to 17.5 million metric tonnes 

(mmt) led by higher utilization of Paradip refinery (utilization at 88% from 32% in Q1FY17) 

coupled with 5.3% YoY increase in sales volume to 22.5mmt. EBITDA (reported) declined by 

41.5% YoY on account of inventory loss of Rs4,040cr (refining inventory loss of Rs2,806 crore 

and product inventory loss of Rs1,234 crore) as against gains of Rs7,500cr in Q1FY17. 

However, the company also had a one-off gain on account of settlement of Rs2,810cr liability 

for entry tax in Haryana. After adjusting for one-off gain, EBITDA declined by 62.1% YoY. Net 

profit (reported) declined by 45% YoY. Going forward, we expect IOCL’s adjusted net profit to 

grow at a CAGR of 3% over FY17-19E mainly due to lower or nil inventory gain on the back of 

stable crude oil prices. 
 

Outlook and Valuation 

Within state oil marketing companies (OMCs), IOCL is our top pick given its diversified business 

model, further ramp-up of Paradeep refinery and potential upside in marketing segment. 

Besides, the successful commissioning of 15 MMTPA Paradip refinery make IOCL a pivotal 

player in complex refining. The full benefit (in terms of better refining margins) will start 

flowing from FY19E as it will start processing heavy crude oil. Moreover, IOCL also offers an 

attractive dividend yield of ~4.5%. Hence, we continue to maintain BUY rating to the stock 

with a target price (TP) of Rs468 using sum of the part (SOTP) valuation methodology wherein 

we value its standalone business at Rs427 (P/E of 10.0x for FY19E) and investments at Rs41. 
 

 

Sum of the parts (SOTP) valuation 

  Methodology Multiple Year 
Value of 

IOC's Stake 
Value Per 

Share (Rs.) 

IOCL EPS 10.0x FY19E - 427 

CPCL Market Price - - 3,404 7 

Investments           

- Lanka IOC Market Price - - 504 1 

- Petronet LNG Market Price - - 2,172 5 

- ONGC Market Price - - 15,731 33 

- GAIL Market Price - - 1,636 3 

- Oil India Market Price - - 1,137 2 

20% holding discount to investments and CPCL -11 

  Target Price 468 

 
 

 

 

 

 

 

 

 

 

 

 

For private circulation only 

 

 

 

 



 

 

 

 

  

  

 

 

 

 

Revenue to grow at 5% CAGR over FY17-19E 

 
Source: Company, In-house research 
 

EBITDA to grow at 8% CAGR over FY17-19E 

 
Source: Company, In-house research 
 
 

PAT to grow at 3% CAGR (albeit on higher base) over FY17-19E 

 
Source: Company, In-house research 
 

Return ratios to improve led by strong free cash flow generation 

 
Source: Company, In-house research 
 

Key Risks 

 

• Roll-back of de-regulation due to sharp rally in crude price margins. 

• Any potential imposition of subsidies is likely to act as a drag on earnings as we 
estimate nil subsidy contribution by IOCL over FY17-19E. 

• Regulatory change in the form of reduction in duty protection will lower refining 
margins. 

398477 447096 473210 437524 347176

359873

384552 399017

21
12

6

-8

-21

4 7 4

-40

-20

0

20

40

0

200000

400000

600000

FY12 FY13 FY14 FY15 FY16 FY17 FY18E FY19E

Revenue (Rs Cr.) Growth YoY (%)

18721 13769 15606 10145
21049 31781

35035 36921

45

-26
13

-35

107
51

10 5
-100

0

100

200

0

20000

40000

FY12 FY13 FY14 FY15 FY16 FY17 FY18E FY19E

EBITDA (Rs Cr.) Growth YoY (%)

11662 5005 5272 3605 9878 19106
19275 20346

57

-57

5
-32

174

93

1 6
-100

0

100

200

0

10000

20000

30000

FY12 FY13 FY14 FY15 FY16 FY17 FY18E FY19E

Adj Net Profit (Rs Cr.) Growth YoY (%)

8.3
5.4

12.7

20.3 18.6 18.3
9.0 7.1

14.6
21.2 19.7 19.4

0.0

10.0

20.0

30.0

FY14 FY15 FY16 FY17 FY18E FY19E

RoE (%) RoCE (%)

For private circulation only 

 

 

 

 



 

 

 

 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Y/E  (Rs. Cr)  FY16  FY17 FY18E FY19E 

Pre-tax profit  16,827   26,321   26,553   28,030  

Depreciation  4,819   6,223   7,792   8,329  

Chg in Working Capital  2,254   (6,061)  (538)  81  

Others  2,108   7,994   690   563  

Tax paid  (3,063)  (6,726)  (7,278)  (7,683) 

Cash flow from operating 

activities 
22,944  27,751   27,219   29,319  

Capital expenditure (13,961)  (13,960)  (15,738)  (15,738) 

Chg in investments  (9)  142   (1,100)  (2,200) 

Other investing cashflow  1,553   (868)  3,197   3,336  

Cash flow from investing 

activities 
(12,417)  (14,686)  (13,641)  (14,602) 

Equity raised/(repaid)  -     -     -     -    

Debt raised/(repaid)  (3,438)  1,862   2,000   2,000  

Dividend paid  (3,450)  (12,707)  (11,721)  (12,179) 

Other financing activities  (3,479)  (2,429)  (3,887)  (3,899) 

Cash flow from financing 

activities 
(10,367)  (13,275)  (13,608)  (14,078) 

Net chg in cash  160   (209)  (30)  639  

 

Cash Flow Statement (Standalone) 

 

Key Ratios (Standalone) 

 
 Y/E    FY16  FY17 FY18E FY19E 

Valuation(x)         

P/E 10.0 10.3 10.2 9.7 

EV/EBITDA 6.7 7.8 7.1 6.8 

EV/Net Sales 0.4 0.7 0.7 0.6 

P/B 1.1 2.0 1.8 1.7 

Per share data (Rs.)         

EPS  41.7   40.3   40.7   42.9  

DPS  12.1   22.3   20.5   21.3  

Growth (%)         

Net Sales -20.6% 3.7% 6.9% 3.8% 

EBITDA 107.5% 51.0% 10.2% 5.4% 

Net profit 174.0% 93.4% 0.9% 5.6% 

Margin (%)         

EBITDA  6.1   8.8   9.1   9.3  

EBIT 5.3% 8.3% 7.9% 8.0% 

NPM 2.8% 5.3% 5.0% 5.1% 

Return Ratios (%)         

RoE 12.7 20.3 18.6 18.3 

RoCE 14.6 21.2 19.7 19.4 

Turnover Ratios (x)         

Sales/Total Assets 1.6 1.5 1.4 1.4 

Sales/Working 

Capital 
5.3 7.4 7.0 7.3 

 

 

 

Balance Sheet (Standalone) 

 

 

Profit & Loss Account (Consolidated) 

Y/E (Rs. Cr)  FY16  FY17 FY18E FY19E 

Paid up capital  2,370   4,739   4,739   4,739  

Reserves and Surplus  85,765   94,989   102,543   110,710  

Net worth  88,134   99,729   107,282   115,449  

Minority interest - - - - 

Total Debt  42,483   50,385   52,385   54,385  

Other non-current 

liabilities 
 18,157   20,964   20,964   20,964  

Total Liabilities 148,775  171,077   180,630   190,798  

Net fixed assets  91,347   107,879   115,825   123,234  

Capital WIP  21,025   10,738   10,738   10,738  

Goodwill - - - - 

Investments  37,181   47,305   48,405   50,605  

Net Current Assets (17,824)  (9,481)  (9,978)  (10,010) 

Other non-current 

assets 
 17,045   14,636   15,640   16,231  

Total Assets 148,775  171,077   180,630   190,798  

 

 

Profit & Loss Account (Standalone) 

 

 

Profit & Loss Account (Standalone) 

 

 

For private circulation only 

Y/E (Rs. Cr)  FY16  FY17 FY18E FY19E 

Total operating Income 347,176 359,873 384,552 399,017 

Raw Material cost 289,374 283,576 300,498 309,172 

Employee cost 7,114 9,658 10,628 11,466 

Other operating expenses  29,640   34,858   38,391   41,458  

EBITDA  21,049     31,781  35,035   36,921  

Depreciation  4,819   6,223   7,792   8,329  

EBIT  16,230   25,558   27,243   28,593  

Interest Cost  3,090   3,445   3,887   3,899  

Other income  2,322   4,209   3,197   3,336  

Profit before tax  15,462   26,321   26,553   28,030  

Tax  5,584   7,215   7,278   7,683  

PAT  11,242   19,106   19,275   20,346  

Minority Interest - - - - 

P/L from Associates - - - - 

Adjusted PAT  9,878   19,106   19,275   20,346  

E/o income / (Expense)  1,364  -   -     -    

Reported PAT  11,242   19,106   19,275   20,346  

 
 



 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Rating Criteria 
    Large Cap. Return Mid/Small Cap. Return 

Buy More than equal to 10% Buy More than equal to 15% 

Hold Upside or downside is less than 10% Accumulate* Upside between 10% & 15% 

Reduce Less than equal to -10% Hold Between 0% & 10% 

  Reduce/sell Less than 0% 

* To satisfy regulatory requirements, we attribute ‘Accumulate’ as Buy and ‘Reduce’ as Sell. 

* IOCL is a large-cap company. 

  

Disclaimer:  

 

The SEBI registration number is INH200000394. 

The analyst for this report certifies that all the views expressed in this report accurately reflect his / her personal views about the subject 

company or companies, and its / their securities. No part of his / her compensation was / is / will be, directly / indirectly related to specific 

recommendations or views expressed in this report. 

This material is for the personal information of the authorized recipient, and no action is solicited on the basis of this. I t is not to be 

construed as an offer to sell, or the solicitation of an offer to buy any security, in any jurisdiction, where such an offer or solicitation would 

be illegal. 

We have reviewed the report, and in so far as it includes current or historical information, it is believed to be reliable, though its accuracy or 

completeness cannot be guaranteed. Neither Wealth India Financial Services Pvt. Ltd., nor any person connected with it, accepts any 

liability arising from the use of this document. The recipients of this material should rely on their own investigations and take their own 

professional advice. Price and value of the investments referred to in this material may go up or down. Past performance is not a guide for 

future performance. 

We and our affiliates, officers, directors, and employees worldwide: 

1. Do not have any financial interest in the subject company / companies in this report; 
2. Do not have any actual / beneficial ownership of one per cent or more in the company / companies mentioned in this document, or 

in its securities at the end of the month immediately preceding the date of publication of the research report, or the date of public 
appearance; 

3. Do not have any other material conflict of interest at the time of publication of the research report, or at the  time of public 
appearance; 

4. Have not received any compensation from the subject company / companies in the past 12 months; 
5. Have not managed or co-managed the public offering of securities for the subject company / companies in the past 12 months; 
6. Have not received any compensation for investment banking, or merchant banking, or brokerage services from the subject 

company / companies in the past 12 months; 
7. Have not served as an officer, director, or employee of the subject company; 
8. Have not been engaged in market making activity for the subject company; 

This document is not for public distribution. It has been furnished to you solely for your information, and must not be reproduced or 

redistributed to any other person. 
 

 
 

Funds India 
Uttam Building, Third Floor| 
No. 38 & 39| Whites Road| 
Royapettah|Chennai – 600014| 
T: +91 7667 166 166  
Email: contact@fundsindia.com 
 
 

 

 

 Contact Us: 

For private circulation only 

mailto:contact@fundsindia.com


 

 

 

 

 

Dion’s Disclosure and Disclaimer 
 
I, Kaushal Patel, employee of Dion Global Solutions Limited (Dion) is engaged in preparation of this report and hereby certify that all 
the views expressed in this research report (report) reflect my personal views about any or all of the subject issuer or securities.  

Disclaimer 

This report has been prepared by Dion and the report & its contents are the exclusive property of the Dion and the client cannot 

tamper with the report or its contents in any manner and the said report, shall in no case, be further distributed to any third party 

for commercial use, with or without consideration.  

Recipient shall not further distribute the report to a third party for a commercial consideration as this report is being furnished to 
the recipient solely for the purpose of information.  
 
Dion has taken steps to ensure that facts in this report are based on reliable information but cannot testify, nor make any 
representation or warranty, express or implied, to the accuracy, contents or data contained within this report. It is hereby 
confirmed that wherever Dion has employed a rating system in this report, the rating system has been clearly defined including the 
time horizon and benchmarks on which the rating is based.   
 
Descriptions of any company or companies or their securities mentioned herein are not intended to be complete and this report is 
not, and should not be construed as an offer or solicitation of an offer, to buy or sell any securities or other financial instruments. 
Dion has not taken any steps to ensure that the securities referred to in this report are suitable for any particular investor. This 
report is not to be relied upon in substitution for the exercise of independent judgment. Opinions or estimates expressed are 
current opinions as of the original publication date appearing on this report and the information, including the opinions and 
estimates contained herein, are subject to change without notice. Dion is under no duty to update this report from time to time.  
 
Dion or its associates including employees engaged in preparation of this report and its directors do not take any responsibility, 
financial or otherwise, of the losses or the damages sustained due to the investments made or any action taken on basis of this 
report, including but not restricted to, fluctuation in the prices of securities, changes in the currency rates, diminution in the NAVs, 
reduction in the dividend or income, etc.  
 
The investments or services contained or referred to in this report may not be suitable for all equally and it is recommended that an 
independent investment advisor be consulted. In addition, nothing in this report constitutes investment, legal, accounting or tax 
advice or a representation that any investment or strategy is suitable or appropriate to individual circumstances or otherwise 
constitutes a personal recommendation of Dion. 
 
REGULATORY DISCLOSURES: 

Dion is engaged in the business of developing software solutions for the global financial services industry across the entire 

transaction lifecycle and inter-alia provides research and information services essential for business intelligence to global companies 

and financial institutions. Dion is listed on BSE Limited (BSE) and is also registered under the SEBI (Research Analyst) Regulations, 

2014 (SEBI Regulations) as a Research Analyst vide Registration No. INH100002771. Dion’s activities were neither suspended nor has 

it defaulted with requirements under the Listing Agreement and / or SEBI (Listing Obligations and Disclosure Requirements) 

Regulations, 2015 with the BSE in the last five years. Dion has not been debarred from doing business by BSE / SEBI or any other 

authority.  

 

In the context of the SEBI Regulations, we affirm that we are a SEBI registered Research Analyst and in the course of our business, 

we issue research reports /research analysis etc that are prepared by our Research Analysts. We also affirm and undertake that no 

disciplinary action has been taken against us or our Analysts in connection with our business activities.   

In compliance with the above mentioned SEBI Regulations, the following additional disclosures are also provided which may be 

considered by the reader before making an investment decision:  

For private circulation only 



 

 

 

 

 

 

 

1.  Disclosures regarding Ownership  

Dion confirms that: 

(i) Dion/its associates have no financial interest or any other material conflict in relation to the subject company (ies) 

covered herein at the time of publication of this report. 

   

(ii) It/its associates have no actual / beneficial ownership of 1% or more securities of the subject company (ies) covered 

herein at the end of the month immediately preceding the date of publication of this report. 

   

Further, the Research Analyst confirms that: 

(i) He, his associates and his relatives have no financial interest in the subject company (ies) covered herein, and they 

have no other material conflict in the subject company at the time of publication of this report. 

   

(ii)   he, his associates and his relatives have no actual/beneficial ownership of 1% or more securities of the subject 

company (ies) covered herein at the end of the month immediately preceding the date of publication of this report.  

   

2.  Disclosures regarding Compensation:   

During the past 12 months, Dion or its Associates: 

(a) Have not managed or co-managed public offering of securities for the subject company (b) Have not received any compensation 

for investment banking or merchant banking or brokerage services from the subject company (c) Have not received any 

compensation for products or services other than investment banking or merchant banking or brokerage services from the subject. 

(d) Have not received any compensation or other benefits from the subject company or third party in connection with this report  

 

3.  Disclosure regarding the Research Analyst’s connection with the subject company: 

It is affirmed that I, Kaushal Patel employed as Research Analyst by Dion and engaged in the preparation of this report have not 

served as an officer, director or employee of the subject company 

 

4.  Disclosure regarding Market Making activity: 

Neither Dion /its Research Analysts have engaged in market making activities for the subject company. 

 
Copyright in this report vests exclusively with Dion. 

 

 

 

 

 

 

 

 

 

 

For private circulation only 


