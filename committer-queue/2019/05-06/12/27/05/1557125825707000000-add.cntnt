
 

 

 

 

. 

 

. 

 

 

 

 

 

 

 

 

Transformation in motion… 
 

L&T Finance Holdings Ltd. (LTFH) is a financial holding company offering a diverse range 

of financial products and services across retail, corporate, housing and infrastructure 

finance sectors. It also has presence in asset management and investment management 

services. 
 

 

 

 

Investment Rationale 
 

 Strong parentage – a key moat: LTFH derives significant competitive 

advantage over peers because of its parentage. It not only enjoys strong 

backing from the parent but also derives synergy from L&T’s industry 

knowledge in the infrastructure sector. Hence, L&T’s extensive domain 

experience (for its loan underwriting business) and strong brand equity (that 

helps it to raise funds) provides an edge over peers. L&T holds a controlling 

stake of 66.7% (as on Q3FY17) in LTFH. 
 

 Robust and well diversified business model: Owing to difficult macro-

environment and the challenges in large numbers of infrastructure projects 

and CV/CE segment, the company has been on a course correction path over 

the past two years. Thus, LTFH has decided to sell/run down less 

profitable/loss businesses and identified select focus segments which it 

believes offer strong long-term growth opportunities. It includes three key 

lending businesses (Rural, Housing and Wholesale), asset management and 

wealth management. 
 

 Robust improvement in earnings on the card: LTFH’s RoE is expected to 

touch 20% mark by FY20E from current level of 10.6% (FY16) mainly led by 1) 

sell/rundown of the defocused (less profitable) portfolio which includes cars, 

CE/CV, SME term loans and leases, 2) reducing the risk in wholesale/infra 

business by focusing more on operational projects which is likely to reduce 

provisioning 3) delivering industry leading growth in focus segments like micro-

finance and housing finance, 4) improving cost to income ratio with increasing 

productivity, 5) reallocating capital towards more profitable businesses and 6) 

creating value in the asset management business and increasing fee income 

from wealth management. 
 

Valuation: After undergoing significant business restructuring in FY16, LTFH 

has made substantial progress in achieving scale in its focus business verticals. 

As a result, we expect return ratios (1.9% RoA and 17.2% RoE by FY19E) to 

show a marked improvement over the next two years. We expect LTFH to 

deliver a loan growth CAGR of ~15% over FY16-19E which coupled with higher 

operating efficiency and improvement in credit cost will help in delivering 

~24% earnings CAGR for the consolidated entity. We initiate coverage on LTFH 

with a BUY rating and assign TP of Rs135 (P/ABV of 3.0x for FY19E) valuing it at 

a premium to its four-year mean (P/ABV of 2.2x for 1 year forward ABV). 

Rating   BUY 
CMP (Rs.)   121 

Target (Rs.)   135 
Potential Upside    12% 

Duration   Long Term 

Face Value (Rs.)   10.0 

52 week H/L (Rs.)   124/61 

Adj. all time High (Rs.)   124 

Decline from 52WH (%)   2.6 

Rise from 52WL (%)   96.8 

Beta   1.8 

Mkt. Cap (Rs.Cr)   21,218 

 

Market Data 

Feb. 23, 2017 

 
BSE Code: 533519  NSE Code: L&TFH Reuters Code: LTFH.NS  Bloomberg Code: LTFH:IN 

 

50

100

150

M
ar

-1
6

A
p

r-
1

6

M
ay

-1
6

Ju
n

-1
6

Ju
l-

1
6

A
u

g-
1

6

Se
p

-1
6

O
ct

-1
6

N
o

v-
1

6

D
e

c-
1

6

Ja
n

-1
7

Fe
b

-1
7

M
ar

-1
7

LTFH Sensex (rebased)

Y/E FY16 FY17E FY18E FY19E 

Net Interest 
Income (Rs. Cr) 

6,605 7,843 8,994 10,448 

Pre-Pro Profit 
(Rs. Cr) 

2,034 2,611 3,072 3,580 

Net Profit  
(Rs. Cr) 

857 1,013 1,305 1,614 

EPS 4.9 5.8 7.4 9.2 

P/E (x) 24.7 20.9 16.2 13.1 

P/BV (x) 2.9 2.5 2.3 2.2 

P/ABV (x) 4.2 3.1 2.9 2.6 

ROE (%) 10.6 11.9 14.8 17.2 

ROA (%) 1.5 1.5 1.7 1.9 
  

One-year Price Chart 

Promoters (%) 66.7 66.7 - 

Public (%) 33.3 33.3 - 

 

Fiscal Year Ended 

For private circulation only 

Shareholding Pattern 

 

Dec-16 Sep-16 Chg. 

 

 
 

Volume No. 2 Issue No. 112 

 

L&T Finance Holdings Ltd. 

. 



 

 

 

 

 
 

 

 

L&T Finance Holdings Ltd. - Company Overview 
 

L&T Finance Holdings Ltd. (LTFH) was incorporated in 2008 and is promoted by the 
construction major Larsen & Toubro (L&T). LTFH is a financial holding company offering a 
diverse range of financial products and services across retail, corporate, housing and 
infrastructure finance sectors. It also has presence in asset management and investment 
management services through its wholly owned subsidiaries. Further, the company has a 
strong parentage and a highly-qualified management team which has significant experience 
in evaluating long-gestation infrastructure projects. 
 

The company has gradually built-up several businesses/product lines through both organic 
and inorganic route over the past few years. LTFH has identified select segments to grow its 
loan book— Housing finance, Tractor, Two-wheeler and Microfinance in the retail business 
and roads, renewable energy projects in the wholesale segment. 
 

Sell down of unprofitable businesses to improve profitability 
 

LTFH has articulated its new business strategy in FY17 wherein it will exit the segments which 
are earning sub-optimal returns. Hence, in 9MFY17, the company reported 39% YoY decline 
in de-focused portfolio which includes CV (commercial vehicle), Cars, CE (commercial 
equipment), Leases and SME (small and medium enterprise) Term Loans. De-focused   
segments currently account for 5.1% (Q3FY17) of the advances. The defocused segment 
reported negative RoE of 25.3% in 9MFY17 thus dragging the overall RoE of the consolidated 
business. However, the management is looking to run down or sell the defocus businesses 
and will churn some of the wholesale assets to Infrastructure Debt Funds (IDF). We believe 
this strategy of moving away from long term SME and CV/CE financing is a step in the right 
direction as it will reduce business cyclicality and improve RoE. 
 

Focus on select products to help achieve top quartile RoE 
 

LTFH has gradually built-up several businesses/product lines through both organic and 
inorganic route over the past few years. However, the company has now identified lending, 
investment management and wealth management as the three main pillars of business 
growth. 
 

 
Source: Company, In-house research 
 

 

Further, the company has redefined its lending business strategy to focus on nine distinct 
lending products across three business verticals viz. wholesale, housing and rural. This will 
drive asset growth and help company in achieving top quartile RoE of 20% by FY20. 

 

 

 

 

 

 

LTFH is a financial holding 

company offering a diverse 

range of financial products 

and services across retail, 

corporate, housing and 

infrastructure finance sectors. 

 

 

 

 

 

 

 

 

 

 

 

 

 



 

 

 

 

  

 

 

 

                                   Reclassification of focused businesses and products 

 
 Source: Company, In-house research 
 

Realignment of rural business to drive higher profitability 
 

In a strategic move, the company has trimmed the number of lending products under rural 

business based on its analysis of competitive positioning, scalability and profitability. As a 

result, LTFH has identified microfinance, two-wheeler and tractors (farm equipment) as the 

key product segments. Further, the company has decided to de-emphasize its small and light 

commercial vehicles (SCV/LCV) and car loans businesses. Hence, the company has primarily 

shifted its focus in lending to income generating assets across rural areas. 
 

Further, to mitigate the risk in the rural segment, loan origination and collection in the 

consumer, auto finance and microfinance businesses have been migrated completely to 

mobility solutions. Under mobility solutions, loan origination is carried out through 

tablets/mobile handsets that aim to drive sales effectiveness by reducing the turnaround time 

(TAT). Collections are also carried out through mobile phones, coupled with thermal bluetooth 

printers to issue receipts. Further, mobile based rule engine has also been developed to 

improve TAT and approval rate with minimal the chance of errors. 
 

Rural Finance - Loan Composition / Product Mix 

 
Source: Company, In-house research 
 

Growth momentum to continue in microfinance business 
 

It is the most profitable business with ~30%+ of RoE for the company. Further, the loan book 

also grew at a robust pace of 141% YoY (albeit on a low base) in FY16.  Under microfinance 

business, the company offers micro loans through a joint liability group model. This business is 

currently operational in eight states of India - Tamil Nadu, Karnataka, Maharashtra, Gujarat, 

Orissa, West Bengal, Madhya Pradesh and Kerala and likely to enter in the state of Uttar 

Pradesh in FY17. Moreover, the company is ahead of the curve as it had fully implemented 

cashless disbursement for its customers even before demonetisation. Hence, the company 

had minimal impact on its business and the loan book increased 86% YoY in 9MFY17. Further, 

the company is planning to migrate to Aadhaar linked biometric technology for cashless 

disbursements. Going forward, we expect growth momentum to continue and project loan 

book to grow at a robust CAGR of 50% over FY17-19E. 

Rural  Business

•Microfinance

•Two Wheeler Finance

•Farm Equipment

Housing Business

•Home Loans

•LAP

•Real Estate Finance

Wholesale Business

•Infra Finance

•Structured Corporate 
Finance

•Supply Chain Finance

25.8 36.1
42.1 48.3

20.4
21.3 20.7

19.7
53.8 42.6

37.2 32.0

0%

50%

100%

FY16 FY17E FY18E FY19E

Microfinance Two wheeler Farm Equipment

 

 

For private circulation only 

 

 

 



 

 

 

 

 
 
  

For private circulation only 

  

 

 

Increasing market share in two-wheeler business 
 

LTFH entered the two-wheeler financing space after acquisition of Family Credit in FY13. The 
company has since reported strong growth in the segment. While the industry volume 
increased at a moderate pace of 3% YoY during FY16, the book size of LTFH grew strongly by 
24% YoY (↑18% YoY in 9MFY17), resulting in an increase in market share. This superior 
performance was driven by better penetration in existing locations as well as active efforts to 
reach new markets. The company currently ranks amongst top 5 two-wheeler financiers with 
10% market share which it aims to increase to ~15% over next 1-2 years. Hence, we expect 
loan book to grow at a strong CAGR of 25% over FY17-19E. 
 

Better monsoon to back gradual improvement in farm equipment business 
 

This business continuous to reel under stress due to weakness in rural economy post two 
consecutive draughts in FY15 and FY16. Consequently, the farm equipment business (tractor) 
reported 9% YoY decline in 9MFY17 and 12% YoY in FY16. As a result, the company’s market 
share of tractor financing declined from the peak level of 10-11% to 7% in Q3FY17. However, 
to counteract this, the company started focusing on financing pre-owned tractors and 
refinancing opportunities. Going forward, we expect gradual improvement in this business on 
the back of good monsoon this year and project 13% CAGR in loan book over FY17-19E. 
 

Overall, we believe that strong growth momentum to continue in rural business and project 
26% CAGR in loan book over FY16-19E. 
 

Trend in loan growth of rural finance 

 
Source: Company, In-house research 
 

 

Housing business - another growth engine 
 

LTFH entered the housing finance space with the acquisition of Indo-Pacific Housing Finance in 
2012. Its business got a boost when it acquired the housing portfolio of CitiFinancial in 2014. 
Further, the company has been scaling up the business by increasing its geographical spread 
(44 locations) as well as by increasing market share in the existing markets. Hence, loan book 
for retail and construction business grew at a stellar 78% and 108% YoY, respectively in FY16. 
Likewise, loan disbursements in the retail business grew by 62% YoY while that of construction 
business grew by 149% YoY. 
 

Overall, the company reported 34% YoY growth in loan book in 9MFY17 largely led by home 

loans & LAP (↑36% YoY) segment. Although the company witnessed short term negative 

impact of demonetization on the business, the management expects to regain growth 

momentum gradually over the next 1-2 quarters. We expect housing finance business to be 

one of the key growth driver for the company going forward. Hence, we estimate loan book to 

grow at a CAGR of 25% over FY16-19E. 

 

8,509
10,159

13,075

17,111
19.4

28.7 30.9

0

10

20

30

40

0

5000

10000

15000

20000

FY16 FY17E FY18E FY19E

Rural Loan Book (Rs Cr) YoY Growth (%)

 

 

 

 

 

 

 

 

 



 

 

 

 

 
  

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Housing Finance - Loan Composition / Product Mix 
 

 
Source: Company, In-house research 
 

Trend in loan growth of housing finance 

 

Source: Company, In-house research 
 

Strategic shift in product mix to strengthen wholesale finance business 
LTFH entered the wholesale finance segment in 2006 with the objective of lending to key 

infrastructure development projects with a focus in power, telecom, roads, water and oil & 

gas sectors. The company was awarded the PFI (Public Financial Institution) status and an 

IFC (Infrastructure Finance Company) status by RBI which enables it to access low cost long-

term funds and also at the same time allows it to take higher exposure while lending. 
 

The wholesale finance business comprises of infrastructure and non-infra wholesale 

financing. The company’s wholesale business witnessed pressure over the last three years 

due to slowdown in infrastructure sector. Hence, the company has tweaked its business 

strategy to de-risk its wholesale business by focusing more on relatively low-risk renewable 

energy and road projects. Further, the company intends to increase proportion of 

operational projects in the total loan assets to contain portfolio risk. As a result, the share of 

operating projects in its portfolio has increased to 61% in FY16 as compared to 21% in FY11. 

Moreover, LTFH is looking to aggressively grow its Infrastructure Debt Fund (IDF) to benefit 

from lower funding cost (as IDF is an AAA-rated entity), tax exemptions and lower risk 

weight (reduction available on the operational projects), thus freeing up its capital. Thus, we 

expect wholesale finance to deliver loan CAGR of 14% over FY16-19E (impacted by rundown 

of defocused products) driven by transmission, renewable energy and roads sectors. 
 

Wholesale Finance - Loan Composition / Product Mix 

 
Source: Company, In-house research 
 

63 65 65 65

37 35 35 35

0%

50%

100%

FY16 FY17E FY18E FY19E

Home Loans & LAP Real Estate Finance

9,790
12,189

15,237
19,046

24.5

25.0 25.0

24

25

25

26

0

10000

20000

FY16 FY17E FY18E FY19E

Housing Loan Book (Rs Cr) YoY Growth (%)

79 82 83 84

14 13 12 11
7 6 5 5

0%

50%

100%

FY16 FY17E FY18E FY19E

Infra Finance Structured Corporate Finance Supply Chain Finance

 

 

 

For private circulation only 

 

 

 

 

 

 

 

 

 

 

 

 



 

 

 

 

 
  

Trend in loan growth of wholesale finance 
 

 
Source: Company, In-house research 
 

The wholesale finance business has evolved from being a corporate lender to a specialised 

project financier and has shifted focus to operating projects to balance the overall risk of the 

portfolio. 
 

Shifting focus to operating projects from corporate lender 

 
Source: Company, In-house research 
 

The business has also emerged as a leading financier in the renewable energy and road 

sector as is visible in the chart below. 
 

Sector-wise composition - Increasing composition of renewable energy 

 
Source: Company, In-house research 
 

Investment management & wealth management business gaining strong traction 
 

The Company’s Investment Management business is carried out by L&T Investment 

Management Ltd. (LTIM), its wholly owned subsidiary. The average assets under 

management grew by 15% YoY to Rs25,945cr in FY16 mainly led by equity assets (↑21% 

YoY). This was achieved through a combination of consistent fund performance, building 

scale in existing product offerings and improving acceptance in various distribution 

channels. Hence, we expect contribution of fee income to increase from investment 

management and thus aid in RoE expansion.  
 

The Company’s Wealth Management business is carried out through L&T Capital Markets 

Ltd. (LTCM), its wholly owned subsidiary, offering services to Ultra High Net Worth (UHNI) 

and High Net Worth (HNI) individuals. It is present in 9 locations, including Dubai. The  

34,155 39,454 44,752
50,809

15.5

13.4 13.5

12

14

16

0

50000

100000

FY16 FY17E FY18E FY19E

Wholesale Loan Book (Rs Cr) YoY Growth (%)

21
35 31 37

47
6127

29 29 26
28

1848
32 37 32

22 17
5 4 2 5 3 3

0%

20%

40%

60%

80%

100%

FY11 FY12 FY13 FY14 FY15 FY16

Operational Project U/C Project Corporate Equity & Investments

12 19 21 17
27 33

23 14 16 20
21

2210 8 3 4
9

7
9 14 14 15

13 1312 11 13 13
10 814 11 13 13
6 4

21 23 21 18 14 13

0%

20%

40%

60%

80%

100%

FY11 FY12 FY13 FY14 FY15 FY16

Renewable Power Roads Real Estate Thermal power Power - Corp+T&D Telecom Others

 

 

 



 

 

 

 

 
  

 

 

 

business has been progressing well, led by a robust model built on the fundamental tenets 

of client centricity, intellectual property and execution efficiency. The average assets under 

service grew by 33% YoY to Rs9,315cr in FY16. We expect wealth management to 

contribute positively going forward to profitability as it has achieved breakeven in Q2FY17. 
 

Consolidated loan book to grow at 15% CAGR over FY16-19E 

LTFH has reported a strong 26% CAGR in consolidated loan book over FY11-16 driven by 

diversified product portfolio as well as acquisition of number of companies. In CY12, the 

company acquired Indo Pacific Housing Finance to foray into housing finance and Family 

Credit to enter into two-wheeler financing. Further, LTFH acquired CitiFinancial’s housing 

finance portfolio worth Rs700cr in FY14 to achieve scale in housing finance. Going forward, 

we expect LTFH to deliver industry leading growth in microfinance and housing verticals 

while wholesale vertical is expected to report steady growth (impacted by rundown of 

defocused portfolio) leading to 15% CAGR in consolidated loan book over FY16-19E. 
 

AUM to grow at 14.6% CAGR over FY16-19E 

 
Source: Company, In-house research 
 

Changing business mix in favour of retail 

 

Source: Company, In-house research 
 

Margins to broadly remain stable 

While margin in the rural business is likely to remain high, wholesale business margin is 

expected to remain under pressure given the increasing share of low yielding operational 

projects. Hence, we expect LTFH’s margin to remain largely stable at current level of 5.0% 

over FY16-19E. 

Margins to remain relatively stable at 5% over FY16-19E 

 
Source: Company, In-house research 

 

 

 

 

25,440
33,310

40,082

47,232
57,831 64,505

74,416
86,966

39.5
30.9

20.3

17.8
22.4

11.5 15.4
16.9

0

20

40

60

0

50000

100000

FY12 FY13 FY14 FY15 FY16 FY17E FY18E FY19E

Loan Book (Rs Cr) YoY Growth (%)

14.9 15.7 17.6 19.7
17.1 18.9 20.5 21.9

59.7 61.2 60.1
58.4

8.3 4.2 1.8

0%

50%

100%

FY16 FY17E FY18E FY19E

Rural Housing Wholesale Defocused

12.7 13.0 12.6 12.8 12.9 12.9

4.4 4.9 4.7 5.0 5.1 5.1

9.3 8.9 8.6 8.6 8.6 8.6

0.0

5.0

10.0

15.0

FY14 FY15 FY16 FY17E FY18E FY19E

Yield on Funds (%) NIM (%) Cost of Fund (%)

 

 

 

For private circulation only 



 

 

 

 

 
   

 

 

 

 

 

Cost to Income (C/I) ratio to improve significantly with improving productivity 

Under the management’s new strategy, we expect C/I ratio to decline significantly from 

39% in FY16 to 33% in FY19E. To achieve this, the company has taken various steps: (1) 

automation and digitalisation of various processes under mobility solutions, (2) reduction 

in workforce in both the branches and head-office, (3) optimization of office space and (4) 

increased focus on mobile branches (low cost). 
 

Asset quality restoration on track 

LTFH’s asset quality has been on a restoration path after suffering deterioration during 

FY11-FY16. Thus, gross NPA ratio has remained broadly stable at 4.9% as of Q3FY17 which 

is similar to FY16 level. However, the provision coverage ratio (PCR) improved significantly 

from 21% in FY16 to 36% as of Q3FY17 which helped Net NPA ratio to improve to 3.1% as 

of Q3FY17 as compared to 3.8% in FY16. LTFH has moved to 120 days past due (dpd) 

recognition of assets except for housing finance business which is already at 90 dpd. 

However, we believe transition to 90 dpd for rest of the portfolio will not have any 

significant impact on asset quality of the company due to lower delinquencies in the focus 

segments. Further, as per the management, income reversals based on 90 days will be 

done in FY17 itself however reporting of Gross/Net NPA is likely to happen only in FY18. 

We expect gross NPA ratio to improve to 3.7% by FY19E as we expect NPA formation to 

subside while the PCR is also likely to improve to 50% during the similar period. 
 

 

Asset quality ratios to improve over FY16-19E 

 
Source: Company, In-house research 
 

Consolidated earnings to grow at 24% CAGR over FY16-19E 

Profitability remained under pressure in FY16 due to change in business strategy, lower 

NIM and higher credit cost. However, we expect consolidated PAT to grow at a 24% CAGR 

over FY16-19E driven by strong business growth coupled with reduction in operational as 

well as credit cost. 
 

Earnings to grow at a CAGR of 24% over FY16-19E 

 
Source: Company, In-house research 

 

 

 

 

3.2 3.1

4.9 4.6
4.1 3.7

2.3 2.1

3.8
2.7

2.3 1.9

0

20

40

60

0.0

2.0

4.0

6.0

FY14 FY15 FY16 FY17E FY18E FY19E

GNPA (%) NNPA (%) PCR (%)

455
730 597

855 857
1013

1305
1614

16.3

60.6

-18.3

43.2

0.2

18.2

28.9 23.6

-50

0

50

100

0

500

1000

1500

2000

FY12 FY13 FY14 FY15 FY16 FY17E FY18E FY19E

Net Profit (Rs Cr) YoY Growth (%)

 

 

 



 

 

 

 

 
  Focus on execution to drive RoE 

 

LTFH’s RoE remained subdued (10-12%) over FY14-16 mainly due to 1) losses in defocussed 

portfolio driven by higher delinquencies 2) high operating cost especially in housing finance 

and family credit business and 3) zero contribution to profitability from investment 

management subsidiary. However, we believe return ratios should improve meaningfully 

hereon as LTFH has put forth a very clear execution plan including quarter to quarter 

milestones for achieving top quartile RoE. Further, the company is on its track to achieve 

top quartile ROE driven by a sharp focus on cost optimisation, reallocation of capital 

towards focused businesses and creating Centres of Excellence (CoE) in the chosen areas. 

Moreover, the company aims to augment its lending income with higher share of fee 

income from both lending and non-lending businesses. Hence, we expect ROEs to improve 

by 530 bps to 17.2% by FY19E. 
 
 

 

RoE to expand by 530 bps to 17.2% by FY19E 

 
Source: Company, In-house research 
 

 

Key Risks:  
 

➢ Asset quality risks in rural portfolio: Higher-than-expected delinquencies in LTHF 

business, particularly the farm equipment portfolio which has so far shown strong 

resilience despite headwinds which the industry is facing, could impact earnings 

and our valuations. 

➢ Regulatory changes could impact profitability: LTHF was impacted by regulatory 

changes in the microfinance segment and had to make significant provisions on its 

Andhra Pradesh portfolio. Any significant change in regulations (related to 

exposure, LTV, lending rates or capital requirements) could significantly impact 

the company’s profitability. 

➢ Delays in scaling up business segment: Inability to scale-up its microfinance and 

two wheelers portfolios, which are the key growth drivers in rural finance would 

cap the expected expansion in RoE. 

2.0
2.3

1.5
1.8

1.5 1.5 1.7
1.9

11.9
13.3

9.2
11.7 10.6

11.9
14.8

17.2

0

5

10

15

20

0.0

0.5

1.0

1.5

2.0

2.5

FY12 FY13 FY14 FY15 FY16 FY17E FY18E FY19E

RoA (%) RoE(%)

 

 

 

 

 

 



 

 

 

 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Balance Sheet (Consolidated) 

 

 

Profit & Loss Account (Consolidated) 

For private circulation only 

 

Y/E (Rs. Cr)   FY16  FY17E  FY18E FY19E 

Interest Income  6,605   7,843   8,994   10,448  

Interest Expense  4,124   4,786   5,444   6,334  

Net Interest Income  2,481   3,057   3,550   4,113  

Non Interest Income  866   936   1,081   1,264  

Net Income  3,347   3,994   4,631   5,377  

Operating Expenses  1,313   1,382   1,558   1,797  

Total Income  7,471   8,780   10,074   11,712  

Total Expenditure  5,437   6,168   7,002   8,131  

Pre Provisioning Profit  2,034   2,611   3,072   3,580  

Provisions  781   1,236   1,299   1,388  

Profit Before Tax  1,253   1,376   1,773   2,193  

Tax  399   367   473   585  

Net Profit  857   1,013   1,305   1,614  

 
 

Y/E (Rs. Cr) FY16 FY17E FY18E FY19E 

Liabilities     

Capital 2,967 2,968 2,968 2,968 

Reserves and 

Surplus 
5,442 5,638 6,092 6,693 

Minority Interest 100 100 100 100 

Borrowings 51,616 57,583 66,441 77,661 

Provisions 592 661 762 891 

Other Liabilities 

and Provisions 
3,030 3,521 4,226 5,131 

Total Liabilities 63,746 70,471 80,590 93,444 

Assets     

Fixed Assets  1,335 1,295 1,295 1,295 

Investments 3,563 3,768 4,108 4,522 

Advances 56,468 62,718 72,045 83,835 

Other Assets 1,979 2,181 2,486 2,870 

Cash & Bank 

Balances 
402 510 657 922 

Total Assets 63,746 70,471 80,590 93,444 

 

 

Profit & Loss Account (Consolidated) 

 

 

Profit & Loss Account (Consolidated) 

 

 

     Key Ratios (Consolidated) 

 
Y/E FY16 FY17E FY18E FY19E 

Per share data (Rs.)      

EPS 4.9  5.8  7.4  9.2  

DPS 0.8  1.7  2.2  2.8  

BV 41.0  49.1  51.6  55.1  

ABV 28.8  39.0  42.0  45.9  

Valuation (%)     

P/E 24.7  20.9  16.2  13.1  

P/BV 2.9  2.5  2.3  2.2  

P/ABV 4.2  3.1  2.9  2.6  

Div. Yield 0.7  1.4  1.8  2.3  

Spreads (%)     

Yield on Funds 12.6  12.8  12.9  12.9  

Cost of Funds 8.6  8.6  8.6  8.6  

Asset (%)     

GNPA 4.9  4.6  4.1  3.7  

NNPA 3.8  2.7  2.3  1.9  

PCR 21.2  40.0  45.0  50.0  

Management (%)     

Debt/ Equity 6.1  6.7  7.3  8.0  

Cost/ Income 39.2  34.6  33.7  33.4  

Earnings (%)     

NIM 4.7  5.0  5.1  5.1  

ROE 10.6  11.9  14.8  17.2  

ROA 1.5  1.5  1.7  1.9  
 

 

 



 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Rating Criteria 
    Large Cap. Return Mid/Small Cap. Return 

Buy More than equal to 10% Buy More than equal to 15% 

Hold Upside or downside is less than 10% Accumulate* Upside between 10% & 15% 

Reduce Less than equal to -10% Hold Between 0% & 10% 

  Reduce/sell Less than 0% 

* To satisfy regulatory requirements, we attribute ‘Accumulate’ as Buy and ‘Reduce’ as Sell. 

* L&TFH is a large-cap NBFC. 

 

Disclaimer:  

 

The SEBI registration number is INH200000394. 

The analyst for this report certifies that all the views expressed in this report accurately reflect his / her personal views about the subject 

company or companies, and its / their securities. No part of his / her compensation was / is / will be, directly / indirectly related to specific 

recommendations or views expressed in this report. 

This material is for the personal information of the authorized recipient, and no action is solicited on the basis of this. I t is not to be 

construed as an offer to sell, or the solicitation of an offer to buy any security, in any jurisdiction, where such an offer or solicitation would 

be illegal. 

We have reviewed the report, and in so far as it includes current or historical information, it is believed to be reliable, though its accuracy or 

completeness cannot be guaranteed. Neither Wealth India Financial Services Pvt. Ltd., nor any person connected with it, accepts any 

liability arising from the use of this document. The recipients of this material should rely on their own investigations and take their own 

professional advice. Price and value of the investments referred to in this material may go up or down. Past performance is not a guide for 

future performance. 

We and our affiliates, officers, directors, and employees worldwide: 

1. Do not have any financial interest in the subject company / companies in this report; 
2. Do not have any actual / beneficial ownership of one per cent or more in the company / companies mentioned in this document, or 

in its securities at the end of the month immediately preceding the date of publication of the research report, or the date of public 
appearance; 

3. Do not have any other material conflict of interest at the time of publication of the research report, or at the  time of public 
appearance; 

4. Have not received any compensation from the subject company / companies in the past 12 months; 
5. Have not managed or co-managed the public offering of securities for the subject company / companies in the past 12 months; 
6. Have not received any compensation for investment banking, or merchant banking, or brokerage services from the subject 

company / companies in the past 12 months; 
7. Have not served as an officer, director, or employee of the subject company; 
8. Have not been engaged in market making activity for the subject company; 

This document is not for public distribution. It has been furnished to you solely for your information, and must not be reproduced or 

redistributed to any other person. 
 

 
 

Funds India 
Uttam Building, Third Floor| 
No. 38 & 39| Whites Road| 
Royapettah|Chennai – 600014| 
T: +91 7667 166 166  
Email: contact@fundsindia.com 
 
 

 

 

 Contact Us: 

For private circulation only 

mailto:contact@fundsindia.com


 

 

 

 

 

 

Dion’s Disclosure and Disclaimer 
 

I, Kaushal Patel, employee of Dion Global Solutions Limited (Dion) is engaged in preparation of this report and hereby certify that all 
the views expressed in this research report (report) reflect my personal views about any or all of the subject issuer or securities.  

Disclaimer 

This report has been prepared by Dion and the report & its contents are the exclusive property of the Dion and the client cannot 

tamper with the report or its contents in any manner and the said report, shall in no case, be further distributed to any third party 

for commercial use, with or without consideration.  

Recipient shall not further distribute the report to a third party for a commercial consideration as this report is being furnished to 
the recipient solely for the purpose of information.  
 
Dion has taken steps to ensure that facts in this report are based on reliable information but cannot testify, nor make any 
representation or warranty, express or implied, to the accuracy, contents or data contained within this report. It is hereby 
confirmed that wherever Dion has employed a rating system in this report, the rating system has been clearly defined including the 
time horizon and benchmarks on which the rating is based.   
 
Descriptions of any company or companies or their securities mentioned herein are not intended to be complete and this report is 
not, and should not be construed as an offer or solicitation of an offer, to buy or sell any securities or other financial instruments. 
Dion has not taken any steps to ensure that the securities referred to in this report are suitable for any particular investor. This 
report is not to be relied upon in substitution for the exercise of independent judgment. Opinions or estimates expressed are 
current opinions as of the original publication date appearing on this report and the information, including the opinions and 
estimates contained herein, are subject to change without notice. Dion is under no duty to update this report from time to time.  
 
Dion or its associates including employees engaged in preparation of this report and its directors do not take any responsibility, 
financial or otherwise, of the losses or the damages sustained due to the investments made or any action taken on basis of this 
report, including but not restricted to, fluctuation in the prices of securities, changes in the currency rates, diminution in the NAVs, 
reduction in the dividend or income, etc.  
 
The investments or services contained or referred to in this report may not be suitable for all equally and it is recommended that an 
independent investment advisor be consulted. In addition, nothing in this report constitutes investment, legal, accounting or tax 
advice or a representation that any investment or strategy is suitable or appropriate to individual circumstances or otherwise 
constitutes a personal recommendation of Dion. 
 
REGULATORY DISCLOSURES: 

Dion is engaged in the business of developing software solutions for the global financial services industry across the entire 

transaction lifecycle and inter-alia provides research and information services essential for business intelligence to global companies 

and financial institutions. Dion is listed on BSE Limited (BSE) and is also registered under the SEBI (Research Analyst) Regulations, 

2014 (SEBI Regulations) as a Research Analyst vide Registration No. INH100002771. Dion’s activities were neither suspended nor has 

it defaulted with requirements under the Listing Agreement and / or SEBI (Listing Obligations and Disclosure Requirements) 

Regulations, 2015 with the BSE in the last five years. Dion has not been debarred from doing business by BSE / SEBI or any other 

authority.  

 

In the context of the SEBI Regulations, we affirm that we are a SEBI registered Research Analyst and in the course of our business, 

we issue research reports /research analysis etc that are prepared by our Research Analysts. We also affirm and undertake that no 

disciplinary action has been taken against us or our Analysts in connection with our business activities.   

For private circulation only 



 

 

 

 

In compliance with the above mentioned SEBI Regulations, the following additional disclosures are also provided which may be 

considered by the reader before making an investment decision:  

 

1.  Disclosures regarding Ownership  

Dion confirms that: 

(i) Dion/its associates have no financial interest or any other material conflict in relation to the subject company (ies) 

covered herein at the time of publication of this report. 

   

(ii) It/its associates have no actual / beneficial ownership of 1% or more securities of the subject company (ies) covered 

herein at the end of the month immediately preceding the date of publication of this report. 

   

Further, the Research Analyst confirms that: 

(i) He, his associates and his relatives have no financial interest in the subject company (ies) covered herein, and they 

have no other material conflict in the subject company at the time of publication of this report. 

   

(ii)   He, his associates and his relatives have no actual/beneficial ownership of 1% or more securities of the subject 

company (ies) covered herein at the end of the month immediately preceding the date of publication of this report.  

   

2.  Disclosures regarding Compensation:   

During the past 12 months, Dion or its Associates: 

(a) Have not managed or co-managed public offering of securities for the subject company (b) Have not received any compensation 

for investment banking or merchant banking or brokerage services from the subject company (c) Have not received compensation 

for products or services other than investment banking or merchant banking or brokerage services from the subject. (d) Have not 

received any compensation or other benefits from the subject company or third party in connection with this report  

 

3.  Disclosure regarding the Research Analyst’s connection with the subject company: 

It is affirmed that I, Kaushal Patel employed as Research Analyst by Dion and engaged in the preparation of this report have not 

served as an officer, director or employee of the subject company 

 

4.  Disclosure regarding Market Making activity: 

Neither Dion /its Research Analysts have engaged in market making activities for the subject company. 

 
Copyright in this report vests exclusively with Dion. 

 

 

 

 

 

 

 

 

For private circulation only 


