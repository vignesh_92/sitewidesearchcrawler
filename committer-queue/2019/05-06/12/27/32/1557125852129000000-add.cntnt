
 

 

 

 

. 

 

. 

 

 

 

 

 

 

 

 

Power play with attractive dividend yield…. 
Rural Electrification Corporation Ltd. (REC) is one of the leading public 
Infrastructure Finance Company in India with a network of 18 project offices 
including 5 Zonal and 3 sub offices along with 1 Training centre. REC’s business 
model spans across the value chain of power infrastructure financing along 
with consultancy and advisory for power distribution and transmission projects 
catering to large and marquee state run power generation and transmission-
distribution companies. 
 

Investment Rationale  

 Business growth momentum to sustain on the back of generation and 
renewable segments: REC has grown its loan book at a healthy pace of 15% 
CAGR over FY12-17. Segmental mix has broadly remained unchanged over the 
years with transmission & distribution segment contribution remaining at 
50.8% as of FY17 (49.2% in FY12). Based on borrower profile, exposure to 
private sector generation has grown at a rapid pace of 25.4% CAGR over FY12-
17 growing to 16.5% of total exposure in FY17 as compared to 10.5% in FY12. 
Going forward, we expect RECL’s loan book to grow at a healthy CAGR of 12% 
over FY17-20E mainly led by higher growth in generation and renewable 
sectors. 
 

 Net interest margin (NIM) to stabilize from hereon: REC steadily 
maintained its NIM above 4.5% over the last five years on the back of access of 
low cost of funds along with healthy spreads supported by low competition 
from banks. However, NIM remain suppressed and declined to 3.8% in 9MFY18 
owing to higher interest reversals along with repayments of high yield SEB 
loans. However, we expect NIM has bottomed out and to stabilize from hereon 
and hover around current levels over FY17-20E. 
 

 Return ratios to decline but remain competitive: REC used to enjoy ~20%+ 
RoE and ~3%+ RoA over FY12-17 on account of healthy loan growth, higher 
operating efficiency and lower credit cost. However, with compression in NIM 
along with increase in credit cost, REC’s RoE declined to 14.5% and RoA 
declined to 2.3% in 9MFY18. Going forward, we expect RoE and RoA to remain 
around 15% and 2.3%, respectively over FY17-20E which is competitive as 
compared to industry average of Banks and NBFCs. 
 

 Adequately capitalized for future growth: REC would not be required to 
raise equity capital to deliver the aforesaid asset growth given the company’s 
high capital adequacy ratio (CAR) of 21.2% (Tier‐1 ratio at 18.6%) currently 
along with RoE much higher than asset growth. 
 

Valuation: REC’s profitability will continue to remain under pressure over 
FY17-20E as provisions are likely to remain high given the increasing stress in 
power sector and high level of private restructured loan book (~5.6% of loans). 
However, the attractive dividend yield of ~7.5% will restrict any further fall in 
the stock price. Notably, REC consistently rewards its shareholders with 
healthy dividend, showing that the company is investor friendly. Further, REC is 
diversifying its portfolio toward renewable projects. Hence, we recommend 
BUY rating on the stock with a target price (TP) of Rs146 (P/ABV of 0.8x for 
FY20E). 

 

Rating   BUY 
CMP (Rs.)   128 

Target (Rs.)   146 
Potential Upside    14% 

Duration   Long Term 

Face Value (Rs.)   10 

52 week H/L (Rs.)   224/123 

Adj. all time High (Rs.)   224 

Decline from 52WH (%)   42.6 

Rise from 52WL (%)                             4.2 

Beta   1.0 

Mkt. Cap (Rs.Cr)   25,388 

 

Market Data 

 

March 16, 2018 

 

BSE Code: 532955  NSE Code: RECLTD  Reuters Code: RURL.NS  Bloomberg Code: RECL:IN 

Y/E FY17A FY18E FY19E FY20E 

Interest Income 
(Rs.Cr) 

22,936 22,169 24,839 28,139 

Interest Expense 
(Rs.Cr) 

13,775 13,856 15,551 17,646 

Net Interest 
Income (Rs. Cr) 

9,160 8,313 9,287 10,493 

Pre Pro Profit 
(Rs. Cr) 

9,969 8,729 9,780 11,084 

EPS 31.6 26.1 29.0 32.7 

P/E (x) 4.1 4.9 4.4 3.9 

P/BV (x) 0.8 0.7 0.6 0.6 

P/ABV (x) 0.8 0.8 0.8 0.7 

ROE (%) 20.2 14.8 15.2 15.5 

ROA (%) 3.0 2.3 2.3 2.3 
  

One year Price Chart 

Promoters (%) 58.3 58.9 (0.6) 

Public (%) 41.7 41.1 0.6 

 

Fiscal Year Ended 

For private circulation only 

Shareholding Pattern 

 

Dec-17 Sep-17 Chg. 

 

Volume No. I Issue No. 166 Rural Electrification Corporation Limited 

. 

 

 

 

75

125

175

225

M
ar

-1
7

A
p

r-
17

M
ay

-1
7

Ju
n

-1
7

Ju
l-

17

A
u

g
-1

7

S
ep

-1
7

O
ct

-1
7

N
o

v
-1

7

D
ec

-1
7

Ja
n

-1
8

F
eb

-1
8

M
ar

-1
8

RECL Sensex (rebased)



 

 

 

 

 
 

Rural Electrification Corporation Limited (REC) - Company Overview 
 

Rural Electrification Corporation Ltd. (REC) is one of the leading public Infrastructure Finance 
Company in India with a network of 18 project offices including 5 Zonal and 3 sub offices 
along with 1 Training centre. REC’s business model spans across the value chain of power 
infrastructure financing along with consultancy and advisory for power distribution and 
transmission projects catering to large and marquee state run power generation and 
transmission-distribution companies. 
 

Business growth momentum to sustain on the back of generation and renewable 
segments 
 

REC has grown its loan book at a healthy pace of 15% CAGR over FY12-17. Segmental mix has 
broadly remained unchanged over the years with transmission & distribution segment 
contribution remaining at 50.8% as of FY17 (49.2% in FY12). Based on borrower profile, 
exposure to private sector generation has grown at a rapid pace of 25.4% CAGR over FY12-17 
growing to 16.5% of total exposure in FY17 as compared to 10.5% in FY12. State PSUs form 
75.3% of outstanding loans as of FY17 while joint contributes remaining 8.3%. 
 

Loan book to grow at a CAGR of 12% over FY17-20E 

 
Source: Company, In-house research 

 
In Q3FY18, loan book grew at 11% YoY led by power generation sector (46% of total loans) 
which grew at a similar pace in Q3FY18. While loans to Joint sector (9% of total loans) and 
state sector (77% of total loans) grew by 22% YoY and 12% YoY, respectively, loans to private 
sector grew at a meagre pace of 1% YoY as the company continues to remain cautious given 
the higher stress in the sector. Sanctions and disbursements increased by a robust pace of 
124% YoY and 25% YoY on the back of 340% YoY and 32% YoY growth in the generation 
sector, respectively. However, T&D sector’s sanctions and disbursements declined by 5% YoY 
and 28% YoY, respectively. Going forward, we expect RECL’s loan book to grow at a healthy 
CAGR of 12% over FY17-20E mainly led by higher growth in generation and renewable 
sectors. 
 

Net interest margin (NIM) to stabilize from hereon 
 

REC steadily maintained its NIM above 4.5% over the last five years on the back of access of 
low cost of funds along with healthy spreads supported by low competition from banks. 
However, NIM remain suppressed and declined to 3.8% in 9MFY18 owing to higher interest 
reversals along with repayments of high yield SEB loans. Besides, the rising competition from 
the banks also dented margin. However, we expect NIM has bottomed out and to stabilize 
from hereon and hover around current levels over FY17-20E. 

1,01,426 
1,27,356 

1,48,641 

1,79,647 
2,01,278 2,01,929 

2,25,352 
2,53,394 

2,88,969 24.1 
25.6 

16.7 

20.9 

12.0 

0.3 

11.6 12.4 
14.0 

0

5

10

15

20

25

30

0

50,000

1,00,000

1,50,000

2,00,000

2,50,000

3,00,000

3,50,000

FY12 FY13 FY14 FY15 FY16 FY17 FY18E FY19E FY20E

Loans (Rs Cr) YoY (%)

 

 

For private circulation only 

Rural Electrification 

Corporation Ltd. (REC) is one 

of the leading public 

Infrastructure Finance 

Company in India. 

 

 

 

 

 

 

 

 

 

 

 

 

 



 

 

 

 

  
Return ratios to decline but remain competitive 
 

REC used to enjoy ~20%+ RoE and ~3%+ RoA over FY12-17 on account of healthy loan growth, 
higher operating efficiency and lower credit cost. As a result, the company reported 17.3% 
CAGR in net profit over FY12-17. However, with compression in NIM along with increase in 
credit cost, REC’s RoE declined to 14.5% and RoA declined to 2.3% in 9MFY18. Going forward, 
we expect RoE and RoA to remain around 15% and 2.3%, respectively over FY17-20E which is 
competitive as compared to industry average of Banks and NBFCs. 
 

Return ratios to decline but remain healthy over FY17-20E 

 
Source: Company, In-house research 

 

Asset quality overhang to continue 
 

Asset quality deteriorated significantly as Gross/Net non-performing asset (NPA) ratio 
increased by 45/37 bps QoQ to 3.0%/2.0% in Q3FY18 mainly led by slippages of Lanco Anpara 
where it has exposure of ~Rs1,250cr. However, gross restructured assets declined by 9% QoQ 
following upgrade in public sector unit (PSU). As per management, some more PSU 
restructured accounts are likely to get upgraded in future. However, given stress in the power 
sector and the uncertainty surrounding private restructured assets (Rs12,644cr), we expect 
the asset quality issues to continue over near to medium term and the company’s Gross/Net 
NPA ratios to increase significantly to 4.2%/2.7% by FY19E. 
 

Asset quality to improve gradually from FY20E 

 
Source: Company, In-house research 
 

Adequately capitalized for future growth 
 

REC would not be required to raise equity capital to deliver the aforesaid asset growth given 
the company’s high capital adequacy ratio (CAR) of 21.2% (Tier‐1 ratio at 18.6%) currently 
along with RoE much higher than asset growth. 

2.9
3.2 3.3 3.1

2.9 3.0

2.3 2.3 2.3

20.5

23.7 24.6 23.1
21.0 20.2

14.8 15.2 15.5

0

5

10

15

20

25

30

0.0

0.5

1.0

1.5

2.0

2.5

3.0

3.5

FY12 FY13 FY14 FY15 FY16 FY17 FY18E FY19E FY20E

RoA (%) RoE (%)

0.5 0.4 0.3
0.7

1.7

2.4

3.7

4.2 4.1

0.4 0.3 0.2
0.5

1.6 1.6

2.4
2.7 2.6

0.0

1.0

2.0

3.0

4.0

5.0

FY12 FY13 FY14 FY15 FY16 FY17 FY18E FY19E FY20E

GNPA (%) NNPA (%)

 

 

 

e
ll 
m
a
n
a

For private circulation only 
 

 

 

 



 

 

 

 

 
 
  

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Outlook and Valuation 
 

REC’s profitability will continue to remain under pressure over FY17-20E as provisions are 
likely to remain high given the increasing stress in power sector and high level of private 
restructured loan book (~5.6% of loans). However, the attractive dividend yield of ~7.5% 
will restrict any further fall in the stock price. Notably, REC consistently rewards its 
shareholders with healthy dividend, showing that the company is investor friendly. Further, 
REC is diversifying its portfolio toward renewable projects. We believe this enhances the 
company’s potential to expand its loan book. With the gap between sanctions and 
disbursements widening, we believe that REC has substantial room to increase advances, 
even without fresh sanctions. Hence, we recommend BUY rating on the stock with a target 
price (TP) of Rs146 (P/ABV of 0.8x for FY20E). 
 

Key Risks:  
 

➢ REC’s growth depends on its ability to remain effectively competitive in the power 

financing space and to pass the higher cost of funds to customers. 
 

➢ Given the higher restructured loan book (~8.9% of total loans), any major slippage 

or ineffective recoveries can raise NPAs significantly, adversely affecting 

profitability and growth. 
 

➢ REC is subject to risk arising from asset-liability mismatch as majority of its loans 

are long-term in nature due to wholesale financing of large power projects, 

whereas its borrowings are relatively for shorter term. 
 

➢ REC is exposed to project-specific and general risks inherent to the power sector. 

Any delay in the power sector projects due to lack of fuel supplies, supply of key 

equipment or delay in getting environment clearances can adversely affect the 

profitability of power projects, increasing the company’s NPAs. 

For private circulation only 

 

 

 



 

 

 

 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Balance Sheet (Standalone) 

 

 

Profit & Loss Account (Consolidated) 

For private circulation only 

 

Y/E (Rs. Cr)   FY17A  FY18E  FY19E FY20E 

Interest Income 22,936 22,169 24,839 28,139 

Interest Expense 13,775 13,856 15,551 17,646 

Net Interest Income 9,160 8,313 9,287 10,493 

Non Interest Income 1,160 743 832 943 

Net Income 10,320 9,055 10,119 11,436 

Operating Expenses 352 326 339 352 

Total Income 24,095 22,911 25,671 29,082 

Total Expenditure 14,127 14,182 15,891 17,998 

Pre Provisioning Profit 9,969 8,729 9,780 11,084 

Provisions 1,109 1,175 1,376 1,627 

Profit Before Tax 8,859  7,554  8,404  9,457  

Tax 2,615  2,400  2,670  3,004  

Net Profit 6,244  5,154  5,734  6,453  

 
 

Y/E (Rs. Cr) FY17A FY18E FY19E FY20E 

Liabilities     

Capital 1,975 1,975 1,975 1,975 

Reserves and 

Surplus 
31,351 34,134 37,497 41,578 

Borrowings 167,517 187,268 210,570 240,133 

Provisions 2,043 2,366 2,661 3,034 

Other Liabilities 6,351 7,887 9,502 11,559 

Total Liabilities 209,236 233,630 262,204 298,279 

Assets     

Fixed Assets 181 219 264 319 

Investments 2,696 2,254 2,534 2,890 

Advances 200,373 224,901 252,887 288,391 

Other Assets 1,496 4,056 4,561 5,201 

Cash and Bank 

Balances 
4,490 2,200 1,959 1,478 

Total Assets 209,236 233,630 262,204 298,279 

 

 

Profit & Loss Account (Standalone) 

 

 

Profit & Loss Account (Consolidated) 

 

 

Key Ratios (Standalone) 

 
Y/E FY17A FY18E FY19E FY20E 

Per share data (Rs.)      

EPS 31.6  26.1  29.0  32.7  

DPS 9.7  10.0  10.0  10.0  

BV 168.7  182.8  199.9  220.5  

ABV 152.4  154.9  164.6  182.0  

Valuation (%)     

P/E 4.1  4.9  4.4  3.9  

P/BV 0.8  0.7  0.6  0.6  

P/ABV 0.8  0.8  0.8  0.7  

Div. Yield 7.5  7.8  7.8  7.8  

Capital (%)     

CAR 21.2  20.7  20.3  19.9  

Tier I 18.4  18.0  17.6  17.1  

Tier II 2.8  2.8  2.8  2.8  

Asset (%)     

GNPA 2.4  3.7  4.2  4.1  

NNPA 1.6  2.4  2.7  2.6  

PCR 33.6  34.4  35.2  36.0  

Management (%)     

Credit/ Deposit 5.0  5.2  5.4  5.6  

Cost/ Income 3.4  3.6  3.4  3.1  

Earnings (%)     

NIM 4.5  3.9  3.9  3.9  

ROE 20.2  14.8  15.2  15.5  

ROA 3.0  2.3  2.3  2.3  

 

 

 



 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Rating Criteria 
    Large Cap. Return Mid/Small Cap. Return 

Buy More than equal to 10% Buy More than equal to 15% 

Hold Upside or downside is less than 10% Accumulate* Upside between 10% & 15% 

Reduce Less than equal to -10% Hold Between 0% & 10% 

  Reduce/sell Less than 0% 

* To satisfy regulatory requirements, we attribute ‘Accumulate’ as Buy and ‘Reduce’ as Sell. 

* RECL is a large-cap stock. 

 

Disclaimer:  

 

The SEBI registration number is INH200000394. 

The analyst for this report certifies that all the views expressed in this report accurately reflect his / her personal views about the subject 

company or companies, and its / their securities. No part of his / her compensation was / is / will be, directly / indirectly related to specific 

recommendations or views expressed in this report. 

This material is for the personal information of the authorized recipient, and no action is solicited on the basis of this. I t is not to be 

construed as an offer to sell, or the solicitation of an offer to buy any security, in any jurisdiction, where such an offer or solicitation would 

be illegal. 

We have reviewed the report, and in so far as it includes current or historical information, it is believed to be reliable, though its accuracy or 

completeness cannot be guaranteed. Neither Wealth India Financial Services Pvt. Ltd., nor any person connected with it, accepts any 

liability arising from the use of this document. The recipients of this material should rely on their own investigations and take their own 

professional advice. Price and value of the investments referred to in this material may go up or down. Past performance is not a guide for 

future performance. 

We and our affiliates, officers, directors, and employees worldwide: 

1. Do not have any financial interest in the subject company / companies in this report; 
2. Do not have any actual / beneficial ownership of one per cent or more in the company / companies mentioned in this document, or 

in its securities at the end of the month immediately preceding the date of publication of the research report, or the date of public 
appearance; 

3. Do not have any other material conflict of interest at the time of publication of the research report, or at the  time of public 
appearance; 

4. Have not received any compensation from the subject company / companies in the past 12 months; 
5. Have not managed or co-managed the public offering of securities for the subject company / companies in the past 12 months; 
6. Have not received any compensation for investment banking, or merchant banking, or brokerage services from the subject 

company / companies in the past 12 months; 
7. Have not served as an officer, director, or employee of the subject company; 
8. Have not been engaged in market making activity for the subject company; 

This document is not for public distribution. It has been furnished to you solely for your information, and must not be reproduced or 

redistributed to any other person. 
 

 
 

Funds India 
Uttam Building, Third Floor| 
No. 38 & 39| Whites Road| 
Royapettah|Chennai – 600014| 
T: +91 7667 166 166  
Email: contact@fundsindia.com 
 
 

 

 

 Contact Us: 

For private circulation only 

mailto:contact@fundsindia.com


 

 

 

 

 

 

Dion’s Disclosure and Disclaimer 
 

I, Kaushal Patel, employee of Dion Global Solutions Limited (Dion) is engaged in preparation of this report and hereby certify that all 
the views expressed in this research report (report) reflect my personal views about any or all of the subject issuer or securities.  

Disclaimer 

This report has been prepared by Dion and the report & its contents are the exclusive property of the Dion and the client cannot 

tamper with the report or its contents in any manner and the said report, shall in no case, be further distributed to any third party 

for commercial use, with or without consideration.  

Recipient shall not further distribute the report to a third party for a commercial consideration as this report is being furnished to 
the recipient solely for the purpose of information.  
 
Dion has taken steps to ensure that facts in this report are based on reliable information but cannot testify, nor make any 
representation or warranty, express or implied, to the accuracy, contents or data contained within this report. It is hereby 
confirmed that wherever Dion has employed a rating system in this report, the rating system has been clearly defined including the 
time horizon and benchmarks on which the rating is based.   
 
Descriptions of any company or companies or their securities mentioned herein are not intended to be complete and this report is 
not, and should not be construed as an offer or solicitation of an offer, to buy or sell any securities or other financial instruments. 
Dion has not taken any steps to ensure that the securities referred to in this report are suitable for any particular investor. This 
report is not to be relied upon in substitution for the exercise of independent judgment. Opinions or estimates expressed are 
current opinions as of the original publication date appearing on this report and the information, including the opinions and 
estimates contained herein, are subject to change without notice. Dion is under no duty to update this report from time to time.  
 
Dion or its associates including employees engaged in preparation of this report and its directors do not take any responsibility, 
financial or otherwise, of the losses or the damages sustained due to the investments made or any action taken on basis of this 
report, including but not restricted to, fluctuation in the prices of securities, changes in the currency rates, diminution in the NAVs, 
reduction in the dividend or income, etc.  
 
The investments or services contained or referred to in this report may not be suitable for all equally and it is recommended that an 
independent investment advisor be consulted. In addition, nothing in this report constitutes investment, legal, accounting or tax 
advice or a representation that any investment or strategy is suitable or appropriate to individual circumstances or otherwise 
constitutes a personal recommendation of Dion. 
 
REGULATORY DISCLOSURES: 

Dion is engaged in the business of developing software solutions for the global financial services industry across the entire 

transaction lifecycle and inter-alia provides research and information services essential for business intelligence to global companies 

and financial institutions. Dion is listed on BSE Limited (BSE) and is also registered under the SEBI (Research Analyst) Regulations, 

2014 (SEBI Regulations) as a Research Analyst vide Registration No. INH100002771. Dion’s activities were neither suspended nor has 

it defaulted with requirements under the Listing Agreement and / or SEBI (Listing Obligations and Disclosure Requirements) 

Regulations, 2015 with the BSE in the last five years. Dion has not been debarred from doing business by BSE / SEBI or any other 

authority.  

 

In the context of the SEBI Regulations, we affirm that we are a SEBI registered Research Analyst and in the course of our business, 

we issue research reports /research analysis etc that are prepared by our Research Analysts. We also affirm and undertake that no 

disciplinary action has been taken against us or our Analysts in connection with our business activities.   

For private circulation only 



 

 

 

 

In compliance with the above mentioned SEBI Regulations, the following additional disclosures are also provided which may be 

considered by the reader before making an investment decision:  

 

1.  Disclosures regarding Ownership  

Dion confirms that: 

(i) Dion/its associates have no financial interest or any other material conflict in relation to the subject company (ies) 

covered herein at the time of publication of this report. 

   

(ii) It/its associates have no actual / beneficial ownership of 1% or more securities of the subject company (ies) covered 

herein at the end of the month immediately preceding the date of publication of this report. 

   

Further, the Research Analyst confirms that: 

(i) He, his associates and his relatives have no financial interest in the subject company (ies) covered herein, and they 

have no other material conflict in the subject company at the time of publication of this report. 

   

(ii)   He, his associates and his relatives have no actual/beneficial ownership of 1% or more securities of the subject 

company (ies) covered herein at the end of the month immediately preceding the date of publication of this report.  

   

2.  Disclosures regarding Compensation:   

During the past 12 months, Dion or its Associates: 

(a) Have not managed or co-managed public offering of securities for the subject company (b) Have not received any compensation 

for investment banking or merchant banking or brokerage services from the subject company (c) Have not received compensation 

for products or services other than investment banking or merchant banking or brokerage services from the subject. (d) Have not 

received any compensation or other benefits from the subject company or third party in connection with this report  

 

3.  Disclosure regarding the Research Analyst’s connection with the subject company: 

It is affirmed that I, Kaushal Patel employed as Research Analyst by Dion and engaged in the preparation of this report have not 

served as an officer, director or employee of the subject company 

 

4.  Disclosure regarding Market Making activity: 

Neither Dion /its Research Analysts have engaged in market making activities for the subject company. 

 
Copyright in this report vests exclusively with Dion. 

 

 

 

 

 

 

 

 

For private circulation only 


