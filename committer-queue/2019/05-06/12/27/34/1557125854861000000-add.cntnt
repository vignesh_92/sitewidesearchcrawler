
 

 

 

 

. 

 

. 

 

 

 

 

 

 

 

 

Established in 1970, SRF Ltd is engaged in the manufacturing of industrial and 

specialty intermediates. SRF’s diversified business portfolio consists of 

Technical Textiles, Fluorochemicals, Specialty Chemicals, Packaging Films and 

Engineering Plastics. 
 

Investment Rationale 

   Packaging films business drives growth: SRF posted healthy 22% YoY 

growth in revenue in Q4FY18 on consolidated basis primarily driven by strong 

growth in packaging films business which delivered robust 39% YoY growth in 

revenue led by full utilisation of newly commissioned BOPET and BOPP film 

plants at Indore. Further 11% YoY growth in revenue in chemicals & polymer 

business (as a result of significant growth in the refrigerants business) has 

helped to offset 1.5% YoY decline in revenue in technical textiles business. 
 

  Traction in margin going ahead: Despite strong growth in revenue EBITDA 

margin grew by only 64bps YoY to 16.9% owing to moderation in gross margin. 

Packaging films segment witnessed strong expansion of 405 bps YoY in EBIT 

margin to 13.7% reflecting full ramp up of newly commissioned BOPET and 

BOPP film plants. Likewise, EBIT margin in technical textiles segment soared by 

354 bps YoY to 13.6% led by improved performance of NTCF (owing to better 

volumes and cost improvement measures) and belting fabrics businesses (due 

to higher volumes and increased contribution from value added products). 

While the expansion of EBIT margin in chemicals & polymer business was only 

90 bps YoY to 17.5% owing to continued slowdown in agrochemical demand. 

Despite healthy operational performance, reported PAT declined by 4% YoY 

mainly impacted by higher depreciation and interest charges. Going ahead, 

robust growth outlook in speciality chemicals & packaging businesses will 

translate into 420bps expansion in EBITDA margin over FY18-20E. 
 

  Better growth visibility in speciality chemical business: Although the 

demand recovery in the global agrochemicals industry is expected by the end 

of CY18, management expects 40-50% growth in revenue from specialty 

chemicals business in FY19 on the back of ramp up of new capacities and 

increase in offtake. To boost performance of the speciality chemical business, 

it will continue to focus on increasing penetration in geographies like US, 

Europe and Japan. Management has provided capex guidance of ~Rs. 7 bn in 

FY19 which will be mainly spent towards expansion of refrigerant capacity, 

commissioning of BOPET film line and metallizer in Hungary and speciality 

chemicals business. Hence, we factor revenue CAGR of 12% over FY18-20E. 
 

 

Valuation: SRF’s dominant position in the domestic refrigerant gas space, 

capacity expansion drive and robust growth outlook in the speciality chemicals 

business will drive 29% earnings CAGR over FY18-20E. Hence, we upgrade the 

stock to ‘BUY’ from ‘HOLD’ earlier with a revised TP of Rs. 2,128 based on 18x 

FY20E EPS. 
 

 

 

 

Rating   BUY 
CMP (Rs.)   1,695 

Target (Rs.)   2,128 
Potential Upside    26% 

Duration   Long Term 

Face Value (Rs.)   10 

52 week H/L (Rs.)   2,443/1,420 

Adj. all time High (Rs.)   2,443 

Decline from 52WH (%)   30.6 

Rise from 52WL (%)   19.4 

Beta   2.0 

Mkt. Cap (Rs.Cr)   9,732 

 

Market Data 

Feb 10th, 2017 

 

Promoters (%) 52.4 52.4 - 

Public (%) 47.6 47.6 - 

 

Fiscal Year Ended 

 

1,000

1,500

2,000

2,500

Ju
n

-1
7

Ju
l-

1
7

A
u

g-
1

7

Se
p

-1
7

O
ct

-1
7

N
o

v-
1

7

D
ec

-1
7

Ja
n

-1
8

Fe
b

-1
8

M
ar

-1
8

A
p

r-
1

8

M
ay

-1
8

Ju
n

-1
8

SRF Sensex (Rebased)

For private circulation only 

Y/E FY17 FY18 FY19E FY20E 

Net sales (Rs.Cr)  4,739   5,511   6,289   7,016  

Net profit (Rs.Cr)  969   906   1,196   1,432  

EPS (Rs.)  88.1   71.1   92.2   118.2  

P/E (x) 19.2 23.8 18.4 14.3 

P/BV (x) 3.1 2.8 2.5 2.2 

ROE (%) 17.3 12.3 14.2 16.1 

  

Shareholding Pattern 

 

Mar-18 Dec-17 Chg. 

 

   BSE Code:     503806                   NSE Code: SRF                   Reuters Code:   SRFL:NS                              Bloomberg Code: SRF:IN 

 

One year Price Chart 

 

 

Volume No. I Issue No. 179 SRF Ltd. 

. 

                         June 29, 2018 



 

 

 

 

 
 

SRF Limited: Business Overview 
  

Established in 1970, SRF Ltd is engaged in the manufacturing of industrial and specialty 

intermediates. SRF’s diversified business portfolio consists of Technical Textiles, 

Fluorochemicals, Specialty Chemicals, Packaging Films and Engineering Plastics. With twelve 

manufacturing plants in India, two in Thailand and one in South Africa, the company exports 

to more than 75 countries. 
 

Revenue mix across segments 
 

 

Source: Company, In-house research 
 

                                                Quarterly Financials (Consolidated) 

(Rs cr) Q4FY18 Q4FY17 
YoY 

Growth % Q3FY18 

QoQ 
Growth 

% 

Sales  1,612   1,326   21.6   1,397   15.4  

EBITDA  273   216   26.4   232   17.7  

Margin (%)         16.9         16.3       64bps         16.6     33bps  

Depreciation  86   73   18.0   77   11.8  

EBIT  186   143   30.6   155   20.6  

Interest  43   23   86.4   24   78.0  

Other Income  13   26   (49.7)  21   (37.7) 

Exceptional Items  6   19   (65.3)  21   -  

PBT  163   164   (0.6)  173   (5.4) 

Tax  40   35   12.4   42   (4.8) 

PAT  124   129   (4.1)  131   (5.6) 

Minority Interest  -     -    -  -    - 

Reported PAT  124   129   (4.1)  131   (5.6) 

Adjustment  (6)  (19)  -     (21)  -    

Adj PAT  117   111   6.1   110   6.7  

No. of shares (cr)  5.7   5.7   -    5.7  -    

EPS (Rs)  20.5   19.3   6.1  19.2  6.7  

 
Source: Company, In-house research 
 

 

Technical 
Textiles, 
36.6%

Chemicals & 
Polymer, 

32.1%

Packaging Films, 
31.3%

 

For private circulation only 

 



 

 

 

 

  

 

Packaging films business drives growth  
 

SRF posted healthy 22% YoY growth in revenue in Q4FY18 on consolidated basis primarily 

driven by strong growth in packaging films business which delivered robust 39% YoY growth in 

revenue led by full utilisation of newly commissioned BOPET and BOPP film plants at Indore. 

Further 11% YoY growth in revenue in chemicals & polymer business (as a result of significant 

growth in the refrigerants business) has helped to offset 1.5% YoY decline in revenue in 

technical textiles business. 
 

Traction in margin going ahead 
 

Despite strong growth in revenue EBITDA margin grew by only 64bps YoY to 16.9% owing to 

moderation in gross margin. Packaging films segment witnessed strong expansion of 405 bps 

YoY in EBIT margin to 13.7% reflecting full ramp up of newly commissioned BOPET and BOPP 

film plants. Likewise, EBIT margin in technical textiles segment soared by 354 bps YoY to 13.6% 

led by improved performance of NTCF (owing to better volumes and cost improvement 

measures) and belting fabrics businesses (due to higher volumes and increased contribution 

from value added products). While the expansion of EBIT margin in chemicals & polymer 

business was only 90 bps YoY to 17.5% owing to continued slowdown in agrochemical 

demand. Despite healthy operational performance, reported PAT declined by 4% YoY mainly 

impacted by higher depreciation and interest charges. Going ahead, robust growth outlook in 

speciality chemicals & packaging businesses will translate into 420bps expansion in EBITDA 

margin over FY18-20E. 
 

EBITDA margin to expand by 420 bps over FY18-20E 
 

 
Source: Company, In-house research 

 

Better growth visibility in speciality chemical business 
 

Although the demand recovery in the global agrochemicals industry is expected by the end of 

CY18, management expects 40-50% growth in revenue from specialty chemicals business in 

FY19 on the back of ramp up of new capacities and increase in offtake. To boost performance 

of the speciality chemical business, it will continue to focus on increasing penetration in 

geographies like US, Europe and Japan. Management has provided capex guidance of ~Rs. 7 bn 

in FY19 which will be mainly spent towards expansion of refrigerant capacity, commissioning 

of BOPET film line and metallizer in Hungary and speciality chemicals business. Hence, we 

factor revenue CAGR of 12% over FY18-20E.  

963 969 906 

1,196 
1,432 

21.0% 20.1%

16.2%
19.0%

20.4%

0.0%

5.0%

10.0%

15.0%

20.0%

25.0%

 -

 500

 1,000

 1,500

 2,000

FY16 FY17 FY18 FY19E FY20E

EBITDA (Rs. Crores) EBITDA Margin %

 

 

 

 

 

 

 

 

 

 

For private circulation only 

 

 

 



 

 

 

 

 
 
 
 
  

Revenue to grow at a CAGR of 12% over FY18-20E 
 

 
 

Source: Company, In-house research 

 

Healthy traction in net profit growth going ahead 

 
 

Return ratios trend 
   

 
Source: Company, In-house research 
 

Key risks 
 

• Delay in recovery in the global agrochemicals sector 

• Adverse currency movement 

• Significant volatility in raw materials cost could impact margin 
 

4,523 4,739 
5,511 

6,289 
7,016 

0.7%

4.8%

16.3%

14.1%
11.6%

0.0%

5.0%

10.0%

15.0%

20.0%

 -

 2,000

 4,000

 6,000

 8,000

FY16 FY17 FY18 FY19E FY20E

Revenue (Rs. Crores) Growth %

430 
515 

415 

539 

691 

9.4%

10.7%

7.4%
8.6%

9.8%

0.0%

2.0%

4.0%

6.0%

8.0%

10.0%

12.0%

 -

 100

 200

 300

 400

 500

 600

 700

 800

FY16 FY17 FY18 FY19E FY20E

Net Profit (Rs. Crores) PAT Margin %

17.0% 17.3%

12.3%
14.2%

16.1%

14.9% 15.1%

11.5%
13.8%

15.7%

0.0%

5.0%

10.0%

15.0%

20.0%

25.0%

FY16 FY17 FY18 FY19E FY20E

RoE (%) RoCE (%)

  

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

For private circulation only 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 



 

 

 

 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Balance Sheet (Consolidated) 

 

 

Profit & Loss Account (Consolidated) 

Y/E (Rs. Cr)  FY17  FY18 FY19E FY20E 

Paid up capital              58               58               58               58  

Reserves and 

Surplus 
      3,124          3,506          3,947          4,539  

Net worth        3,183          3,565          4,005          4,598  

Minority interest              -                 -                 -                 -    

Total Debt        1,977          2,758          2,758          2,758  

Other non-current 

liabilities 
             60               68               68               68  

Total Liabilities       5,220          6,390          6,831          7,423  

Total fixed assets        4,400          5,118          5,452          5,755  

Capital WIP           259             559             559             559  

Goodwill                5                 4                 4                 4  

Investments           196             122             122             122  

Net Current assets           444             647             753          1,042  

Deferred tax assets 
(Net) 

(284)           (291)           (291)           (291) 

Other non-current 

assets 
      200             232             232             232  

Total Assets      5,220          6,390          6,831          7,423  

 

 Cash Flow Statement (Consolidated) 

 

Profit & Loss Account (Consolidated) 

 

 

Profit & Loss Account (Consolidated) 

 

 

Key Ratios (Consolidated) 

 

For private circulation only 

Y/E  (Rs. Cr)  FY17  FY18E FY19E FY20E 

Pretax profit        657             535             738             946  

Depreciation       283             316             366             397  

Chg in Working Capital      (215)           (202)             (69)             (74) 

Others         36               38               92               89  

Tax paid      (116)           (120)           (199)           (256) 

Cash flow from operating 

activities 
      645             567             928          1,102  

Capital expenditure      (674)        (1,334)           (700)           (700) 

Chg in investments        (22)              74               -                 -    

Other investing cashflow         83               69               73               76  

Cash flow from investing 

activities 
    (613)        (1,191)           (627)           (624) 

Equity raised/(repaid)              -                 -                 -                 -    

Debt raised/(repaid)       (91)            781               -                 -    

Dividend paid       (83)             (84)             (98)             (98) 

Other financing activities       (70)           (124)           (165)           (165) 

Cash flow from financing 

activities 
     (243)            573            (264)           (264) 

Net chg in cash     (211)             (51)              37             215  

 

Y/E (Rs. Cr)  FY17  FY18 FY19E FY20E 

Total operating Income         4,822          5,589          6,289          7,016  

EBITDA            969             906          1,196         1,432  

Depreciation            283             316             366             397  

EBIT            686             590             830          1,035  

Interest cost             102             124             165             165  

Other Income              73               69               73               76  

Profit before tax            657             535             738             946  

Tax            142             120             199             256  

Profit after tax            515             415             539             691  

Minority Interests              -                 -                 -                 -    

P/L from Associates              -                 -                 -                 -    

Adjusted PAT            515             415             539             691  

E/o income / (Expense)              -                 46               -                 -    

Reported PAT            515             462             539             691  

 
 

Y/E    FY17  FY18 FY19E FY20E 

Valuation(x)         
P/E 19.2 23.8 18.4 14.3 

EV/EBITDA 12.2 13.9 10.5 8.6 

EV/Net Sales 2.5 2.3 2.0 1.8 

P/B 3.1 2.8 2.5 2.2 

Per share data     

EPS 88.1 71.1 92.2 118.2 

DPS 12.0 12.0 14.0 14.0 

BVPS 544.6 609.9 685.3 786.7 

Growth (%)     

Net Sales 4.8 16.3 14.1 11.6 

EBITDA 0.7 -6.5 31.9 19.8 

Net profit 19.8 -19.3 29.7 28.2 

Operating Ratios     

EBITDA Margin (%) 20.1 16.2 19.0 20.4 

EBIT Margin (%) 14.2 10.6 13.2 14.8 

PAT Margin (%) 10.7 7.4 8.6 9.8 

Return Ratios (%)     

RoE 17.3 12.3 14.2 16.1 

RoCE 15.1 11.5 13.8 15.7 

Turnover Ratios (x)     

Net Sales/GFA 1.0 1.0 1.0 1.0 

 

 

 



 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Rating Criteria 
    Large Cap. Return Mid/Small Cap. Return 

Buy More than equal to 10% Buy More than equal to 15% 

Hold Upside or downside is less than 10% Accumulate* Upside between 10% & 15% 

Reduce Less than equal to -10% Hold Between 0% & 10% 

  Reduce/sell Less than 0% 

* To satisfy regulatory requirements, we attribute ‘Accumulate’ as Buy and ‘Reduce’ as Sell. 

* SRF is a mid-cap company. 

 

Disclaimer:  

 

The SEBI registration number is INH200000394. 

The analyst for this report certifies that all the views expressed in this report accurately reflect his / her personal views about the subject 

company or companies, and its / their securities. No part of his / her compensation was / is / will be, directly / indirectly related to specific 

recommendations or views expressed in this report. 

This material is for the personal information of the authorized recipient, and no action is solicited on the basis of this. It is not to be 

construed as an offer to sell, or the solicitation of an offer to buy any security, in any jurisdiction, where such an offer or solicitation would 

be illegal. 

We have reviewed the report, and in so far as it includes current or historical information, it is believed to be reliable, though its accuracy or 

completeness cannot be guaranteed. Neither Wealth India Financial Services Pvt. Ltd., nor any person connected with it, accepts any 

liability arising from the use of this document. The recipients of this material should rely on their own investigations and take their own 

professional advice. Price and value of the investments referred to in this material may go up or down. Past performance is not a guide for 

future performance. 

We and our affiliates, officers, directors, and employees worldwide: 

1. Do not have any financial interest in the subject company / companies in this report; 
2. Do not have any actual / beneficial ownership of one per cent or more in the company / companies mentioned in this document, or 

in its securities at the end of the month immediately preceding the date of publication of the research report, or the date of public 
appearance; 

3. Do not have any other material conflict of interest at the time of publication of the research report, or at the  time of public 
appearance; 

4. Have not received any compensation from the subject company / companies in the past 12 months; 
5. Have not managed or co-managed the public offering of securities for the subject company / companies in the past 12 months; 
6. Have not received any compensation for investment banking, or merchant banking, or brokerage services from the subject 

company / companies in the past 12 months; 
7. Have not served as an officer, director, or employee of the subject company; 
8. Have not been engaged in market making activity for the subject company; 

This document is not for public distribution. It has been furnished to you solely for your information, and must not be reproduced or 

redistributed to any other person. 
 

 
 

Funds India 
Uttam Building, Third Floor| 
No. 38 & 39| Whites Road| 
Royapettah|Chennai – 600014| 
T: +91 7667 166 166  
Email: contact@fundsindia.com 
 
 

 

 

 Contact Us: 

For private circulation only 

mailto:contact@fundsindia.com


 

 

 

 

 

Dion’s Disclosure and Disclaimer 
 

I, Abhijit Kumar Das, employee of Dion Global Solutions Limited (Dion) is engaged in preparation of this report and hereby certify that 
all the views expressed in this research report (report) reflect my personal views about any or all of the subject issuer or securities.  

Disclaimer 

This report has been prepared by Dion and the report & its contents are the exclusive property of the Dion and the client cannot 

tamper with the report or its contents in any manner and the said report, shall in no case, be further distributed to any third party 

for commercial use, with or without consideration.  

Recipient shall not further distribute the report to a third party for a commercial consideration as this report is being furnished to 
the recipient solely for the purpose of information.  
 
Dion has taken steps to ensure that facts in this report are based on reliable information but cannot testify, nor make any 
representation or warranty, express or implied, to the accuracy, contents or data contained within this report. It is hereby 
confirmed that wherever Dion has employed a rating system in this report, the rating system has been clearly defined including the 
time horizon and benchmarks on which the rating is based.   
 
Descriptions of any company or companies or their securities mentioned herein are not intended to be complete and this report is 
not, and should not be construed as an offer or solicitation of an offer, to buy or sell any securities or other financial instruments. 
Dion has not taken any steps to ensure that the securities referred to in this report are suitable for any particular investor. This 
report is not to be relied upon in substitution for the exercise of independent judgment. Opinions or estimates expressed are 
current opinions as of the original publication date appearing on this report and the information, including the opinions and 
estimates contained herein, are subject to change without notice. Dion is under no duty to update this report from time to time.  
 
Dion or its associates including employees engaged in preparation of this report and its directors do not take any responsibility, 
financial or otherwise, of the losses or the damages sustained due to the investments made or any action taken on basis of this 
report, including but not restricted to, fluctuation in the prices of securities, changes in the currency rates, diminution in the NAVs, 
reduction in the dividend or income, etc.  
 
The investments or services contained or referred to in this report may not be suitable for all equally and it is recommended that an 
independent investment advisor be consulted. In addition, nothing in this report constitutes investment, legal, accounting or tax 
advice or a representation that any investment or strategy is suitable or appropriate to individual circumstances or otherwise 
constitutes a personal recommendation of Dion. 
 
REGULATORY DISCLOSURES: 

Dion is engaged in the business of developing software solutions for the global financial services industry across the entire 

transaction lifecycle and inter-alia provides research and information services essential for business intelligence to global companies 

and financial institutions. Dion is listed on BSE Limited (BSE) and is also registered under the SEBI (Research Analyst) Regulations, 

2014 (SEBI Regulations) as a Research Analyst vide Registration No. INH100002771. Dion’s activities were neither suspended nor has 

it defaulted with requirements under the Listing Agreement and / or SEBI (Listing Obligations and Disclosure Requirements) 

Regulations, 2015 with the BSE in the last five years. Dion has not been debarred from doing business by BSE / SEBI or any other 

authority.  

 

In the context of the SEBI Regulations, we affirm that we are a SEBI registered Research Analyst and in the course of our business, 

we issue research reports /research analysis etc that are prepared by our Research Analysts. We also affirm and undertake that no 

disciplinary action has been taken against us or our Analysts in connection with our business activities.   

In compliance with the above mentioned SEBI Regulations, the following additional disclosures are also provided which may be 

considered by the reader before making an investment decision:  

For private circulation only 



 

 

 

 

 

 

 

1.  Disclosures regarding Ownership  

Dion confirms that: 

(i) Dion/its associates have no financial interest or any other material conflict in relation to the subject company (ies) 

covered herein at the time of publication of this report. 

   

(ii) It/its associates have no actual / beneficial ownership of 1% or more securities of the subject company (ies) covered 

herein at the end of the month immediately preceding the date of publication of this report. 

   

Further, the Research Analyst confirms that: 

(i) He, his associates and his relatives have no financial interest in the subject company (ies) covered herein, and they 

have no other material conflict in the subject company at the time of publication of this report. 

   

(ii)   he, his associates and his relatives have no actual/beneficial ownership of 1% or more securities of the subject 

company (ies) covered herein at the end of the month immediately preceding the date of publication of this report.  

   

2.  Disclosures regarding Compensation:   

During the past 12 months, Dion or its Associates: 

(a) Have not managed or co-managed public offering of securities for the subject company (b) Have not received any compensation 

for investment banking or merchant banking or brokerage services from the subject company (c) Have not received any 

compensation for products or services other than investment banking or merchant banking or brokerage services from the subject. 

(d) Have not received any compensation or other benefits from the subject company or third party in connection with this report  

 

3.  Disclosure regarding the Research Analyst’s connection with the subject company: 

It is affirmed that I, Abhijit Kumar Das employed as Research Analyst by Dion and engaged in the preparation of this report have not 

served as an officer, director or employee of the subject company 

 

4.  Disclosure regarding Market Making activity: 

Neither Dion /its Research Analysts have engaged in market making activities for the subject company. 

 
Copyright in this report vests exclusively with Dion. 

 

 

 

 

 

 

 

 

 

 

For private circulation only 


