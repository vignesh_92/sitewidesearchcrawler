
 

 

 

 

. 

 

. 

 

 

 

 

 

 

 

 

 

0

50

100

150

A
p

r-
1
5

M
a

y
-1

5

J
u
n

-1
5

J
u
l-
1

5

A
u

g
-1

5

S
e

p
-1

5

O
c
t-

1
5

N
o
v
-1

5

D
e
c
-1

5

J
a
n

-1
6

F
e

b
-1

6

M
a

r-
1
6

A
p

r-
1
6

BOSCHLTD NIFTY

One year Price Chart 

Readying itself for next leg of growth 

Bosch Ltd, promoted by Robert Bosch GmbH, is India’s leading auto 

ancillary company. It is a dominant player in the diesel engine segment 

with ~70% market share. In FY15, Bosch derived 88% & 12% of its 

turnover from auto & non-auto segment respectively. 
 

Investment Rationale 

    Pricing power to remain intact on the back of leadership position & 

technological excellence: Bosch has a dominant ~70% market share in India for 

diesel fuel injection products. It is the leading provider of groundbreaking 

automotive technologies and services. Being a dominant player in fuel injection 

(FI) segment, FI products contribute ~70% to the company’s revenue. Bosch 

Group (parent) spends ~10% of its turnover every year on R&D. Further, Bosch 

Group provides most of its technologies at a low royalty rate (1.6% of its 

turnover) to the Indian arm (Bosch Ltd). Thus, Bosch Ltd, stands to benefit from 

the technology leadership profile of its parent as implementation of new and 

advanced technologies pick up ground in India. Hence, pricing power would be 

maintained given its leadership in technology. 

 Early implementation of stricter emission norms to drive growth 

ahead: In order to tackle air pollution, the government has announced to 

upgrade to stricter fuel standards (BS VI) from 2020 (skipping BS V altogether). 

BS VI was originally proposed to come in by 2024. Further, BS IV emission norms 

would be applicable across India from April 2017. Therefore, Bosch emerges as 

one of the main beneficiaries as it is a key supplier for fuel injection system for 

vehicles. As per industry estimates, implementation of BS IV on pan-India basis 

offers ~Rs 5,000 crores opportunity annually. Hence, we expect FI segment to 

grow at a CAGR of 18.3% over FY15-FY18E. The commitment of government to 

combat pollution would lead to content increase with common-rail in BS IV 

(2017) and selective catalytic reduction (SCR) in BS VI (2020). Moreover, major 

competitors such as Delphi and Denso do not have presence in India with SCR, a 

key technology for BS VI. Going forward, this would boost realisations for Bosch. 

      Non-Auto business aids in providing revenue diversification: During 

CY11-FY15, the non-auto business grew at a CAGR of 13.3%. While non-auto 

business of Bosch Ltd contributes ~12% to the overall revenues, global non-auto 

business’ contribution is ~32%. This business has a robust growth potential and 

is expected to benefit from the pick up in the economic activity. We expect this 

business to grow at a CAGR of 15.2% over FY15-FY18E. 

Valuation: Bosch is in a sweet spot given its leadership position, technology 

focus and unique positioning in the Indian auto industry. We expect revenue and 

PAT to grow at a CAGR of 4.5% and 11.6% over FY15-FY18E. Further, we rate the 

stock as ‘BUY’ assigning a forward P/E of 36.5x (given acceleration in growth 

trajectory due to advanced emission norms) arriving at a target price of Rs.   

22,083 which implies potential upside of ~10% for next 12 months.  

Rating   BUY 
CMP (Rs.)   20,082 

Target (Rs.)   22,083 
Potential Upside (%)   10 

Duration   Long Term 

Face Value (Rs.)   10.0 

52 week H/L (Rs.)   26,797/15,736 

Adj. all time High 
(Rs.) 

  27,990 

Decline from 52WH 
(%) 

  25.1 

Rise from 52WL (%)   27.6 

Beta   0.6 

Mkt. Cap (Rs.Cr)   63,057 

 

Market Data 

Y/E 
FY15 (15 
months) 

FY16E FY17E FY18E 

Revenue 
(Rs.Cr) 

 12,086   10,705   11,720   13,800  

Adj. Profit 
(Rs.Cr) 

1,366  1,188  1,505 1,900 

EPS (Rs.)  434.9   378.4   479.2   605.0  

P/E (x) 58.4 53.1 41.9 33.2 

P/BV (x)  10.9   7.6   6.7   5.8  

ROE (%)  20.0   15.2   17.0   18.7  

 

Fiscal Year Ended 

 

Apr 4th, 2016 

 

 BSE Code: 500530            NSE Code: BOSCHLTD                   Reuters Code: BOSH.NS                   Bloomberg Code: BOS:IN 

Volume No. I Issue No. 65 Bosch Ltd 

For private circulation only 
 

 

 

Shareholding Pattern 

 

Dec-15 Sep-15 Chg.  

 Promoters (%) 71.2 71.2 0.0  

FII (%) 7.7 8.4 (0.7) 

DII (%) 11.5 11.0 0.5 

Others (%) 9.6 9.5  0.1 

 



 

 

 

 

  

 
 
 

Bosch Ltd: Dominant player in the fuel injection segment 

Bosch, promoted by Robert Bosch GmbH (holds 71.18% stake), is India’s leading auto ancillary 

company. It is a dominant player in the diesel engine segment with ~70% market share. The 

company has a broad-based product portfolio of diesel and gasoline fuel injection systems, 

automotive aftermarket products, starter motors & generators, special purpose machines, 

packaging machines, electric power tools, security systems etc. The automotive segment 

contributes 88% to the overall revenues. The company also has one of the largest distribution 

network of spare parts in the country, with aftermarket business accounting for ~20% of 

revenues. Its key manufacturing facilities are located at Bengaluru, Nashik, Naganathapura, 

Jaipur, Gangaikondan, Goa and Bidadi. 
 

                                                    Segment-wise Revenue Mix                     
 

 

 

 

Business divisions of Bosch Ltd 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 

Bosch’ Product Portfolio  
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 

Source: Company, In-house research 

Source: Company, In-house research; *: Assuming shareholders nod for the sale of SG division  

Automotives 
Products, 88%

Non-Auto, 12%

 

Bosch Ltd is a dominant 

player in the diesel engine 

segment with ~70% market 

share. 

For private circulation only 

The Board has approved the 

sale of its starter motor 

&generators (SG) division to a 

100% subsidiary of its parent 

for Rs 486 crores. SG 

constitutes ~10 % of Bosch Ltd 

turnover and around 1% of 

Bosch Ltd EBIT. 

Diesel Systems           Gasoline Systems     Packaging Technology        Power Tools 

 

     
Automotive           Starter Motors *            Energy & Building Solns.        Security Technology 

Aftermarket           & Generators              & Thermo-technology  

              

Business 

Line 

 

Segments 

 

Products 

 

Target Segment 

 

 

 

 

Automotive 

 

Gasoline Direct gasoline injection PV 

Gasoline port injection 

Diesel Common rail systems CV, PV, Tractors 

Electric drives 

 

Actuators All 

Pumps & Valves 

Electronics Electronic control units All 

Mechatronic modules 

 

 

 

 

Non-Automotive 

Power tools 

 

Surveying equipment, Range Finders Construction,  Wood & 

Metal working Impact wrenches, Drill Machines 

Industrial equipment 

 

Metal cutting machines  

Industrial 
Assembly equipment 

Packaging solutions 

 

Form, fill & seal machines Pharmaceuticals, Food 

Flow wrap machines 

Security systems IP-based CCTV surveillance & Access Control 

Systems 

Hotels, Metro Rail, 

Stadium 

Fire Alarm & Intrusion systems 

 



 

 

 

 

  Cyclical recovery in CVs and PVs to drive strong growth 

During FY11-15, automotive volumes (excluding 2Ws) grew at a CAGR of ~2%. Of late, there 

has been a visible traction in the CV space (YTD FY16 growth was 9% YoY) aided by growing 

demand from the infrastructure sector and the opening up of the mining sector. Going 

forward, with meaningful recovery in overall capex cycle, CV segment in India is expected to 

grow at a CAGR of ~14-17% over the next two years. With, CVs contributing about 50% to the 

company’s revenues, demand recovery in the CV space coupled with BS-IV compliance on 

pan-India basis by April 2017 bodes well for the company. Similarly, PV segment is expected to 

grow at a CAGR of 11-13% over the next two years on the back of new product launches (PV 

contributes nearly 15% to the overall revenues). While pressure on the tractor segment is 

likely to continue in the near term, it may improve, going forward. After two consecutive 

years of poor monsoon, it is predicted that India will receive normal rainfall this year. Besides, 

government has recently taken several initiatives (crop insurance, enhanced allocation for 

NREGA in Union Budget) to revive rural growth. 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 

              

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 Engine parts constitute ~31% 

of the total auto ancillary 

demand. 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

For private circulation only 

  

    Bosch caters to ~40% of Indian auto ancillary industry         Bosch’s revenue growth is in sync with auto industry volumes 

 
 

Source: ACMA, In-house research                                                                                                                                                                                             

 

 

 

 
 

 

 

 

Engine 
Parts, 
31%

Drive 
Transmission 

& Steering 
Parts, 19%

Body & 
Chassis, 

12%

Suspension & 
Braking Parts, 

12%

Equipments, 
10%

Electrical 
Parts, 9%

Others, 7%

28.2

9.1
-1.0 -5.8

5.0

39.6
13.9

6.0 1.0
10.0

-50.0

0.0

50.0

FY11 FY12 FY13 FY14 FY15

(%
)

Automotive Industry Volume Growth (%)

Bosch Net Sales Growth (%)

Early implementation of stricter emission norms to drive growth ahead 

In order to tackle air pollution, the government has announced to upgrade to stricter fuel 

standards (BS VI). India will be the first country worldwide to skip one level of emission 

legislation (BS V). Implementation of the BS V standard was earlier scheduled for 2019. BS VI, 

originally proposed to come in by 2024 has been now advanced to 2020, instead. Further, BS 

IV emission norms would be applicable across India from April 2017. Already, BS-IV is 

applicable in almost all major cities including Delhi-NCR, Mumbai, Chennai, Kolkata and 

Hyderabad. 

Bosch emerges as one of the main beneficiaries as it is a key supplier for fuel injection system 

for vehicles. Thus, implying incremental revenue opportunity for Bosch Ltd & we expect FI 

segment to grow at a CAGR of 18.3% over FY15-FY18E. While almost all PVs sold in India are 

already BS IV compliant, majority of the CVs are still running on BS III standard. Hence, the 

incremental changes required in CV segment would give an impetus to company’s powertrain 

business. As per industry estimates, implementation of BS IV on pan-India basis offers ~Rs 

5,000 crores opportunity annually.  

Nationwide rollout of BS Emission Norms in India: Timeline 
 

Emission Norm Deadline 

Bharat Stage I 2000 

Bharat Stage II 2005 

Bharat Stage III 2010 

Bharat Stage IV 2017 

Bharat Stage V Skipped 

Bharat Stage VI 2020 

Source: Ministry of Road Transport and Highways 

 

 

 

 

 

 

 The government has decided 

to implement stricter emission 

norms of Bharat Stage (BS) VI 

from April 1, 2020, by skipping 

BS V altogether. 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Source: Company, In-house research 

 



 

 

 

 

  

BS IV 

Implementation 
BS III 

Implementation 

BS II 

Implementation 

Historically, it has been seen that whenever advanced emission norms are implemented 

nation-wide, revenues rose sharply in that particular year. Generally, with each change in the 

stage of an emission norm, there is an increase in content, leading to Bosch outperforming 

the industry growth. In CY10, when BS III standard was implemented, Bosch’s revenues grew 

~40% aided by a sharp recovery post the global financial crisis. Similarly, revenues grew ~25% 

in CY05 when BS III standard was implemented. 

Going forward, the government is planning to skip BS V standard by migrating directly to BS 

VI from BS IV. This move would lead to content increase with common-rail in BS IV (2017) 

and selective catalytic reduction (SCR) in BS VI (2020). The major competitors such as Delphi 

and Denso do not have presence in India with SCR, a key technology for BS VI. Further, Bosch 

sees opportunities in the Indian two-wheeler market as this segment is expected to be 

covered under BS VI norms. Currently 2Ws in India are carburetor based & they too will be 

required to shift to fuel injection systems. 

Implementation of advanced emission norms augur well for Bosch’s sales growth 
 

 
Source: Company, In-house research 
 

Pricing power to remain intact on the back of leadership position & technological 

excellence 

Bosch has a dominant ~70% market share in India for diesel fuel injection products. It has 

been the leading provider of groundbreaking automotive technologies and services for over 

nine decades in India. Being a dominant player in fuel injection (FI) segment, FI products 

contribute ~70% to the company’s revenue. 

Bosch Ltd enjoys the benefits of its global parent’s strong technology dominance and, 

thereby, has a higher new product acceptance rate among OEMs. Over years, Bosch Ltd has 

imported newer technologies in the wake of emission changes, which have helped it to cater 

to the Indian market. Bosch Ltd has generally followed the policy of importing the technology 

and then gradually localising it as its acceptance increases in the market. 

The Bosch Group enjoys a strong technological leadership in fuel injection systems and is a 

trusted ancillary partner for most global auto manufactures. This is on account of the huge 

R&D spend that is carried out by Bosch Global. In 2014, the Bosch Group spent ~5 billion 

euros (around 10% of sales revenue) and filed 4,593 patents worldwide. Interestingly, Bosch 

Group (parent) offers most of its technologies at a low royalty rate (~1.6% of its turnover) to 

Bosch Ltd. 

The major competitors of Bosch Global are Delphi & Denso Corp. Both these competitors 

supply products in segments ranging from fuel injection systems to exhaust systems. 

However, Bosch Global remains the leader in the space. More importantly, in India, both the 

competitors have limited penetration. 

EBITDA margins have remained steady above 15% for all years (except CY13) in the last 5 

years. Given its leadership in technology, it is expected that pricing power would be 

maintained going forward.  

 

22% 23%
28% 27%

13% 6% 5%

40%

20%
6% 1% 10% 11% 9%

18%

C
Y0

3

C
Y0

4

C
Y0

5

C
Y0

6

C
Y0

7

C
Y0

8

C
Y0

9

C
Y1

0

C
Y1

1

C
Y1

2

C
Y1

3

FY
1

5
 (

A
d

j)

FY
1

6
E

FY
1

7
E

FY
1

8
E

For private circulation only 
 

 

Bosch Group offers most of its 

technologies at a low royalty 

rate (~1.6% of its turnover) to 

the Indian arm. 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 



 

 

 

 

  

 Bosch derives about 20% of 

revenues from the automotive 

aftermarket division 

 

 

 

 

While non-auto business of 

Bosch Ltd contributes ~12% to 

the overall revenues, global non-

auto business’ contribution is 

~32%. 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Non-Auto business aids in providing revenue diversification 

The revenue contribution of non-auto business to Bosch’s topline has risen from 10% in 

CY11 to 12% in FY15. The non-auto business comprises of three verticals: Industrial 

Technology, Consumer Goods (Power Tools & Household appliances) and Energy & Building 

Technology. While non-auto business of Bosch Ltd contributes ~12% to the overall revenues, 

global non-auto business’ contribution is ~32%. This business has a strong growth potential 

and is expected to benefit from the pick up in the economic activity. We expect this business 

to grow at a CAGR of 15.2% over FY15-FY18E.  

Structure of Bosch’s non-auto business 

 

 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Non-Auto’s contribution to the total revenues on the rise 
 

 
Source: Company, In-house research; Note: FY15 is a 15 month period due to change in accounting year 
 

Traction in Automotive aftermarket business augurs well  

Bosch derives about 20% of revenues from the automotive aftermarket division. This 

division is responsible for the supply, sales & distribution of all Bosch-branded automotive 

parts in India and the SAARC region. The product range offered is the largest under one 

brand in India and finds extensive application in 2Ws, 3Ws, cars, MUVs, LCVs, HCVs, buses, 

tractors etc. The Bosch automotive aftermarket distribution network is the largest in India, 

with over 1,000 authorized distributors, over 3,000 authorized workshops and direct 

distribution reach beyond 60,000 semi-wholesale and retail points with presence in all the 

key markets.  

The products marketed by this division include diesel and gasoline fuel injection system & 

components, alternators, starter motors, spark plugs, automotive filters, automotive 

batteries, automotive belts, automotive software,2& 3 wheeler clutch plates etc. 

Besides, it is responsible for Bosch service workshop concepts for vehicle service and 

maintenance. It manages the largest independent service network in India with over 3,000 

workshops/ service network comprising over 500 Bosch Car Service, 1,000 Bosch Diesel 

Service Centers, 600 Electric Modules, 250 Express Car Service and 150 Express Bike Service 

in India, covering ~1,200 cities. Hence, we expect this division to grow at a CAGR of 18% over 

FY15-FY18E led by new products introduction, increased use of electronics coupled with 

increased preference for authorised services.      
 

 

11%
12% 12% 12%

13% 13%

5%

8%

11%

14%

CY12 CY13 FY15 FY16E FY17E FY18E

 

 

 

For private circulation only 
 

 Non-Auto 

Consumer 

Goods 

(Power Tools 

Industrial 

Technology 
Energy & 

Building 

Technology 

Industrial 

Equipment 

Packaging 

Technology 
Security 

Technology 

Bosch 

Energy & 

Building 

Solutions 

Thermotech

nology 



 

 

 

 

 
We expect top-line of the 
company to grow at a CAGR of 
4.5% over FY15-FY18E. 

  Automotive Aftermarket revenues to grow at a CAGR of 18% during FY15-FY18E 
 

 

 
 

 
 

 
 
Source: Company, In-house research; Note: FY15 is a 15 month period due to change in accounting year 

 

Overall EBITDA margins to expand significantly, going forward 

Engine parts are impacted the most by emission norm changes. We believe there is significant 

scope of margin expansion in the coming years led by the implementation of advanced 

emission norms (BS IV in 2017 & BS VI in 2020) across the country. The stricter norms would 

lead to content increase with common-rail in BS IV (2017) and SCR in BS VI (2020). Given 

limited competition in this space, we expect realisations to increase for Bosch. Further, the 

sale of starter motor & generator division would provide fillip to the overall margins (this 

division constitutes ~10% of Bosch Ltd turnover and merely 1% of Bosch Ltd EBIT). Thus, we 

believe Bosch’s EBITDA margin to grow to 19.5% in FY18E from 16.4% in FY15. 
 

Revenue and PAT to grow at a CAGR of 4.5% and 11.6% respectively over FY15-18E 

During FY15-FY18E, we expect the top-line of the company to grow at a CAGR of 4.5% on the 

back of regulation requirements (BS IV in 2017 & BS VI in 2020) which will lead to greater-

than-normal content increase. Further, we estimate 11.6% CAGR in Adjusted PAT over FY15-

18E mainly on account of EBITDA margin expansion. Moreover, we believe that the company 

would report improvement in its ROE and ROCE on the back of healthy profitability coupled 

with strong revenue growth. While ROE is likely to improve from 15.2% in FY16E to 18.7% in 

FY18E, ROCE is projected to increase from 22.6% in FY16E to 27.1% in FY18E. 

 

 

 

    Revenue to grow at a CAGR of  4.5% over FY15-FY18E                           Return Ratios expected to improve  
 

 

  
 

Source: Company, In-house research; *: FY15 is a 15 month period due to change in accounting year 

16.4% 16.5%
18.5% 19.5%

11.3% 11.1%
12.8% 13.8%

0%

5%

10%

15%

20%

25%

 -
 2,000
 4,000
 6,000
 8,000

 10,000
 12,000
 14,000
 16,000

FY15* FY16E FY17E FY18E

R
s.

 C
ro

re
s

Revenue EBITDA Margin (%) Adj. PAT Margin (%)

20.0%

15.2%
17.0%

18.7%

28.9%

22.6%
24.6%

27.1%

0%

5%

10%

15%

20%

25%

30%

35%

FY15* FY16E FY17E FY18E

ROE (%) ROCE (%)

 

 

 

For private circulation only 
 

 Key Risks: 

1 Slowdown in CV space may affect the revenue growth. 

2 Any delay in implementation of advanced emission norms. 

3 Upward revision in royalty rates may impact margins. 

 

 

 

 

 

1,917 1,965 2,024 2,327 
2,746 

3,323 

 500

 1,500

 2,500

 3,500

CY12 CY13 FY15 FY16E FY17E FY18E

(R
s.

 C
ro

re
s)



 

 

 

 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Balance Sheet  

Profit & Loss Account (Consolidated) 

 

Y/E (Rs.Cr) FY15* FY16E FY17E FY18E 

Total operating 

Income 
 12,086   10,705   11,720   13,800  

Raw Material cost  6,457   5,654   6,179   7,169  

Employee Cost  1,663   1,375   1,505   1,772  

Other operating 

expenses 
 1,984   1,906   1,870   2,175  

EBITDA  1,981   1,770   2,166   2,685  

Depreciation  548   399   448   493  

EBIT  1,433   1,372   1,719   2,192  

Interest cost  14   5   2   2  

Other income  565   399   464   563  

Profit before tax  1,984   1,765   2,181   2,753  

Tax  618   577   676   854  

Profit after tax  1,366   1,188   1,505   1,900  

Minority Interests  -     -     -     -    

P/L from Associates  -     -     -     -    

Adjusted PAT  1,366   1,188   1,505   1,900  

E/oincome/ (Expense)  (28)  -     -     -    

Reported Profit  1,338   1,188   1,505   1,900  

 
 

Y/E (Rs.Cr) FY15* FY16 FY17E FY18E 

Paid up capital  31   31   31   31  

Reserves and 

Surplus 
 7,316   8,224   9,390   10,883  

Net worth  7,347   8,256   9,421   10,915  

Minority Interest  -     -     -     -    

Total Debt  56   35   15   15  

Other non-current 

liabilities 
 479   526   579   637  

Total Liabilities  7,881   8,817   10,015   11,567  

Total fixed assets 

(inc CWIP) 
 1,244   1,345   1,497   1,605  

Goodwill  -     -     -     -    

Investments  2,890   2,890   2,890   2,890  

Net Current 

assets 
 3,112   3,925   4,946   6,364  

Other non-current 

assets 
 636   658   682   709  

Total Assets  7,881   8,817   10,015   11,567  

 

 Cash Flow Statement  

 

Profit & Loss Account  

 

 

Profit & Loss Account (Consolidated) 

 

 

Y/E  (Rs.Cr) FY15* FY16E FY17E FY18E 

Pretax profit  1,956   1,765   2,181   2,753  

Depreciation  548   399   448   493  

Chg in Working 

Capital 
 132   (224)  (130)  (257) 

Others  (551)  (394)  (462)  (561) 

Tax paid  (691)  (577)  (676)  (854) 

Cash flow from 

operating activities 
 1,394   969   1,360   1,574  

Capital expenditure  (409)  (500)  (600)  (600) 

Chg in investments  (446)  -     -     -    

Other investing 

cashflow 
 (325)  399   464   563  

Cash flow from 

investing activities 
 (1,180)  (101)  (136)  (37) 

Equity 

raised/(repaid) 
 -     -     -     -    

Debt raised/(repaid)  (28)  (20)  (20)  -    

Dividend paid  (202)  (280)  (339)  (406) 

Other financing 

activities 
 (9)  (5)  (2)  (2) 

Cash flow from 

financing activities 
 (238)  (305)  (361)  (409) 

Net chg in cash  (23)  563   863   1,129  

* Change in accounting year, FY15 is a 15 month period 

Note: Assuming company to receive shareholders approval for the sale of starter motors 

& generators division. But, we haven’t included any Profit/Loss from the sale of this 

division. Further, assuming the company to carve out this division by the end of June 

2016. 

 

 

 

 Y/E   FY15* FY16E FY7E FY18E 

Valuation (x) 
   

  

P/E  58.4   53.1   41.9   33.2  

EV/EBITDA  39.4   34.3   27.6   21.8  

EV/Net Sales  6.6   5.8   5.2   4.4  

P/B  10.9   7.6   6.7   5.8  

Per share data (Rs.)     

EPS  434.9   378.4   479.2   605.0  

DPS  85.0   74.0   89.7   107.5  

BVPS 
 

2,339.8  

 

2,629.1  

 

3,000.4  

 

3,476.0  

Growth (%)     

Net Sales  37.9   (11.3)  9.5   17.8  

EBITDA  53.7   (10.7)  22.4   24.0  

Net Profit  54.4   (13.0)  26.6   26.3  

Operating Ratios (%)      

EBITDA Margin  16.4   16.5   18.5   19.5  

EBIT Margin  11.9   12.8   14.7   15.9  

PAT Margin  11.3   11.1   12.8   13.8  

Return Ratios (%) 
    

RoE  20.0   15.2   17.0   18.7  

RoCE  28.9   22.6   24.6   27.1  

Turnover Ratios (x) 
    

Net Sales/GFA  2.6   2.1   2.0   2.1  

Sales/Total Assets  1.2   1.0   0.9   1.0  

Sales/Working Capital  10.0   7.8   7.4   7.6  

Liquidity&Solvency Ratios (x)     

Current Ratio  2.2   2.6   2.8   3.1  

Net Debt/Equity  (0.3)  (0.3)  (0.4)  (0.4) 

 

 

 
For private circulation only 

Key Ratios  



 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Rating Criteria 
 

    Large Cap. Return Mid/Small Cap. Return 

Buy More than equal to 10% Buy More than equal to 15% 

Hold Upside or downside is less than 10% Accumulate* Upside between 10% & 15% 

Reduce Less than equal to -10% Hold Between 0% & 10% 

  Reduce/sell Less than 0%. 

* To satisfy regulatory requirements, we attribute ‘Accumulate’ as Buy and ‘Reduce’ as Sell. 

 

 

Disclaimer:  

 

The SEBI registration number is INH200000394. 

The analyst for this report certifies that all the views expressed in this report accurately reflect his / her personal views about the subject 

company or companies, and its / their securities. No part of his / her compensation was / is / will be, directly / indirectly related to specific 

recommendations or views expressed in this report. 

This material is for the personal information of the authorized recipient, and no action is solicited on the basis of this. It is not to be 

construed as an offer to sell, or the solicitation of an offer to buy any security, in any jurisdiction, where such an offer or solicitation would 

be illegal. 

We have reviewed the report, and in so far as it includes current or historical information, it is believed to be reliable, though its accuracy or 

completeness cannot be guaranteed. Neither Wealth India Financial Services Pvt. Ltd., nor any person connected with it, accepts any 

liability arising from the use of this document. The recipients of this material should rely on their own investigations and take their own 

professional advice. Price and value of the investments referred to in this material may go up or down. Past performance is not a guide for 

future performance. 

We and our affiliates, officers, directors, and employees worldwide: 

1. Do not have any financial interest in the subject company / companies in this report; 
2. Do not have any actual / beneficial ownership of one per cent or more in the company / companies mentioned in this document, or 

in its securities at the end of the month immediately preceding the date of publication of the research report, or the date of public 
appearance; 

3. Do not have any other material conflict of interest at the time of publication of the research report, or at the  time of public 
appearance; 

4. Have not received any compensation from the subject company / companies in the past 12 months; 
5. Have not managed or co-managed the public offering of securities for the subject company / companies in the past 12 months; 
6. Have not received any compensation for investment banking, or merchant banking, or brokerage services from the subject 

company / companies in the past 12 months; 
7. Have not served as an officer, director, or employee of the subject company; 
8. Have not been engaged in market making activity for the subject company; 

This document is not for public distribution. It has been furnished to you solely for your information, and must not be reproduced or 

redistributed to any other person. 
 

 

 
 

Funds India 
Uttam Building, Third Floor| 
No. 38 & 39| Whites Road| 
Royapettah|Chennai – 600014| 
T: +91 7667 166 166  
Email: contact@fundsindia.com 
 
 

 

 

 Contact Us: 

For private circulation only 

mailto:contact@fundsindia.com


 

 

 

 

 

Dion’s Disclosure and Disclaimer 
 

I, Rohit Joshi, employee of Dion Global Solutions Limited (Dion) is engaged in preparation of this report and hereby certify that all the 
views expressed in this research report (report) reflect my personal views about any or all of the subject issuer or securities.  

Disclaimer 

This report has been prepared by Dion and the report & its contents are the exclusive property of the Dion and the client cannot 

tamper with the report or its contents in any manner and the said report, shall in no case, be further distributed to any third party 

for commercial use, with or without consideration.  

Recipient shall not further distribute the report to a third party for a commercial consideration as this report is being furnished to 
the recipient solely for the purpose of information.  
 
Dion has taken steps to ensure that facts in this report are based on reliable information but cannot testify, nor make any 
representation or warranty, express or implied, to the accuracy, contents or data contained within this report. It is hereby 
confirmed that wherever Dion has employed a rating system in this report, the rating system has been clearly defined including the 
time horizon and benchmarks on which the rating is based.   
 
Descriptions of any company or companies or their securities mentioned herein are not intended to be complete and this report is 
not, and should not be construed as an offer or solicitation of an offer, to buy or sell any securities or other financial instruments. 
Dion has not taken any steps to ensure that the securities referred to in this report are suitable for any particular investor. This 
report is not to be relied upon in substitution for the exercise of independent judgment. Opinions or estimates expressed are 
current opinions as of the original publication date appearing on this report and the information, including the opinions and 
estimates contained herein, are subject to change without notice. Dion is under no duty to update this report from time to time.  
 
Dion or its associates including employees engaged in preparation of this report and its directors do not take any responsibility, 
financial or otherwise, of the losses or the damages sustained due to the investments made or any action taken on basis of this 
report, including but not restricted to, fluctuation in the prices of securities, changes in the currency rates, diminution in the NAVs, 
reduction in the dividend or income, etc.  
 
The investments or services contained or referred to in this report may not be suitable for all equally and it is recommended that an 
independent investment advisor be consulted. In addition, nothing in this report constitutes investment, legal, accounting or tax 
advice or a representation that any investment or strategy is suitable or appropriate to individual circumstances or otherwise 
constitutes a personal recommendation of Dion. 
 
REGULATORY DISCLOSURES: 

Dion is engaged in the business of developing software solutions for the global financial services industry across the entire 

transaction lifecycle and inter-alia provides research and information services essential for business intelligence to global companies 

and financial institutions. Dion is listed on BSE Limited (BSE) and is also registered under the SEBI (Research Analyst) Regulations, 

2014 (SEBI Regulations) as a Research Analyst vide Registration No. INH100002771. Dion’s activities were neither suspended nor has 

it defaulted with requirements under the Listing Agreement and / or SEBI (Listing Obligations and Disclosure Requirements) 

Regulations, 2015 with the BSE in the last five years. Dion has not been debarred from doing business by BSE / SEBI or any other 

authority.  

 

In the context of the SEBI Regulations, we affirm that we are a SEBI registered Research Analyst and in the course of our business, 

we issue research reports /research analysis etc that are prepared by our Research Analysts. We also affirm and undertake that no 

disciplinary action has been taken against us or our Analysts in connection with our business activities.   

In compliance with the above mentioned SEBI Regulations, the following additional disclosures are also provided which may be 

considered by the reader before making an investment decision:  

For private circulation only 



 

 

 

 

 

 

 

1.  Disclosures regarding Ownership  

Dion confirms that: 

(i) Dion/its associates have no financial interest or any other material conflict in relation to the subject company (ies) 

covered herein at the time of publication of this report. 

   

(ii) It/its associates have no actual / beneficial ownership of 1% or more securities of the subject company (ies) covered 

herein at the end of the month immediately preceding the date of publication of this report. 

   

Further, the Research Analyst confirms that: 

(i) He, his associates and his relatives have no financial interest in the subject company (ies) covered herein, and they 

have no other material conflict in the subject company at the time of publication of this report. 

   

(ii)   he, his associates and his relatives have no actual/beneficial ownership of 1% or more securities of the subject 

company (ies) covered herein at the end of the month immediately preceding the date of publication of this report.  

   

 

2.  Disclosures regarding Compensation:   

During the past 12 months, Dion or its Associates: 

(a) Have not managed or co-managed public offering of securities for the subject company (b) Have not received any compensation 

for investment banking or merchant banking or brokerage services from the subject company (c) Have not received any 

compensation for products or services other than investment banking or merchant banking or brokerage services from the subject . 

(d) Have not received any compensation or other benefits from the subject company or third party in connection with this  report  

3.  Disclosure regarding the Research Analyst’s connection with the subject company: 

 

It is affirmed that I, Rohit Joshi employed as Research Analyst by Dion and engaged in the preparation of this report have not served 

as an officer, director or employee of the subject company 

4.  Disclosure regarding Market Making activity: 

Neither Dion /its Research Analysts have engaged in market making activities for the subject company. 

 
Copyright in this report vests exclusively with Dion. 
 
Dion Global Solutions Limited, Registered Office: 54, Janpath, New Delhi – 110001, India. CIN: L74899DL1994PLC058032, Website: 

www.dionglobal.com. Scrip code with BSE: 526927. SEBI Registration No. INH100002771. Compliance Officer Details: Ms. Rinki Batra, 

Tel.: 91-120-4894813, Fax No. 0120-4894854 or E-mail: rinki.batra@dionglobal.com 

For private circulation only 

mailto:rinki.batra@dionglobal.com

