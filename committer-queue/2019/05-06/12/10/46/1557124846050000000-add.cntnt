

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Views: The lowdown on sector funds

            		November 1, 2017 . Bhavana Acharya
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Research Desk . Views
                    
		
            	

            	

Why you should be wary of sector funds

	Different sectors come to the fore in different markets
	Returns are often deceptive as you can spot top performers only when a sector peaks
	Limited choice of sector funds to play
	Timing entry and exit becomes important in sector funds
	If ill-timed, they can deliver negative returns even over 3 & 5-year periods 




Look at the top performing funds in any year. Often, the toppers are sector-specific funds. In some years, the returns these sector funds post are far above that of diversified funds. Take 2017 for example. In January this year, the best one-year return was a whopping 44%, clocked by DSP BlackRock Natural Resources fund. The best return any diversified equity fund posted was 15%. In fact, of the top 10 funds eight were sector-specific.

 Open  a FREE Account Now! 

Today, IDFC Infrastructure looks tempting with a 47% 1-year return. There are specific drivers such as the huge outlay on road construction that the government announced last week. But is this enough to propel the entire infrastructure sector? If it does, how long will the run last?

Investing in sector funds calls for a keen eye, the ability to keep track of stock markets, domestic economic developments, global developments, and knowing when to get out – even if it means booking losses. For most of us, the very reasons for investing in mutual funds are to circumvent these factors. This is why we usually advise against investing in sector funds as part of any portfolio. The new SEBI rules limit an AMC to a single fund per category except sectors and themes. If AMCs come up with new sector funds, it’s best to be cautious.

Before getting into the characteristics and risks of sector funds, know that there are two types of sector funds. The first type invests only in a single sector such as banking or technology. The second type is a fund that is built along a common theme, such as consumption or manufacturing. While the second type is broader than a pure sector fund, it is still just as risky. This is because you’re still betting on a specific driver or growth factor which has its peaks and troughs.

Timing is the key

One, in each market, different sectors come to the fore. A sector, once it falls out of favour, may not come back up for years. Consider infrastructure. Funds in this sector clocked 1-year returns in excess of 60% for much of 2007 and 2008, sometimes holding a solid 20 percentage points above regular diversified equity funds. This gap narrowed from mid-2008 onwards as the market crash hit. By end 2008, 1-year losses in these funds were 50-60%, sliding below the average for diversified funds. Infrastructure funds trailed in this manner right on until 2014 when the euphoria over the new government and reforms sent the sector up again for a while. By 2015, infrastructure fell back and then picked up again later in 2016.

Similarly, MNC funds were the rage in 2015 and 2016 when they trumped diversified funds by an enormous margin. The preference for low-debt companies with quality governance sent these stocks up as the market hunted for quality. This has now given way with the two MNC funds now trailing other equity funds. In each calendar, the top funds have always belonged to a different sector. If today it is infrastructure, the topper funds in January 2016 was pharmaceuticals. In January 2014 toppers were technology funds, in January 2013 it was banking.

Two, returns for sector funds swing sharply. Often, the one-year return catches the eye. And this is a bad metric to go by. The trick in sector funds is to catch it when the returns are low, not high. Returns in these funds rise very steeply, very quickly. Looking at the past returns and investing when it looks good simply means that you have lost the best of the returns already. These funds can also fall equally quickly.

 Open  a FREE Account Now! 

Technology funds clocked strong 1-year returns in January 2014, with ICICI Prudential Technology and Franklin India Technology returning 61% and 53%, respectively. By January 2015, the two funds’ returns dropped to 30% and 18%. Today, the 3-year return of these funds is less than 5%. Another investor favourite last year was DSP BlackRock Natural Resources fund. In October 2016, the 1-year return was 51%. Today, the 1-year return is 44%.

On an average, the volatility in returns for sector funds is much higher than other equity funds – in the past five years, the deviation in their returns is on an average, 20% higher than diversified funds. The proportion of times they deliver losses, even on a 3-year or 5-year basis is much higher as well.

Therefore, this makes timing very important for both entry and exit. If you catch it too late, you lose the bulk of returns. If you fail to exit, your returns will slide into losses. Using the logic of holding on for the long-term may not work out if markets don’t pick the sector or if its fortunes change completely. The software sector is such an example today where there is a structural shift in the way they do business. If you run SIPs when the sector is trending up, you will be averaging your cost up. If you run SIPs in a sector that is falling out of favour, you will be throwing good money after bad.

The knowledge you need and the factors to look at are vast and varied too. Sector funds are a motley mix and thus the indicators to track are many. Commodities are driven by global factors. Raw material prices and spending patterns drive FMCG fortunes. Rural India is influenced by wage growth, irrigation and monsoon levels, crop yields, seed and fertiliser prices, and minimum support prices. Infrastructure depends on commodity costs, interest rates, government spending and finances, and state of the economy.

You would need to keep track of all these if you are to identify sector funds worth investing in and to know when to exit. It is of no use to track one or two sectors alone. Once you do exit the fund, you would need to find another investment opportunity to redeploy those proceeds. Besides, if you do have this knowledge and ability, you could always invest in a few stocks instead of a fund where you cannot control the portfolio!

Diversified funds do the same job

Choices are limited in sector funds. Of the 65 sector funds available, more than half are in infrastructure, banking, pharmaceuticals, and technology. Other sectors or themes have at best a couple of representative funds. There are only two FMCG, commodity, and MNC funds. Other single sector funds represent manufacturing, rural India, urban consumption, media, and power. Even if you were to spot a theme or sector correctly, your choice will be restricted. If that fund happens to be inept at managing the sector correctly, you may stand to lose even if your call to invest was correct.

Now, take the most populous sectors as far as sector funds go – banking and infrastructure. Banking and financial services, given that they are the single biggest contributor to the listed space, unfailingly find representation in every equity fund. Your portfolio would anyway hold a good part of this sector. Similarly, diversified equity funds today hold several sectors from cement to power to steel to engineering to construction. All these play into the infrastructure theme already. Holding an infrastructure fund over and above this simply increases your exposure.

Diversified funds take over the role of tracking sectors, booking profits, and reinvesting the proceeds in new opportunities. Depending on opportunities, funds pick up or scale back holdings in sectors. Over the past couple of years, for example, funds scaled back exposure to pharmaceuticals and software, traded in expensive FMCG stocks for cheaper durables stocks, picked up NBFCs instead of banks, raised exposure to oil & gas and pure construction companies and metals, and booked profits in media stocks. It is true that equity funds don’t all get their calls correctly – but then you have dozens of funds to choose from and you have past performance to judge their quality.

This apart, there are sectors with a lot of potential but without enough listed stocks to devote an entire fund to them. For instance, defence spending may be a good choice given the government’s focus here. However, there is no way to play this because there aren’t pure defence stocks to begin with, and the number of stocks that participate in such contracts are very few. Therefore, most sector funds currently don’t serve much of a tactical allocation, since the sectors are already held by diversified equity funds. For niche or fast-growing sectors too, regular equity funds are the only participation route.

At the end of all this, what if you still want to invest in sector funds? Well then, keep in mind these pointers:

 Open  a FREE Account Now! 

	Don’t allocate more than 10% to sector funds as it ups your portfolio risk too much.
	Don’t keep them as part of your core portfolio or run SIPs in them for a long period. These funds should only be used to lift returns from time to time.
	Don’t go by 1-year returns or even 3-year or 5-year returns to make a decision. It will lead you to invest in sectors that have already run up.
	Use themes as far as possible, and not sector-specific funds as it spreads your risk a little better than sector funds.
	If you find all of the above cumbersome to follow, you will do well to simply stick to a diversified equity fund.


Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.

Note: This article has been updated to provide more clarity around timelines of sector fund returns.

Other articles you may like
	"Our fund management is process driven and not dependent on a single fund manager"
	Changes in FundsIndia’s Select Funds
	FundsIndia Recommends: Birla Sun Life Top 100
	The real risk to your investments...





			
			Post Views: 
			2,916
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Advisory. Investments. Mutual funds. Personal Finance. sector funds
                

			

            	


			
			11 thoughts on “FundsIndia Views: The lowdown on sector funds”		


		
			
			
				
					
												Vikram Morey says:					


					
						
							
								November 1, 2017 at 1:41 pm							
						
											


									

				
					I am new invester I put 15000

IN DSP BlackRock Natural Resources fund

Last night

What should I do .

Your comment make so scared.

It’s 20 of my portfolio .

When should I take my money off.

Your article is very good and Tx for this all

Funds India

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								November 1, 2017 at 4:54 pm							
						
											


									

				
					Hello sir,

It will be hard to comment on your fund without knowing the other funds you hold. Since it forms a smaller part of your portfolio, you can consider retaining it for a year to avoid exit loads and taxes if you are willing to take the risk and if you do not have other high-risk funds in your portfolio. You could also consider deploying fresh money in other funds suitable to your risk and timeframe to reduce the impact this particular investment has. Please contact your advisor for a better understanding on where your investments stand.

Thanks,

Bhavana

				


				Reply
			




	
			
				
					
												Vikram Morey says:					


					
						
							
								November 1, 2017 at 1:44 pm							
						
											


									

				
					DSP BlackRock Natural Resources fund.i have invested 15000 last night

Your article make me so scared what should I do

Your article is very good and Tx for all your efforts

God bless you

Give me your fed back on my invesment plz

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								November 1, 2017 at 5:00 pm							
						
											


									

				
					Hello sir, I’ve replied to your previous comment…please do not panic and make investment decisions as that will simply lead to hasty decisions. 

Thanks,

Bhavana

				


				Reply
			
	
			
				
					
												Vikram Morey says:					


					
						
							
								November 2, 2017 at 9:45 am							
						
											


									

				
					I have sip in Sbi blue chip from. Last 2 years 1000

All are lum sum

I have birls sunlife frontline equity fund 18000 rerun 25%

Dsp small cap 10000 giving return 22%

Frankline India smaller companies 10000 30 .%Reuters

Axis Elss 5000

Sbi magnum midcap cap 10000 very volatile

Icici our long term debt fund 15000

Sbi  dynamic  bond fund 10000

( Motila 35 15000

Dsp black rock 15000

I buy this two on  Monday )

Can you give me your opinion about my portfolio

I want to sell dsp small company

Sbi magnum midcap also

Any suggestions for me please reply mam

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								November 2, 2017 at 4:19 pm							
						
											


									

				
					Hello,

Please use your FundsIndia account to schedule an appointment with your advisor (its available from your dashboard). All FundsIndia investors are assigned an advisor. We don’t take up portfolio-specific queries here. You can always sign up for an account if you don’t have one 🙂

Thanks,

Bhavana

				


				Reply
			










	
			
				
					
												Suneel says:					


					
						
							
								November 1, 2017 at 9:14 pm							
						
											


									

				
					Really Good article. But my question is – if suppose you are ready to keep SIPing in Sector funds for 7-8 years

and redeem the investment when the sector is at peak – like the peak mentioned in above article of Infrastructure, IT, Pharma, FMCG etc., you will get far better returns than equity diversified funds. Every sector has to peak in every 7 to 8 years.

Dear experts/ Bhavna,

Pl. correct me if I am wrong 

Regards,

Suneel.

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								November 2, 2017 at 4:07 pm							
						
											


									

				
					Thanks, Suneel! 

As far as your question goes, all sectors do not have to move in cycles. FMCG went virtually nowhere in the early 2000s until it really took off around 2009 or 2010. The sector still hasn’t actually corrected yet. Then take a sector like pharmaceuticals, long considered defensive. With the industry itself undergoing major upheaval, it is not possible to say how long it will take to get back on its feet or adjust to the changes. For someone who invested in it through a SIP for seven years, the return would be lower than diversified funds. Infrastructure has recovered, but is this the peak? Indications are that it is not, if the thrust on infrastructure holds good – remember that infrastructure is a very wide definition and involves several different sectors. 

The point we’re trying to make is this – regular equity funds anyway hold all these sectors. Good funds change their portfolio in time to capture opportunities so they would exit on down cycles and get back in on upcycles. There is really no need to further go overweight on sectors, especially when there aren’t that many niche funds. 

You would have to be very adept at juggling sector funds in your portfolio in order to beat diversified funds; additionally, you don’t have as wide a choice of sectors as regular equity funds do. SIPs will not automatically help you generate higher returns. If you’re able to spot opportunities that well on your own, why not own the stocks directly to boost your overall returns 🙂 Hold regular equity funds in addition to this and you will be fine!

If you have the wherewithal to spot sector peaks and troughs, by all means utilize sector funds to your advantage. But most do not have this ability and are drawn mostly by the 1-year and 3-year returns, and so enter when sectors have already run up. Holding good quality diversified funds and periodic review is the most optimal here. Hope your question has been answered now!

Thanks,

Bhavana

				


				Reply
			




	
			
				
					
												Prakash Kumar says:					


					
						
							
								November 4, 2017 at 10:14 pm							
						
											


									

				
					I have invested Rs.6.20 lakhs in hdfc prudence mutual fund…

3.50 lakhs in ppf..

Six sips in dsp black Rock, Aditya Birla sl fund, Franklin prima, kotak focus select, L & T prudence fund, mirage asset india opportunity fund… 1000 each every month,

R’s.  10,000 in DSP black Rock tax saver ELSS, R’s.  40,000 in RELIANCE LIQUID FUND….

Rs. 7,000 in IDFC BANK shares..

Rs. 17,000 in LAKSHMI VILAS BANK

Pls advice me,  should I continue with the current investment pattern or make any changes…

My age is 33 years and I m got.  Service…

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								November 8, 2017 at 11:39 am							
						
											


									

				
					Hello, 

Apologies for the delay in reply. We won’t be able to review portfolios and provide related advice on this platform. All FundsIndia investors are assigned an advisor who will guide you in your investments. You can contact your advisor at any time through the Talk to Your Advisor feature, available on your MF dashboard once you log in.

Thanks,

Bhavana

				


				Reply
			




	
			
				
					
												Suneel says:					


					
						
							
								November 5, 2017 at 9:19 pm							
						
											


									

				
					Thanks Bhavna for your detailed reply.

Regards,

Suneel.

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


