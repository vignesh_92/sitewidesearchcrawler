

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Strategies: Make the best of an India, US reset

            		November 10, 2016 . Vidya Bala
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Research Desk . Strategies
                    
		
            	

            	The US elections are over, and world markets simply seemed to signal that it’s no big deal, at least for now. What remained a bigger deal at the end of the day, for Indian markets, seemed to be the Indian government’s sudden move to curb the black economy.  The latter is a significantly positive move over the long term.

If that is the case, are we ready for a full-fledged rally? We think not. Rather, we think there is still scope for event-based volatility that would afford reasonable averaging opportunities, before the market can take off.

The volatility on the US election result day is a case in point. A few such events would provide bargain buys for you. As investors, if you take advantage of these opportunities, you will likely have more than optimal returns on your portfolio.  

 Open  a FREE Account Now! 

Here’s why we think there could be volatile movements in the markets from here to the end of this financial year at least, and moving to even up to the first quarter of the next fiscal.

What’s the Trump plan?

While initial reactions suggest that the markets are unlikely to brood over the shocking success of Donald J. Trump, his nation-building plan and trade policy roll out will be watched for with keen interest; not only by nations but by the FII community as well. Given that there is a vacuum today on any news on this aspect, we believe some amount of reaction by the US market itself and by FIIs is inevitable when the story unfolds.

As far as India is concerned, while our trade relations with the US are far lower compared with other nations (see data on left), we rank 9 in terms of imports by US from India. IT and pharma being key Indian sectors that export to the US, it remains to be seen if Trump’s protectionist policies, if any, will hurt these sectors. To this extent, any news on this front may see reaction by the market. The roll out of plans on trade and international ties may also see FIIs reshuffling their portfolio, causing some short-term volatility.

Indian economy reset – near term implications

The move to demonetize about 85% of currency in circulation is perhaps among the boldest moves a government can make, to curb fake money and bring black money into the mainstream economy. The positive implications of this in terms of easing inflation and fiscal burden, bringing in more money into the banking system and contributing to economic growth will likely happen over a couple of years.

 Open  a FREE Account Now! 

But in the near term, there is bound to be short-term pain. First, the immediate impact on the cash economy may unfold itself slowly and market reactions could be only post facto. Impact on the real estate sector, jewellery and other luxury commodities would also unfold slowly.

As this would result in some amount of wealth destruction (black money or not, it is still wealth that would have been used by the individual), some amount of lowering of the propensity to consume, especially high-end goods and services – such as consumer durables, auto, travel and leisure is inevitable. Similarly, any dip in real estate activity can have a ripple effect on allied sectors that provide inputs. The market’s sector-specific reaction has also been on these grounds and further correction can be expected where valuations appear stretched.

Corporate earnings and governance

While it may seem that Corporate India has started expanding its revenue in the last couple of quarters, the earnings growth has not kept pace with valuations across market-cap segments. Take the 126 companies from the BSE 200 for which the September quarter results were out (as of Tuesday). The sales growth of 5.7% for the quarter ended September 2016 over quarter ended September 2015, and the earnings growth of 10% for the same period, may seem like a marginal improvement over previous quarters. However, when this universe is split into those companies with market cap of Rs 20,000 crore and below (midcaps), the picture changes. 53 of the 126 companies in this lower market cap segment not only saw lower sales growth (2.9%) but saw their net profits fall in the September 2016 quarter compared to the September 2015. The fall may be steep because of banks, but even after removing banks and financial service companies, some of which made losses, the bottom line fell.


	126 of BSE 200 companies	Quarter ending Sep 2016 (YoY)	Quarter ending Jun 2016 (YoY)
	Net sales	5.71%	0.41%
	 Adjusted PAT	9.68%	7.06%
	53 of BSE 200 companies*	Quarter ending Sep 2016 (YoY)	Quarter ending Jun 2016 (YoY)
	Net sales	2.90%	-6.20%
	Adjusted PAT	-31.50%	-9.70%
	*Companies with market cap of less than Rs 20,000 crore. List includes banks and finance companies. Adjusted PAT – adjusted profit after tax, adjusting for one-off income/expense.



Simply put, the earnings have not kept pace with the valuation (P/E in early 30s for midcaps) that this segment is commanding and that means either a segment/stock specific correction or volatility in prices until such time earnings keep pace. Downgrade of earnings estimate, for stocks, by the end of the calendar (to be expected given the very optimistic forecasts made early this year) may also provide correction opportunities.

 Open  a FREE Account Now! 

Impact of GST and the Budget

Even as markets may remain lackadaisical from the above, implementation of GST may see company-specific reactions. For instance, the impact of working capital can be initially significant on companies as all transactions, including stock transfers would be subject to GST. Besides, those companies which had units in certain tax-free ones will also have to pay GST and claim credit on tax paid. This would result in a higher need for working capital that may have impact on the bottom line (again for mid-sized companies) until such time the working capital cycle settles down. This means, further volatility to earnings.

While we do not expect any significant tax disincentives on issues such as capital gain exemption, the weeks leading to the Budget (expected in early February) may also provide some volatility, especially if FIIs book out.

Opportunity to buy

While we don’t think any massive correction can be expected, given the current state of the economy, volatility triggered by the above events can be expected to provide some opportunities for you to average on your holdings.

	For those of you running SIPs on large-cap funds (or diversified funds), we would recommend that you average on those stocks on dips in the next 6 months or run additional STPs on them if you have surplus. We think valuations are more reasonable in the large-cap segment, relative to earnings growth, and they afford better upside now.
	For those wanting to play it safe, funds such as Kotak Select Focus and SBI Bluechip which have relatively lower exposure to sectors such as IT and pharma (sectors that can be impacted from US trade policies) and higher exposure to internal-growth driven sectors can be options.  Run weekly STPs until June 2017 if you have lump sums and review further course of action through your advisor. Do note that this is a tactical call and is only in addition to the existing long-term portfolios that you may run or plan to run. Avoid disturbing those.

 Open  a FREE Account Now! 


FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis of investment decisions. To know how to read our weekly fund reviews, please click here.

Other articles you may like
	FundsIndia Debt Strategy: Delayed Gratification, A Positive For You
	FundsIndia Recommends: ICICI Prudential MIP 25
	FundsIndia Recommends: SBI Bluechip Fund
	FundsIndia Views: Recapitalisation – Impact beyond the banking sector





			
			Post Views: 
			1,466
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                                    

			

            	


	
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


