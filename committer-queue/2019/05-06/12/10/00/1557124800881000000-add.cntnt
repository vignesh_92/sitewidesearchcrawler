

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		The 2019 stock market rally explained

            		April 29, 2019 . Vignesh  Sayeesundar
                    

	
                    
                                    Mutual Fund Research . Research Desk
                    
		
            	

            	Investors were in for a reality check post a dream run of 2017, with most stocks falling sharply through 2018. But since February this year, markets took a turn upwards. And despite the recent sharp intra-day volatility, indices have made hefty gains.

What’s driving this rally? How have stocks moved?

	Primarily, FII investments and a general positive global sentiment has pushed markets up. Markets were heading into bear territory in 2018, as concerns over global growth, corporate earnings, and trade mounted. These risks have been factored in, and US interest rates are also heading downward.
	Back home, expectations of a stable government post elections rose. FIIs turned upbeat, net buying Rs 64,115 crore in 2019 so far against a net selling of Rs 32,783 crore in 2018.
	The market rally has been broad-based, spanning most stocks. The rally has also been very quick and gains sharp.
	Mid-caps and small-caps clocked much higher gains. Beaten-down stocks and sectors have participated the most in the rally.


Here’s more.

Broad-based rally

Since the start of the rally in February, here’s how key indices have performed:


	Index	Returns from 15th Feb – 23rd Apr, 2019	Returns for calendar year 2018
	NIFTY 50	+9.8%	+3.2%
	S&P BSE Sensex	+9.7%	+5.9%
	NIFTY 500	+10.7%	-3.4%
	NIFTY Midcap 100	+13.1%	-15.4%
	S&P BSE Smallcap index	+14.5%	-23.5%



In the latest rally, there has been a broad recovery. 90% of the Nifty 500 stocks have moved up. Of these, three out of every four stocks are up more than 10%. This indicates that the rally has been quite sharp.

Stocks have performed well across market capitalization too. Splitting the Nifty 500 into the large, mid, and small-cap buckets shows about 90% of stocks in each category ending higher over the last two months.

However, the extent of the rally has been greater in mid and small-cap companies. Large-caps gained 13.1% on average, having fallen 4.1% last year. Mid-caps are up 14%, after a 10% loss in 2018, while small-caps are up 18.4% since mid-Feb, after a 24.8% fall last year.

Beaten-down scrips recover

This better performance of the beaten-down stocks is a characteristic of the ongoing market movement. Of the stocks that have gained, more than half had suffered losses of over 20% in 2018. For example, Tata Motors tanked 60% in 2018, but has surged 41.5% since mid-Feb. The jump has come partially due to a low base, but also on the back of JLR’s improved outlook for the near future. Allahabad Bank, Corporation Bank, Bank of India, Bank of Maharashtra and Oriental Bank of Commerce have exited the PCA framework. These stocks had fallen in the range of 21%-39% last year, and have gained between 15% and 35% this rally. Below are some of the biggest gainers from this category:


	Stock	Returns from 15th Feb – 23rd Apr, 2019	Returns for calendar year 2018
	Lakshmi Vilas Bank	+54.9%	-41.1%
	Indiabulls Real Estate	+49.9%	-60.8%
	SAIL	+32.1%	-38.8%
	Reliance Nippon AMC	+28.1%	-47.1%
	Grasim Industries	+26.2%	-29.2%



Other beaten-down sectors have also made a comeback. Bank stocks, as mentioned above, recovered strongly as some came out of the PCA framework, the government undertook another round of recapitalization, NPA woes looked to be fading, earnings numbers began to pick up, and FIIs turned buyers. For instance, PSU banks are up 26% after a 34.7% crash last year. Private banks are up 22.3% since mid-Feb, having fallen 10.8% in 2018.

Oil stocks gained as pressure of high oil prices waned. Iron & Steel, infrastructure and consumer durables, capital goods and power were other prominent names. Most of these sectors had given negative returns in 2018. Improving industrial activity, healthier order books and order inflows, and capacity utilization inching higher all helped. All OMCs participated in this trend; IOC, BPCL and HPCL made gains ranging from 9% to 25%, after losing between 30 and 40% last year. Jindal Steel and Power shed 19.6% last year, and is currently up 34.8%. NCC is up 29% after a 34.1% dip in 2018, while BEL has gained 25% post a 51.7% fall last year.

Gainers still gain

2018, though a bad year for most stocks, did have a small share of performers. With 2019 paying attention to the beaten-down, have these stocks been left behind? Not really, barring traditional defensives FMCG and IT.

94 stocks ended 2018 in the green and have continued to move up over the last two months. Here are some prominent names:


	Company	Gain in 2018	Gain in current rally
	Bajaj Finance	50.6%	18.9%
	TCS	40.2%	8.2%
	Bajaj Finserv	23.8%	28.8%
	Reliance Industries	21.7%	10.1%



A pull-back by the beleaguered stocks is a positive development; for one, it reduces the extent to which index returns are skewed towards a few stocks. Two, it suggests better support to a longer rally. Three, it is an indicator of an economic and earnings turnaround. And lastly, as equity funds looked to value-based and contrarian calls over 2017 and 2018, it has begun to pay off.

 

Other articles you may like
	FundsIndia Reviews: DSP BlackRock Opportunities Fund
	Is the market rally backed by fundamentals?
	FundsIndia Recommends: Aditya Birla Sun Life Short Term Fund
	FundsIndia Reviews: UTI Transportation and Logistics Fund





			
			Post Views: 
			197
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                                    

			

            	


			
			1 thought on “The 2019 stock market rally explained”		


		
			
			
				
					
												Rahul Patel says:					


					
						
							
								May 2, 2019 at 11:37 am							
						
											


									

				
					That’s a very good explanation for stock markets. I liked the fact that you shared the key statistics to strengthen your points. Thank you for share this blog.

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


