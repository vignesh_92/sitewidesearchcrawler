

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Reviews: PPFAS Long Term Value Fund

            		June 1, 2016 . Bhavana Acharya
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Reviews
                    
		
            	

            	PPFAS Long Term Value Fund is among the more unusual equity funds. What sets this diversified fund apart from other multi-caps is that, one, it can invest up to 35 per cent of its portfolio in foreign securities. It used this mandate and maintained an overseas exposure of around a quarter of its portfolio over the past two years. Two, it can take arbitrage positions if it finds such opportunities, and if the prevailing market environment doesn’t provide other investment opportunities. This multi-cap fund is also the AMC’s only fund. 

Given PPFAS Long Term Value’s (PLTV) offbeat approach, and its recent crossing of the three-year mark, now is a good time to take stock of the fund’s performance.

 Open  a FREE Account Now! 

Performance


In its short history, the fund has put up a good show. In the one-year period, PLTV ranks in the top quartile when compared to other multi-cap funds. In the three-year period, it is a mid-quartile performer. On an annual rolling return basis since its inception, the fund has beaten its benchmark Nifty 500 index 87 per cent of the time. This indicates a good record of consistency.

 

In the past three years, there has not really been a prolonged market slide like that of 2011 or 2008, to test the fund’s mettle in bear markets. The equity market did slip into losses towards the close of 2015 and was well in the red for the first couple of months in 2016. In this period, PLTV still eked out slight gains, putting it well ahead of the index. Few peer funds managed the level of outperformance over the Nifty 500 that PLTV did in this period.

This is partly because it has a greater share of small-cap and mid-cap stocks, which are yet to correct as much as large-caps have. Two, its good international exposure of around a quarter of its portfolio which include stocks such as Alphabet Inc.(Google), helped. 

On a risk-adjusted basis, PLTV is the best among its peers. Its volatility is also far lower than the category’s average.

Winning strategy

The fund looks for stocks that are trading below intrinsic value, or which are cheaper, with a strict long-term view. This approach goes for both its domestic and international holdings. For example, IBM, which makes up around 3 per cent of its portfolio, trades at a PE of around 12 times, compared to the BSE IT index’s 19 times PE. 

Similarly, the international equivalents of stocks such as Nestle India and 3M India, which the fund holds, are much cheaper while also being larger companies. On the domestic front, holdings such as ICICI Bank, Zydus Wellness, and Persistent Systems are still relatively cheaper than peers.

Its portfolio turnover is extremely low at just 3-4 per cent. Maharashtra Scooters and ICRA, its current top holdings, for example, have been a part of its portfolio almost since its inception in May 2013.

The fund also has no bias towards market capitalisation or sectors. This is because, for one, it follows a stock-specific approach and does not look at overall macro numbers, and two, it has a compact portfolio of 20-25 stocks that does not give much room to play with sector allocations. 

 Open  a FREE Account Now! 

Its exposure to the besieged banking sector, therefore, is far lower when compared to both benchmark and peers. Over the past year, the fund has kept 3-6 per cent of the portfolio for arbitrage opportunities. It also recently upped cash holdings to 8 per cent of the portfolio for lack of investment opportunities. All these have contributed to the fund’s strong performance in the past year.


The fund will have at least 65 per cent of its portfolio in domestic stocks, which ensures that capital gains tax will be nil on holding for over one year. It also hedges its currency exposure and therefore, foreign exchange losses are unlikely to eat into returns. Of course, exchange gains won’t boost returns, then, either!

The fund offers a good portfolio diversification, allowing participation in foreign stocks that have few or no domestic equivalents. But given the multi-cap nature of the fund and its long-term views on its portfolio, the fund requires a long-term time-frame and a moderate risk appetite. The fund is managed by Rajeev Thakkar, and has an AUM of Rs. 664 crore.

FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis of investment decisions. To know how to read our weekly fund reviews, please click here.

Other articles you may like
	FundsIndia Explains: Sharpe ratio
	FundsIndia Recommends: DSP BlackRock Opportunities
	Why SWPs are great for regular income - Part I
	One Year of Re-Categorisation: Looking back





			
			Post Views: 
			4,165
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    fund review. ppfas. value investing
                

			

            	


			
			8 thoughts on “FundsIndia Reviews: PPFAS Long Term Value Fund”		


		
			
			
				
					
												raj says:					


					
						
							
								June 1, 2016 at 8:21 pm							
						
											


									

				
					Are there any funds which dont have International Exposure have given  much better return than this one .

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								June 3, 2016 at 6:14 pm							
						
											


									

				
					Hi Raj,

There are multi-cap funds with longer track records and consistency in performance that are better than the PPFAS fund without international exposure, such as Franklin India Prima Plus and Mirae Asset India Opportunities. In the one-year period, as we said, PPFAS is almost at the top. In the three-year period, it is lower down.

Thanks,

Bhavana

				


				Reply
			




	
			
				
					
												Bharani says:					


					
						
							
								June 3, 2016 at 7:21 pm							
						
											


									

				
					Finally, PPFAS is in Fundsindia’s radar 🙂 Good analysis on the fundamentals.

It would’ve been nicer to include mention of Parag Parikh, as he meant everything for this fund till his demise. Many of the holdings have been handpicked by Parag Sir, though he has also set up the amazing process.

Some of the bets are yet to pay off (I think Noida toll bridge was there, or was it in QLTE?), but if I really want to know how value picking works, I would follow the research done by LTV. We better not 1 yr/3 yr, but a much longer view for these to pay off, because these are quality businesses.

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								June 4, 2016 at 12:01 pm							
						
											


									

				
					Thanks, Bharani! Yes, you’re right. This fund needs a much longer view to pay off, given its strategy.

Regards,

Bhavana

				


				Reply
			




	
			
				
					
												KAUSHIKI PADA CHAKRABARTI says:					


					
						
							
								June 25, 2016 at 12:42 pm							
						
											


									

				
					SIR,

I WOULD LIKE TO INVEST IN PPFAS Long Term Value Fund. BUT FORMS ARE NOT AVAILABLE IN KOLKATA. HOW CAN I GET IT?

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								June 29, 2016 at 1:17 pm							
						
											


									

				
					Hello,

You can invest online through FundsIndia; the fund is available here.

Thanks,

Bhavana

				


				Reply
			




	
			
				
					
												Hrishikesh Jadhav says:					


					
						
							
								January 7, 2018 at 7:10 pm							
						
											


									

				
					How can I invest in your firm….nd can I get good returns out of it….and is it possible for me to invest ol

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								March 8, 2018 at 11:55 am							
						
											


									

				
					Hello,

Apologies for the delayed reply. We’re an online platform to invest in mutual funds, so you definitely can do it online. The account is free and you will be assigned an advisor who can guide you through your investments. Our platform offers several features to manage your portfolio, a variety of SIPs, adding family members to your account to have all your investments in one place, etc. You can set up your account here.

Thanks,

Bhavana

				


				Reply
			







		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


