

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Views: Recapitalisation – Impact beyond the banking sector

            		November 8, 2017 . Vidya Bala
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Research Desk . Views
                    
		
            	

            	While the stock market may have cheered the announcement of bank recapitalisation instantly, we think this is more a long-drawn but game-changing play – not just for the banks but, more importantly, for other sectors and the broad economy.

Here’s what we think, in short, could be the impact of this capital infusion: Armed with fresh capital infusion, public sector banks (PSBs) will not only be better placed to provide for stressed assets in their own books but also expedite balance sheet clean up in many stressed sectors and trigger assets sales. While that happens, many sectors may get nimble-footed and ready for a possible capex/opex recovery triggered partly by Bharatmala road spending.

In this article, we are not getting into the nitty-gritty of how much the recapitalisation will clear up bank balance sheets, how much of a ‘hair-cut’ banks will have to take with their bad loans and how much they will grow. We will leave that to the sector experts and rating agencies.

Instead, let us try to understand why the bank recap execution may have a much more broad-based impact on the economy, than just on banks. 

Let’s step back to understand the slow and painful dip in lending to the industrial sector and the build-up of bad assets in the country.

 Open  a FREE Account Now! 

Fall in bank credit to industries

Banks were aggressively lending to industries before the financial crisis in 2008. But this was utilised in capacity expansion/infrastructure building thereby generating income to service the debt. After peaking in 2008, such lending went down to modest double digits in 2009 only to pick up again in 2010 when a seeming revival happened. Since then, credit growth to industries remained steadily high and crossed the teens in 2012. The data below will tell you that after peaking in 2012-13, there has been a steady dip in the growth; with actual credit contraction from October 2016. The next bar chart will also tell you that credit to industries has steadily come down as a proportion of bank’s lending. From 45% of total non-food credit in September 2012, it fell to 37% in 5 years (September 2017). Retail lending took the share away.





Why banks stopped lending to industries

Now, this lower credit growth to industries happened because of 2 reasons: one, interest rates went up, but two, and more importantly, what banks had lent thus far was turning bad. The graph below will tell you that the stressed assets in the system went up significantly in the case of industries even as lending to other categories such as retail or services remained low.  In the graph below, stress advances indicate gross non-performing assets (loans where borrowers did not pay for more than 90 days) and those loans that were restructured (either longer period or lower rates or such concessions) to enable the borrower to pay. Hence, stressed advances ratio provides a good indicator of not only bad loans but loans that could have gone bad.



Needless, to say, PSBs account for almost 90% of the gross NPAs in the banking system.  The graph below will tell you how gross NPAs (GNPAs) and stressed advances ratio have deteriorated significantly for PSBs over the past 4 years.

The inability of companies to repay causes banks to provide higher for such bad assets, thus hurting their profitability.

Unfortunately, the story doesn’t end with banks. The distressed companies, bogged down by heavy debt, become cash-strapped, are unable to be competitive in the market and do not carry out any investment/expansion activity. Sectors dependent on such industries also get hit because of non-payment, lower orders and so on. Thus, it becomes a financial and economic contagion.



Sectors sag with the burden

The data below will tell you where the stress lies. Sectors such as metals, cements, textiles, auto and construction have a larger proportion of their loans in a stressed condition. And these are large sectors –  which means stress in these sectors will account for a chunk of the total loans as well.



For example, infrastructure (power, road and telecom, in that order in terms of value) accounts for 34% of total bank lending and metals 15%. And that means high stress in these segments can really hurt banks and that has exactly been the case in the past few years.

 Open  a FREE Account Now! 

What can recapitalisation do

While there are mixed views on whether recapitalisation will only help provide for stressed assets or make available fresh bank capital for lending, the clear first-order impact is that PSBs will be lighter without the burden of the stressed assets. That is fine and the present re-rating in the PSB stocks is towards that.

But think of the borrowers of these loans. Any write-off or sale of these stressed assets or any kind of lower burden could make a significant impact not just on their balances sheets but for the sector as well. How? Let’s take an example of steel sector: according to analyst reports close to 50% of the steel, capacity lies with companies that are under stressed assets of banks. If this is the case, large players like Tata Steel or JSW Steel or JSP could well look at inorganic growth (through asset acquisition/consolidation), rather than add capacities, especially at a time when infrastructure is set to take off. We took the example of metals as it accounts for a chunk of total lending by banks and has high NPAs. Similarly, take another stressed sector – power.

While asset sale has already been happening in this space, any freeing up of debt commitment of the power companies (if banks decide to take what is called a haircut whereby they settle for lesser than the lent amount) can help them provide more competitive tariffs that will encourage state electricity boards to draw power from these players. This can not only benefit power company themselves but also bring down the current spike in merchant power prices and reduce input cost at a time when companies are witnessing higher cost of sales. 

Essentially, recapitalisation can also free up balance sheets of sectors (asset impairments notwithstanding) and lead to consolidation in select sectors where an uptick is imminent (steel, cement, infrastructure, capital goods etc.).

In short – the ripple effect of cleaning up balance sheet may mean asset creation or better asset utilization across various sectors. This is where the corporate earnings uptick story begins; not stopping with just a PSB revival.

The Bharatmala connection

Our earlier discussion on stressed assets and lower credit to infrastructure would have already made it clear that the road sector too, has not been receiving funds. After double-digit growth in credit until December 2014, credit to this sector dwindled to low single digits and then fell over the last 5 months ending September 2017 (graph below).



This came on the back of aggressive bidding and untested public-private partnership (PPP) models that failed. Analysts have therefore been opining that the current plan of 83,677 km of roads (45km/day), over the next 5 years is aggressive on two counts: one, private players may not actively participate given difficulties in forecasting traffic volumes (where there is toll sharing involved), their weak balance sheets and procedural hurdles of land acquisition and other clearances.

However, we think a few key points must be noted about the changing project award landscape and funding landscape in road projects:

	First, it is true that there has been a drop in projects awarded in FY-17 over FY-16. But the reason is important: the government targeted to acquire 80% of land for BOT (build operate transfer projects) and 90% for EPC projects before even awarding project. This initiation in FY-17 had its own hiccups. Similarly, it was the first year of the hybrid annuity model (part EPC and part annuity) and lenders were just beginning to adjust to this, although data (below) suggests that it has taken off well.




	Secondly, several players have either auctioned their assets (tolls) or are beginning to go for InViTs to ensure they can monetise existing assets and reduce cash flow stress. NHAI, by itself, can use such options.
	Thirdly, government authorities’ views and a recent report by a rating agency suggests that measures such as increasing approval limits of NHAI, higher land compensation for farmers under the new land acquisition policy and even measures such as getting retired state officials to expedite land acquisition may, together, help expedite project award; whether the full target is met or not.
	Lastly, at 15% of total outlay, the investment needed by private players is not very high.



	Funding for Bharatmala & other schemes	Rs trillion
	Market borrowings	2.09
	Private Investment	1.06
	Central Road Fund	2.37
	Budgetary support	0.6
	Expected monetization of national highways through ToT (toll-operate-transfer) route	0.34
	Toll-Permanent Bridge Fee Fund (PBFF) by NHAI	0.46
	Total	6.92
	Source: Ministry of Road Transport and Highways, GOI




(Click here to go to Press Information Bureau and view the list of presentations at the end of the press release: http://pib.nic.in/newsite/PrintRelease.aspx?relid=171930)

What it means

Spending on roads has multiple impacts. One, it directly feeds into order uptick for cement, steel, capital goods and engineering sectors – all of which are part of highly stressed advances of banks. That means for these sectors, supply side (funding) may ease with lower debt and demand side (orders) too may pick up at the same time. Two, as Bharatmala focuses on bringing down road time of goods, through efficient connectivity, the impact in terms of improved logistics can be felt across several sectors in the long term.

 Open  a FREE Account Now! 

Hence, in our opinion, the challenges notwithstanding, even a small uptick in this activity can act as a trigger (although road building of this order may not contribute significantly enough to GDP) for other economic activities.

Bank recapitalization and road outlay, in our opinion, may not create fresh private investment rapidly in the near term. But it can well spur higher and more efficient utilisation of capacity and we think that is good enough to push corporate earnings.

You will likely see the portfolios of fund managers shift positions, albeit with caution, towards many of these cyclical sectors, if they are not already invested in them. For you, the story is not about owning banking funds or banking stocks, it is about investing, and staying invested.

Other articles you may like
	How FundsIndia Provides Advice
	FundsIndia Reviews: SBI Magnum Global
	FundsIndia Recommends: Reliance MIP
	FundsIndia Reviews: SBI Magnum Balanced Fund





			
			Post Views: 
			2,141
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    banks. economy. Investments. Personal Finance. PSU Bank. recapitalisation. recovery. view
                

			

            	


			
			2 thoughts on “FundsIndia Views: Recapitalisation – Impact beyond the banking sector”		


		
			
			
				
					
												Vidya Bala says:					


					
						
							
								November 9, 2017 at 11:19 am							
						
											


									

				
					Hello,

Infra spending together with lower stressed assets can transform. Original cause for infra problems were as wells: in roads and airports BOT models were new to the country and forecasts and estimates ade were faulty. Also, projects were awarded before land was acquired. All these has been taken care of now. In power, the PPA agreements did not foresee coal price hikes and inability to pass through the same to SEBs. This is only partly addressed. Vidya

				


				Reply
			

	
			
				
					
												Vidya Bala says:					


					
						
							
								November 9, 2017 at 11:33 am							
						
											


									

				
					Hello,

Infra spending together with lower stressed assets can transform. Original cause for infra problems were as wells: in roads and airports BOT models were new to the country and forecasts and estimates ade were faulty. Also, projects were awarded before land was acquired. All these has been taken care of now. In power, the PPA agreements did not foresee coal price hikes and inability to pass through the same to SEBs. This is only partly addressed. Vidya

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


