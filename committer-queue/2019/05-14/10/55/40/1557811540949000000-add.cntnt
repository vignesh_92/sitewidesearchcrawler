

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Recommends: Aditya Birla Sun Life Equity

            		July 11, 2018 . Bhavana Acharya
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Recommendations . Research Desk . Research desk featured
                    
		
            	

            	

What: An equity fund investing across market capitalizations

Why: Ability to generate market-plus returns, by deftly adjusting of sector weights

Whom: High-risk investors with a 5-year plus timeframe


Aditya Birla Sun Life Equity Fund-Growth, a multi-cap fund, has established itself in the top quartile of its category in the past few years. Its 3-year and 5-year return are 2 to 6 percentage points higher than the BSE 200 TRI, its benchmark. Even against the tougher Nifty 500 TRI – a valid benchmark since the fund invests across market caps – the fund scores well.

Strategy and suitability 

Aditya Birla Sun Life Equity Fund (ABSL Equity) has a strategy of identifying sector trends and based on that, lift and drop weights to different sectors relative to the index. The fund has been adept at this shifting of sector weights.

For example, it dropped weights to the auto sector in 2017 and the sector undershot the Nifty 500. It had an equal to overweight exposure to the sector in 2016, when the sector was doing well. Similarly, it added to media in 2016, when the sector was doing well and cut back in 2017 as performance dropped.

ABSL Equity was among the few funds to go overweight on metals in mid 2016; the sector has rallied sharply in 2017. It added software last year, which is paying off now. It has been severely underweight in energy – while the sector did well for most of 2016 and 2017, it has sharply corrected this year. Within each sector, the fund then focuses on stocks that are sound and can grow well.

This strategy can, however, be risky since a sector call going awry would hurt returns much more than a few stocks underperforming. The fund can also take contrarian calls – its focus on metals in 2016 is an example. It also has a good exposure to pharmaceuticals. While some stock calls in this sector did pay off, such as Dishman Pharma, others failed to deliver.

In the present market climate, a strategy that focuses more on adjusting sector weights could work well. Several stocks have run ahead of fundamentals. Markets in the past few years have also had the tendency to gloss over fundamental strength as well as quickly picking up or cutting down stocks based on sentiment. Conventional stock picking metrics based on valuations, earnings visibility, growth prospects, and so on, have seen many funds struggling to beat the market. Therefore, a strategy based on predicting which sectors would do well in the market and opportunistically adjusting weights to these sectors can balance out the more stock-focused strategies.

ABSL Equity serves as a good portfolio diversifier. It can be an addition to a core portfolio consisting of funds following a long-term stock-picking strategy. However, given the higher volatility and the opportunistic nature of its strategy, the fund requires investors to have a higher risk appetite and a long-term horizon.

Performance

ABSL Equity has been an outperformer for the past 4-5 years. It had gone through a phase of underperformance in earlier years, and saw fund managers change in 2012. Its 3 and 5 year returns are above the average of other multicap funds by a margin of 3-4 percentage points.

Its recent returns are below the Nifty 500 TRI, as top stocks such as ICICI Bank and ITC have lagged the market. Other smaller weights such as Century Textiles and Vedanta have been recent underperformers. However, the fund is otherwise a consistent performer, especially over the longer term. Rolling 1-year returns for the past three years has the fund beating the Nifty 500 TRI and its multicap peers 75% of the time.

Rolling 3-year returns for a longer 5-year period has the fund beating the Nifty 500 TRI and peer average 90% of the time. The margin of this outperformance over the Nifty 500 TRI is a good 5 percentage points. Average outperformance over the category is also sound at over 3 percentage points.



On other performance metrics too, ABSL Equity scores well. Its risk-adjusted returns (measured by Sharpe) are above average and on par with funds such as Kotak Standard Multicap, Franklin India Equity and Mirae Asset India Equity. ABSL Equity’s upside capture ratio, which measures how much of an index’s gains a fund captures, is also in the top quartile of funds in its category. That is, the fund is among the best in being able to outstrip the market during upturns.

Given its strategy the fund is more volatile than peers. This apart, though it is able to contain losses during corrective phases, it does not match up to many peers. Funds such as Franklin India Equity, Kotak Standard Multicap, or Motilal Oswal Multicap 35 have better downside captures than ABSL Equity.

Portfolio

ABSL Equity maintains a large portfolio of over 60 stocks. Apart from the two or three, individual stock weights are not high. In market capitalisation, the fund has leaned more on the large-cap segment. Using SEBI’s defined market-cap cut-offs, the fund has maintained about 60% in large-caps on an average in the past two years.

The fund is currently betting on financials, with a mix of private sector banks and NBFCs. While the fund does have public sector banks such as PNB and Bank of Baroda in its portfolio, allocations here are small. Other bets include consumer goods, with stocks such as Hindustan Unilever, Dabur India, and ITC forming a good part of this exposure. While ITC is yet to deliver, the other two stocks have delivered well. Pharmaceuticals and telecom are contrarian bets.



The fund has an AUM of Rs 9,376 crore. The fund is classified under the multi-cap category in the new categorization. Earlier too, the fund was already a multi-cap fund and therefore should not see a change in strategy. Anil Shah is the fund’s manager.

FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis for investment decisions. To know how to read our weekly fund reviews, please click here.

To Invest in Mutual Funds & SIP Please login to FundsIndia.com.

Other articles you may like
	"Corporate bond opportunity funds ideal for long-term allocation to bonds"
	FundsIndia explains: Arbitrage and equity savings funds
	Why it is important to stay invested in equity
	FundsIndia Recommends: ICICI Prudential Value Discovery





			
			Post Views: 
			1,944
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Aditya Birla SL Equity Fund Growth. Aditya Birla SL Equity Fund(G). Advisory. fund recommendation. Investments. Mutual funds. Personal Finance. recommendations. wealth
                

			

            	


			
			3 thoughts on “FundsIndia Recommends: Aditya Birla Sun Life Equity”		


		
			
			
				
					
												Ankit says:					


					
						
							
								July 12, 2018 at 12:47 pm							
						
											


									

				
					IS ABSL a conservative fund that has s a top-down stock selection approach which  is a bit inferior to bottom up stock selection. I was reading some comments so wanted your views

Also does high AUM does not translate into a good fund. Example AXIS ELSS

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								August 30, 2018 at 5:29 pm							
						
											


									

				
					Hello,

Apologies if I’ve replied to your comment earlier – some comments that we’ve answered appear to have gone missing so I’m going over them again. ABSL Equity is not a conservative one – it is quite aggressive as mentioned in the article. A top-down approach is not an inferior one. There are funds that follow such an approach and are able to deliver market-plus returns and ABSL Equity is an example. Funds that follow a top-down approach do not ignore stock specifics – they still will pick only those that have the potential to grow. These funds just use the macro picture to identify opportunities. In any case, most funds follow a mix of top-down and bottom-up. There are very few purely bottom-up funds.

Thanks,

Bhavana

				


				Reply
			




	
			
				
					
												Bhavana Acharya says:					


					
						
							
								July 24, 2018 at 6:24 pm							
						
											


									

				
					Hi Ankit,

ABSL Equity is not a conservative fund. It is aggressive in both its marketcap orientation and its strategy. A top-down approach is not superior or inferior to a bottom-up approach. Most funds mix these two strategies. Some funds are pure bottom-up, and in markets where sector cues are more uncertain, like in the current market, a bottom-up strategy may work better. But this is not always the case. In the longer term, it simply boils down to whether the fund can get its calls right whichever strategy is being used. In a portfolio, one can have a mix of funds that follow different strategies in order to make the most of market opportunities.

Thanks,

Bhavana

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


