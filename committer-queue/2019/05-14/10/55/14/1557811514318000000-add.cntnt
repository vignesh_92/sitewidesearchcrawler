

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Strategies: How to make the best of the great Indian bank clean up

            		April 26, 2017 . Vidya Bala
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Research Desk . Strategies
                    
		
            	

            	The stock markets hit record highs on global political cues and post a special report by Federal Reserve on the positive impact of GST on India’s GDP. But elsewhere, the banking regulator, the RBI, has been quietly tightening the screws on banks. And this, we believe is a more important event for the markets.

Last week saw a couple of important notifications from the RBI that may have a near to medium-term impact on the markets. The crux of those notifications are as follows:

 Open  a FREE Account Now! 

	The RBI advised banks to make higher provisions (than the current 0.4% regulatory minimum) to assets that have not gone bad but are in stressed sectors. Banks must have a board-approved policy for such provisioning.
	The RBI indicated that telecom could be one such stressed sector at present and asked banks to review their loan books to this sector by June 30, 2017 and make higher provisions where needed.
	In another notification, the RBI said that where the additional provisioning requirements identified by them for each bank is higher than 15% of what the bank publishes, then the divergence must be disclosed in the annual report.


Why the above norms

There are a few reasons why RBI is doing what it is doing. For one, over the last few years, banks have been on and off disclosing the skeletons in their closet – that is loans that have gone bad and could not be recovered. This story has not seen the end of day, thus preventing banks from getting into full-fledged lending mode again. The RBI, by asking banks to assess stressed sectors (even if they are servicing their debt), is simply asking banks to take the cautious route and manage loan books and profitability in a less volatile manner. Two, where it is felt that banks are not fair in their disclosure, the RBI is forcing them to disclose an independent view (the RBI’s audit of their loan book) on how much provisioning is really needed and what was provided. Both are steps towards ensuring banks clean up their books as and when warranted, do not take in sudden shocks and provide a true and fair view of their financial position.

What this will do

From a big picture perspective, this will distinguish the better managed banks from those that run risks to earn profits. From a bank’s perspective, higher provisions that banks hadn’t done for normal assets may now have to be done. This means that in the medium-term, banks may take a hit in the margin. However, in the long term, the banking space may be professionally run and see less shocks even when various sectors go through cycles. It may also have a more normalised growth as opposed to the volatile growth they witness now.

What’s the scene today

While it is very early to comment on which banks will be forced to provide more in sectors such as telecom and other stressed sectors, there are some research reports on the quantum of exposure to these sectors. Data below is sourced from the respective research reports mentioned:

 Open  a FREE Account Now! 



Exposure of banks to the telecom sector is at Rs 82,000 crore or 1.2% of their loans. This is not too high if you consider the below-mentioned stressed sectors. Their exposure can be split into fund-based and non-fund based exposure (shown above). Fund-based exposure are loans and other direct forms of credit given to a company while non-fund based exposure is typically in the form of letter of credit, bank guarantees and so on. Both can be a source of risk to banks, if the telecom company is unable to service its obligations on time.

While there could be some provisioning made by banks in the telecom space (given RBI’s current emphasis on this), we believe it is the other stressed sectors that will start coming into the quarterly radar of the boards of each bank, as required by RBI.



What’s with mutual funds

 Open  a FREE Account Now! 

The telecom sector, together with the equipment space accounts for less than 1.2% of equity mutual funds’ exposure. Hence, it is not a large exposure and stocks in the space have seen some turbulence already, post Jio entry. However, debt funds do hold many AAA rated instruments in this space. For now, there does not seem any concern, although we will be keeping a close watch on instruments held by the debt category.

The bigger story and the bigger opportunity lies with exposure of equity mutual funds to banks. With banking and financial services accounting for about a third of the bellwether index Nifty 50, mutual funds are not far behind with their exposure of 28.3% (as of March 2017) in this segment. That means as and when banking stocks see turbulence from provisioning norms, the market too (and consequentially mutual funds) will see a jerk. We think such events will be opportunities for you to buy and average better.

The long-term story

Remember, the RBI norms do not in any way mean the end of banks. They simply mean banks must go through a clean-up phase and a more stringent ‘vigilance’ mode. Even as this clean up happens, there is more positive news in the economy in the form of GST and its benefits, as well as other reforms.

Let’s take the GST: a recent Federal reserve paper states thus – ‘The first (assumption) gives an aggregate weighted GST of 16 percent with a positive impact on real GDP of 4.2 percent, whereas our second allocation gives an aggregate weighted GST rate of 20 percent with a lesser positive impact on a GDP of 3.1 percent.

Now, the above number is not small in any sense of the term, as it amounts to Rs 6.5 lakh crore (stated to be higher than the government’s borrowing) and is underpinned by manufacturing output. A local-estimate (by NCAER) had placed this number at 1-2% of the GDP.

Now, take a step back and look at the not-so-cheap valuations especially in certain sectors – auto and auto components, cement, certain industrials and consumer durables –  to name a few and more recently energy, logistics and transport. The market’s willingness to pay high multiples (valuations) in a sustained manner can only point to one thing – that it hopes reforms will pay off, even if it takes time. And remember, these multiples may be justified even if the return on equity of many of the companies (that have fallen since 2008), move up a bit. Interestingly, many of these sectors are a play on commodities as well as consumer spending. To this extent, they can offer double benefits, when there are tailwinds on both these fronts.

 Open  a FREE Account Now! 

What to do

We believe that the headwinds in the market in the form of bank earnings should be viewed as opportunities to average. Fund managers will take a call on which banking stock to hold and which ones to prune. As investors, we think your job will be to capitalise on the turbulence. Systematic transfer plans or better still, value averaging plans (or value averaging transfer plans) will be the way to go for the next 6-9 months at least to make the best of such turbulences. 

If you wish to know how to go about setting up these or which funds to invest in, talk to our advisors to understand which of the funds you currently hold will be better suited to invest in further now. If you are a moderate-risk investor, avoid taking direct exposure to banking sector funds afresh. Any averaging already being done may be continued with a long-term time frame.

FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis of investment decisions. To know how to read our weekly fund reviews, please click here.

Other articles you may like
	FundsIndia Debt Strategy: Funds in a ‘No Rate Change’ Scenario
	Why the flurry of hybrid funds in the market now?
	FundsIndia Debt Strategies: Should you play the duration game now?
	FundsIndia Recommends: HDFC Medium Term Opportunities





			
			Post Views: 
			2,470
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Advisory. fund recommendation. Investments. Mutual fund. Mutual funds. Personal Finance
                

			

            	


			
			2 thoughts on “FundsIndia Strategies: How to make the best of the great Indian bank clean up”		


		
			
			
				
					
												DARA KALYANIWALA says:					


					
						
							
								April 27, 2017 at 2:12 pm							
						
											


									

				
					Very nice article. Crisp, timely (FY 17 result season has just started ) and thought provoking.

				


				Reply
			

	
			
				
					
												Rashi says:					


					
						
							
								May 1, 2017 at 12:45 pm							
						
											


									

				
					Thank you so much, For the information, that’s really gonna help me alot

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


