

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Strategies: How A Young Investor Can Build A MF Portfolio

            		August 6, 2013 . Vidya Bala
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Research Desk . Strategies
                    
		
            	

            	We have many young, first-time investors, wanting to know where and how to make a start in building a fund portfolio. While with FundsIndia, you can always seek our help through our ‘Ask Advisor’ feature, it will also help if you try to take a step-by-step approach to mutual fund investments. This may make it simpler for you to build solid portfolios as your life’s commitments increase at a later stage.

In this article, we will deal with investors starting a savings habit for the first time and wish to start investments in mutual funds.


The New Investor

If you have just started a career and wish to kick start some savings, then mutual funds can be a great way to do it systematically.

But then, if you are new to equity markets, then investing in equity mutual funds and experiencing any short-term volatility can spook you out of mutual funds, which otherwise make for great long-term products.

Hence, for an investor with little or no knowledge about equities, the following steps may make life easier:

– Consider making a start with debt-oriented products (such as MIPs), which offer 10-25% exposure to equities. This will provide a good entry point to equity investing.

– It is a misconceived notion that MIPs are only for those who look for monthly income. MIPs can be held with a growth option. Their simple aim is to generate debt-plus income, with the additional equities they hold. This may be a better option than going for a recurring deposit.

– Hold some surplus in liquid/ultra short-term funds to build an emergency fund, or if you would like to set  aside some amount for your own spending – ranging from buying a mobile, to going on a vacation, or for expenses incurred to apply for B-school a year or two later.

Consider parking your incentives and bonuses in this kind of contingency fund. You can always shift this money to other investment avenues later. It is likely that it will be spent if it remains in your savings account.

– In about a year’s time, you can gradually add balanced funds which will offer 75% exposure to equities (and the rest in debt) and then a year from then, start off with diversified equity funds with a large-cap exposure.  Again, use only the SIP route.

– At this point, you can start considering tax-saving mutual funds as well, for your Section 80C benefits. Until then, use other tax options such as PPF, EPF and some basic term insurance and medical insurance for your tax benefits.

– By the time you are over 3 years into your career, you may start having specific goals such as saving to buy a property, upgrading your lifestyle with consumer durable products, saving for your own marriage, or investing for your retirement or for your family’s needs – such as child’s education.

At this juncture, you can have a well allocated portfolio of equity and debt funds. This may have liquid/ultra short-term funds for short-term requirements and a combination of equity and income funds for long-term goals.

Please note that you may even begin this whole process with a good asset allocation strategy, if you already have fixed goals in mind, have an idea on how to go about it, or have the right guidance. Otherwise, the above will be a more phased approach.

The biggest advantage with mutual funds is that it allows you to ride different asset classes (equity, debt, gold) using the same vehicle. Hence, it becomes easy for you to have an asset-balanced approach once you start investing towards specific goals.

Not New To Equities

But if you are one of those young investors acclimatised with the equity markets while you were a   student, then it is that much easier for you to start off with equity mutual funds. Since your job may not allow you to spend too much time tracking stock markets, mutual funds could be a good platform to invest in the markets. In this case:

	Start off with a basket of diversified funds and slowly take some exposure (up to a third) to mid-cap funds.
	If you wish to play any specific sectors, use your familiarity in the stock market to pick stocks directly in such a sector, instead of going for a sector fund.
	Try to use active tools such as trigger options or value averaging plans, if you understand gyrations on the equity markets.
	Actively consider allocating some money to debt funds so as to have an asset balanced portfolio. This will come to your rescue in times of a bear onslaught. You may like to note that even a 5-year equity portfolio is not immune from bear markets and can generate negative returns, especially if you invested a lot in the peak. Debt allocation acts as shock absorbers then.
	A contingency fund is also a must here if you do not already have one.


But there may be some baggage that you may be carrying from your equity investing/trading days that you need to shed, if you are making a start with mutual funds.

One, there can be no trading business in equity funds. It follows that equity funds are meant for the long term. The longer the better. Think in multiples of 5, if you can, when you think of equities.

Two, lower NAV does not mean a fund is cheap. NAVs are nothing but the current unit value of the money managed by the fund.  It is the return potential from the point you invest, that matters.

Three, you may have booked profits in stocks or exited them when you made your money. You don’t have to do that in mutual funds because the fund manager is constantly churning the portfolio, booking profits and entering new opportunities on your behalf.

That means he/she is actively managing your portfolio. Hence, avoid constant chasing of returns and churning of funds. Resort to selling if you have the following reasons: One, you are exiting simply because you need the money at that point or are nearing your goal. Two, the fund has been consistently underperforming its benchmark and peer group for several quarters. Three, when you do portfolio rebalancing.

Now, the third point requires some explanation. If you have a portfolio with allocation towards different asset classes, you should consider rebalancing them once a year if they have moved away from your original allocation. For instance, if you had a 75% exposure to equities and it moved to 85% after a rally, then this may be the time you reallocate by exiting some equity and moving to debt.

To sum up, as a young investor, invest systematically, take on equities in a phased manner to help achieve long-term goals and use debt judiciously to counter equity volatility. Gold, will remain an option, if you wish to use it as a diversifier.

Consider using FundsIndia’s Select Funds list to pick your fund from any of the categories mentioned there, according to your choice.

Happy investing!

Other articles you may like
	FundsIndia Reviews: NFO of ICICI Pru Value Fund – Series 1
	Why you need mutual funds for retirement planning
	Three things to do after you get your first salary
	FundsIndia Explains: Debt fund jargon





			
			Post Views: 
			4,332
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    fund recommendation
                

			

            	


			
			12 thoughts on “FundsIndia Strategies: How A Young Investor Can Build A MF Portfolio”		


		
			
			
				
					
												Santosh says:					


					
						
							
								August 6, 2013 at 2:52 pm							
						
											


									

				
					Very nice article, Vidya!

Very useful for the new investors like me.

Thanks a lot!

				


				Reply
			

	
			
				
					
												Ramachandran says:					


					
						
							
								August 7, 2013 at 2:04 pm							
						
											


									

				
					I am new to share market.  I am interested in trading and want to have a monthly income of Rs.10000 to 15000/-.   I would like to know in detail which one is safe and risk free.  To start with how much I will  have to invest to get the monthly income as above and what are all the

assistance /guidance you will provide so that I myself become self-confident and familiar with the markets and trade on daily basis.

Expecting your valuable response please.

Thanking you,

V.Ramachandran

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								August 7, 2013 at 3:44 pm							
						
											


									

				
					Hello sir,

there is no such thing as safe or risk free stock trading. If someone says that or offers fixed returns in any market-linked product be in shares, commodities or currencies, pl. stay away. Share trading on  daily basis is highly risky and involves understanding of the stock market. They may generate as much or more losses as they generate gains. Hence, it is not a good idea to depend on share trading to fetch you a fixed monthly income, esp. when you do not have knowledege about it. If you are still keen, you may kindly check FundsIndia’s equity platform and call the support team for more details: http://www.fundsindia.com/content/jsp/learn/equity_acc.jsp

tks

				


				Reply
			




	
			
				
					
												rajat says:					


					
						
							
								August 8, 2013 at 10:11 pm							
						
											


									

				
					Hello mam,

I have started earning a few years ago and already have an RD, ELSS investments for every financial year, Fixed deposists and sweep-in enabled in my savings account. Most of the personal finance forums i visit including FI encourage investors to start early for single reason, compounding i.e. giving time for investment to grow. But at the same time you people also encourage youngsters like me to diversify and invest in stock markets and mutual funds (for supposed risk-taking appetite due to less responsibilities).. what i don’t understand is how can compounding and shares go hand-in-hand?? stocks are totally un-guaranteed instruments and it is entirely possible that even after 5-6 years stocks remain at same levels (granted transient fluctuations).. how does compounding work with stock markets?

You may say that stocks generally go up in the long term, but since this presumption is totally a guess, is it wise to relate stock based returns(if any) with something as precise and as mathematically verifiable as compounding??

Your insight will be appreciated. Thanks..

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								August 12, 2013 at 11:39 am							
						
											


									

				
					Hello Rajat,

Compounding simply works when you allow what you gain to remain invested. In a FD, if you do not take the interest our and opt for a cumulative option, then the interest again earns interest. Similarly, in stock or debt market, there are gains to be made everyday (of course there are losses too, but over a long period, history suggests that gains have been handsome) and these gains remain invested or shall be say reinvested thus earning more gains.

If you want to know how compounding work, try taking any stock say an ITC and try calculating how much a Rs 10000 investment would have delivered in say 5 year.Now in the same stock, remove the gains at the end of every year and allow rs 10,000 to be invested at the end of each year and see how much you made. You will know the difference. In mutual fund SIPs, compounded works well, simply because you not only add capital in a phased manner but allow the gains (if you go for the growth option) to remain in the fund. Remember shares are not about some wildly bought and sold instruments in the market. There is a company behind each one of those instruments – whether a TCS or ITC or ONGC, which has been growing and will continue to grow. It is the fundamentals of these companies which drive the prices of their shares.

Thanks.

				


				Reply
			




	
			
				
					
												roby says:					


					
						
							
								August 11, 2013 at 4:12 am							
						
											


									

				
					I am interested in investing in mutual funds but have no financial know how . i would also like to plan my finances , investment insurance and wealth creation .. Do you offer any advice or help to sort it out ?

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								August 12, 2013 at 10:35 am							
						
											


									

				
					Hello Roby, Yes, Once you open your free account with FundsIndia, you will get investment advisory on an ongoing basis free of cost. We also have innovative services such as Smart Solutions to help people save for their children’s education, marriage or for your retirement. These portfolios will be monitored and fund changes (when funds underperform) and asset rebalancing will be recommended in a timely manner to you. You can simply choose to make the changes at a click. Pl. read details in our website to open an account. You can then request for “calls schedule” with our advisor (click the help tab in your FundsIndia account) and have a detaield chat. Advisory services are available for all investors with an activated account.Thanks.

				


				Reply
			




	
			
				
					
												biswajit says:					


					
						
							
								August 20, 2013 at 3:30 pm							
						
											


									

				
					Hello Vidya mam,

I want to start investment in mutual fund.I have read so many articles online about mutual fund.I know SIP investment and One time investment…but mam my confusion is that suppose I don’t want to set any SIP.But I will buy one particular mutual fund same way as SIP. i.e. every month u can say I will buy fixed 2000… but when market is low I want to buy more may be 3 times in a month , may be 20000/each time…this is the reason I don’t want to fix any SIP…But is that possible ? as I have understood that investor can invest in any particular fund either through SIP or one time investment ….So pls can u clearify  the same ?

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								August 21, 2013 at 11:34 am							
						
											


									

				
					Hello Biswajit,

What you need is available as  value-add tools with FundsIndia. You can use any of these 3 options based on your reqmt: one is the trigger option where you can actually set levels of the market or NAV at which you wish to buy. So you can add units in the same fund that you already hold when such trigger is generated and an alert is sent to you for buying. The funds will be bought under the same folio in which you bought at first. Two, there is what is called a flexi SIP where you can buy diff amounts (give a range) every month but then the date is fixed.

 Three, there is VIP, where the backend formula will design purchases in a way that you buy more when markets are down and less when markets are up. Outside of these 3, you can always make any addl. purchases at any time you want, in the funds that you are already invested in (whether you invested first through SIP or through lump sum). Do read http://pages.fundsindia.com/pages/learning/using-fundsindia-value-added-features/ for the value add features we offer. once you have activated your account, you can also seek our advice/review for funds using the ‘Ask Advisor’ feature in your account.This is free of cost.

Thanks, Vidya

				


				Reply
			




	
			
				
					
												S.Sudharsan says:					


					
						
							
								August 25, 2013 at 5:28 pm							
						
											


									

				
					Hi Vidya

Iam new to MFs,Iam planning to save money for higher studies. I need the money in 2 years and currently I can save upto Rs 10000 pm. I have opened a RD of Rs 5000 pm for 2 yrs at 9.5% and a FD of Rs 25000 for 2yrs at 9.5%. I would like to accumulate at least 6 Lakhs at end of 2 years. Since i had taken a life insurance, my tax is exempted.Kindly suggest me MFs which give returns of 12 to 15%. 

Regards,

S.Sudharsan

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								August 26, 2013 at 10:13 am							
						
											


									

				
					Hello Sudarshan, I hope you are aware that you have a steep goal. Your RD of Rs 5000 a month at 9.5% will fetch you about 1,32,530 (before tax) and your FD will give you about Rs 30,000. That leaves you with about Rs 4,38,000 to be saved in 2 years. Even assuming a 12% annual return (which is very aggressive for a short time frame), you will need about Rs 16200  of savings every month in addition, for the next 2 years. With your current surplus (Rs 10,000 less the RD amount of Rs 5000) of Rs 5000 a month you can build about Rs 133000 at 10%, leaving with over Rs 3 lakh deficit.

For your time frame MIPs/debt funds would be safer than pure equities and returns for this category can only be about 10%. MIPs may deliver more if equity markets perform. For portfolio suggestions, we would like to answer queries using the ‘Ask Advisor’ feature available for free to all our investors. Thanks.

				


				Reply
			




	
			
				
					
												jmehta1j says:					


					
						
							
								December 28, 2018 at 10:52 pm							
						
											


									

				
					Stocks Versus Mutual Funds. Stocks are riskier than mutual funds. Funds pool a lot of stocks (in a stock fund) or bonds (in a bond fund). That reduces risk because, if one company in the fund has a poor manager, a losing strategy, or even just bad luck, its loss is balanced by other businesses that perform well. Thanks for sharing

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


