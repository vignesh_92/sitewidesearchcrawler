

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Views: 2017 – Outlook and strategy

            		January 5, 2017 . Vidya Bala
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Research Desk . Views
                    
		
            	

            	If the below data has weakened your faith and interest in equity, then it could be a good thing!

‘The safest and most potentially profitable thing is to buy something when no one likes it’ said Howard Marks, one of America’s more successful investor. If you are not liking what you are seeing below, then you know Howard Marks’ wisdom should be your strategy for equity in 2017.

But what you buy and how you buy would truly matter and this article will seek to address that after letting you know what to expect in 2017.


	Indices	2016	2015
	Sensex	2.60%	-5.00%
	Nifty 50	3.70%	-4.10%
	S&P BSE 100	4.20%	-3.20%
	S&P BSE 500	4.30%	-0.80%
	S&P BSE Midcap	8.50%	7.40%
	S&P BSE Small cap	2.30%	6.80%
	Returns for calendar years.



Equity markets to be volatile

 Open  a FREE Account Now! 

We believe the equity markets could be volatile for a good part of 2017 because of the following near-to-medium-term risks, all of which are a spill-over effect of what went on in 2016.

	Impact of demonetisation on money supply and economic growth
	Impact of GST; the pain from which would kick in well into the year as its implementation has been postponed to June from April
	Impact of any higher-than expected rise in crude and commodity prices on commodity users
	Impact of US Fed rate hikes, if they come in quick succession


To add to these, there are too many global factors at play. These include elections due in multiple nations, including powerhouses France and Germany across Europe, Brexit being implemented, and policies of the new US government, and the rate and extent to which the US Fed hikes rates. The exact outcome and impact of what is yet to happen is best left out at this stage. What holds is that these have been, and will be, continuously evolving scenarios, impacting either corporate earnings growth or FII flows. In other words, you can expect bouts of volatility through the year. 

What will keep the market going

Among the various events that will unfold in 2017, Indian markets will be clued on the following:

	Budget announcements
	Rate cuts
	Corporate earnings growth


While there may be much talk of a fiscal stimulus through the budget, we believe both rate cut and government spending (monetary and fiscal policy) must go hand in hand to tackle not only the pressure of a slowdown from demonetisation but also the unwillingness of companies to borrow and build.

We are not going to get into more details of these macros at play. What matters to us is earnings, stock market valuations and what can deliver in this scenario. Let’s dive into this.

Why there is upside yet

Despite two years of flat returns, the markets are at a price earnings ratio of around 20 times trailing (22 for the Nifty) and at 16 times on a forward basis (2017-18), considered to be trading marginally at a premium. If that be the case, where are the returns to be had? Where would fund managers go, to pick good stocks for you?

Let us take a broader index like MSCI India (which represents 85% of the India listed equity universe) to take stock of the valuations and returns of the sectors.

 Open  a FREE Account Now! 


	Sector	Returns	PE
		2016	2015	2014	Dec-16	Dec-15	Dec-14
	Consumer discretionary	5%	-7%	31%	20.4	23.7	15
	Consumer staples	-5%	3%	20%	35.4	38.9	44
	Energy	5%	0%	7%	13.2	12.8	11.1
	Financials	-4%	-5%	47%	16.0	15.2	17.5
	Healthcare	-15%	7%	42%	26.0	36.8	28.7
	Industrials	0%	-12%	43%	30.2	30.2	26.4
	Tech	-9%	4%	15%	16.4	19.7	19.7
	Materials	33%	-21%	-8%	48.3	-58.6	16.1
	Telecom	-27%	-4%	-8%	19.2	23.7	28.4
	Utilities	11%	-10%	13%	15.0	15.7	13.1
	Source: MSCI, Morgan Stanley Research



The above table will tell you the returns delivered by respective sectors and their price earnings ratio at different points in time. Now, the point to note here is that the sectors that are now reasonably valued are not only the ones that account for a chunk of the index, but also account for a chunk of the profits of the index. In other words, there is still potential in the broad markets from ‘value’ sectors. And a few triggers could move these value sectors around:

	While the banking clean-up may get lengthened a bit if there are demonetisation-related defaults, it could well be offset by better financial strength and financial turnaround commodity companies (especially steel) that were large defaulters earlier. High CASA coming from demonetisation would likely take care of any further pain in the space
	The energy space has already been boosted post the recent decision of OPEC and certain non-OPEC countries to cut back on crude oil production. Both upstream and downstream companies can be expected to benefit from this
	Power utilities, especially regulated ones have better standing in a slowdown as their cost-plus model and assured returns provide earnings stability
	Commodity plays could also benefit from any recovery in global demand coupled with a supply shortage developing in the non-ferrous metal space
	The high sentiments in the US if translated into meaningful spending, could well lift sentiments for the IT space and current modest valuations could help


Given the low base of earnings for these sectors in the year gone by and that they account for a chunk of the earnings of the bellwether indices, a modest increase in earnings could deliver returns for the index.

We expect this is where fund managers would try and pick their stocks. But this does not mean the other sectors would not provide opportunities. For instance, the correction in consumer stocks as well as pharma could well provide select stock opportunities.

Hence, even if markets do remain volatile, it may be too pessimistic to assume that the market no longer offers opportunities.

Where to invest in equity

 Open  a FREE Account Now! 

As an investor, we think investing a higher proportion in large-cap and multi-cap/diversified funds will yield better than going behind midcaps. For one, the trailing valuation of midcaps (Nifty Free Float Midcap 100) at close to 30 times is overheated compared with the large-cap space (Nifty at 22 times). Two, many of the sectors with potential that we discussed above have better quality companies in the large-cap space and in any turnaround, the larger companies get to reap the benefits first. Three, the below chart will tell you that the return on equity in the large-cap space is far superior and deserves better valuations compared with midcaps.


 

We will, over the course of the next few months, come out with calls on funds that are well-positioned to make the best of the opportunities in 2017. 

Diversify your debt holding

For those invested in duration funds such as dynamic bond funds, it would have been a super-yielding 2016. The rate cuts and ensuing bond rally ensured double-digit gains. While those investing afresh may still have money to be made in this space, we would suggest addition of accrual funds besides dynamic bond funds.

Why do we say this? For one, the returns from duration is over for a good part, although you can expect it to comfortably beat the FD returns over the next few years. Two, while rates cuts may have to come about, when and how calibrated they are would depend on economic growth numbers as well as the Fed rate hikes. Three, there is already a significant pull out in the gilt space by FPIs who have been booking profits earlier. Fresh money from their end may reduce as the Fed rate hikes will reduce the spread between the two nations’ bonds.

On the other hand, there is still plenty of opportunity on the corporate bond space, where marginally higher yields do not necessarily come with high risk. Adding short-term debt funds (2 years) or income funds (at least 2-3 years) or quality credit opportunity funds (at least 3 years) if you have risk appetite for the last, would be a necessary diversification when you add duration funds.

Holders of bank deposits that mature this year, would have little choice but to go for debt funds to ensure that they do not suffer from reinvestment risks.

Asset allocation necessary

 Open  a FREE Account Now! 

2016 illustrated quite well the benefit of asset allocation (see graph). We believe such allocation, based on your time frame and risk profile would be a necessity to shock-proof your portfolio. While the below returns should not be expected from debt in 2017, holding debt will certainly reduce your portfolio volatility while allowing you to participate in the debt rally that still holds steam.





FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis of investment decisions. To know how to read our weekly fund reviews, please click here.

Other articles you may like
	FundsIndia Recommends: Canara Robeco Large Cap+
	“60 per cent of Sensex’ earnings are immune to most macro issues”
	Mid and small cap funds turning wary of small-cap stocks
	FundsIndia Strategies: The why and how of portfolio diversification





			
			Post Views: 
			2,364
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    fund recommendation. Investments. Mutual funds. Personal Finance
                

			

            	


			
			2 thoughts on “FundsIndia Views: 2017 – Outlook and strategy”		


		
			
			
				
					
												Vijay says:					


					
						
							
								January 5, 2017 at 3:37 pm							
						
											


									

				
					Will equity LTCG be taxed in 2017 onward?

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								February 12, 2017 at 8:10 pm							
						
											


									

				
					LTCG on equity (held for more than 1 year) continues to be exempt. On debt, LTCG (holding of more than 3 years) continues to get indexation benefits.

				


				Reply
			







		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


