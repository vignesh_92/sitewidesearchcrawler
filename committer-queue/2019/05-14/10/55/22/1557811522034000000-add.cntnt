

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Views: 2018 Debt Market Outlook

            		January 17, 2018 . Vidya Bala
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Research Desk . Views
                    
		
            	

            	
Synopsis
	Further rate cut possibility bleak for now
	Debt markets appear to have overdone concerns and factored rate hikes
	Economic scenario not set for any near-term rate hikes
	Accrual strategy is the way to go
	Hold duration funds for your original time frame and avoid premature exit 




These are extraordinary times. The equity market has rallied without earnings entirely coming on-board with hope that there would be an economic revival. The debt market on the other hand, appears to be factoring an interest rate hike, as if there is sound economic growth underway; that can force a rate hike. The equity market is optimistic and the debt market, writ with pessimism. But we prefer the pessimism in the debt market for the simple reason, that there is more opportunity in pessimism.

What transpired

Before we move on to our view on the debt market and the strategy you need to adopt, let us quickly understand the events in 2017 and the risks for debt in 2018.

2017 saw a roller coaster ride for duration funds. The data below will tell you that from a peak 1-year return of close to 14% In January 2017, the Crisil composite bond fund index’s (which is the benchmark for most dynamic bond funds), return has steadily dwindled to less than 4 percent now.



From a surprise neutral stance in early 2017 followed by a 25-basis point cut in August and a further no rate cut stance, the RBI kept the debt market guessing for a good while, causing volatility. The image below will show you that yields of 10-year gilts started moving up despite the rate cut in August as the market started factoring other worries of rising inflation and concerns over fiscal deficit. Rate hikes elsewhere in US, we think, the market handled quite maturely.



What’s causing the stress

From 6.5% in September 2017, the 10-year gilt has moved a straight 7.5% now. If we must narrow down the concerns over this yield move, they would be – inflation, fiscal deficit, and rising commodity prices. Let us look at these factors.

Inflation and rising commodity prices: Retail inflation moved from 3.58% in October 2017 to 5.21% in December 2017. High food inflation and impact of HRA hike under the 7th Pay Commission besides a low base are reasons for the rise. While, these factors are likely to settle, especially with food inflation showing no primary risk, sharp hike in crude price may pose a challenge to inflation. However, most experts opine that with US shale production remaining sound, the likelihood of countries like Russia opting out of production freeze and the continuing thrust on renewable energy in developed countries, a price shock in crude is unlikely. Hence, while this factor needs observation, it is far from threatening at present.

Fiscal deficit: With government spending reaching 96% of its targeted fiscal deficit in October 2017, worries of fiscal slippage dogged the market. Lower GST revenue because of rate cuts, further caused concerns. To add to this, the recent announcement by the Government of an additional Rs 50,000 crore of borrowing (0.3% of GDP), added fuel to the debt market’s fear. This number was reduced to Rs 20,000 crore recently.

Despite a Moody’s upgrade on our rating and a positive outlook by S&P, besides record inflows into debt market by FPIs, the market has taken the above-mentioned factors seriously enough to foresee a rate hike. Concerns exist, without a doubt. But is the reaction realistic or overdone?

Are concerns overdone?

While it is increasingly evident that rate cut scenarios are bleak now, is there a case to factor rate hike so quickly, as the market seems to?  Let us understand this in the light of when a rate hike is typically resorted to.

Let us look at earlier rate hike moves. 2013 was a clear one, with deterioration on every front and very importantly with a dangerously slipping rupee. We are not there now. So, let us go to 2006-07 when there were rate hikes under a different economic condition. Let us see the circumstances then and now.

In the equity market, experts have been stating that we are no where near the 2007 peaks, not just in terms of valuation but in terms of macro-economic growth as well. And that there is enough upside to grow to justify the multiples that the market is rewarding to stocks.

The data below also suggests that we are at a low in terms of economic growth, since the slowdown that started in 2016, ahead of even the demonetization (read our article on the slowdown here). We have sufficient surplus capacity now while a blazing economy like in 2007, typically operates at high capacities. Our credit growth has been poor, and the little growth that came was largely supported by retail credit. Even while there has been pick up in the last couple of months, industrial credit growth is still lost in the woods.


	Macro Indicators	Dec-07	Dec-17
	Capacity utilisation (as of Mar 2017)	91.70%	71.20%
	Credit growth (YoY as of Dec 08, 2017)	23.30%	9.80%
	IIP (twelve months trailing)	15.58%	2.2%*
	GDP Growth**	9.60%	6.30%
	*As of October 2017. ** For quarters ending December 2007 and September 2017.



The simple point is that the economy is just shrugging off the impact of slowdown as well as the impact of major structural reforms. It is nowhere near the 2007 exuberance in terms of real growth nor near the 2010-11 feel good period of forced, indiscreet growth. If this be the case, what can force a near-term rate hike? Fiscal slippage is not at unacceptable levels, given that there has been no imprudent spending and there have been major reforms; inflation is under control, despite trending upwards and not having fuelled economic growth. And importantly the currency remains stable.  On the other hand, the economy is struggling to shrug off challenges and revive. Hence, factoring in a rate hike scenario at this stage appears a case of too much, too soon. So, what are we expecting?

What we expect

	We are not expecting any rate cuts unless there is any significant slowdown in the economy or very low inflation. Therefore, portfolios still positioned to play rate cuts can be risky.
	Having said that, a no rate cut does not mean the yields cannot ease from here. A moderate fall in inflation and data from Budget may provide cues for a yield ease. And remember, FPIs are piling up Indian gilts, seeing ‘value’ in the swift up move in yields.
	The increase in yields in the short-term medium term corporate bond space i.e. 1,2,3 and 5-year corporate bonds is higher than the increase in long-term gilts in the past one month. That makes the short to medium space attractive in terms of both accrual and in the event of a yield easing. This comes with less risk than long-dated gilts.
	A rate hike in the near term looks unlikely for the arguments we placed earlier. What we are expecting is a pause for at least a few quarters.
	Even in the event of any rate hike in the later part of the year, the current harsh move in yields is likely to leave the market less hurt and cause accrual to get more attractive.


What should your strategy be?

	We think a high risk-reward strategy is not a prudent one to play now. That means, taking calls on duration now can be risky, although an easing of yields can provide some quick gains. Avoid fresh entry into duration funds such as dynamic bond funds. If you are already running STPs or SIPs in this category, continue them until the end of this March fiscal and then continue to hold the funds but shift the SIPs to accrual funds.
	If you are simply holding duration funds (dynamic bond funds or gilt) we would strongly urge you to hold them for your originally intended period, to see through the cycle. We recommend these funds for not less than 3 years. Holding them to that tenure will be necessary to derive benefits. Exiting at this stage, especially if you entered in late 2016 or early 2017, will not be a wise decision. It is the same as exiting equity when market is down. For those who entered this space thinking that debt funds never deliver negative returns, you need to take your time frame more seriously and align your time frame with the right category of fund. That is paramount if you wish to avoid pain in the debt space.
	For fresh investors, an accrual strategy is adequate to earn FD-plus returns at this stage. With short to medium term yields rising sharply, we think the full spectrum of accrual – ultra short-term, short-term and income funds all provide sufficient opportunities with good quality debt instruments.
	However, we wish to alert you to the fact that these are abnormal times of liquid funds delivering higher than most other debt categories. This may not sustain as short-term rates are already stretched. Hence, choose your category based on your time frame rather than based on current returns.


We will review our debt outlook if the budget or inflation numbers throw any negative surprises.

FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis for investment decisions. To know how to read our weekly fund reviews, please click here.

Other articles you may like
	"Our preference for under-levered companies helped us in economic downturn"
	The opportunity lies in cyclical sectors
	Why you should not invest in equity funds for dividends
	FundsIndia explains: What is portfolio concentration?





			
			Post Views: 
			1,873
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    debt funds. debt markets. investing. Investments. Mutual funds. Personal Finance
                

			

            	


	
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


