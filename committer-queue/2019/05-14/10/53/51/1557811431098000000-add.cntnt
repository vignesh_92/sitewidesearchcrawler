

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		2018 – When Select Funds stayed ahead of the pack

            		December 19, 2018 . Mutual Fund Research Desk
                    

	
                    
                                    Advisory . General . Mutual funds . Research Desk . Views
                    
		
            	

            	2018 was a tough year. For equity markets, for debt markets and for currency. It was a tougher year for mutual funds what with the swings to their portfolio as a result of SEBI’s new categorisation.

Almost through the year, equity markets – as pointed out by us earlier – saw key indices being influenced by a few stocks which have a high index weight doing well. Therefore, 2018 markets have also been marked by the phenomenon of index saying one thing while the stocks were doing something quite different. It is against this backdrop that we take stock of the equity Select Funds performance.

Putting it in perspective

Overall, 2018 was a hard year for us in terms of equity funds. We will need to closely track how funds play the market and their ability to beat the indices. On the debt side, our tight norms have helped us steer clear of the troubles hurting other funds apart from one-off unpredictable instances. We’ll be taking a relook at some debt strategies and build in further criteria to limit impact of rating downgrades.

But do note that year-end or any short-term performance should not be used to judge funds. This review is just an exercise to show you how our funds delivered, what we did to achieve outperformance and the lessons we’ve learnt. To put returns into better perspective, a look at how our funds fared over the longer term – 5 years for equity and 3 years for debt, given their holding periods – is below.


	Returns of FundsIndia's Select Funds
		5 years		3 years
	Equity- Moderate risk	17.30%



	Debt - Short term

	7.70%


	S&P BSE 200 TRI	15.20%

	CRISIL Short Term Bond Fund Index

	7.40%


	Nifty 500

	15.30%

	Short duration/ banking & PSU average

	7.10%




	Largecap/multicap/ value/ dividend yield category 	15.70%

 

		 

 
				
	Equity- High risk

	22.20%

	Debt - Long term

	

7.40%


	Nifty 500	15.30%

	CRISIL Composite Bond Fund Index

	7.60%


	Nifty Midcap 100 TRI	19.70%

	Corp. bond/ medium duration/ dynamic bond/ credit risk average	7.20%
	Equity large & midcap/ midcap/ smallcap category average	18.60%		



2018 performance

We now move on to the performance in this calendar. On an average, largecap and multicap Select Funds outperformed their categories by a significant margin but lagged benchmark indices. Mid-cap and small-cap Select Funds outdid both benchmark and category.

In debt markets, even with the rollercoaster ride, short-term debt funds in FundsIndia’s Select Funds beat the category average and even the benchmark, no mean feat given the caution we adopt in debt funds. Long-term debt funds though, were a mixed bag.

Equity funds – moderate risk: This category mixes largecap funds and those from other categories that maintain a largecap bias to their portfolios. We have slowly been reducing the number of pure large-cap funds in the Select list. We saw the return potential for largecap funds shrinking with the new market-cap rules and with the largecap space maturing. For our moderate-risk funds, the BSE 200 would be a better index. The moderate risk category was down 5.12% on an average, trailing the BSE 200’s 2.85% loss and the BSE 100’s 1% loss (up to Dec 7).


	Returns (Calendar returns up to Dec 7, 2018)
	Equity - moderate risk

	-5.1%


	Category average*

	-7.3%


	BSE 200

	-2.9%


	Nifty 500	-5.8%



*includes largecap, multicap, value/contra, dividend yield

A few reasons explain the lag. One is the index itself, as mentioned earlier. Large gains in a few stocks that have high index weights have influenced benchmark returns. To give you a quick understanding, the Nifty 100 index is up 1.3% for the year. The Nifty 100 Equal Weight index – which assigns all the stocks with the same weight – is down 10.3%. All large-cap biased equity funds were in similar waters. 

Second, we’ve also had a couple of funds pulling down the average. We still took a conscious call of not pulling out sound funds with short-term underperformance. Our funds have been better than peers even with the index lag. Third, funds that changed categories went through phases of adjusting their portfolios that caused some underperformance.

Among the underperformers, DSP Equity Opportunities, stepped up mid-cap allocation forced by the new categorization just at the peak of the mid-cap rally and this led to returns dipping. Similarly, longer term calls that are yet to work have bogged down funds such as Franklin India Equity, Aditya Birla Sun Life Frontline Equity and SBI Bluechip. Motilal Oswal Multicap 35, a fund we moved into this category midway through the year was hit by a couple of wrong calls. Its focused approach amplified the effect of stock price drops.

But as our list is a mix of unique strategies, we still stayed ahead of peer average. For this calendar, largecap and multicap funds (including value/dividend yield) averaged a worse 7% loss. For example, growth-oriented Mirae Asset India Equity has done well, as has a focused Invesco India Growth Opportunities. A wholly different Parag Parikh Long-Term Equity outperformed even the indices. These compensated the slide in other funds.

Further, our taking cognisance of the increasing challenges for largecaps to beat their benchmarks saw us removing Franklin India Bluechip. We will continue to be watchful of the large-cap space and funds that aren’t showing signs of picking up.

Equity – high risk: In this space, we mix of aggressive large-and-midcap funds, midcap funds and smallcap funds. Essentially those that have a tendency to be biased towards midcaps or sport a very high-risk strategy. On an average this calendar, these funds have lost 13.3% (up to Dec 7th). The Nifty Midcap 100 is down 19%. The Nifty 500 index sports a much lower loss (down 5.8%), but our high-risk category features mostly mid-and-small cap based funds. Here again, we’ve done better than peers. The average for large-and-midcap, midcap and small-cap categories is a 14.7% loss.


	Returns (Calendar returns up to Dec 7)
	Equity - high risk

	-13.3%


	Category average*



	-14.8%


	Nifty Midcap 100	-19.3%



*Includes large & midcap, midcap, and smallcap

The midcap/smallcap category has been hard for us especially in the 2016-17 period when the midcap and smallcap rally really heated up. As we looked for optimal returns for lower volatility, we stayed away from many of the chart toppers. Our view has always been that falls, when they occur, are steep in the mid-cap and smallcap segment and extreme aggression is not preferable. This had often caused our Select midcap and smallcap picks to miss opportunities that other funds captured.

Our insistence on downside containment in a high-risk category served well in this market. Our midcap-based funds especially those such as Franklin India Prima, HDFC Midcap Opportunities and Mirae Asset Emerging Bluechip contained downsides better than the Nifty Midcap 100 and most peers. We had also removed underperformers such as BNP Paribas Midcap and earlier drags such as SBI Magnum Midcap. Mixing strategies worked well, as it did in the moderate risk space. The addition of Invesco India Contra an offbeat part-value part-growth fund and the opportunistic Aditya Birla Sun Life Equity helped.

Lags in this category include L&T India Value which is transitioning from a midcap dominated fund into a more multi-cap play and Principal Emerging Bluechip, which is among our more aggressive picks. We’ve also worked on being a little more aggressive in this space, especially post the new categorization.

Debt funds – short term: After a tumultuous 2017, debt markets had a second year of ups and downs. The outlook at the start of this year seemed clear enough. But an uncommon mix of high oil prices, depreciating rupee, low inflation, rising global interest rates, and tight NBFC liquidity plagued debt markets through the year. However, debt funds have mostly stuck to accrual strategies and portfolio yields have trended higher.


	Returns (Calendar returns up to Dec 7, 2018)
	Debt long term



	6.3%






	 Category average*	5.5%




	CRISIL Composite Bond Fund Index

	6.0%



*Includes short duration and banking & PSU funds

In the short-term space, our Select Funds delivered an average return of 6.32% this calendar (up to Dec 7th). This beats the CRISIL Short-term Bond Fund index, no mean feat given that the index is a near-perfect mix of papers with different maturities and risk levels.

The returns are above the category average 5.54% as well; again, this is quite the achievement since we have built in very strict criteria for fund selection here. Our view on debt in the short-term space has been to forgo returns for lower risk. Shorter timeframes do not leave room for risk, on the credit side or duration side.

We still managed to beat peers due to several reasons. One, our funds capture a range of opportunities in the debt space. Funds such as UTI Banking & PSU Debt capitalise on bank borrowing rates rising, while those such as HDFC Short Term Debt and Aditya Birla Sun Life Corporate Bond snag good yields in the corporate debt market. Two, our funds have consistently held above the peer average due to their ability to identify higher-yield papers without stepping up risk. We look for funds that keep above average on returns and below average on risk.

Debt funds – long-term: While we try to have a mix of strategies in this category, we were hurt by dynamic bond strategy and reduced it. We also decided to stay away from credit barring one fund. In a year parched for returns, unless one held high yielding strategies or made some gains from last minute duration play (post October), the returns in this category was hard to find. Erring on the side of conservatism, we did neither and hence tripped.


	Returns (Calendar returns up to Dec 7)
	Debt long term

	4.2%




	Category average*



	4.9%


	CRISIL Composite Bond Fund Index	4.9%



*Includes dynamic bond, credit risk, medium duration and corporate bond categories

The long-term debt funds average returns this calendar holds at 4.2%. This is slightly below the average of the corporate bond, dynamic bond, medium duration and credit risk categories. This slight lag is primarily due to a delayed reaction to the duration risk of last year, which continued into this year. Specifically, we had given both Aditya Birla Sun Life Dynamic Bond and UTI Dynamic Bond the leeway based on past record and that rate outlook last year was extremely volatile. The latter fund changed over almost entirely to accrual earlier on and was less aggressive, limiting its drag. But ABSL Dynamic Bond was much more aggressive, suffered much more and we only recently pulled this fund from our list.

DSP Credit Risk, the only credit risk left in our list also dragged performance despite a good track record earlier. The fund had straddled the gap between high-quality low-risk accrual and low-credit high-risk credit space. Its re-categorisation into a credit risk fund saw it lag pure credit risk funds. It was also the only fund in our list that had to take a massive write-off on an NBFC default. This episode teaches us that risk in debt can be a zero one game.

Apart from these instances, our long-term debt funds have all stayed above the average.

Keep in mind that 2018 was a tough year for the markets and that consistency over the longer term is more important than 1-year returns. At every quarter review, we provide an explanation for performance and action that you need to take, if any, for your funds. Please rely on those communications or your advisor, to make decisions.

Other articles you may like
	FundsIndia Recommends: Tax-saving Funds Under Section 80C
	Let's have the 'tax talk'
	Simple rules to make your portfolio work better
	FundsIndia Reviews: Invesco India Contra





			
			Post Views: 
			3,582
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                                    

			

            	


	
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


