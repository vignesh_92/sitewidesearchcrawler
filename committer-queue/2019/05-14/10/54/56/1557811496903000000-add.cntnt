

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Recommends: HDFC Mid-Cap Opportunities

            		May 24, 2017 . Lakshmeenarasimhan S
                    

	
                    
                                    Advisory . General . Mutual funds . Personal Finance . Recommendations
                    
		
            	

            	
What?

	A mid-cap fund
	Can invest up to a quarter of the portfolio in large-cap stocks


Why?

	Lower volatility compared with peers
	Consistent performance across market phases


Who?

	 For high-risk investors with a long-term investment horizon





HDFC Mid-Cap Opportunities fund (HDFC Mid-Cap), a mid-cap fund with a consistent track record, has delivered 18 percent annualised returns since its inception in June 2007. Its launch as a closed-ended fund, ahead of the 2008 financial debacle, helped it manage the volatility of the 2008 crisis well. This helped the fund stay fully invested and bounce back the following year. But the fund has proven its consistency even after it became an open-ended fund thus cementing its track record.

The fund beat both its category average and its benchmark over longer time frames of 3, 5 and 7 years as well. This has come on the back of a combination of good stock picks, low volatility and consistent performance. There has so far been no change in fund manager in this fund. This has also helped maintain continuity in strategy.

 Open  a FREE Account Now! 

Suitability

At a time when midcaps have seen a steep run (with the Nifty Free Float Midcap 100 index delivering 21 percent compounded annual return in the past 3 years), you may wonder whether a mid-cap fund should form part of your investment now. Mid-cap funds do hold potential to generate superior returns and are good additions to any long term portfolio. However, given the rally in this space, many stocks have run up ahead of their earnings potential. Stocks well down the market-cap curve, which several mid-cap and small-cap funds invest in, are even riskier with limited liquidity and other quality concerns.

Therefore, it is important to choose those mid-cap stocks that still have the potential to grow over the long term, are reasonably valued, and are not extremely risky. On these counts, HDFC Mid-cap delivers. It holds almost a fourth of its portfolio in large-caps (as allowed in its mandate) and contains downside well during market corrections. It avoids small-cap and micro-cap stocks. In the mid-cap space, the fund has stayed away from short-term opportunities and instead chosen stocks that are beneficiaries of a long-term turnaround in the economy.

It is this mix of large-cap strategy, diversified approach and avoid smaller market-cap segments that has allowed this fund to manage a mid-cap theme at a time when quite a few mid-cap funds have restricted their inflows because of growing AUM size. HDFC Mid-Cap has not thus far restricted investments, despite its sizeable AUM.

 Open  a FREE Account Now! 

However, given the market conditions, exposure needs to be taken in a phased manner through SIPs/STPs. A long-term time frame of not less than 5 years and a high risk appetite is warranted.

Performance

While short-term point-to-point returns may not place HDFC Mid-Cap in the toppers’ chart, the fund scores on consistency in performance.

When the fund’s one-year returns are rolled daily over three-year time frames, it beats the benchmark an impressive 92 percent of the time. It has beaten the category average 82 percent of the time as well, a commendable figure, given that it avoids the high-returning smaller sized stocks that peers have invested in.  The fund’s performance gets even better over longer time frames. It beat both its benchmark and its category on all occasions when its three-year returns were rolled daily over the last five years.



HDFC Mid-Cap has the lowest standard deviation amongst its peers suggesting that its returns do not swing wildly. The fund’s risk-adjusted return (Sharpe ratio) is in the top quartile suggesting that the fund is delivering returns commensurate to the risk it takes. It has also contained downsides better than the index at all times aided by a well-diversified portfolio with selective large-cap picks. For example, during the calendar year of 2013 when the markets were down for much of the period, the fund delivered 9 percent returns while the index was down 5 percent This performance was also better than some of its peers such as Franklin India Prima, Mirae Asset Emerging Bluechip and L&T India Value Fund.

Portfolio and strategy

The fund held a highly-diversified portfolio of 76 stocks as of April 2017. The top 10 stocks account for only 23 percent of the portfolio, with the top exposure a tad above 3 percent. A well-diversified portfolio especially in the mid-cap space helps contain downsides better.

Although a mid-cap fund, HDFC Mid-Cap has leeway to invest up to 25 percent of the portfolio in large caps and it utilises this option to the hilt. In the past two years, the large cap exposure has been consistently at around 25 percent.  This has lent stability to the portfolio. With a fourth of its holding in large caps and with also a highly-diversified portfolio, how did the fund generate returns commensurate to this category? One, although midcap stocks as a segment have outperformed large-caps, the large-cap stocks held by this fund have done well in the last 2 years. Of the 12 large-cap stocks that were present in the portfolio through the year, two in three delivered returns greater than even the BSE Small-cap index which had delivered 38 percent (1 year performance as of 31 Mar 2017) during the same period.

 Open  a FREE Account Now! 

Two, the fund’s early sector calls and its patience with holding stocks and exiting them at the right time also paid off. For example, the fund has been consistently overweight on the auto sector compared with its category average. Its call on tyre and battery stocks yielded good results. Of the current six auto stocks in the portfolio five have been in the portfolio for over two years. The exit of Amara Raja Batteries was timed well as the stock has not performed as well as its peer Exide Industries, in the portfolio.

Similarly, the fund took a prescient call in going underweight on IT stocks by August 2016. It has been underweight in pharma since 2014, the time since the FDA issues with the industry started cropping up.

The fund steadily played the Indian consumer story with over 40 percent of the portfolio invested in sectors such as auto, consumer goods, private banks, and NBFCs. These three sectors along with industrials have consistently constituted more than half of the portfolio over the last two years. Stocks such as Sundaram Fasteners, Whirlpool India, Blue star, Balkrishna Industries, Tube Investments and Greenply industries which have more than tripled since 2014 have been in the portfolio even prior to 2014.



The fund follows a buy and hold approach, with a very low turnover at around 20 percent. More than half the stocks have been held for more than two years.

The fund’s AUM is Rs 15,734 crore and the fund manager is Chirag Setalvad.

FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis for investment decisions. To know how to read our weekly fund reviews, please click here. 

Other articles you may like
	Ladies, feel liberated with a purse of your own!
	Rate cut to provide benefit to your loans and your investments
	Are these mistakes coming in the way of your first million?
	Changes to FundsIndia’s Select Funds list





			
			Post Views: 
			2,441
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Advisory. fund recommendation. Investments. Mutual funds. Personal Finance
                

			

            	


			
			4 thoughts on “FundsIndia Recommends: HDFC Mid-Cap Opportunities”		


		
			
			
				
					
												Arun Agrawal says:					


					
						
							
								May 24, 2017 at 6:15 pm							
						
											


									

				
					Dear Sir : But during the last 8 days 16 May -24 May , the NAV of this Fund has fallen by more than

5% . As we have monthly SIP of Rs 2000 in this Fund , we are wondering whether we should exit ,

as other investors are also advising us . I made a reference to Ms Janaki as well yesterday and

waiting for her response .

				


				Reply
			
	
			
				
					
												Lakshmeenarasimhan S says:					


					
						
							
								June 8, 2017 at 1:10 pm							
						
											


									

				
					HDFC Mid-cap opportunities is a fund that is well managed and invests in  larger of the mid-cap companies. Hence its volatility is comparatively lower than its peers as well. These are minor corrections that are bound to happen when the market has been trending up consistently. Also do keep in mind that mid-caps do tend to correct more than the large-caps, that said, when the markets are doing well mid-caps will generally outperform large-caps.

Do keep your SIPs running since such corrections offer you with an opportunity to accumulate more units at lower NAV for the same investment amount. And when the market recovers you can view the effect of compounding.

				


				Reply
			




	
			
				
					
												Vikas says:					


					
						
							
								August 10, 2018 at 4:56 pm							
						
											


									

				
					Is it good time to start new SIP for 10-15 years fro wealth creation.

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								August 28, 2018 at 5:27 pm							
						
											


									

				
					If you are investing through SIPs, when you start should not matter, given your time frame. Thanks, Vidya

				


				Reply
			







		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


