

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		Low-risk hybrid MF options – how they differ

            		May 6, 2019 . Gourav Kumar
                    

	
                    
                                    Mutual Fund Research . Personal Finance
                    
		
            	

            	Hybrid funds combine the growth prospects of equity with the stability of debt to try to deliver returns with lower volatility.

Among hybrid funds – those with a bias for equity are called hybrid aggressive funds (earlier called equity-oriented funds). Those with more debt are called hybrid conservative funds (earlier called MIP or debt-oriented funds).  Over the last few years, a new type of hybrid funds have gained popularity. These hybrid funds employ derivative instruments, like futures and options, to hedge their portfolio, instead of using debt. The hedged portion of the portfolio essentially acts as debt, allowing the fund to lower the risk. At the same time, the unhedged portion generates higher returns. Essentially they are equity-oriented in their portfolio but come with risks similar to debt-oriented funds.

So hybrid low risk funds can be classified as under:


	Category	Classification	Equity	Debt	Tax Treatment
	Conservative hybrid	Debt-oriented	10-25%	75-90%	ST – Marginal rate

LT – 20% with indexation*
	Equity savings	Equity-oriented	Minimum 65%, partially or fully hedged	Minimum 10%	ST – 15%

LT – 10%
	Dynamic asset allocation or balanced advantage	Equity-oriented	Dynamic	Dynamic	ST – 15%

LT – 10%



*10% without indexation for NRIs

While all of the above seek to reduce volatility, they achieve this in different ways. Conservative hybrid funds invest a maximum of 25% of their assets in equity and the remaining goes into debt. Equity savings and balanced advantage funds, on the other hand, typically invest at least 65% of their holdings in equity and hedge a part of it using derivative instruments. Some portion is also invested in debt.

Let’s compare how these funds achieve the job of containing downsides/volatility in different ways.

How have they performed

Volatility and downside containment

The general theory that debt reduces risk in a portfolio is true of hybrid conservative funds as well. This category of funds appears to be marginally better than equity savings funds in containing downsides. The standard deviation of monthly rolling returns over a 3-year period, is slightly lower for conservative hybrid funds as compared to equity savings funds (4.1 vs. 4.7). Given the higher equity exposure of balanced advantage funds, it is no surprise that their annualised risk is highest at 6.7. However, the median probability of getting negative returns on a 1-month basis is pretty much the same for conservative hybrid and equity savings funds at a little over 25%. It slightly higher in balanced advantage funds at 30%.

On a rolling 1-year basis though, conservative hybrid funds fare much better than their equity counterparts. The median downside probability of the category is a mere 0.3% while those of equity savings and balanced advantage funds are 2.4%  and 8.3% respectively. The extent to which conservative hybrid funds can fall is also lower. The worst 1-year return by a conservative hybrid fund was -6.3% (-4.1% if we ignore funds affected by the IL&FS crisis), whereas those of equity savings and balanced advantage were -6.5% and -10.2% respectively.


Returns

It is mostly clear that even after considering the debt crisis, debt-oriented hybrid funds have a lower risk. But does the higher risk of equity funds translate into higher returns as well?

1-year returns from debt oriented hybrid funds have gone as high as 25.1% during the past 3 years, while those of balanced advantage funds have gone up to 40.3%. The category average of 1-year rolling returns for the last 3 years shows balanced advantage funds in the lead.

So prima facie, it seems balanced advantage funds are indeed providing better returns for the risk. This is not surprising considering that their net exposure to equity is way higher. However, to substantiate this claim, we need to look at the risk adjusted returns. We do this by looking at the sharpe ratio. The following table shows the Sharpe ratio based on 1-year rolling returns over the past 3 years. Here it is evident that the equity-biased funds did better.


	 	 Conservative Hybrid	Equity Savings	Balanced Advantage
	Average returns	7.55%	6.95%	8.64%
	Median Sharpe Ratio	0.23	0.27	0.32



But there’s another angle to it as well. Most of these funds are held for a period shorter than 3 years. So while conservative hybrid funds will get taxed at your slab rate, equity-oriented hybrid funds will either be taxed at 15% (for holding period of less than one year) or can be tax free (for holding period of more than a year up to gains of Rs. 1 Lakh) or be taxed at just 10%. So the post tax returns differential may well work out to be higher.

Here is a scatter plot showing risk vs. returns of all the three categories of funds. We can see that the debt-oriented funds are relatively close in their risk returns characteristics while the equity-oriented funds are more spread out.


The risk in low-risk options

It is often assumed that equity = risk. But when the equity portion is hedged, the risk is partly negated. However, the upside returns are also capped. This is what hybrid funds with derivatives do.

With debt forming the major portion of conservative hybrid funds, they are not without risks. Interest rate risk and credit risk can hurt this category of funds. For instance, in early 2018, when the bond yields had been rising and equity markets turned bearish, hybrid conservative funds with higher average duration fell more. HSBC Regular Savings, SBI Debt Hybrid, and Invesco India Regular Savings were consistently underperforming their category during February and March of 2018. Invesco and SBI managed to reduce their duration exposure quickly, while HSBC has continued to maintain above average duration. The median of the average duration of funds in this category has come down from its peak of 8.55 years in November 2016 to 2.62 years at present.


With the turbulence seen in the debt market over the past few months after credit crisis of IL&FS, credit quality of the debt holdings in hybrid conervative also gains importance. Funds like DSP Regular Savings and Aditya Birla Sun Life Regular Savings have suffered significantly due to the IL&FS crisis. Both these funds have given negative returns on a 1-year basis. More recently, more hybrid funds were hurt by downgrades in the Reliance ADAG group papers.


As for the equity-oriented funds, the risk comes primarily from their unhedged equity holdings. Here the average equity holdings of balanced advantage category tend to be higher as compared to that of equity savings funds. However, the percentage of unhedged equity holdings varies considerably across funds even within the same category. Typically, balanced advantage funds have a higher portion unhedged comapred with equity savings funds. Also, within the equity savings category, the unhedged equity holdings  vary from 34.8% in Tata Equity Savings to 72% in IDBI Equity savings. However, due to a clearer definition of the requirements by SEBI, we see lesser variation in equity savings funds.


Suitability

The low-risk hybrid options discussed above are meant to be held for 2-3 years. However, which category is suitable for whom would depend on the risk appetite, the purpose of the fund in the portfolio and taxable bracket of the investor. Talk to your FundsIndia advisor to know whether you should stick to the traditional hybrid conservative (earlier called MIP) or do for the low risk equity-oriented options.

 

Other articles you may like
	Give your salary the power of 5!
	FundsIndia Views: Why markets fell and what you should do
	Budget 2018 impact - Why ELSS still scores over PPF
	FundsIndia Views: Why people are exiting their real estate investments





			
			Post Views: 
			248
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                                    

			

            	


	
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


