

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		Who do we represent? AMC or the Investor?

            		September 28, 2012 . Srikanth Meenakshi
                    

	
                    
                                    General . Mutual funds
                    
		
            	

            	An interesting twitter conversation ensued today between CIEL’s Uma Shashikant and Mint’s Hindustan Times’s Gautam Chikermane. It started with a conversation that Gautam had with Raghuram Rajan in which the latter made some interesting (and a bit unexpected) comments about financial products distribution. (You can read the full interview here).

During the twitter conversation, Gautam asked the oft-repeated question regarding financial service intermediaries – who do they represent? Do they represent the manufacturers (such as the AMCs) or the consumers (the investors)? Whose interest is of concern to them?

(Full conversation here )

I responded (using the @fundsindia twitter account) saying that this question presents a false choice. That is, the question presumes that it is an either-or situation without a third or a middle option possible.

The reason I said so was that at FundsIndia, we have been successfully doing both over our tenure. And I do know quite a few other advisors/distributors who can claim to do the same as well.

We represent AMCs to the investors – there are close to thousand investible schemes in the mutual fund market today, and I often hear representations from the AMCs who try to highlight good funds to me that go unnoticed. Whenever such claims have merit, we use that as input when we talk to investors then on. For example, a mid-level AMC with a very good balanced fund recently visited our offices and showed us how their fund has done better than more well-known names in the market. Another even smaller AMC showed us how their MIP product is as good or better than many other such products in the market. There are many such pitches every week, but with these two, we found that their arguments have merit and we have absorbed them into our recommendations.

And, we are always representing investor views back to the AMCs.  Regardless of the fact we get our commissions from the AMCs, we know that all the money eventually comes out of the investor’s kitty. We service our customers by working every day with AMCs to make this or that accommodation or exception as much as possible. Apart from that, to all the sales and marketing folks from the AMCs who visit our offices, we make it a point to tell them what we are hearing from the investors in terms of performance of their funds or what kind of products they’d like to see (if I had a dime for every time an investor asked for a silver ETF, I’d have many, many dimes).

FundsIndia or any intermediary business will work only if we have a robust group of manufacturers make good, useful products and also if we have a investor population who are served well. It is not a choice, you need both. As an intermediary, it is not a choice for us either.

So, we do both – represent each side to the other and work for the benefit of the industry and the market place as a whole. It is possible to act with that sense of enlightened self-interest that calls for a healthy industry with a well-served market so that both (as well as us, the intermediary) thrive and prosper over the long-term.

Other articles you may like
	Nifty Weekly Outlook April 22-26
	FundsIndia explains: Internal rate of return on mutual funds
	Gamifying your Investments
	FundsIndia Views: Equity Outlook for 2018





			
			Post Views: 
			2,725
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Mutual funds
                

			

            	


			
			8 thoughts on “Who do we represent? AMC or the Investor?”		


		
			
			
				
					
												Uma Shashikant says:					


					
						
							
								September 28, 2012 at 9:41 pm							
						
											


									

				
					A balanced view, indeed. My take on this debate has been that advisory services need specialist ability to select from various product options. Simple distribution needs the ability to reach a large number.  A sharp analyst of products, dislikes the sales job; the smart salesman is seldom patient with intense research.  Business models that bring the two together, and earn distinct incomes from both streams will survive. We suffer today when distribution masquerades as advice; and advice is unable to earn enough to sustain the effort.  The costs are high for both investors and AMCs.  A model that sees distribution as subservient and dependent on advice, will restore incentives to good quality advice.

				


				Reply
			
	
			
				
					
												Srikanth Meenakshi says:					


					
						
							
								September 29, 2012 at 7:21 am							
						
											


									

				
					Uma,

I see the line that you are drawing between distribution and advisory (both here and in your blog/columns). At FundsIndia, we are LIVING on that line, in that we do both. We are a unique model in that sense in that I do not know any other entity that does both core distribution and core advisory services to different segments of an audience using a same platform/infrastructure. So, while your point is valid in general, I request you to accommodate the possibility of existence of legitimate, good hybrid models in your world view.

There is a good reason why we do this, and it has to do with one problem that you allude to in your comment above. The problem of scalability. Since the beginning of FundsIndia, we are constantly trying to answer the question of how to deliver advisory services in a scalable way to a LOT of people. And in a way that makes business sense (from unit economics perspective). Our on-demand advisory services (over phone), automated portfolio builders, pre-packaged portfolios, built-in value adds such as value-averaging, triggers etc. are our attempts to push the envelope in terms of how we can get quality advice and a high fidelity platform to even a guy who is doing Rs. 1000 a month SIP. We are going to do more such things – automate portfolio review, bring in an order-of-magnitude more pre-packaged portfolios to suit different life situations and investor profiles.

It is such efforts that get hurt when, with a stroke of a pen, the regulator brings in a disrupting effect such as the direct plan. Apart from being an affront to the precepts of free market enterprise, this will hurt companies like FundsIndia who have diligently created a business model around a low margin service and are trying to solve real problems with technology and innovation.

Thanks for your time, please stop by our offices when you are in Chennai next.

Regards,

Srikanth

				


				Reply
			




	
			
				
					
												Uma Shashikant says:					


					
						
							
								September 29, 2012 at 9:49 am							
						
											


									

				
					Yes, there is no doubt that technology and standardisation can deliver quality solutions at a scale.  I have emphasised rule-based advisory with institutional distributors interact with. My compliant is that regulators have discarded the embedded pricing model without much thought, leading to the distribution model being more profitable than advisory. The new advisor regulation will entrench this distortion further.  I would be happy for the advisor to be able to take the expense now being reduced in the direct plan, but to expect investors to pay that variable fee is unrealistic.  An embedded advisory fee in the form of trail would have enabled moving ahead and setting disclosure, performance and other standards for the advisory profession, enabling them to compete efficiently.  I also believe thee is space for standardised strategic allocation (passive advice if you will) and tactical advice that would be a top-down active allocation model, both capable of being scaled and priced differently.  The markets for useful products such as mutual funds will expand when players are able to build business models given a framework for compliance and conduct. To advocate one mode over other, using regulatory diktat, kills choice and enterprise.  This is more so in India where we culturally are not conformists to a process, but innovators who find alternatives.  I have heard a lot of good things about the FundsIndia model and am keen to visit you and learn more. Shall get in touch when I am there next.

				


				Reply
			
	
			
				
					
												Srikanth Meenakshi says:					


					
						
							
								September 29, 2012 at 10:13 am							
						
											


									

				
					Thanks, Uma, for the considered, reasoned response. 

Look forward to seeing you here when we get a chance,

Srikanth

				


				Reply
			

	
			
				
					
												Satyanarayana says:					


					
						
							
								January 7, 2013 at 8:39 pm							
						
											


									

				
					This is very well articulated comment. Of course, you already know that.

My question is about your comment “in India where we culturally are not conformists to a process, but innovators who find alternatives”

What I observed is, we indians still follow age old customs as given in ‘Puranas’ and perform all the rituals so diligently. Our innovation is limited to find loopholes in any process, rather than accepting it and then improving it.

What is your opinion?

Satyanarayana

				


				Reply
			




	
			
				
					
												Ranjan says:					


					
						
							
								October 21, 2012 at 12:39 am							
						
											


									

				
					Blending distribution and advisory is like walking the tightrope! It’s a thin line and the probability of tripping for lesser mortals like me is very high. 🙂

But though I personally have had little success in following your footsteps of representing the financial service providers as well as the investors/clients, I have not lost hope. 

I am working on articulating the “cost of free advice” and it’s a chapter in my upcoming, self published book. 

Even though it’s a hard climb reaching out to investors and explaining the nuances, more and more people understand the implications. 

(On a lighter vein, you talk about using technology to scale up but talk of “one investor at a time” in your tag line 🙂

				


				Reply
			
	
			
				
					
												Srikanth Meenakshi says:					


					
						
							
								October 21, 2012 at 8:34 am							
						
											


									

				
					Ranjan,

Thanks for chipping in with a response.

I think blending distribution and advisory is key to having scalable personal investment solutions in India. Apart from institutional (or individual) discipline in this regard, a critical element for the industry to be able to achieve this is the availability of diverse good products from different companies. That is, good investment products across different asset classes being available from more than a handful of companies is key. This is true today, I believe. In the MF world, we can cite many schemes from different AMCs across asset types that we can blend and recommend to our investors without bias or an appearance thereof. As long as this holds (and smaller AMCs are not forced out of the game by regulations or other machinations),  running an honest distribution plus advisory business is do-able, I believe.

Regarding “One investor at a time”, honestly, it is an adaptation of a similar tag from the historic US brokerage Dean Witter, who said “We measure success one investor at a time”. They served 3.2 million clients at one point in time 🙂

Regards,

Srikanth

				


				Reply
			




	
			
				Pingback: Voices of Financial Services Industry | Ranjan Varma's Blog 			





		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


