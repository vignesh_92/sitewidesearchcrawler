

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		Should you invest in Motilal Oswal Nasdaq 100 FoF?

            		November 14, 2018 . Vidya Bala
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Personal Finance . Research Desk . Research desk featured
                    
		
            	

            	
What
	Motilal Oswal Nasdaq 100 (FoF) is a passive new fund offer that will invest in units of Motilal Oswal Nasdaq 100 ETF.
	The Nasdaq 100 Index is an equity index of top 100 non-financial securities and is among the top delivering key indices in the US. 


Who

	Investors looking to meaningfully diversify outside India and a long-term holding period
	Investors planning for expenses in US currency in the long term. 




If there is an equity market that is less correlated to the Indian market and provides a diverse set of companies to invest in, it is the US market. Hence, for portfolio diversification, we have traditionally preferred US to any other global market.

If you decide to use the US route to diversify your portfolio, then it is best done through an index that accounts for more than quarter of the market cap of the New York Stock Exchange. We are talking of the Nasdaq 100. The NFO of Motilal Oswal Nasdaq 100 Fund of Fund (FoF) offers a simple route to gain exposure to the Nasdaq 100 without the hassle of a broking or demat account.

The offer

Motilal Oswal Nasdaq 100 (FoF) is a passive fund investing in units of Motilal Oswal Nasdaq 100 ETF (underlying scheme). Motilal Oswal Nasdaq 100 ETF trades on the Indian stock exchanges and replicates the Nasdaq 100.

The local ETF has a track record of over 7 years but has traditionally suffered from poor liquidity. As a result, there may be days when you are unable to buy/sell the ETF. The ETF price also deviates from its NAV. The FoF route can help overcome the limitation of liquidity from an investor’s perspective. Your investments will only be done in local currency (rupees). The fund will be open ended and available for regular subscription and SIPs post the NFO period.

About Nasdaq 100

For those not familiar with the US stock market, the most powerful and profitable companies on the globe are nested in the Nasdaq 100 index. The Nasdaq 100 Index is an equity index of top 100 non-financial securities listed on the NASDAQ stock exchange in the United States.

The index has close to 45% exposure to technology stocks including global leaders such as Apple, Microsoft, Amazon, Google and Facebook. The index was launched in 1985 but gained traction in the past 15 years. This period has also seen the index constituents showcase tremendous growth in fundamental metrics. The index components posted a revenue growth of 14% and earnings growth of 24% compounded annually (from 2003 up to financial year December 2017. Source: Nasdaq). 


The index has delivered superior returns to the S&P 500 and the Dow Jones Industrial Average in the past 5 years. Although the Nasdaq 100 is tech heavy because of new-economy technology companies growing at tremendous pace in the internet age, the index also has other heavyweight names such as auto company Tesla, pharma company Mylan, food player Kraft-Heinz, toy maker Hasbro and trading giant Costco. There are a variety of ETFs that track this index in the US and across the globe.

About Motilal Oswal Nasdaq 100 ETF

The Nasdaq 100 ETF from Motilal Oswal was launched in March 2011 and has an AUM of less than Rs 100 crore. It is the only domestic ETF tracking the Nasdaq 100 index.

The ETF delivered 18.9% annually in the past 5 years. This is superior to one of the best large-cap indices in India – the Nifty 100’s return of 12% in the same period. The Nasdaq 100 index itself delivered 13.8% over this period. The higher returns of the local ETF can be attributed primarily to rupee depreciation. In other words, local funds investing internationally benefit from a depreciating rupee. Still, lack of liquidity has seen the ETF deviate from its NAV in price and seen volatility in price daily. In recent days though, the price of the ETF has been converging with the NAV, suggesting that the AMC has been actively creating market makers and improving liquidity for the ETF.


	 	1 year	3 years	5 years
	MO Nasdaq 100 ETF	25.60%	18.00%	18.90%
	Nifty 100	2.00%	10.80%	12.10%
	Returns as of November 13, 2018. NAV returns of the ETF



Motilal Oswal Nasdaq 100 ETF has also delivered better than most other international funds in India in the past 5 years; barring a few commodity funds that have flashes of high returns and then slip. For example, over a 5-year period, this ETF delivered 17.2% annually and the next best international fund (DSP US Flexible Equity) delivered 11.5%.

Also, given that the US has a track record of well-managed indices like the Nasdaq 100 traditionally beating actively managed funds there, a US index, when available for Indian investors, would be a better way to invest in that market.

Suitability

Motilal Oswal Nasdaq 100 FoF, like any other equity product, is only meant for long-term investors. Added to the risk of fundamental factors in a foreign county, currency risk also exists in the fund. However, as the rupee has traditionally depreciated against the dollar, local investments into US have delivered higher than the dollar returns of their investments. The index is an aggressive high growth portfolio of stocks. It has also seen high volatility in the past.

The NFO is suitable for all those looking to hold unique companies that are not present in the Indian stock markets. While the fund can be used as a general portfolio diversifier, it can specifically be used by those planning for expenditure (like foreign education) in the US. This will help counter the impact of a depreciating rupee.

If you are serious about using the fund as a diversification tool meaningfully, then an exposure of at least 15-20% of your total equity would have to be allocated. Anything less may not lend market diversity nor meaningfully help counter any Indian market volatility. Giving the recent correction and volatility in the index, any lumpsum investments are best followed up through SIPs.

Note that the FoF and the ETF receive only ‘debt fund’ status for tax purposes since they invest abroad. Hence, you will have to hold the fund for at least three years to benefit from capital gains indexation and a post indexation tax rate of 20%.

The SID states that the expense ratio of the FOF can be up to 2.5%. However, based on the draft regulations of SEBI for a new expense ratio structure, the fund house has stated that the expense ratio of the FOF will have to up to 1%.

Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.

Other articles you may like
	FundsIndia Reviews: L&T India Value Fund
	FundsIndia Debt Strategy: Delayed Gratification, A Positive For You
	RBI rate cut - One for the budget
	FundsIndia Reviews: PPFAS Long Term Value Fund





			
			Post Views: 
			4,714
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Advisory. debt funds. ETF. Investments. Mutual funds. NFO. Personal Finance
                

			

            	


			
			6 thoughts on “Should you invest in Motilal Oswal Nasdaq 100 FoF?”		


		
			
			
				
					
												Rohit says:					


					
						
							
								December 24, 2018 at 3:31 pm							
						
											


									

				
					Can someone advice on this? This appears to solve liquidity issues in MOST Nasdaq 100 ETF. Is it right to say that fund house will purchase this when investor is trying to sell? whereas in ETF, you need another investor?

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								December 28, 2018 at 1:21 pm							
						
											


									

				
					Hi Rohit,

In an FOF (or any mutual fund), you’re buying and selling directly from the fund. So if you hold Motilal Nasdaq FOF and you redeem from it, the fund will return the proceeds to you. There’s no trading that will happen on the exchange. If you buy Motilal Nasdaq ETF, you will be buying and selling on the exchange like you would with any stock. So you’d need a matching order for your buy/sell order.

The Motilal FOF will improve liquidity of the ETF itself, as it can have higher volumes and more trading activity. It does not mean that the fund will purchase units every time you want to sell. AMCs employ market makers to ensure that there is liquidity in the ETF so that buying/selling happens smoothly and does not distort prices. Motilal Nasdaq FOF is not a market maker for the ETF. Its just another investor. Hope this clarifies your doubt.

Thanks,

Bhavana

				


				Reply
			




	
			
				
					
												Suraj says:					


					
						
							
								January 10, 2019 at 10:05 am							
						
											


									

				
					How to buy Motilal Nasdaq FoF ?

Please share direct investment option and link

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								January 12, 2019 at 2:16 pm							
						
											


									

				
					Hello,

If you’re a FundsIndia investor, the fund will be available under the Invest option in Mutual Funds or through New Investment on your portfolio dashboard. Else, please check with the investment platform you are using.

Thanks,

Bhavana

				


				Reply
			




	
			
				
					
												Rajesh says:					


					
						
							
								January 16, 2019 at 8:20 pm							
						
											


									

				
					hi, I have a doubt, during redemption how FX from USD to INR converted?

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								January 21, 2019 at 3:21 pm							
						
											


									

				
					Hello,

Do you mean at the time you redeem from the fund? There’s no conversion that happens at any point. You invest in rupees in the fund and redeem in rupees. The fund’s holding is in rupees. When you buy or sell ETF units, you’re doing so in rupees. The Motilal FOF invests in units of the ETF. The ETF’s underlying portfolio will invest in the Nasdaq 100. Your buying and selling ETF units does not involve nor affect the ETF’s underlying portfolio. Only money infused directly into the ETF will change that. The ETF’s NAV and market price (you buy and sell at market prices) will reflect the rupee appreciation or depreciation. Hope its clear now.

Thanks,

Bhavana

				


				Reply
			







		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


