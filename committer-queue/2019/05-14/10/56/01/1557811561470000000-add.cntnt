

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		Is the correction good enough to invest?

            		October 22, 2018 . Ashwini Arulrajhan
                    

	
                    
                                    Mutual Fund Research . Research Desk
                    
		
            	

            	Markets have been falling steadily over the past two months. The Sensex and the Nifty 50 have lost more than 12% and the broader markets have seen a deeper fall. The astute investor will see this correction as an opportunity to buy on lows, especially after the long bull market of 2014-2017. The more important questions then become, is this a steep enough correction to buy? Are the markets going to fall even more? Well, nobody can time market highs or market lows. But you can still take advantage of corrections without being carried away by these concerns. Here’s how.

Don’t wait for steep corrections

The table below shows the historical corrections since 2008. It is evident that a steep fall like the one during 2008 translated into tremendous returns later. But if we look at the recent years, we have not seen corrections like that. Corrections in the recent years ranged between 7% and 12%. But here’s the good news – even those falls, if used to average efficiently, would have perked up your returns.


	Periods of correction	Correction	6 months later	1 year later	2 years later	3 years later	5 years later
	Jan 2008 - Oct 2008	-59%	34%	 101%	140% 	 93%	129% 
	Jan 2010 - Feb 2010	-11%	15%	14%	10%	25%	85%
	Jan 2011 - Feb 2011	-15%	-2%	-1%	13%	17%	42%
	May 2011 - June 2011	-8%	-13%	-9%	13%	38%	53%
	Jul 2011 - Dec -2011	-20%	11%	27%	37%	88%	 75%
	Mar 2012 - Jun 2012	-11%	21%	24%	52%	74%	 95%
	Jul 2013 - Aug 2013	-12%	16%	42%	57%	56%	 110%
	Mar 2015 - Jun 2015	-11%	-5%	1%	18%	34%	
	Aug 2015 - Sep 2015	-12%	-1%	14%	28%	55%	
	Oct 2015 - Feb 2016	-16%	21%	23%	56%		
	Nov 2016 - Dec 2016	-7%	21%	27%			
	Jan 2018 - Apr 2018	-9%	7%				
	Aug 2018 – Oct 2018*	-13%					



*As of Oct 11, 2018

What’s more, markets periodically provide such opportunities. The table below shows the number of months which saw at least an 8%-13% correction in the last 10 years. This re-emphasizes the point that running SIPs will help you make the best out of volatile markets.


	No of months which saw	5 years	7 years	10 years
	More than 8% fall	17	33	56
	More than 10% fall	13	25	45
	More than 13% fall	7	15	30



Use opportunities systematically

We have spoken enough about the benefits of SIPs. The first and most important benefit is that SIPs bring in a discipline in investing and ensure that you don’t compromise on building wealth. The second key benefit is that it helps you avoid mistiming your investments, since you run SIPs across market cycles.

But for investors who want to actively take advantage of volatile markets, using every correction to add on will help average costs even lower and allow you to accumulate more when markets are cheap. It makes your SIP more effective. The question is, of course, at what point during the correction should you invest? As we’ve said several times, it is not possible to call out highs or lows before they happen.

The best route is to use cut-offs and invest a small amount in addition to your SIP every time the market fall breaches this cut-off. The cut-off can just be a certain fall from the 1 year market high. There are several advantages to this method.

One, you do not leave it up to prediction and guesswork into whether or not the correction will continue; this allows you to actually catch a fall instead of losing the opportunity. Two, it allows you to catch even the shallower corrections as explained above. Three, investing smaller amounts is practical, since you may not have a large enough surplus to invest on a low; typically, investing a large sum on a steep decline is needed to bring costs down. Four, it is not too complicated a strategy to follow or execute.

To empirically test the results of this method, we did the following. Say, you were running a SIP of Rs 10,000 for the last 5 years. By investing every month, you would have anyway bought on few of the falls. You are willing to put aside a certain amount of surplus to invest more during falls. Every time the market falls more than 10% from its 1-year high, you decide to add on a third of your SIP amount, that is, Rs.3000.


	 	Normal SIP	SIP + Investing in lows
	Investment Amount	6,00,000	6,39,000
	Final Value	7,59,809	8,11,856
	Gains	1,59,809	1,72,856
	XIRR	9.37%	9.53%



*Returns as on Oct 15,2018

In the last 5 years, there were 13 instances where the market fell more than 10%. If you had invested on those dates, you would have invested Rs.6,39,000 against Rs.6,00,00 in your regular SIP. By investing Rs.39,000 extra, you would have gained Rs. 13,047 during the 5 year period. And that is good returns, considering the extra investment amount you put in.

Smart investing vs timing the market

We have, time and again, advised against timing the market. Does this amount to timing the market? In a way, it does. But this method is smarter that waiting for the low that proper market timing would require. And while you are waiting for the low, markets race past you, only for you to spot a low in hindsight.

You could also flip our argument to say that one needs to stop investing at highs – what if you stop investing every time the market moves up a certain cut-off from the low? Stopping SIPs would immediately hurt your goals, since you would be investing lesser – who knows where a rally would stop? The other option is to invest lower at highs. For that, we have the answer and that is the value averaging plan! If you’re an experienced investor, you could use VIP more effectively than an SIP. Or something as simple as rebalancing (moving to your original asset allocation) on an annual basis can help sweep some profits from equity to debt.

What we are telling you here in this article is to invest smartly. By observing these patterns that repeat themselves, we know that markets are going to keep providing you such opportunities. It may not be a 60% fall like in 2008, but a 10%-13% fall is still enough for you to enhance your returns.

Other articles you may like
	FundsIndia Strategies: Lump sum or SIP?
	FundsIndia Views: Why you can go wrong with point-to-point returns
	Why SWPs are great for regular income - Part I
	Rewinding 2018: Sectors mutual funds bought into





			
			Post Views: 
			1,600
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    bull market. correction. SIP
                

			

            	


			
			1 thought on “Is the correction good enough to invest?”		


		
			
			
				
					
												jmehta1j says:					


					
						
							
								January 9, 2019 at 4:34 pm							
						
											


									

				
					One should understand that rushing back to buy simply because they have fallen by 10% or 15% does not make sense. Thanks for sharing a great article.

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


