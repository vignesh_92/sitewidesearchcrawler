

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Views: What’s driving the GDP

            		September 6, 2018 . Vidya Bala
                    

	
                    
                                    Advisory . Mutual Fund Research . Research Desk . Research desk featured . Views
                    
		
            	

            	The 8.2% real GDP growth for the June quarter left many analysts and economists surprised. Notwithstanding the low base effect, especially in the manufacturing space, experts acknowledge the numbers as excellent data points. Why is this so? Simply because the growth has been broad based, coming from a revival in manufacturing and construction and surprisingly agriculture too. Besides, from an expenditure side, private consumption has remained robust, contributing 55% to the GDP.


Before we get into whether the growth is sustainable and what is likely to drive the growth, let us look at some of the macro data points to get a better understanding of the GDP growth itself, the positives and concerns.

The data below looks at the drivers of the economy from two angles – one from the industry (and services) side and the other, the retail side.


	 	Jun-17	Jun-18
	Industry/Services
	Cement production	-3.3%	13.2%
	Commercial vehicles sales	1.4%	41.7%
	Petro product consumption (industrial)	-10.6%	9.3%
	Core infrastructure	1.0%	6.7%
	Index for industrial production (IIP)	-0.3%	7.0%
	Credit growth (Industry)	-1.1%	0.9%
	Credit Growth (services)	4.7%	23.3%
	Retail
	Passenger vehicle sales	-10.9%	37.5%
	Two-wheeler sales	4.0%	22.3%
	Credit growth (retail)	14.1%	17.9%
	Source:  Kotak Report, MOSPI, Office of the Economic Adviser, RBI




Is there a recovery in manufacturing?

If you look at the industry side of the data, the revival in cement production, petro-product consumption by industries, growth in manufacturing (IIP) and core infrastructure all point to increased manufacturing activity.  If we break the core infrastructure which feeds into manufacturing (IIP), key elements such as steel, electricity, coal and mining have shown a healthy improvement. For example, steel production increased to 7.3% in June 2018 from 3.4% a year ago. Electricity production grew to 8.5% from 2.1% a year ago and mining to 6.6% from 0.1%. Now, while we can call this a low base effect, the month-on-month growth numbers between these 2 periods talks of a slow gradual increase and not any flashy soar. This essentially means that it is not entirely low base at play. There is an uptick in activity.

The question however is – what is triggering the uptick? Is it increased investment activity or just higher utilization of capacity? The answer lies in credit growth in the industry. The data above will tell you that credit growth is at a measly 0.9% even now. Contrary to belief (a few months ago) that credit growth for companies has already revived, not much has happened in terms of lending activity. This is what experts talk of when they say there is little revival of capex activity in companies. In other words, companies are not borrowing to expand.

So, whatever growth is happening is essentially coming from higher utilization of capacities and in other cases a fallout of consolidation (steel, cement etc.). While this is a good sign, any sustained trigger to GDP growth from this segment can come only on the back of strong capex. Even assuming demand pushes companies to seek borrowing, it may not be that easy given the troubles of banks with bad assets and the stringent norms in assets being categorized as delinquent ones. In other words, massive capex helping GDP growth is not a story that we expect to play out in the medium term.

We also looked at other angles to see if there are green shoots to corporate spending. One other indicator of an uptick in capex activity in companies can be an increase in non-oil, non-gold import. Traditionally, capex spending was accompanied or preceded by increase in capital goods import. However, this time around the non-oil import has other components such as consumption goods such as electronics or at best some commodities, while capital goods import has not seen any evident jump.



This part of the GDP story, therefore is not an easy one to take off, in our opinion. The story here may lie in better operating efficiencies of companies in the capital goods and engineering space.

Consumption driven

If not manufacturing, where can the growth come from? If the GDP’s private consumption expenditure of 8.6% says anything, it is this: consumption has been the driver of the economy. Unlike the industries that received shocks on various fronts because of demonetization and GST, the consumption space has received a steady boost in the past 2 years. To name a few: One, FY-17 saw a big hike in the Centre’s wage bill on account of the 7th Pay Commission while FY-18 saw state governments implementing it. Two, farm loan waivers have provided steady support to rural consumption. Three, over the past 2 years, thanks to easier financing options and lower rates, whether it was auto or any retail purchases, the credit availability to the retail borrower has significantly increased. Banks’ focus on the less risky retail lending option and closing doors on corporate lending has meant more money in the hands of the consumer.

And the consumption story has been robust despite subdued participation from consumer discretionary, if one looks at the corporate results. A slowdown in real estate could have been a primary reason for this. That means, that a revival in real estate could only further push the spending spree.

And this story may well continue. Why are we saying this? Look at the numbers in the table above again. You will see that the credit growth for the retail space has remained in high double digits, despite a high growth last year too.

That said, can they buttress an 8%-plus GDP growth? That may be difficult since the tailwinds of Pay Commission and farm loan waivers many not repeat themselves. To this extent, the consumption could normalize to healthy levels than the extraordinary levels seen in the past 2 years.

A revival or not?

So, are we saying that there is no real recovery in GDP? Not really. Government spending has been the lone sentinel on the industry spending side but this too has slowed down with government spending at 7.5% in June 2018 versus 17.5% a year ago quarter. On the consumption side, as discussed earlier, with the effect of pay commission or farm loan waiver declining, the rate at which consumption can drive the economy may also come down. In other words, the latest GDP growth print may not be sustainable.

Be that as it may, consumption is likely to remain the larger driver of the economy.  Here’s another cue: the RBI’s data suggests that net financial savings (gross savings less financial liabilities) of the household sector as a percentage of gross national disposable income has fallen to 6.7% in 2016-17 (likely to be higher in FY-18 given the higher retail credit) from 8.1% a year ago. This was the lowest in 5 years. Lower financial savings means higher borrowing to spend. And that is where the consumption-driver economy is likely to keep ticking.

For the markets, this is a cue on where the money is to be made. While retail financial services have already been rewarded, there are still a host of other play on this story that will likely be tapped more by mutual funds.

FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis for investment decisions. To know how to read our weekly fund reviews, please click here.

Other articles you may like
	FundsIndia Recommends: UTI MIS Advantage
	FundsIndia Reviews: HDFC Top 200
	FundsIndia Views: 3 reasons why you should be investing now
	Frequently asked questions on annual portfolio review





			
			Post Views: 
			2,553
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Advisory. economy. equity market. GDP. Investments. Personal Finance
                

			

            	


	
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


