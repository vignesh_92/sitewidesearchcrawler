

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia explains: How to tell if your fund is doing well

            		January 2, 2017 . Mutual Fund Research Desk
                    

	
                    
                                    Mutual funds . Simply Important
                    
		
            	

            	Looking back on 2016, your equity funds have not matched up to your expectations. Where did those 20% returns go? Should you sell your fund? Don’t do that just yet. Remember that you never look at a fund’s performance in isolation. So how do you tell if your fund is doing well? Answer these questions first.

 Open  a FREE Account Now! 

Is it able to beat the market?

A fund aims at delivering returns higher than the market. By market, the fund means its benchmark. A fund generates these returns based on stock price movements. At a time when the stock market itself goes nowhere (2015 and 2016 are great examples of such a flat period), how is your fund supposed to generate those high double-digit returns? So when you look at the fund’s return, always look at what its benchmark is doing. As long as the fund has done better than its benchmark – which is its purpose – your fund is doing fine. Take Franklin India Bluechip, for example. Its 1-year return is 4.5%. Not good, in your book. But the fund’s benchmark is the Sensex. This index has gained 0.7% for one year. The fund has, therefore, delivered almost 4 percentage points more than the market. That poor return looks healthier now! Fund aggregator websites always provide returns of both the fund and its benchmark. 

Is it able to beat its peers?

So the fund beats its benchmark. You can find several that manage that! Move to the next step. The fund should additionally be able to beat its category. Doing better than both its benchmark and its category is the hallmark of a good fund. If it is not able to beat category average, it means that there are better funds out there. 

By category, we mean the general type of fund it is. You need to compare similar funds only. An equity fund will invest in only large-cap stocks (largecap fund), or only in mid-cap and small-cap stocks (midcap/smallcap funds) or in a mix of these (diversified funds). A debt fund will invest in very short-term papers (liquid and ultrashort term funds), or short term papers (short-term funds), or long-term papers (income funds), or lower-quality papers (credit opportunity funds), or government bonds (gilt funds), or a mixture (dynamic bond funds). Each category has a different driver of performance. Each set behaves differently in different market cycles.

Continue the example above. You look at the 11% return of Mirae Asset Emerging Bluechip and decide that’s a better fund than Franklin India Bluechip. But Mirae Asset Emerging Bluechip is a mid-cap fund and the Franklin fund is a largecap fund through and through. 2016 was a year when mid-cap and small-cap stocks soared far ahead of large-cap stocks. Would it be fair to a large-cap fund, then, to compare it to a mid-cap fund? You’re right, it wouldn’t! Always compare a fund to its own category to get the right picture.

 Open  a FREE Account Now! 

Has it always beaten benchmark and category?

Today, you look at returns and you see that the fund has delivered better than the benchmark and the category. Was it able to do that last year? Two years ago? A fund should be able to deliver benchmark-beating and category-beating performance at all times. In other words, it should consistently be an above-average performer. Checking consistency in performance is important. There is little benefit in holding a fund that alternately outperforms and underperforms by a wide margin. A consistency metric will tell you if a fund’s strategy enables it to pick a reasonable number of winners in all market cycles, all the time.

Looking merely at the 1, 3, and 5-year returns on a single day will not tell you anything about consistency. Ideally, the way to gauge consistency is to roll the annual returns daily for three years at least, or even five years. That can be tough for you to do. So you can look at returns in each calendar year, which you can calculate yourself or obtain from aggregator websites. Fund factsheets provide one-year returns for three consecutive years at the end of each quarter for each fund and its benchmark. Pick up the factsheet in different quarters for different years to measure consistency.

Past record will also tell you if you should allow the fund more time to perform. Your fund may underperform sometimes and recover later. You can give a fund that has been consistent in performance so far some leeway if it is not up to the mark right now. 

There are several other metrics and ratios to judge fund performance, not to mention fund strategy and the fund manager. But if you have ticked off yes to the three questions above, it answers a significant part of the performance question. It’s also a good enough yardstick to address your worry when you see those return numbers ticking down. 

Other articles you may like
	Are You Using the SWP, STP Options Wisely?
	FundsIndia Recommends: HDFC Top 200
	FundsIndia explains: What is compounding?
	FundsIndia reviews: Large-cap funds





			
			Post Views: 
			1,807
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    analysis. Mutual funds
                

			

            	


			
			2 thoughts on “FundsIndia explains: How to tell if your fund is doing well”		


		
			
			
				
					
												Paras Vij says:					


					
						
							
								August 30, 2017 at 12:18 am							
						
											


									

				
					In one of your other Articles about ‘Rolling Returns & How to use it’ it is stated that Rolling the Returns for lesser Frequency is less useful. However this Article suggests to Roll Returns on daily basis to gauge consistency of the Fund. Please clarify.

Also how to get Data for Weekly/Monthly frequencies?

Thanks

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								August 31, 2017 at 4:11 pm							
						
											


									

				
					Hi Paras,

In the rolling returns article, we’ve explained that there are three parts – the period for which you’re taking the returns, the frequency with which you’re taking the retuns, and the number of years during which you’re looking at these returns. Daily in this article and your doubt refers to the second part or the frequency with which you’re taking returns. Daily frequency is the highest frequency since in a single year you will be looking at around 250 data points (accounting for holidays and weekends). A weekly frequency has 52 data points which is much fewer, and a monthly frequency has even fewer. The lesser frequency simply means the number of times you’re looking at the returns – so a weekly rolling basis is less frequent than a daily rolling basis. To get data for weekly/monthly rolling, you’d need to adapt your spreadsheet formula so that you’re taking the returns every week instead of every day (as the example in the article explained). 

Thanks,

Bhavana

				


				Reply
			







		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


