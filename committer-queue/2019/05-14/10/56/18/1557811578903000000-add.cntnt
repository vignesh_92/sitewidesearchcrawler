

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		Containing losses is important

            		September 10, 2015 . Bhavana Acharya
                    

	
                    
                                    General . Mutual funds
                    
		
            	

            	With market mood turning somber and a potential slide in the offing, it’s a good time to discuss how your fund manages bearish markets. How well a fund contains losses in a market downturn is rather more important than how well it does when markets climb. Why? Because it takes a lot of effort to just recover the losses. And it’s only after this that gains can be made. 

Take a look at the table alongside.  

Say you invested Rs 50,000 in Fund A. But as markets bled, it lost 20 per cent. The fund then bounced back with a 35 per cent gain when sentiments turned, doing much better than the broad market. But 25 per cent would be needed to just recover what you lost. Only gains beyond this would help you make money on your investment. In this case, the 35 per cent rise will bring your investment to Rs 54,000. The gain you will actually be sitting on after the market rise works out to 8 per cent.

That puts a Fund B that lost 10 per cent in a better position, as it has a smaller loss to recoup. Fund B can thus quickly recover and deliver returns even if its performance in the market uptick doesn’t quite match up to Fund A. Assume Fund B gains 25 per cent. Your returns at the end would be at 12.5 per cent.

 Open  a FREE Account Now! 

To consider a real-time example, in the mid-cap fuelled 2012 rally, funds such as HSBC Midcap Equity or Principal Emerging Bluechip comfortably beat their category averages by 3-4 percentage points. But in the 2011 market downturn, they had had steeper losses than their category to the tune of 9-20 percentage points. Overall gains across the two cycles were thus lower by 16-37 percentage points than peers such as SBI Magnum Global or Canara Robeco Emerging Equities, which didn’t gain as much but then, didn’t lose as much either. 

Look for protection

Therefore, a fund that tops the charts in bull markets will not be of much use if it is at the bottom of the heap during bearish phases. For one, it will cap the overall return you make as the recovery from losses swallows a good chunk of the gain. Besides, in order to deliver the high returns needed, the fund may take riskier bets. This feeds back into the problem, since the tendency would be to pick stocks that are the flavor of the market. Such stocks can sink quickly when they aren’t fancied anymore. Two, the effect of compounding of returns reduces when losses are steep. And three, swift rises and sharp falls increase a fund’s volatility and risk. It’s low volatile strategies that work best over the long term.

Other articles you may like
	‘NPS is a simple and portable retirement product’
	Save early, save yourselves from these retirement nightmares
	Why your income is not a part of your credit score
	You don’t have to add schemes when you increase investments





			
			Post Views: 
			2,791
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    bear market. losses
                

			

            	


			
			7 thoughts on “Containing losses is important”		


		
			
			
				
					
												Vijay says:					


					
						
							
								September 16, 2015 at 9:22 am							
						
											


									

				
					True.  This is one reason why I prefer (and invest in ) a balanced fund as against a large-cap fund.  

For example, HDFC Top 200’s annualized returns for 1, 2, 3 and 5 yeas are -1.9%, 24.6%, 15.6%, and 7.9% respectively; whereas those of HDFC Balanced fund’s are: 9.0%, 32.7%, 20.4% and 14.0% respectively (as on September 15, 2015).  The returns are truly based on how the fund contains losses in a falling market than a gains in a rising market.

				


				Reply
			

	
			
				
					
												Rohit says:					


					
						
							
								September 25, 2015 at 1:57 pm							
						
											


									

				
					If someone is investing with a 25 year horizon for accumulating wealth, then such comparison still matter. Which fund would you suggest. HDFC Top 200 Vs HDFC Bal

As marked by Vijay, HDFC Bal knocked out Top 200. Why is that

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								September 29, 2015 at 2:42 pm							
						
											


									

				
					Hi Rohit,

Apologies for the delayed reply. HDFC Top 200 is a pure equity fund, with no debt component. Its heavy dependence on bank stocks (State Bank of India and ICICI Bank) has hurt its recent performance; the fund is also tilted more towards cyclical sectors such as energy, capital goods, power, and construction which have seen their stock market run slow. The fund tends to take longer term view of sector and stock prospects. HDFC Balanced fund is not directly comparable to HDFC Top 200, as it is in a completely different category and risk profile. This fund benefited from both the equity and the debt rally. Hope this helps.

Thanks, Bhavana

				


				Reply
			
	
			
				
					
												Rohit says:					


					
						
							
								October 6, 2015 at 1:17 pm							
						
											


									

				
					Thanks Bhavana for your reply. Valid point.

Can you answer this: If someone is investing with a 25 year horizon for accumulating wealth and an aggresive investor, then such comparison still matter. Which fund would you suggest. HDFC Top 200 Vs HDFC Bal

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								October 6, 2015 at 6:06 pm							
						
											


									

				
					Hi Rohit,

A balanced fund aims at containing risk and downsides. For an aggressive investor, especially one who has a horizon of 25 years can stay invested in equities and won’t really need a balanced fund at all. That said, whether or not HDFC Top 200 (or HDFC Balanced) has to be invested in depends on what other schemes are already held and how much the investment is, besides other factors.

Thanks,

Bhavana

				


				Reply
			










	
			
				
					
												Rohit says:					


					
						
							
								September 29, 2015 at 1:23 pm							
						
											


									

				
					Can you please advice..

Thanks

				


				Reply
			
	
			
				
					
												Vijay says:					


					
						
							
								October 1, 2015 at 10:39 am							
						
											


									

				
					My idea behind comparing a pure large cap fund to a balanced one was solely based on how each fund handles a downturn.  The debt part in the balanced definitely provides a cushion during a bear run, and doesn’t have to work as hard as a large-cap to recover losses.

I invest in HDFC balanced via SIP.  Remember to avoid any fund which is tilted to sector (as Bhavana explained Top 200 is).

				


				Reply
			







		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


