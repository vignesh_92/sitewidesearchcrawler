

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Views: The debt fund spook and what it means

            		January 31, 2019 . Vidya Bala
                    

	
                    
                                    Mutual Fund Research . Mutual funds . Views
                    
		
            	

            	The debt fund landscape is undergoing a change. And change seldom happens without disruptions.  The structure, regulation and liquidity aspects of debt instruments have been put to test in the past six months. For you, understanding these risks is paramount to take investment decisions. 

This article will discuss the nature of risks that have emerged, their impact or possible impact and what is our view on such risks.

The risks 

Let’s first sum up the broad range of risks that we have seen in the debt market from September 2018. Please note that the risks we are discussing here has nothing to do with interest rate risk – which is the risk coming from changing interest rates. That risk exists in every cycle turn and is handled by the market reasonably well.

	Credit risk and rating effectiveness: The possibility of companies not paying their interest or principal, on time, is the risk that has been troubling debt funds since September 2018. This is not unusual, especially when the credit quality of an instrument is low. But what is different this time around is that lenders have been caught unaware by top rated (AAA) papers like IL&FS suddenly defaulting on their short-term and medium-term papers. This also raises the issue of how effective rating agencies are in anticipating trouble, given that a AAA-rate paper slid to default in no time. 
	Liquidity risk: When a debt of Rs 91,000 crore (from IL&FS) is under risk, lenders panic. The market is eclipsed by fear of further default. This happened and resulted in liquidity tightening for many companies – especially NBFCs, who need a steady stream of funding. Higher demand and lower supply of funding caused a spike in short-term rates putting further pressure on the repayment capacity of companies. Also, lenders refused to reinvest (called rollover) leading to stress in large NBFCs.
	Share price fall and the impact on debt: In September 2018 when the IL&FS issue broke out, shares of other NBFCs too fell sharply. Many of the promoter shares of such companies were pledged to borrow loans for group companies. MFs too had lent on such security.  Since such loans against shares carried stipulations that the share value had to be 1-2.5 times (varies between lenders) the loan value, share price falls caused this agreement to be breached. This, even as there were no defaults. Such events happened outside the NBFC space too. For example, the fall in Yes Bank shares led to stress in loan against shares raised by some group companies. This was the same case with the Essel group when shares of Zee Entertainment fell last week.
	Lack of legal clarity: Given the complexity of the structure of the group of companies in IL&FS, what followed was a legal tussle. In early January 2019, IL&FS sponsored special purpose vehicles (SPVs) for roads jointly run with various state governments and generating healthy cash flows refused to pay their interest to lenders. They stated that an order of the NCLAT (National Company Law Appellate Tribunal) in October 2018 put a moratorium on all assets/payments of IL&FS group. However, the SPVs were supposedly ring-fenced – that is the sponsor (IL&FS) cannot dig into the SPVs’ cash flows dedicated for repayment of debt. This issue once again led to non-payment of interest and therefore a ‘default’.  


Result

The risks stated above result in the following possibilities: 

	The instrument is downgraded when the risk of non-payment is enhanced. This results in a mark down in NAV and the impact is high or low depending on the quantum of rating downgrade and the proportion of the scheme’s holding in the paper. For example, a rating downgrade from AA+ to AA- may be just 2 notches while a fall from AA+ to BBB would be 6 notches. Similarly having a 1% exposure to an instrument causes less pain compared with say a 6% exposure.
	Any event of non-payment of interest results in ‘default’ rating by the rating agency and mutual funds take a mark down. Again, the impact of the markdown depends on the earlier rating and the fund’s exposure to that debt.
	The amount due is fully written off by the mutual fund scheme if the principal is not returned on due date. This causes a large hit to NAV if exposure is high.
	When MFs lend against shares and the loan to value level is breached, it can trigger a downgrade/default by rating agency. This did not happen with the Zee group where the borrower and lender agreed there would be no event of default triggered merely on account of share price. 


The impact

Default/downgrade: In the case of the IL&FS default event in September 2018, funds from various AMCs – notably DSP, Principal, BOI Axa, Tata, Motilal Oswal, Invesco – held IL&FS and group companies. These had exposures that hurt the NAVs. Others such as Aditya Birla MF held papers of the group that did not see any impact. 

Most of the IL& FS papers matured by December 2018 and the fund houses had no choice but to write them off.  Others such as DSP, Motilal Oswal and Invesco wrote off before it fell due. A few such as Tata Corporate Bond still hold papers due in November 2019 with valued written off by 50%. 

The write-offs led to single-day NAV erosions as high as 2-7 percent. When multiple day write-offs were done, the effective 1-year returns were wiped off in many funds. For example, DSP Credit Risk’s current 1-year returns is -2.3% while that of Invesco Credit Risk is -3%. Even a low duration fund such as Tata Money Market now sports a mere 0.2% returns for 1 year now. While what we quoted here is the extreme impact when exposure goes up, it highlights the kind of NAV erosion that write-offs can have on funds. 

Legal tussle: The more recent problem with the SPVs of IL&FS (mentioned in the earlier part of this article) saw fund houses marking down their NAVs; some on a cautious note even before the downgrade happened. UTI Dynamic Bond, UTI Banking & PSU Debt, UTI Bond, HDFC Dynamic Bond and Aditya Birla Sun Life Medium Term Plan had over 5% exposures and therefore saw a notable dip in their NAV while others such as HDFC Short Term Debt or Aditya Birla Sun Life Credit Risk had limited exposure and their hit was insignificant. 

Share price fall: The other recent event of share price fall in Zee has not thus far triggered any rating action. But it remains a risk event that can transpire if there is any payment default. Since this a recent event, here’s a list of funds with exposure (of over 3%) to group companies that have borrowed against the shares of Zee Entertainment. 


	Fund

	Instrument	Exposure 	Maturity
	Aditya Birla Sun Life Credit Risk 	ZCB	9.21%	Mar-20 & Apr-21
	Aditya Birla Sun Life Dynamic Bond	ZCB	12.27%	Mar-20 & Apr-21
	Aditya Birla Sun Life Medium Term Plan 

	ZCB



	12.54%	Mar-20 & Apr-21
	Baroda Credit Risk 



	ZCB	9.89%	Jul-19
	Baroda Short Term Bond





	ZCB	7.95%	Jul-19


	Baroda Treasury Advantage 

	ZCB

	8.26%	Jul-19
	Baroda Ultra Short Duration

	ZCB

	6.31%	Jul-19
	DHFL Pramerica Credit Risk 	ZCB

	3.30%	Mar-20
	Franklin India Low Duration 

	ZCB	4.78%	May-20 & Jun-20
	Franklin India STIP 



	ZCB	3.61%	May-20 & Jun-20
	ZCB - Zero coupon bonds			



In the above case, in the event of any of the group companies not paying up any of the lenders (banks, NBFCs or MFs) a default may occur. Or, if rating houses believe that the credit quality has deteriorated, a downgrade may occur. A quick, successful sale of Zee Entertainment stake by promoters may prevent these risks from manifesting. 

What is new? 

If you ask us whether downgrades or default is a new phenomenon, it is not. What is new is the scale of a single default (IL&FS and group) leading to tightening liquidity in the entire system and pressuring other borrowers. The lack of clear regulations and speedy resolutions after such events transpire has come to fore only now, with the recent series of events post NCLAT’s ruling on IL&FS. The Amtek Auto default case of 2015 dragged on till 2018 when UK-based Liberty House acquired the stressed firm. Lenders still face a problem with their dues even with the enactment of the Bankruptcy Code.  

When to hold and when to fold

Events such as the IL&FS case and its contagion effect, or a sudden sharp fall in share prices, are impossible to foresee. Most of the time, what we can do with debt funds is only a postmortem. In some cases, we sound off the risk in a fund ahead of write-off (Like the case of Motilal Oswal Ultra Short Term) when we know a similar event has occurred in another fund. In other cases (like the Zee issue) we take a tough, precautionary stance. 

In all other cases, our job is to find out how best you can recover – by staying or switching. To be candid, when it comes to debt funds, there can be no clue on rating slippages given that rating agencies themselves are caught off-guard. Given the enormous number and range of debt securities that funds hold, financials and debt structures of all these papers are very hard to access or track, leading to a reliance on the credit rating of the papers alone. Broadly, our calls are as follows:

	Avoiding risk: When we know funds hold risky instruments per se, we avoid them. Most of the funds in the primary IL&FS issue did not figure in our list. Funds from the Invesco, BOI or Baroda house that took concentrated credit risks were not in our list despite scoring high on returns. 
	When to hold: We ask you to hold in two cases – one, when the hit is not large and the fund is meant for a longer-term holding. For example, Franklin funds were on their path to recovery soon after the JSPL default in 2015, despite taking a 25% loss on those papers. We recommended continued holding of those funds. The funds have recovered smartly from this drop. But we curtailed further exposures to such funds. Two, when the issue in hand is not a result of a credit risk (the toll road tussle currently on between IL&FS SPVs and MFs) and is a matter of legal clarity, we prefer to wait. 
	When to fold: When funds demonstrate returns and have not shown any tendency of overshooting risk parameters, we recommend them. But here, when a AAA-rated instrument gets hit (as was the case with DSP Credit Risk) there is little we can do. What we then do is check how soon the fund can recover. If we assess that you would be better off with other funds even if you exited at a loss, we recommend a switch to such funds. We do this based on the fund’s existing portfolio yields and where it can get returns, its expense ratios, other funds’ yield and maturities, and the interest rate cycle. This is what we did this for DSP Credit Risk.


Next, when the risk of credit downgrade continues to be high (case of ZEE group), or the group has been accused of corporate governance issues, or the debt structures are opaque, then we take a call (as we did for ABSL Dynamic Bond and ABSL Medium Term) of reducing the risk to your portfolio. In such cases, our call is based on how painful a negative outcome can be. We may also recommend an exit on loss due to a downgrade if the holding period is short-term and we see possibility of further downgrades down the line.

How to read our calls

When we ask investors to stay, it is mostly done when the hit is already taken, and the fund seems on its way to recover. However, when there is more uncertainty and pain left, we prefer to clean up the mess and start afresh. In the process, when we give switch/exit calls some of you will have exit load and tax implications. 

From our perspective, we are alerting you to events/risks. When we see the outcome of risk posing a significant erosion on NAV, that is when we call it out. If the event does not turn out to be negative, you would have lost out on exit load or taxes. But in our opinion, it is better to be careful and cede some gains than to see risks materialize and suffer real losses. You will need to take an informed decision by weighing the risks and the load/tax implications. 

Higher risks are par for the course in equity as it nets you an extra 2-5 percentage points over time. But for the additional 50-100 basis points that you would get in higher-risk debt, a slide of 7-8% is certainly not a worthy risk. 

What we know is that these kinds of events tend to shake up fund houses and perhaps instill more prudence and conservatism. At the same time, that debt funds are a superior option to traditional FDs is beyond debate. Hence, instead of labelling them as risky, we wish to reassess what risk is ‘good risk’ when it comes to debt. Given the changing landscape, it is now evident that looking at credit rating, looking at parent backing of papers and fund house strength in debt is not enough. we need to wear our thinking caps to find better processes to identify and curtail risks in portfolios. We will come back to you with our views on this. 

 

Other articles you may like
	FundsIndia Reviews: Small-cap Funds
	FundsIndia Views: More bank clean-up and what to expect
	Why High MF NAVs should not deter you
	Budget 2018 impact: Dividend distribution tax on equity funds





			
			Post Views: 
			2,727
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    debt funds. FundsIndia. Investments. Mutual funds
                

			

            	


	
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


