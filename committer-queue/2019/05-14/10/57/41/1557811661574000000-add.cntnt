

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Views: Opportunities in Brexit

            		June 27, 2016 . Bhavana Acharya
                    

	
                    
                                    Mutual funds . Recommendations
                    
		
            	

            	The epic, if divisive, vote last Thursday that decided the United Kingdom’s exit from the European Union was a bombshell for the financial and political worlds. Stock markets across Europe, the US, and Asia crashed between 4 to 8 per cent, the pound plummeted, and commodity prices dipped.

What does this massive upheaval mean for your investments?

India, being a relatively smaller trade partner with the UK and EU and a net commodity importer, is shielded from much of the impact from Brexit. Hence, our growth story is still intact. Where it can hurt our markets is in foreign institutional inflows and currency fluctuations in the short to medium-term.

 Open  a FREE Account Now! 

Financial markets, including our own, will be unstable for the next several weeks as the financial world digests the news.  And that presents opportunities for you. This is the time for you to deploy your surplus and use this volatility to average down your investment costs. Over the long term, India, being the few spots of growth, in a tottering global economy should attract foreign flows once more. 

Global impact

Brexit’s hardest impact will be, obviously, on UK’s economy as it breaks away from the union it has been a part of for decades. For starters, it loses access to the single market EU bloc, which forms about half its trade It will also mean no common trade agreements, as much of its other trade also came through the EU bloc agreements. This will pose the risk of more trade barriers and higher trade costs. It also requires the drawing of fresh trade agreements with countries. 

Second, the EU is a major source of FDI for the UK and the city of London is the largest global financial centre. Changing regulations post the exit can affect both. Third, renegotiations involved in a number of other issues – such as access to open air space, status of immigrants and movement of Britons and EU nationals within the Eurozone – could all mean uncertainty in various industries, travel, tourism and also the job market. 

Until the UK and the EU are able to find a new normal, there could be an intermediary slowdown in their trade, productivity and investments, thus causing an economic slowdown. This in turn could pull down global growth – especially as nations exporting to the zone see a dip in demand. While central banks across countries have pledged to support growth, what measures they will take remains to be seen.

Of course, the exit process has to be officially invoked first, and this in itself can get delayed. Further, once begun, the entire seceding process can take years to complete, and the method and impact is far from clear.

What is clear is that uncertainty will rule for the short to medium-term, shrinking the risk appetites of the financial markets until clarity emerges. 

Impact on India

There are four key areas where India will be affected, to varying degrees:

Currency: Exchange rate fluctuation is the biggest fallout, for India, from Brexit. Uncertainty and a lower risk appetite can lead to an appreciation in the dollar and other safe havens such as gold and the yen, in turn depreciating the rupee and other emerging market currencies. Further, the FCNR redemptions due in September-November this year can pressure the rupee. Companies that have overseas borrowings can also get hit, depending on their loan tenure and extent of hedging. At the same time, the currency depreciation can keep India reasonably competitive, especially in a slowing demand scenario.

Trade: India is not a huge trade partner with the UK. Ministry of Commerce data shows that goods exports to UK have accounted for just around 3 per cent of the total from 2012-13 onwards, while imports are half that. Depreciation in the pound, a UK economic slowdown, and renegotiation of trade deals will thus have minimal overall impact. FDI inflows into our economy from the UK are also low at 2 per cent of the total in 2015-16, down from the 14 per cent in 2013-14, according to DIPP data.

Sector-specific: Though India is, more or less, insulated from the UK-EU turmoil, specific sectors will be hit on rupee appreciation against the pound or euro. The first is software, where reports peg UK’s share at 17 per cent of the total software exports. Some deals can become unviable while there can be delays in signing new deals. The IT sector has the second-heaviest weight in the BSE 500 and the Sensex.


The second is pharmaceuticals, which account for 5 per cent of exports to the UK. However, the US is by far the biggest market for this sector. The third large sector that will be impacted is auto ancillaries, which are huge exporters to the entire EU zone – for some listed players, the region forms close to half their revenues. Other smaller sectors that are exporters, but which don’t have much stock market representation are textiles, (accounts for 32 per cent of our exports to the UK) and gems and jewellery (6 per cent of UK exports). Metals and steel companies may also be affected as a growth slowdown caps commodity prices and realisations.

But given the diversified nature of companies in these sectors, the fall-out is likely to be very stock-specific. The market hasn’t reacted too sharply to the sectors as a whole, either. 

Stock markets: The changing global economy, central bank actions, and currency fluctuations can prompt FPIs to rework their strategies. Besides, London is a leading financial centre, home to one of the largest stock and futures markets, several global pension funds, hedge funds, mutual funds, and insurance companies. Global equity flow can be further affected if regulations concerning capital flow and taxes change. Domestic markets can, therefore, see turbulence in the short to medium-term. FPIs hold 40 per cent of the BSE 500’s free-float capital and their moves sway the market. 

India’s fundamentals hold

Over the longer term, however, FPI flows can return as India is among the few regions of growth in the slowing global economy. Besides, our own markets are strong – mutual funds have used FPI selling as opportunities over the past year thereby shoring up the market. So, a sustained equity inflow into MFs can be of some support.

	We are a net commodity importer. Muted prices of commodities such as steel, oil, and metals due to a potential slowdown will in fact be beneficial for us.
	 We are a more domestic-driven economy. Consumer durable production captured in the IIP has been growing from last June. Retail bank credit is up 19.2 per cent in the past one year. They point to urban consumption recovery. The Seventh Pay Commission, due to be implemented, will increase domestic income. A consumption uptick can eventually drive production revival.
	 Monsoon progress across the country has caught up, and the deficit in rainfall that had been in place so far is rapidly shrinking. Rural incomes, hit by two years of drought, can stage a comeback, broadening the consumption base.
	 The index for the eight core industries has been improving from December, on the backs of increased production in electricity, cement, fertilisers, refineries, and coal. Sales of commercial vehicles, road toll rates, infrastructure project awards and progress, and so on are also on the uptick. All these are initial pointers to recovery in production and signs of capital investment.
	 Corporate revenues finally arrested their decline in the March 2016 quarter, with even sectors such as infrastructure and engineering clocking revenue growth. Sustained low input costs can further boost profitability and earnings.
	 RBI repo rate cuts and liquidity management measures are beginning to translate into lower interest rates. With more room to cut rates and a stable fiscal, the hitherto sedate industrial credit can pick up.


All this indicates that economic and corporate growth is bottoming out and long-term growth prospects are firmly in place. Even among other emerging market nations, India has stronger fundamentals. 

What should you do?



The first and most important point is to continue with your SIPs. The bedlam gripping markets now should not push you into selling out or stopping your SIPs. With volatility on the cards, this is the best time to average investment costs lower. Since uncertainty can prevail for a good while, staggering your surplus over five to six tranches over the next few weeks will be a prudent move, especially for those of you who missed the opportunity in January and February this year. This investment can be in addition to your regular SIPs. 

 Open  a FREE Account Now! 

You can increase investments in your existing equity funds. If you’re looking for other funds to invest in, choose from the three below. We are suggesting these as they are a bit more volatile (allowing you better averaging in a short period), they have a mix of large-caps and mid-caps and represent different sector plays. Large-caps may also fall more than mid-caps in the immediate future if foreign investors pull out.

Birla Sun Life Frontline Equity: This is a large-cap fund with exposure to key export sectors such as IT and pharma, making it a slightly contrarian theme should exports from these sectors take a hit. Still, it has lower weights to these sectors compared with the index. 

Kotak Select Focus: This is also a large-cap fund with some exposure to mid-caps. It has a low allocation to software, pharma, and auto ancillaries. This fund would be better if you wish to play on domestic themes such as banking and construction, and avoid any potential export fallouts. 

Franklin India Prima Plus: This is a diversified fund with slightly higher standard deviation. The fund is currently betting on banking, engineering, and telecom. It has limited allocation to software, automobiles, and pharma. 

For long-term investors, the takeaway is to deploy more, or, at the very least, stay invested. We know that market crashes happen, just as we know that bounce backs also follow.

FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis of investment decisions. To know how to read our weekly fund reviews, please click here.

Other articles you may like
	FundsIndia View: Buy in May and stay that way
	Changes to FundsIndia’s ‘Select Funds’ List
	FundsIndia Explains: Why all mutual funds may not suit you
	FundsIndia Strategies: Challenge fund managers with this election portfolio





			
			Post Views: 
			2,912
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    brexit. fund recommendations. view
                

			

            	


			
			5 thoughts on “FundsIndia Views: Opportunities in Brexit”		


		
			
			
				
					
												vipin says:					


					
						
							
								June 27, 2016 at 5:18 pm							
						
											


									

				
					Is it possible for me to invest 10000 RS more in next month SIP? If so how can I do that? how that is going to benefit me?

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								June 29, 2016 at 1:07 pm							
						
											


									

				
					Hi Vipin,

Do you mean a one-time investment of Rs 10,000 next month? If so, you won’t be able to add it to your SIP, since SIPs are regular investments. The benefit in adding to your investments in phases over the next few weeks is to take advantage of any volatility that can come in. While the market is up for the past two days, there is still uncertainty in global markets. Investing more in such times will help average down investment costs if you hold for the long-term. As such, the extent to which you will be able to average costs depends on your existing investment and how much you can invest.

Thanks,

Bhavana

				


				Reply
			




	
			
				
					
												Ranga says:					


					
						
							
								June 29, 2016 at 6:24 pm							
						
											


									

				
					Detailed and Helpful article indeed.

Please clarify: when you say “short to medium term” , how long tenure are we looking at? (like, x months to x years)

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								July 2, 2016 at 1:44 pm							
						
											


									

				
					Thanks, Ranga. It is hard to specifically say ‘x’ months because a lot depends on what the exit procedure is, what central banks do (in fact, expectations that central banks will be more accommodative to push growth has boosted sentiments this week), and the extent of the actual impact. Our own monsoon progress and earnings numbers for the next couple of quarters are other drivers for the market. All this together will help address uncertainties, in turn reducing volatility.

Regards,

Bhavana

				


				Reply
			




	
			
				
					
												Raghu says:					


					
						
							
								June 29, 2016 at 11:04 pm							
						
											


									

				
					I am following your articles regularly from few months. It provides very useful key informations to the beginners like me in mf feild. I am Investing 2000 rs per month in sbi small and midcap fund and pharma fund from one year.is my portpholio on right track? Could you please sujest other than this? If any.

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


