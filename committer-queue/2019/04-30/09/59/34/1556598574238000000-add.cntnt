

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Explains: What is Risk

            		April 23, 2018 . Mutual Fund Research Desk
                    

	
                    
                                    Simply Important
                    
		
            	

            	In the context of any investment product and specifically mutual funds, we often use the term ‘risk’. Risk acquires different meanings to different people. They also assign such meaning differently for different products.  For example, risk to a 25-year old may connote higher returns for risk. Risk , to a 60-year old could mean possibility of losing money. 

Understanding what risk really is and where it arises from is important for choosing the right product. Right product means one which suits your requirements. You also need to be prepared to handle risks associated with them. So let us try to understand more about the nature of risk involved in both equity and debt securities.

What is risk

Risk essentially means uncertainty of outcome. When you expect something to happen, but are not certain of its occurrence, if you still go ahead and put your time/effort/money there, you are taking a risk. 

Take this simple example. When you go for a swim, you are not certain you will come out. There’s a chance, no matter how small, that you might drown. But you still go for a swim. By doing so, you are taking a risk. 

Why people take risks

The psychological aspects of risk-taking behaviours are outside the scope of this article. However, to put it simply, individuals take risk primarily for two reasons. First is the thrill of it. Certain activities may involve considerable risk. But the thrill and joy of those experiences make them irresistible. Examples may be trekking, paragliding, etc. In such cases, we usually mitigate the risk by having an expert guide us.

The second reason for taking risk is the expectation of greater rewards. Take for example, taking swimming lessons. It is considerably risky as you are getting into the water when you can’t swim. But the reward is high. If successful, you will learn swimming and will forever feel safer in water.

When does risk arise

Risk arises when the outcome of any activity cannot be guaranteed. Take an election for example. Parties contesting the election are taking risk because none of them know the outcome. If there were just one party, the outcome would be known and hence there would be no risk.

Risk related to financial products

If you are wondering why I am talking of everything other than financial risk, it is because financial risk is not all that different from risks in other walks of life. Financial risk also involves uncertainty in the outcome of a business activity. 

When you invest money, it invariably goes to a business activity (most of it, at least). There are primarily two ways to invest money in a business:

	invest directly in the business and participate in its gains or losses
	lend it for a fixed rate of return


The first is what you do when you buy shares of a company. In such a case, you are participating in the business. The second one, lending to a business, can be done by buying debentures issued by the company. In this case, your returns will be fixed and regular, subject to the capability of the business to pay you back.

Risk in ploughing your money in a business/company

It is common knowledge that businesses are risky. The outcome of any business cannot be guaranteed. One cannot say how many units will a company sell or at what prices over the next few years. One cannot predict movements in prices of raw materials. There is no way to predict the impact the entry of a new competitor may have. There also risks from geopolitical shifts, regulatory changes, labour unrest, etc.

Well established companies like Nokia, HUL and Airtel can be shaken up by new entrants. Even the biggest of enterprises are not entirely safe. So it is not difficult to understand why directly investing in a business can be risky.

However, the risk involved in lending is trickier to understand. Why is lending risky when the returns are fixed? 

Risk in lending to a business

Lending is risky because the fundamental nature of business is such that it doesn’t preclude the possibility of the businesses completely failing to honour any of their obligations. If a business fails, the company is liable to pay back its lenders from its assets. But if its assets are not sufficient to make such payment, it will not pay back.

So even though interest is fixed it will be paid back only till the business is in good shape. The possibility of a default always exists. Just to note, not all defaults mean that the business is failing. It might be a temporary issue that the business can resolve and pay the interest later.

Credit risk: So now you know why lending can be risky as well. But can all loans be equally risky? Take a company like Reliance Industries, which has been in business for over 40 years, or Tata steel which has existed for over a 100 years. These are good companies, large companies, well established companies. We may think none of these are going to fail any time soon. 

But can intuition serve this purpose for all businesses? Many smaller companies are not well known. That is where credit rating agencies come in. Credit rating agencies like Crisil, ICRA and CARE evaluate businesses and rate their bonds. These agencies have a lot of information about the businesses they rate. They look at the financial statements of these businesses, talk to their management, get additional information as required, and only then they assign ratings. The ratings are a good way to know how reliable the company, or a specific paper, is. Typically, ratings are given as AAA, AA+, AA, AA-, A, and so on. Higher the rating, lower the risk of default.

Duration risk: Now we know that some companies might not be as fundamentally strong as Reliance or Tata Steel. But a business which is seemingly doing well today, can’t suddenly fail tomorrow. Tomorrow is more certain than day after, which is more certain than the next week, which is more certain than the next month, and so on.

To put it simply, uncertainty increases with time-frame. You can be reasonably certain of how a business is going to perform in the next 3–6 months, given sufficient information. But predicting the long term performance will be difficult. How do you know what the company, or the economy, will be doing 8 years from today? Hence risk is higher when you are lending for a longer period and lower when lent for a shorter period.

How to reduce the risk

How do you deal with these risks? One way is to assess your risk with elaborate questionnaires. A simpler and practical way is to choose products based on your time frame of investing.  When it comes to equity, short-term is risky. It is that simple. In the short term the speculation on what is to happen is higher and that leads to volatility in the stock. In the long term, when the company or business proves its growth potential, this risk reduces.  Hence equity should be bought only if you have a long time frame. 

The point of no loss is actually 7 years and above in equities, based on past returns. However, you can further reduce this risk by investing in the market systematically, thus not timing the market and allowing units to be averaged in short-term volatility. And even for the long term the risk in equity can be hedged by introducing debt in your portfolio.

Debt returns vary by time frame. Shorter term debt usually carries lower risk than longer term risk. If you choose longer term debt product, trying to exit them in shorter time frames can hurt you. 

The other way to reduce risk is to take guidance. 

As hinted in the trekking example, taking guidance from an expert can reduce risk. The same can be done for your investments as well. With mutual funds, fund managers do the expert job of managing your portfolio. And in the choice of funds, your advisors can help choose the fund fitting your time frame and risk profile. How much of equity and debt to hold and in what funds to hold will decide how well you manage your risks to building wealth. 

Other articles you may like
	FundsIndia explains: What is NAV?
	What are dividend and growth options in a mutual fund?
	FundsIndia explains: What are diversified or multi-cap funds?
	FundsIndia explains: Should you invest in mid-cap funds?





			
			Post Views: 
			2,371
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    risk
                

			

            	


			
			1 thought on “FundsIndia Explains: What is Risk”		


		
			
			
				
					
												promila bhardwaj says:					


					
						
							
								April 24, 2018 at 12:43 pm							
						
											


									

				
					i love to read all the articles in this space. they are so relevant and aptly put into words that is easy to understand and relate to. this topic is treated very delicately to express the true sense of risk for investors and helping investors to take the right call for themselves.

thank you for your expertise.

promila bhardwaj

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


