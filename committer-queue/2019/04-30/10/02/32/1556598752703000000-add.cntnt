

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Reviews: MNC Funds

            		May 19, 2015 . Sathyamoorthy N
                    

	
                    
                                    Mutual funds . Recommendations
                    
		
            	

            	Over the last few years, Multi-national Corporation (MNC) funds have managed to outperform a good number of large cap and diversified equity funds. Despite the rich valuations of MNC companies, stocks in this space rallied well over the past few years, given that they are typically the preferred option in volatile years (until 2013). What makes this segment of equity stocks tick? What are the risks they carry, and can they be compared with other regular sector funds?

 Open  a FREE Account Now! 

MNC funds invest in multinational firms listed here, and also in stocks that have high Foreign Institutional Investor (FII) holdings. MNCs are favoured by markets for a few reasons. First off, they are usually cash rich and low on debt as their foreign parent adequately funds them. As a result, they have healthy cash flows, and a sound balance sheet, besides facing very little issues on corporate governance. Added to this, they are often stable to high dividend payout candidates. In the last five years, MNC funds have generated close to 25 per cent returns compounded annually. That’s far higher than the large and diversified fund category average of 13 per cent.

Nature of MNC stocks

MNC fund investors though will have to contend with valuation risks most of the times. Being cash rich and belonging to sectors like Fast Moving Consumer Goods (FMCG) and pharma, most MNC stocks tend to trade at high valuations, with their Price Earnings Ratio (PER) in high 20s and 30s. That means their earnings growth has to keep pace with valuations. A failure to do so often leads to markets punishing the stocks.

There are also a few other issues that add to the volatility of MNC stocks. One, issues such as high royalty payment to the parent company crops up from time to time in the case of these stocks, and sometimes, they tend to pull down prices. Two, some of these stocks also see a sharp increase in their prices on rumours of delisting, as many investors tend to take positions based on such news. Such rallies often result in companies not resorting to delisting, even when they had planned to, as the high valuations make it a less attractive position for the company to buy back shares and delist. When a delisting, however, remains a rumour and does not eventually occur, then the stocks often react negatively as positions are unwound. A change made to the delisting regulations in 2014 further made delisting of shares a tough task for MNCs.

Three, these stocks also tend to run up quite a bit on buyback news. However, given that in recent years, the non-promoter stake has to be high enough (not less than 25 per cent), the buyback was not an option for MNCs who held 75 per cent or higher. In fact, promoters had to shed stakes when it was over 75 per cent.

 Open  a FREE Account Now! 

MNCs also pay higher dividends as it means they have to send profits back to the parent without other significant legal/tax implications. But to an investor, it means that instead of reinvesting in the company’s growth, the money is distributed.

While those are some of the limitations, it is true that MNC stocks possess some strong fundamentals that make them a good investment candidate. Besides their cash rich nature and low debt, MNC companies often enjoy demand from a dual market. One is the domestic market (India), where the unit is, while the other one is the other foreign markets of the parent, including its home market, as MNCs often use Indian units as a low-cost outsourcing hub. This is true of many auto, auto ancillary, and capital goods companies. To this extent, MNCs are run at optimal capacities.

MNC companies also have the advantage of the parent sharing latest technology, providing technical know-how, or sometimes, key inputs that make the Indian unit either superior in terms of the quality of the product offered, or in other cases, quite cost efficient compared with a full import that may otherwise be needed. This is often very true of machinery and engineering goods.

Suitability

Thus, MNC funds can boast of stocks with mostly sound fundamentals, albeit highly priced. Although they are thematic funds, they are more diversified than sector funds for one simple reason – they hold a basket of sectors. In addition, they hold a basket of both defensive (FMCG, pharma) and growth sectors (capital goods, engineering and auto) giving them an option to choose sectors based on market conditions.

To this extent, you could say that MNC funds can be less cyclical than other sector funds, and their performance across markets too suggests this. Still, these are considered as theme funds with concentrated exposure to a theme. Hence, your investment should not exceed 10-15 per cent of your overall equity portfolio. At this juncture, if you choose to invest in these funds, instead of investing lump sum, consider investing through Systematic Investment Plans (SIPs) with an investment horizon of at least five years.

Performance




Birla MNC Fund outperformed its peer – UTI MNC Fund, and the CNX MNC Index by a wide margin over the last 15 years. Had you started a Rs. 1,000-a-month Systematic Investment Plan (SIP) in Birla MNC 15 years ago, you would have a handsome Rs. 14.7 lakh today. That’s an Internal Rate of Return (IRR) of 25 per cent. On the other hand, UTI MNC generated an IRR of 23 per cent, compared to the Index IRR of 17 per cent.

 Open  a FREE Account Now! 

Birla MNC and UTI MNC funds’ one-year returns, rolled every day for the last five years, suggests that both the funds beat its benchmark 90 per cent of the times. On a point-to-point returns basis, Birla MNC scores over the UTI MNC by delivering 36 per cent and 25 per cent over the last 3 years and 5 years respectively.

However, on a risk adjusted returns basis, UTI MNC has been less volatile (thanks to its high large cap exposure), generating better risk-adjusted returns when compared with Birla MNC and the CNX MNC Index.




Portfolio




Both the MNC funds follow the growth style of investing, as this segment can seldom be expected to provide ‘value’. Going by past record, Birla MNC holds higher mid-cap MNC stocks, and it currently holds about two-third exposure to them. On the other hand, UTI MNC has more than 50 per cent in large cap stocks.

If we exclude the financial sector, sector preferences were more or less the same for the two funds. Financials is the top sector holding in Birla MNC, followed by FMCG, auto and engineering. FMCG is the top sector holding in UTI MNC, followed by auto, engineering and pharma.

Both the funds have adequately diversified their portfolios by holding about 40+ stocks in 10 different sectors.

ICRA, Kotak Mahindra Bank, Bayer Crop Science, Honeywell Automation and Glaxosmithkline Pharma were some of the top picks in Birla MNC. Maruti Suzuki, Bosch, Hindustan Unilever, Eicher Motors and Cummins India were some of the top picks in UTI MNC.

Ajay Garg manages the Birla MNC Fund and Swati Kulkarni manages the UTI MNC Fund.

FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis of investment decisions. To know how to read our weekly fund reviews, please click here.

Other articles you may like
	Go for gold ETFs this festive season
	FundsIndia Recommends: Kotak Select Focus
	RBI beats expectations with a 50 basis-point rate cut
	Roll back of PF tax – Winning the battle and losing the war





			
			Post Views: 
			3,029
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    fund recommendation
                

			

            	


			
			9 thoughts on “FundsIndia Reviews: MNC Funds”		


		
			
			
				
					
												Syamantak says:					


					
						
							
								May 19, 2015 at 11:48 pm							
						
											


									

				
					“Had you started a Rs. 10,000-a-month Systematic Investment Plan (SIP) in Birla MNC 15 years ago, you would have a handsome Rs. 14.7 lakh today. ”

— 10K per month equates to 120000 per year. Thus in 15 years the investment value itself would have been 18Lakh. Did you mean an SIP of 1,000/-?

				


				Reply
			
	
			
				
					
												Sathyamoorthy N says:					


					
						
							
								May 20, 2015 at 11:03 am							
						
											


									

				
					Hi,

You are right, it is Rs.1,000/Month. Typo error, we have corrected it. Thank you for bringing us to our notice.

Thanks 

Sathya

				


				Reply
			




	
			
				
					
												KAUSHIK SENGUPTA says:					


					
						
							
								May 23, 2015 at 11:59 am							
						
											


									

				
					Does BSL MNC/UTI MNC fit in my portfolio. Please suggest any changes if BSL FL Eq/UTI Opp may be replaced by BSL MNC/UTI MNC in my portfolio.

My Portfolio is ICICI Foc 20%, BSL FL Eq 20%, UTI Opp 10%,HDFC Pru 10%, ICICI Disc 20%, Frank Sm Cos 20%.

				


				Reply
			
	
			
				
					
												Sathyamoorthy N says:					


					
						
							
								May 28, 2015 at 11:09 am							
						
											


									

				
					Dear Sir,

Sorry for the delayed response. As a policy, we won’t give portfolio recommendation through our blog. We will review your portfolio and give recommendation through our dedicated advisory team. They will get in touch with you shortly.

Thanks

Sathya

				


				Reply
			




	
			
				
					
												Aadil Darvesh says:					


					
						
							
								October 31, 2015 at 5:24 pm							
						
											


									

				
					Planning to invest in UTI MNC and Birla MNC funds, 1000 each  for 5 years in November 2015.

Is it a right selection?

				


				Reply
			

	
			
				
					
												Harshit says:					


					
						
							
								January 6, 2016 at 9:25 pm							
						
											


									

				
					Dear Sathya

May i request your team to write me on my email address i want to get my portfolio reviewed the investment is big hence very muvh concerned.

Thanks

Harshit

				


				Reply
			
	
			
				
					
												FundsIndia Desk says:					


					
						
							
								January 19, 2016 at 11:18 am							
						
											


									

				
					Hi, Harshit!

If you hold an account with FundsIndia, please book an appointment with your advisor by clicking here – http://www.fundsindia.com/content/jsp/advisor/AdvisorAppointmentAction.do. He / she will review your investment portfolio and answer any investment queries that you may have. Thank you!

				


				Reply
			




	
			
				
					
												bapanbowri says:					


					
						
							
								May 6, 2016 at 2:41 pm							
						
											


									

				
					Dear sir

I have a sip (uti mnc fund) last year 2015 on 25 April. but m shocked only 28/ is profit on my chart……

Now i am thinking is it possible for me to investment is same or am going for beter one Investment plan…

so, sir please guide me which is best sip for investment.

Thanks

Bapan

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								May 19, 2016 at 12:31 pm							
						
											


									

				
					Hello Sir,

Please get in touch with your advisor if you are a FundsIndia investor. This forum is more for public discussion. We are constrained from giving individual recommendations. thanks, Vidya

				


				Reply
			







		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


