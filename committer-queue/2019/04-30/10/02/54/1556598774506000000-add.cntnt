

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Views: Why ‘time in the market’ is your only answer

            		August 19, 2015 . Vidya Bala
                    

	
                    
                                    General . Mutual funds . Personal Finance
                    
		
            	

            	Last week, we spoke of lump sum investing versus SIP investing. We discussed why you need to hold your investments for longer periods to avoid the risk of negative returns, if you decide to go the lump sum way. 

Just as your ‘time in the market’ (how long you stay in the market) becomes critical when it comes to the question of investing regularly, or at one go, your ‘time in the market’ is also important when it comes to your choice of funds (category allocation), as well as asset allocation.

Risk and time

For instance, every once in a while, a few of you ask us why you should not simply hold a mid-cap fund, or take higher exposure to mid-cap funds as they deliver higher returns, anyway, over the long term. You also substantiate it by saying that investing through SIPs and also holding for the long term anyway reduces risk. 

 Open  a FREE Account Now! 

We largely agree with that view. The point of disagreement, if any, is only on what you think is long term, and what it should ideally be to make your perception a reality. 

Just look at the example below. We have one diversified fund and one mid-cap fund each from two fund houses – UTI Mutual Fund and Franklin Templeton Mutual Fund (taken for illustrative purposes only). All of these are known to be good, steady funds in their respective category. Let us suppose your ‘long term’ was a 5-year period, and that too it started at the beginning of a good market like early 2006 – the beginning of a 2-year rally.

As can be seen, the two diversified funds delivered superior returns to their mid-cap peers, despite a decent holding period of 5 years and averaging rupee cost throughout. So, what happened? 

Two things: you got good returns overall because you had a good down market in 2008 to buy on lows. However, as is the case with mid-caps, they fell hard in a downturn. Also, after every big hit in the market, when a revival happens, mid-caps take longer to bounce back as the bruised market players prefer the ‘quality’ and ‘visibility’ of large cap companies, especially given that they too would be attractively valued post a fall. 

So, while you had good portfolio returns overall, you would have had better returns by holding a higher proportion of diversified equity funds than mid-cap funds. 


Now, what if you had simply continued the above SIP from January 2006 till date (as of August 14, 2015)? The data below clearly tells you that the mid-cap funds from both houses delivered higher than the diversified funds. But over what period was that? Close to 10 years! And that too if the intermediate falls did not spook you into exiting these funds. 


Please note a few things here: I have specifically chosen the 2006-10 time frame (in hindsight) to give you an illustration of years that had both a bull and bear market. Chances are that you may not go through such cycles at all, and that, of course, poses a set of different challenges. But the points I am trying to make here are as follows: 

1. Despite a seemingly long term (5 years) holding and investing period, and despite regular investing discipline (SIPs), your portfolio is not entirely insulated from market vagaries.

2. While you do safeguard your portfolio from negative implications through SIPs, there could still be less than optimal wealth building (lower returns), caused by market vagaries (ups and downs).

3. If your theory of staying with higher risk funds such as mid-caps for higher returns is to come true, it is important for you to really stay in the market over the long term. 

This is true of asset allocation as well. Some of you feel you do not need debt funds because equity delivers in the long term anyway. While that hypothesis is true, time plays a crucial role here. For instance, a portfolio that had a debt fund with an allocation of at least 20-30 per cent in debt would have fared better than a portfolio with no debt – had you invested lump sum between 2008-13, without knowing what market turbulence can do. Of course, in this case, SIPs would have made your portfolio look better, and having debt would have added further strength. Again, you happened to be in a particularly difficult block of 5 years.

So, am I saying equity investing in only for decadal holding? No. The take away is simply this:

– Average across markets, as far as possible, through SIPs if you have a holding period of around 5 years or less than 10.

– It is not enough to average; it is important to diversify across asset classes to hedge for any lengthy bad years in a certain asset class. The diversification can be based on your time frame and risk-taking ability. Your advisor will help you identify the optimal allocation.

– It is not enough to diversify across asset classes; it is important to hold optimally across categories as well. Too many mid-caps may not always deliver higher returns (20-30 per cent of your total equity portfolio can at most be in mid-caps). Also, it may pull down returns if you are in not the best of market times.

 Open  a FREE Account Now! 

Use SIPs, hold across asset classes, and optimally hold across categories if you wish to build wealth optimally.

However, if you think equities are the only way, risky mid-cap funds are the best means to gain, and staying invested for long is not an issue, then you can be right if you hold for at least a decade, or over multi-decades. The wealth you build will certainly be worth the risks only then. 

Your staying power is all that matters, really.

FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis of investment decisions. To know how to read our weekly fund reviews, please click here.

Other articles you may like
	Should you invest in REC tax-free bonds?
	A Glance on Health Insurance for Senior Citizens
	FundsIndia Reviews: DSP BlackRock Opportunities Fund
	FundsIndia Reviews: UTI Transportation and Logistics Fund





			
			Post Views: 
			6,853
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Investments. lump sum. Mutual funds. SIP
                

			

            	


			
			13 thoughts on “FundsIndia Views: Why ‘time in the market’ is your only answer”		


		
			
			
				
					
												Ashish says:					


					
						
							
								August 19, 2015 at 4:52 pm							
						
											


									

				
					Similar to rebalancing one’s portfolio between equity & debt, does it make sense to rebalance between midcap & large cap? How should one go about doing that? Do multicap funds already do it?

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								August 20, 2015 at 2:54 pm							
						
											


									

				
					Ashish, good question. When we rebalance equity and debt, category rebalancing automatically takes place. So it is a subset of overall rebalancing. Multicap funds/diversified funds so it based on opportunities or valuations in different marketcap segments. thanks, Vidya

				


				Reply
			




	
			
				
					
												Romit Chhabra says:					


					
						
							
								August 19, 2015 at 6:52 pm							
						
											


									

				
					Hi Vidya,

I am 26 currently and for building my retirement corpus I am investing only in equity MF n direct shares. Do you think I should diversify among asset class?

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								August 20, 2015 at 2:53 pm							
						
											


									

				
					Hello Romit, As discussed in the article, if you are truly long (which you will be if you will not touch your money till retirement) and can take volatility in your portfolio and have other debt options like EPF, no harm in havign 100% equity funds if you will take such volatility and risk in the short to medium term. With direct equities, please be sure that you can manage the portfolio well. Else it can be risky even over the long term if your calls go wrong and you do not take course correction on time. But if you are in general low on debt, it is good to have at elast 20% of debt in your protfolio for some stability. thanks, Vidya

				


				Reply
			




	
			
				
					
												MANI TEJA Y says:					


					
						
							
								August 19, 2015 at 10:30 pm							
						
											


									

				
					Hi Madam,

I daily fellow your updates which are useful for my future plans. I want to know how far it is good to invest in Liquid Funds as SIP basis. If so, which company is good for invest.

Thanks & Rewards

Y. Mani Teja

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								August 20, 2015 at 2:45 pm							
						
											


									

				
					Hello Teja, Liquid funds are to be used for short-term temporary needs or if you are saving up to pay an impending expense like say an insurance premium or school fee etc down the year. This blog is more of a discussion forum and If you are a FundsIndia investor, please write to us using the advisor appointment (in help tab) and our advisors will help you choose a fund based on your requirement as not many liquid funds have SIP facility. Given their nature, they are usually meant for lump sum. thanks, Vidya

				


				Reply
			




	
			
				
					
												Venkatsubramanian says:					


					
						
							
								August 21, 2015 at 7:51 pm							
						
											


									

				
					Any possibilites to invest in one(liquid) fund from there to different funds for (STP).Like Discovery fund,Hdfc Equity,Birla frontline etc,

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								August 29, 2015 at 9:16 pm							
						
											


									

				
					Hello Sir, Not possible unless inter-AMC transactions become possible. Vidya

				


				Reply
			




	
			
				
					
												Sheetal says:					


					
						
							
								August 24, 2015 at 1:34 am							
						
											


									

				
					Slightly off topic – the classification of funds is a little confusing sometimes with what is shown here when compared with VRO. Good example is of PPFAS – it is shown here as “Equity -Large cap” whereas VRO calls it “Mid n Small Cap”. VRO ratings are referred to in all funds but not its classification!! This kind of stuff is confusing when trying to make a portfolio with different category of funds. Please either refer to VRO in full or none of it – just to keep it simple for subscribers. Thanks.

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								August 26, 2015 at 2:14 pm							
						
											


									

				
					Hello Sheetal, Fair point. Unfortunately, Our data base is not VRO (we can afford to go only with a data base that supports trnsaction data as that is vital for us) and hence we would be unable to give the same classification as VRO. However, wherever there are fundamental differences in terms of the real market cap of the fund (as you right pointed out in this case), we will look into it. THe VRO rating is given merely on account popular demand from investors to have their rating. At some point, when we shift to a consolidated base of common data and classification and rating, we will ensure they are uniform.  For now, we have started a periodic review of the classification we have and change them not based on VRO but based on the true nature and mandate of the fund. You did point out such discrepancies earlier too and we are working on it with our data provider. Thanks a lot for your suggestions. Vidya

				


				Reply
			




	
			
				
					
												Gian singh says:					


					
						
							
								August 30, 2015 at 6:09 pm							
						
											


									

				
					Dear Vidya Bala Ji,

Kindly advise,

I and my wife want to invest Rs.50000/- per month and i have selected the following funds :

1. ICICI pru Focused Blue chip fund-Growth ———– Rs. 6000/month

2. UTI equity Fund – Growth ———————————Rs. 12000/month

3. Franklin India smaller companies fund-Growth——-Rs. 8000/month

4. Axis long term Equity fund -Growth———————-Rs.12000/month

5. ICICI pru tax saver-Growth———————————Rs.12000/month

Kindly advise if any change required ?

Regards,

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								September 9, 2015 at 4:48 pm							
						
											


									

				
					Hello Sir the funds chosen are good. I would not be able to comment on the allocation and its suitability for you unless you are a FundsIndia investor. If you have your account, please route the query through the advisor appointment feature under help tab and we will understand your need and respond to you. This blog is more a general discussion forum. Vidya

				


				Reply
			




	
			
				
					
												Kamlesh Patel says:					


					
						
							
								September 7, 2015 at 6:40 am							
						
											


									

				
					good article.

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


