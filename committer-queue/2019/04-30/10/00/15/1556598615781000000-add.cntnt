

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Strategies: The Equity Fund Playbook for Rookie Investors

            		June 16, 2015 . Vidya Bala
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Research Desk . Strategies
                    
		
            	

            	If you are among the many FundsIndia investors who have started investing in mutual funds in recent months, here’s a ‘Playbook’ for all of you. It is simply about how we can, together, play the mutual fund game right!  Playing the game this way will make your investing life simpler and needless to say, richer.

It’s a ‘long-only’ game

While mutual funds are not just about equity funds, you need equities to generate wealth in the long term. If you decide to invest in equity funds, long is the only way. Else, it is best to stay away. 

 Open  a FREE Account Now! 

Every time you feel confident that you can invest in mutual funds for a year, think of a year like 2008 when the markets fell much more than 50 per cent. That means you would have lost half your money and not recouped it at all (because you came in with a 1-year time frame). To add to it, you would probably shun this efficient wealth-building product forever, simply because you had a wrong time frame for it. 

There are multiple fund categories in debt to suit you short-term needs. Go for it if you need money in the near term. 

The long game means STAYING INVESTED 

An elementary point, but not many seem to get it, going by the investment behaviour of many investors who do not hold their equity funds for even 6 months. Believe me, you cannot exit a fund even for poor performance in that short frame of time! 

Added to it, the market fluctuations can easily force you to exit your fund, or stop your Systematic Investment Plans (SIPs), making you take decisions in fear. Avoid the fear (when market falls), or the temptation (when market rises) to exit unless you have use for the money. The best you can do is not to watch your fund movements daily. It is the biggest favour you can do for your own self. 

Often times, we are so obsessed with returns (and not the actual value of money) that we lose sight of the wealth we need to build. Suppose you had invested Rs. 1 lakh in a fund, and it gave 60 per cent returns in a single year. Yes, you have Rs. 1,60,000 and you might be tempted to exit. Fine. But what next? Where do you propose to deploy it? Very likely, it will waste away in your savings account.

Instead, letting this money lie for say 10 years will have fetched you over Rs. 4 lakh (assuming a 15 per cent annual return). The question here is not just the returns, but the amount you will have in hand when you actually need it. Staying invested will add ‘wealth’; not just ‘returns’.

Non-stop SIPs

You can stop your SIPs and move to another scheme when a fund is not performing well. Stopping SIPs before your intended tenure with no further investment is the worst you can do for your portfolio. 

We can only take a leaf from the fall of 2008 to tell you how much you lose by doing so. 

Let us take an example of say Franklin Prima. Had you stopped SIPs by mid-2008 (started in mid 2007) but just held the fund, your returns by December 2009 would have been a measly 0.7 per cent annually (Internal Rate of Return – IRR). Had you continued the SIPs into 2008 and 2009, then by December 2009, your returns, in contrast, would have been 26 per cent annually. This is because the best period to average at low cost was the second half of 2008 and early 2009. By continuing your SIPs, you gave the fund a chance to average at really low market values. 

But more than anything else, by stopping your SIPs, you are stopping your investment/saving process for your future, fearing near-term movements. And that means you stop saving for your goals. Avoid that at all cost. 

If you are really worried about your fund performance (but do not expect your fund to do well when the market is down), then check with your advisor on whether you should change the scheme. 

Risk = loss

Our advisors come across investors who say they are ‘willing to take risk’. They mean that they are willing to buy equity funds for a short period, or are fine with taking risky bets such as a large exposure to mid-cap funds, or sector funds, and so on. 

But what does risk mean to you? For most of you, it means more returns. Sorry, risk is simply first about losing money – losing 50-60 per cent in a single year like 2008, and then waiting for a good while to build wealth. Your ability to take risk means your ability to lose money. 

Believe me, most of you do not have that kind of risk appetite. A few days’ fall in your fund, and it makes you wonder whether you should redeem, or stop your SIPs. Many of you actually do so. That is a clear indication that you cannot take risks. 

When you pick a ready-made portfolio from FundsIndia, or our advisory team customises one for you, we ensure that the risks are contained through right category allocation and asset allocation. When you choose to go overboard with some categories, especially based on the lure of the past 1-year returns, you are entering the risk zone. Read the next paragraph to know how it can be a slippery slope to depend on 1-year returns for your investment decision.

If it is short, it is slippery 

This is true not only in terms of time frame, but also for choosing funds based on short-term performance. Many investors get enamoured by short-term performance, especially when the returns look spectacular. The question is whether such a repeat performance is possible, more so, with sector funds. Look at the table below.


You will be surprised to know that the chart toppers in 2013, especially technology funds, were in the bottom quartile of 2014’s performance chart. Worse still, the international fund that was among the toppers in 2013 delivered just 1.3 per cent in 2014, what with the rupee appreciation also pulling down its returns. 

2014 had all mid-cap funds hitting the roofs. But now in 2015, we know that they have already taken a bit of a knock. 

 Open  a FREE Account Now! 

So, what are we trying to say here? Go by consistent performers. How will you know a fund is consistent? They would be consistent if they do not swing wildly – that is if they don’t top charts in one year, and hit the dust the next. Of course, this is no easy task for you to decipher, which is why we have our Select Funds list – researched and handpicked after quantitative and qualitative filters. 

This is your answer when you ask our advisors why we do not have some ‘top’ fund in our list: They happen to be top that year; that’s all. They are not consistent. They can pull down your returns the next year.  

Finally, the playbook essential: If you want to build a lot of wealth, it is easy to do so in calculators, adjusting the annual returns from 15 to 20 to 30 or even 40 per cent. That’s a number game. If you want a lot of money, you need to save a LOT of money and for a long enough period. Elementary, but hard to follow.

While equities certainly deliver superior returns, it is unfair and unreasonable to expect the equity market to create miracles. Instead of upping your return expectations, try to up your savings. SIPs and step-up SIPs are a good way to do this regularly. 

While none of us can, nor will we give you any guarantee on how much funds will deliver, I can say for sure, playing by these rules will certainly leave enough in your pocket to play your life – the way you’d like it.

Other articles you may like
	FundsIndia recommends: Reliance MIP
	Three changes
	Why you lose money when you invest
	FundsIndia Explains: What downside containment is, and why it is important





			
			Post Views: 
			2,961
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    equity funds. Investments. Mutual funds. savings. strategies. wealth
                

			

            	


			
			12 thoughts on “FundsIndia Strategies: The Equity Fund Playbook for Rookie Investors”		


		
			
			
				
					
												gaurav bajaj says:					


					
						
							
								June 16, 2015 at 8:57 pm							
						
											


									

				
					Very Nice Article. 1 year returns are most misleading indicator to chose Mutual Fund.

Question : I have SIP in Hdfc top 200 since 2011. Currently I am not sure shoukd I stop and switch to some other large cap fund as its rating has been decreased in recent years?

				


				Reply
			

	
			
				
					
												Gourab says:					


					
						
							
								June 16, 2015 at 11:18 pm							
						
											


									

				
					Hi Madam,

                 Thanks for gifting us such a nice article. Specially I liked the section where you mentioned to stay invested in equity for 10 years @ 15% rather to withdraw the amount after 1 year with 60% return.

                Madam, I’m also investing in MF through FundsIndia. Till now I’m investing in equity MFs. Currently I decided to move my some of my Bank FDs, Company FDs and Post Office TDs to Debt Funds.

               However I’m confused regarding the Debt Funds. There are several debt funds exist in market like Long Term Debt Fund, Short Term Debt Fund, Ultra Short Term Debt Fund, Income Fund, Dynamic Bond Fund, Gilt Fund etc.

               If you can share a topic regarding those funds(what is the significance, pros and cons of each type of fund), then it will be better for me to select the proper fund before investing.

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								June 18, 2015 at 12:40 pm							
						
											


									

				
					Gourab, thanks. I shall re-publish one on debt funds soon. thanks, Vidya

				


				Reply
			




	
			
				
					
												Vijay says:					


					
						
							
								June 17, 2015 at 9:47 am							
						
											


									

				
					Wow! This is an amazing article.

				


				Reply
			

	
			
				
					
												Kartik says:					


					
						
							
								June 18, 2015 at 8:34 am							
						
											


									

				
					Yet another excellent article… well explained in simple language.

				


				Reply
			

	
			
				
					
												Satyendra Tripathi says:					


					
						
							
								June 19, 2015 at 1:20 pm							
						
											


									

				
					Hello,

Yet another very nice article, It’s a kind of patience in investing which reaps a hefty returns in future. I personally feel like being consistent and patience based upon your risk appetite will give best possible that market can offer you.

Many thanks for such articles and we follow your each post and rely on FI to keep us on track of wealth building.

Thanks

Satyendra

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								June 19, 2015 at 10:09 pm							
						
											


									

				
					Thanks Satyendra. We will strive towards helping you in your wealth building mission 🙂 rgds, Vidya

				


				Reply
			




	
			
				
					
												Yogesh K says:					


					
						
							
								June 22, 2015 at 6:57 am							
						
											


									

				
					Great article as always highlighting the importance of time in the market rather than timing the market. 

Vidya – regarding your comment “If you want a lot of money, you need to save a LOT of money and for a long enough period.”, I’d also like to add that you need to earn a LOT of money to increase your savings potential. One can do that by focusing on developing their skills on a daily basis rather than reading about the markets, funds, news etc., which honestly are time-killers.

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								June 22, 2015 at 1:49 pm							
						
											


									

				
					Hello Yogesh, I think earning more has lot of ‘externalities’ to it. Besides, earning more does not automatically lead to saving more. In fact, evidence suggests that it mostly only leads to spending more 🙂 I would not put it as a top point to build wealth. Regular savings for sufficiently long periods is the key. Here is the article where one of our external writers makes the point: https://blog.fundsindia.com/blog/personal-finance/earning-more-and-more-is-not-enough-to-retire-well-2/7634

				


				Reply
			




	
			
				
					
												Suresh says:					


					
						
							
								June 28, 2015 at 9:09 am							
						
											


									

				
					Hi Vidya, really thankful for all your wonderful articles and discussions on mutual funds. Few queries from my side.

1)  In the case of existing investments value reduced by 30%, is it a better strategy to switch the existing investments to debt or liquid funds for the time being while the new SIPs/investments keep continuing?Or any other better approach to follow?

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								June 29, 2015 at 11:02 am							
						
											


									

				
					Hello Suresh,

I don’t quite get your query. If you mean you need to withdraw and park your money temporarily, then liquid funds are good. If you wish to book profits in equity and reallocate assets then go for long-term debt funds such as income funds. This you need to do only if you equity allocation has gone way ahead of where you started with. For instance you had a 70% equity allocation and itis now 75% or more then rebalance. thanks, Vidya

				


				Reply
			




	
			
				
					
												Yogesh K says:					


					
						
							
								June 28, 2015 at 11:08 pm							
						
											


									

				
					I think it all three factors are EQUALLY important – earning more, saving more, and having a long term plan.  People should constantly look for ways to increase their earning capacity – via skill upgrade, acquiring new skills, having multiple sources of income etc. I think most of the ‘personal finance’ articles focus primarily on savings. One can only save practically up to a certain percent of their income, and hence increasing the income is becomes a key factor.

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


