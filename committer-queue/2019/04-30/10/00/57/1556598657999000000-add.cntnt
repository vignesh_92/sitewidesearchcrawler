

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		How to choose liquid funds

            		September 19, 2018 . Vidya Bala
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Research Desk . Strategies
                    
		
            	

            	The recent events around IL&FS’ debt paper downgrades have raised concerns about how safe even short-term debt funds like liquid funds are, for retail investors. Should you fear these or with some careful judgement can you minimize your chances of risks in this category? This article helps you with some simple tips to choose liquid funds and use them as a stable portfolio diversifier.

The recent scare

A few liquid funds from the Principal, LIC, Union houses came under the radar for a hit to their NAVs because of providing for mark-to-market losses on their holdings in IL&FS and its group companies. There were funds from other categories too, holdings papers from the said group. However, a hit to the NAV of a liquid fund, considered the least risky, most liquid and meant for short term, has raised questions on how investors should approach this category. Before we move on to how you can choose liquid funds, it is important to know that the present risk that has surfaced is not an all-pervasive one in the liquid category. For example, barring one fund, the entire universe did not hold short term papers below A1+ as of August 2018. The average holding in instruments below AAA was a mere 0.7% and that too held by a handful of funds. Hence, that liquid funds knowingly go for high risk papers is not an easily acceptable argument. The outlier cases like IL&FS where downgrade happened from top rating will of course be hard to identify but rare to occur in this category.

With that aside, how can you choose funds to reduce the risk of holding a fund that will take a NAV hit?

Don’t go merely by chart toppers: In the liquid space, it is very hard for funds to generate returns that are significantly different from peers unless they take marginal risks. Such risk would primarily come the commercial paper space. For example, Principal Cash Management, which was among the top performers until the recent hit, still has the highest yield to maturity in the category (as of August 2018) at 7.56%; higher than the category average of 7.08%. Hence, do not get lured by the topper in this space. You are better off with funds slightly lower in ranks provided they fulfil the other criterion we mention below.

Fund ratings need to be viewed with caution: Ratings of liquid funds by various online websites are mostly influenced by returns alone and do not adequately consider risk or diversification. As a result, going by ratings alone may not be a prudent idea in this category. At FundsIndia, while our ratings are not merely based on returns, there may be funds with higher ratings that we would not recommend if we have concerns with the size of the fund house or their strategies. These, would not be captured in a quantitative rating methodology. Hence, if a fund has above average category returns and fulfills our other criterion here, you should prefer it even if it is not a 5-star rated fund.

Fund houses: While we are fans of smaller fund houses and their unique strategies, when it comes to liquid funds, we prefer known names. Such fund houses attract treasury money of very large corporate houses and hence have a heightened onus to keep risks at bay and follow enough due diligence. This helps pin more responsibility on the fund manager and in the process your money too has a better chance of being safe.

Size matters in liquid funds: Liquid funds are predominantly used by institutional investors for their treasury needs. When the AUM of a fund is small, redemptions by institutions with large exposure can impact returns. Hence funds with tens of thousands of crores is a blessing not a threat when it comes to this category.

Look for diversification: Instances of top rated (A1+) papers being downgraded to A4 are hard to predict nor can funds with such holdings be avoided. However, what you can do is a check of the concentration/diversification of a fund in the underlying instruments. For example, Axis Liquid has 134 securities in its portfolio. As a result, its exposure to individual securities (barring treasury bills) is low. Hence, even if one of its commercial papers is hit, the impact on NAV will not be high. On the other hand, Principal Cash Management, with just 20 securities, has concentrated exposure to individual instrument and hence any hit also causes more pain.

Instruments to look for: Needless to say, treasury bills are the safest, but a fund cannot load itself only with treasury bills as its supply is not infinite. Hence, funds supplement their portfolios with certificates of deposits (CD), commercial papers (CP), and some NCDs which are nearing maturity. Note that liquid funds cannot hold a portfolio with an average maturity of over 90 days. Hence, there is no duration risk in this category. What you would need to look for is a healthy dose of treasury bills (liquid funds on an average held about 13% in treasury bills) and the rest mostly in CDs and CPs. Concentrated exposure in CPs (over 5%) is something to look for in terms of identifying risks, especially if such CPs are top holdings and are from lesser known companies.

While using the above criteria will not entirely insulate you from NAV shocks, it will certainly reduce your chances. We use these besides established quantitative metrics by providing appropriate weights to them. This helps identify the low-risk, optimal return options.

Funds to invest

Here are some funds that will fit the above criteria quite well and in addition show the least instances of negative returns when returns are rolled weekly over the past 10 years (or since inception). They have also never had negative returns when returns were rolled fortnightly. In other words, over 2-week periods, these funds did not deliver any negative returns in the past decade.


	Fund	1-year returns (%)	YTM (%)	AUM (Rs crore)	No. of instruments
	Axis Liquid	7.13	7.14	30,065	134
	SBI Liquid	7.00	7.20	49,732	128
	UTI Liquid	7.10	7.10	45,537	180
	Returns as of September 18, 2018. YTM and AUM as of August 2018




Please note that you can use these funds for all the regular purposes of a liquid fund – short-term parking, systematic withdrawal or even as a diversifier to your equity portfolio. Where you are doing STPs, you will have to stick to liquid funds in the respective fund houses. In such an event, your objective should be to choose a good equity fund rather than focus on the temporarily held liquid fund.

FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis for investment decisions. To know how to read our weekly fund reviews, please click here.

Other articles you may like
	The birth of a new bull market
	FundsIndia Recommends: Axis Long Term Equity
	Why High MF NAVs should not deter you
	FundsIndia Views: Why your dynamic bond fund returns will not be static





			
			Post Views: 
			4,872
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Advisory. debt funds. fund recommendation. Investments. liquid funds. Mutual funds. Personal Finance
                

			

            	


			
			3 thoughts on “How to choose liquid funds”		


		
			
			
				
					
												M.Jagannathan. says:					


					
						
							
								September 19, 2018 at 10:19 pm							
						
											


									

				
					Dear sir/madam,

I would like to have a clear idea on fundamental analysis and technical analysis before investing.In otherwords.I would like to read plus and minus points myself. I request your help.

with regards,

M.Jagannathan.

				


				Reply
			

	
			
				
					
												The Outrigger says:					


					
						
							
								September 20, 2018 at 10:09 am							
						
											


									

				
					Please add liquid funds to your select funds. And while you’re at it, maybe add arbitrage funds also.

				


				Reply
			

	
			
				
					
												Seema V says:					


					
						
							
								November 16, 2018 at 9:43 pm							
						
											


									

				
					Hello M’am,  the 3 funds mentioned in the blog “How to Choose Liquid Funds”, for 2 referred funds therein, eligibility says “NRI/PIO”. So are Resident Indians  not eligible to invest?

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


