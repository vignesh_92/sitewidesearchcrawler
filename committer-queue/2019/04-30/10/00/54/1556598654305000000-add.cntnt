

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Views: SEBI’s new categories – what’s changing in your debt funds

            		May 16, 2018 . Bhavana Acharya
                    

	
                    
                                    Advisory . Mutual Fund Research . Mutual funds . Research Desk . Views
                    
		
            	

            	Last week, we discussed changes in equity funds as AMCs get into full swing in redefining categories based on SEBI’s new rules. This week we’ll take up the changes involved in debt funds, what the impact of these changes are, and what you should watch out for.

Broadly, SEBI has defined new debt fund categories based on three criteria.

	On the credit quality of the portfolio. Therefore, there is a category stipulating that funds invest only in bonds that are AA+ and above and a category for funds that invest in lower-quality bonds.
	On the nature of instruments. For example, specific categories for investments in government bonds, floating rate instruments, money market instruments, banking & PSU debt instruments.
	On the duration or maturity of the portfolio. Here, SEBI uses a metric called Macaulay’s duration. This measures the average number of years taken for weighted discounted interest payments on a bond to equal the amount paid for the bond (i.e., the principal or the cost price). A bond’s Macaulay’s duration will always be lower than the bond’s actual maturity period. On duration, categories are split into smaller buckets such as less than 91 days, less than a year, 1-3 years and so on.


In total, there will henceforth be 16 debt fund categories. Up until now, debt categories are half that number (per our category definitions at FundsIndia). The important point in the new categorisation is this – a combination of the above is not used to define a category. Therefore, a category that is defined by duration does not mention any conditions on the credit quality that the fund can have. Likewise, a category defined by credit quality says nothing on what duration a fund needs to follow.

Where the new categorisation helps

On the positive side, the category definitions prevent funds from changing strategies based on market conditions. A short-term fund, for example, could include a share of long-term gilt papers to make money off rate cuts but which increases the duration and the volatility to a large extent. An income fund could change tactics during a rate cut cycle and slip into credit or into duration. This moving across maturities and absence of a steady strategy in many funds will now be addressed.

With that, let’s move into specific fund categories, what’s changed, and what is not. Our approach to debt funds has been based on the investor’s holding period or timeframe and the extent of the fund’s duration. The Select funds are also bucketed on these lines. The ‘old’ categories mentioned in the tables below are FundsIndia’s categories. The ‘new’ categories are those SEBI has defined.

For shorter term holding periods


	New category	Old category from which funds will move	Holding period needed
	Overnight funds	Liquid funds	1 day to 3 months
	Liquid funds	Liquid funds	1 day to 3 months
	Ultra short duration funds	Liquid funds 

Ultra short-term funds	3 months to 1 year
	Low duration funds	Ultra short-term funds	6 months to 1 year
	Money market funds	Liquid funds 

Ultra short-term funds	1 year
	Short duration funds	Short-term funds	1 to 3 years
	Floater funds	Ultra short-term funds

Liquid funds	1 to 3 years
	Banking & PSU fund	Banking & PSU debt funds	2 to 3 years



By and large, funds that move to the new categories mentioned in the table may not need to change their strategies or portfolios by much. For example, existing short-term funds have portfolio maturities of 1-3 years and meeting the short duration category’s duration criteria should not be much of a stretch. Ultra short-term funds may need to make only slight adjustments where they move to the ultra short duration category since they already run average maturities of a year or less.

New categories introduced: Money market and floater funds are two new categories. While funds such as ultra short-term and liquid funds anyway invested in money market instruments, there was no exclusive category for these instruments. Funds in this category should invest in money market instruments with a maturity of up to 1 year and thus will be on par with today’s ultra short-term funds in terms of risk and return potential.

Floater funds need to maintain at least 65% in floating rate instruments. Floating rate instruments are debt papers where the coupon is variable throughout the tenure of the bond. Very few funds have slotted themselves into this category. The category requires a watch to see both performance potential and the type of investor requirement it suits. Floating rate instruments work best in rising rate cycles as coupon adjusts higher. There are floating rate funds today. However, they currently can invest in other instruments if such availability of these instruments is poor or do not offer adequate return potential. We would need to see how these funds manage different rate cycles.

What the risks are: As said above, SEBI’s definitions on duration do not make any mention of credit. Therefore, funds can run as they were before – i.e., short term and ultra short-term funds today take on low-rated credit papers to boost returns.

Today, short-term fund portfolios range from zero to 40% on an average in AA and below papers. By and large, it reflects in the portfolio yield-to-maturity. Similarly, in the new low duration category, there will be Principal Low Duration that has averaged 31% in low credit in the past year, a Tata Ultra Short with a 4% average credit exposure and an ICICI Pru Flexible Income with a 13% average credit exposure. In the new ultra short duration category, a high-credit L&T Ultra Short will jostle with Franklin India Ultra Short Bond’s 62% average credit exposure.

Therefore, returns and portfolio yields across the new debt categories will continue to mask the underlying credit risk of the fund. Credit risk in these categories will remain. As before, you will need to exercise caution in comparing funds as higher returns usually are associated with higher risks.

What we will do: We will continue to weigh credit quality, nature of instruments, concentration of high-risk instruments in the portfolio in our recommendations and in our ratings as we have been doing so far.

For longer term holding periods


	New category	Old category from which funds will move	Holding period needed
	Medium duration funds	Income funds

Credit opportunities funds	3 years and over
	Medium to long duration funds	Income funds

Dynamic bond funds	3 years and over
	Long duration funds	Dynamic bond funds	4 years and over
	Dynamic bond funds	Dynamic bond funds	3 years and over
	Corporate bond funds	Income funds

Dynamic bond funds

Credit opportunity funds

Short-term funds	3 years and over
	Credit risk funds	Credit opportunity funds	3 years and over
	Gilt funds	Gilt – Long-term	Tactical calls based on rate cycle
	Gilt fund with 10-year constant duration	Gilt – Long-term	Tactical calls based on rate cycle



Here, fund strategy changes are likely to be more significant than in the shorter-term categories. Categories defined by credit quality do not make a mention of duration. Categories defined by duration do not make a mention of credit. Gilt funds make no mention of duration. Therefore:

	Corporate bond funds will see a mix of portfolio duration. The funds moving to this category are a mix of short-term and long-term accrual funds. Aditya Birla Sun Life Short Term is today a short-term fund while HDFC Corporate Bond (erstwhile HDFC Medium Term Opportunities) is a longer term income fund. While funds moving to this category can continue to run the way they have been so far, they may explore playing with their maturities in different rate cycles.
	Dynamic bond funds moving to categories such as medium-to-long and long will have to rework their strategy to keep their duration within the restrictions given by SEBI. Aditya Birla Sun Life Income Plus, a dynamic bond moving to medium to long duration category, has seen maturity range from 6 to 15 years in the past two years. This freedom is now curbed for the dynamic bond funds moving to this category. Very few funds have moved to the long duration category.


What the risks are: Longer term debt funds come inherently with duration risks – that is, given their long maturities, they are more susceptible to changes in interest rates. This susceptibility reflects in the volatility of returns in shorter periods.

The risk in corporate bond funds is that they may change their duration based on the rate cycle even as they stick to top-rated corporate bond funds. So while they will be safe on credit, they may see short-term volatility from duration. Dynamic bond funds, of course, have duration risks as is the case today. Dynamic bond funds will be run as they are now.

In duration-based categories (first three in the table above), credit risk maybe a factor to look out for, as mentioned earlier for short-term funds. Consider medium duration funds, for example. Some funds moving into this category have taken low-rated credit earlier and can well continue to do so. Aditya Birla Sun Life Medium Term, Kotak Medium Term, UTI Medium Term, Franklin India Income Opportunities or Axis Regular Savings have had over 50% of their portfolio in papers below AA+ on an average. Likewise, funds that were lower on credit risk may continue in the same vein. L&T Resurgent India Bond, BNP Paribas Medium Term or IDFC SSIF-MTP, for example, have zero to less than 20% in low rated credit.

Therefore, credit risk in duration-based categories will remain. As before, you will need to exercise caution in comparing funds as higher returns usually are associated with higher risks.

What we will do: As said earlier, credit quality of funds is a factor we consider at all times and which we will continue to do. This apart, we will be watching strategies in categories such as medium-to-long duration to understand how funds will work within their restrictions to maximise returns. In corporate bonds, we will see if and how funds move around maturities to sustain performance across rate cycles, and how they use the small 20% leeway they have to invest outside high-rated corporate bonds.

What you should do

	If the funds you hold fit into your timeframe, simply continue to hold them. An ultra short-term fund becoming a low duration fund or vice versa will not unduly affect its suitability for you.
	If you are holding a fund for a shorter time frame of say less than 18 months and such a fund is moving into a longer-term category, check with your advisor on its suitability. For now, it appears that short-term debt funds have, at worst, moved to the corporate bond category. This is not a major cause for worry.
	Avoid being influenced by returns alone as they often mask risks in the portfolio.


Finally know this: A debt fund’s risk and return, and suitability for an investor, depends on the credit quality of its portfolio, its maturity and the extent to which this changes. SEBI’s categorisation of funds based on only one criteria and ignoring the others gives funds a good amount of freedom. Changes in fund strategies will not be apparent immediately or even in the next couple of months. Any changes – especially in categories such as corporate bond, credit risk, floater fund etc – will unfold slowly over time.

FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis for investment decisions. To know how to read our weekly fund reviews, please click here.

Other articles you may like
	FundsIndia Recommends: Tax-saving Funds
	FundsIndia Recommends: Tata Dividend Yield
	FundsIndia’s Mutual Fund Ratings – post SEBI's categories
	Auto time the equity market with FundsIndia SmartSIP





			
			Post Views: 
			1,117
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Advisory. debt funds. Investments. Mutual fund. Personal Finance
                

			

            	


	
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


