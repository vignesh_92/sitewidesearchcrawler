

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia reviews: Large-cap funds

            		May 11, 2016 . Bhavana Acharya
                    

	
                    
                                    Advisory . General . Mutual Fund Research . Mutual funds . Reviews
                    
		
            	

            	It has been quite a ride for the stock market, and especially large-cap stocks, since the Sensex peaked slightly above the 30,000 mark in March last year. The BSE 100 index, which represents large-cap stocks, is down 2.9 per cent in the year to date and 4.5 per cent for the year. But from the low in February this year, the index staged a comeback. 

Returns of large-cap funds consequently slid sharply and then recovered smartly from the February 2016 low. The nature of the market rally in March and April have also seen funds with a cyclical portfolio tilt recover. Here’s what large-cap funds have done in the past year.

 Open  a FREE Account Now! 

Making a comeback

As the euphoria from the strong election faded in 2015, and reform and growth predictions met with a reality check, the market headed south. From the high in March 2015 to the low hit in February this year, the BSE 100 index dropped a steep 17.8 per cent. The loss in the Sensex and the Nifty 50 were also around the same levels. Large-cap funds, on an average, lost 17 per cent. These funds have most of their picks coming from the top 100 or 200 stocks by market capitalisation.

But post the largely positive Budget, a return of foreign portfolio investors, fears over a US rate hike receding, and a prediction of normal monsoons this year, the market began to recover. Foreign Portfolio Investments (FPIs) ploughed in a net Rs. 29,558 crore in March and April alone, after being net sellers in nearly every month since last March. The BSE 100 index, thus, gained 11 per cent from March to date. The BSE LargeCap index, which is another representative of the large-cap segment, rose 18 per cent.

What’s different in this recent revival is that sectors and stocks that bore the brunt of the market fall in 2015 were accorded more importance. In the past three months, sectors that have logged the strongest gains are from the cyclical basket, recouping a good part of the losses they had made in the past year or so. 


For one thing, these stocks were beaten down and thus, were ripe for plucking. Two, these stocks were trading at more reasonable valuations than the high-quality or consumption-themed basket. Three, efforts to clean up the banking system, rate cuts and their transmission into lower rates, signs of an uptrend in core industries, improvement in freight rates and commercial vehicle sales, all suggest that the worst of the economic slump is past, and the hitherto stagnant manufacturing and capital goods sectors can revive. Four, predictions of a normal monsoon and no sharp hike in MSP prices both point to steady inflation and a rural revival. Five, government spending on roads, railways, urban and rural infrastructure can also improve fortunes of the industrial sector.

Common causes

On an average, large-cap funds registered a loss of 2.6 per cent for the one-year period compared to the 4.5 per cent loss for the BSE 100 index. The uptick from the February low helped erase a good bit of the heavy losses several funds had suffered. In fact, at the February low the one-year average loss was 18 per cent.


Unlike mid-cap funds, the investible universe for large-cap funds is much smaller and there’s lesser scope for deviation from the index weights. One stock most funds held among their top ten was Maruti Suzuki, whose swift correction pulled down returns. Similarly, the underperforming ITC was an addition to several fund portfolios, being among the few reasonably valued FMCG stocks. Other popular stocks include Infosys and Reliance Industries; the comparatively better run of these stocks countered the falls to some extent. 

In another thread running through most large-cap funds, the mild to steep correction in two fund favourites – software and pharmaceuticals – hit hard. Similarly, banking and financial services, the third must-have sector, had a rough time over 2015 and 2016, and this has affected the entire large-cap fund basket. 

Fund performance

Outside these three main sectors pulling down returns, funds had different weights to sectors such as industrials, cement, oil & gas, telecom, automobiles, and so on. The differences in these sector weights played a role in the extent of the fund’s fall, and their recovery. Some funds followed the value strategy, with a long-term bet on economic revival, more than others. This resulted in their portfolios having exposure to sectors such as construction, oil & gas, petroleum, power, industrials, and so on. So while they slipped badly in the past year, their recent performance has picked up.

HDFC Top 200, for example, held over a third of its portfolio in financial services in August 2015. Within this sector, it held severely beaten down stocks such as SBI, ICICI Bank and Axis Bank. DSP BlackRock Top 100 leaned towards energy, construction, and industrials. ICICI Prudential Select Largecap was weighted towards energy and metals, while UTI Opportunities had a tilt towards construction. These funds were among the worst performers, with the one-year loss between 20 to 24 per cent at the low in February.

In the middle of the pack – losing between 16-18 per cent were ICICI Prudential Focused Bluechip Equity, Birla Sun Life Frontline Equity, Canara Robeco Large Cap+, and L&T India LargeCap. Even in the recent market upswing, these funds remained in the mid-quartile, indicating lower volatility in returns. While the ICICI fund was banking-heavy, it also held NBFCs such as Bajaj Finserv and auto company, Mahindra & Mahindra, that stemmed the fall. The Birla fund also held the better banks, as well as stocks such as Hero Motocorp and Grasim Industries.

SBI Bluechip was among the best in containing losses. It had a much lower banking exposure than its peers, and instead had higher holding in consumer goods and auto. So was BNP Paribas Equity, for which consumer stocks such as Asian Paints, Jet Airways, and Hero Motocorp worked well. Franklin India Bluechip, while holding a good amount of banking stocks had the better ones such as IndusInd Bank, Kotak Mahindra Bank, and Yes Bank, and thus, kept losses in check. Motilal Oswal Focused 25 countered its banking heavyweight with concentrated holding in stocks such as Eicher Motors, CRISIL, and Britannia, which did not fare too badly. Kotak Select Focus was another one that managed to keep its losses lower than its peers. Holding cement stocks that didn’t correct much, apart from names such as Britannia, helped. Cement holdings also helped the fund make strong returns in the market revival. 

But the market’s recent love for cyclical sectors and beaten down stocks helped funds that relied on this theme cut back a good part of their losses. Stocks such as ICICI Bank, SBI, and Bank of Baroda gained between 6 and 14 per cent in from March. L&T, another popular stock for large-cap funds, was up 12 per cent while Tata Motors shot up 26 per cent. Cement stocks that funds held gained between 7 to 30 per cent. 

 Open  a FREE Account Now! 

The three-month return has thus seen some of the worst performers clock the highest return. Of course, this short term bounce back does not definitely indicate that the funds will continue to be the top performers over the long term. While market volatility persists, signs point to a gradual broad-based revival in both corporate revenue and profitability. The takeaway from the roller-coaster ride in the past year is that patience and a long-term perspective is needed.

FundsIndia’s Research team has, to the best of its ability, taken into account various factors – both quantitative measures and qualitative assessments, in an unbiased manner, while choosing the fund(s) mentioned above. However, they carry unknown risks and uncertainties linked to broad markets, as well as analysts’ expectations about future events. They should not, therefore, be the sole basis of investment decisions. To know how to read our weekly fund reviews, please click here.

Other articles you may like
	FundsIndia Equity Strategies: Change spells opportunities for you
	Changes to FundsIndia's Select Funds List
	FundsIndia explains: Asset allocation funds
	Changes to FundsIndia’s Select Funds list





			
			Post Views: 
			2,141
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    fund advice. Investments. large-cap funds. Mutual funds
                

			

            	


			
			1 thought on “FundsIndia reviews: Large-cap funds”		


		
			
			
				
					
												Arun Agrawal says:					


					
						
							
								May 11, 2016 at 1:15 pm							
						
											


									

				
					I believe that the last sentence says it all . Incisive article this one, compliments to the author .

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


