

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		Should your Age Determine your Equity Allocation?

            		December 11, 2017 . Gourav Kumar
                    

	
                    
                                    Mutual Fund Research
                    
		
            	

            	If you are investing in mutual funds, you might have heard people talk about portfolio allocation. It is often advised to keep your equity allocation at 100 minus your age. Is this sound advice and something that you should follow to a T?

It is generally understood that younger people have a higher capacity to take risk. As you age, your capacity to bear risk comes down. Why is this so? When you are young, you have lower commitments and also longer time for big goals in life – say your child’s education or your retirement. That means, you can take risks, even get hurt a bit and yet be on your way to build wealth. Hence, prima facie, it sounds like good advice to have higher allocation to equity (which is seen as a risky asset class given its volatility)  when you are younger and reduce your equity allocation as you age. But is that an all-season rule that can be followed at all times? Not necessarily. But before we explain why, let’s first understand your risk capacity really means.

Risk capacity matters, not risk attitude

Your risk profile can be split into two parts: your risk taking capacity and your attitude toward risk. Risk taking capacity is different from the attitude toward risk. If market volatility worries you, that’s your risk attitude (or risk tolerance). If you panic on seeing a big fall in the value of your investments, that’s your risk attitude. This however, does not indicate your risk capacity. Risk capacity indicates how much risk you could have taken based on your financial health, if you were completely neutral in your attitude to that risk.

Consider the example of a 32-year old IT professional. He started working when the sector was experiencing a boom and is earning a good salary today. He also has decent amount of savings in his bank. This person’s parents had a poor experience with the stock market. As a result, it was inculcated into him that stock markets are akin to gambling and he would do better to keep his money out of stocks. Today he looks at the rise and fall of the Sensex and takes comfort in having put all his money only in bank deposits. This person would be classified as conservative or risk averse. But does that mean this person cannot take any risk at all?

From a purely financial planning perspective, taking some risk will give his investments greater wealth building potential. Any short term fluctuations in the value of his investment would not really matter to him as he would be comfortably placed due to his savings in banks. This indicates that the person has a high risk capacity.

Thus, while risk attitude tells you how you feel about risk, risk capacity tells you how much risk you can actually afford to take.

Relationship between age and risk profile

Let us now look at the relation between age and risk profile in the light of the above discussion.

	
Your attitude toward risk may or may not change with age. It has less to do with your age and more to do with your experience with risky investment products and understanding of such products.


	
Your risk capacity goes down with age because you have lesser time to reach your financial goals. If you have, say,  3 more years to save up for your daughter’s education you have far lower risk capacity than another person with 10 years to go. This is because when you are not far from your goal, you need to invest high sums, save more to reach the goal. This exposes your money to greater short-term risk. Hence, you cannot afford to expose large amounts to huge risk. On the other hand, a person with enough years to go for the goal, can save smaller amounts, expose himself to risk in a more gradual manner and actually make the best of such risk by averaging in ups and downs.


	
Your risk capacity is low when it comes to goals that are a priority as opposed to goals that can be postponed. For example, if you are saving up to pay the school fee for your kid for next year, it is a non-negotiable goal. You cannot afford too much risk there given the time frame and the nature of the goal. If you are saving up to buy an iPhone in 6 months, it is possibly a negotiable goal that can be postponed to even next year. Here, your risk capacity is marginally higher.


	
Your risk capacity may also go up with age because you have a cushion of existing investments to fall back on and fewer financial goals to meet. For example, you may feel more confident investing more in equity in your 40s if you had a large surplus tucked in by then.


	
If you wish to generate regular income, from day one, from your investments, your risk capacity is significantly diminished, irrespective of how old you are. This is because you cannot expose your corpus (the base capital) that you invested to risk of erosion as it needs to generate a fixed amount of cash flow for you.




How far is your goal

The point you should focus on is the time to reach your financial goals and the nature of your financial goals (negotiable or not, or cash flow requirement or not). When you decide on your equity allocation, your age has only so much role to play. What matters more is the time that is left for a particular goal. And for this, it is essential to plan your finances separately for each individual goal. When you do that you will need to know why you cannot use equity for very short-term goals or how much equity to use for medium to long-term goals. For this purpose, it is good to know the risk equity carries over different time frames.

The following table shows the proportion of times equity funds delivered losses for different holding periods in the past ten years. The way this has been calculated is to take returns of each period mentioned in the table every single day since December 2007.


	Proportion of times funds delivered losses in …
		1-year	3-year	5-year	7-year
	Largecap	26.1%	8.1%	2.5%	0.0%
	Midcap	25.5%	9.0%	2.1%	0.0%
	Diversified	25.5%	8.5%	1.3%	0.0%
	Returns were rolled daily for the respective time frames for a 10-year period ending 30-11-2017



As you can see, the proportion of negative returns comes down in all categories as the holding period becomes longer. Also, over the past 10 years, funds have never given negative returns over a 7-year time frame. This number gets diminished when one used SIP.

So how does age really relate to your equity allocation and how should it affect your equity allocation? Truth is, there is no one-size-fits-all answer to this question. Equity allocation should reduce as you age, but rather than looking at it as a function of your age, you should look at it as a function of the time remaining for the goal.

You should review your portfolio every year and at the time of this review, you should also assess your risk profile. Do you feel more comfortable taking risks today than you did a year ago? Has there been a significant change in your financial health since the last year (addition or reduction of dependent members, fulfillment of a major goal, significant reduction in existing investments, etc.) which affects your capacity to take risks? Now combine these with the time remaining for your individual goals. Review the equity allocation in the light of this knowledge. You are likely to construct a much better portfolio with this approach rather than deciding your equity allocation purely based on your age.

Other articles you may like
	FundsIndia Strategies: Elections and Your Investments
	FundsIndia Reviews: DSP BlackRock Opportunities Fund
	Changes to FundsIndia's Select Funds
	Banks support earnings growth for the December 2018 quarter





			
			Post Views: 
			1,328
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Personal Finance. portfolio
                

			

            	


	
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


