

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		The impact of Budget 2018 on your investments

            		February 2, 2018 . Mutual Fund Research Desk
                    

	
                    
                                    Mutual Fund Research
                    
		
            	

            	The Budget 2018 proposal does not seek to leave more money in your hands. But by not pleasing you much, it has sought to keep its fiscal situation from deteriorating further. While we will discuss the benefits of a non-populist budget on the market and the economy in a separate article, here’s what the budget proposes for your finances.

Major tax changes

Imposition of long-term capital gains tax on equity and equity funds

For those new to equity markets and not used to equity taxation, the Budget may present a bit of a shock. From the next financial year, long-term capital gains on stocks and equity-oriented funds will be taxed. There are three aspects to this:



	Long-term is defined as a holding period of more than 12 months. This remains the same as before.
	Gains up to Rs 100,000 in a financial year will be exempt. The gains earned beyond this limit will be taxed at a flat and uniform rate of 10%. There will be no indexation benefits on gains. The table below explains this in simple terms with examples, for a holding period of 12 months and over.



	To prevent any retrospective taxation, and to cushion the impact, the tax proposal exempts all gains made up to January 31, 2018. Only the gains you make from this date onward will be taxed. How does this work? For that, understanding how the investment cost is calculated is necessary. In usual cases, the investment cost is simply the NAV or stock price on the date on which you were allotted mutual fund units or bought the stock. The gain will be the difference between the NAV of the sale date and the NAV of the purchase date. This rule changes for equity-oriented funds and stocks bought before 1st Feb 2018. The investment cost will be the higher of the following values:


Let’s see this with an example. Say you bought a fund on 1st December 2016 at an NAV of Rs 100, and its NAV of 31st January 2018 was Rs 120. You sold it on 1st May 2018. Referring to the table above, the investment cost works out as below under different instances of sale value.

For any investment made from now, the above rule won’t apply and your gain is simply the sale less the cost.

Imposition of dividend distribution tax on equity-oriented funds

Dividends from equity-oriented funds were tax-exempt up to now. From the next financial year onwards, dividends will be taxed at 10% to bring it in line with the capital gains taxation rules. The dividend will be deducted by the AMC and given to you net of tax, as is the case with debt-oriented funds today. The NAV will reduce by the dividend amount and the tax.

How do these changes impact you?

Equity remains the asset class that can deliver superior return over the long term. In the past 15 years, equity funds across large-cap, diversified, and mid-and-small cap on average delivered 22.6% annualised returns. This compares favourably with fixed deposits, whose interest rates have never gone beyond 9.25% since 2003-04 in any timeframe, going by RBI data. Similarly, for those who think the tax-free status of PPF is better for tax-saving purpose, do not lose sight of the fact that PPF interest has been dwindling steadily and is at 8% currently. Compared with this, the long-term return on ELSS (tax-saving funds) remains superior even post the 10% tax on redemption.

Taxation on equity is still more favourable than other asset classes. Fixed deposit interest is taxed at slab rates. Yes, the 5% tax bracket has a lower rate, but as seen above, the differential between equity and fixed deposit returns is large enough that the lower tax rate does not result in higher FD returns. Besides, for those in the low-income tax bracket, it is very unlikely that their gains from equity will exceed Rs 1 lakh. In such a scenario, you will still pay nil tax on your equity funds and be taxed for interest income in other options.  It is worth noting that pre 2004-05, equity long term capital gains were taxed at 20% with indexation or 10% without indexation. It is possible that over a period of time, equity will once again move to that structure. Be that as it may, the superior returns generated in equities in a tax-efficient way, makes it the best asset class to generate wealth. For those prone to a knee-jerk reaction of cashing out of equities fearing taxes, the question you need to ask yourself is where else you will generate superior returns in a tax-efficient manner.

Tax advantage of arbitrage funds fade. Arbitrage funds were used for short-term holding periods to escape taxes as holding beyond one year became tax-free. Their dividends were also being promoted as a tax-free income option. This allure of this option is drastically reduced. For those in the 5% tax bracket, arbitrage funds no longer holds good from tax perspective. For those in the 20% and 30% tax brackets, tax outgo may be only marginally more favourable with arbitrage funds. Note that arbitrage funds aim to deliver liquid-fund like returns and depend on available opportunities. Currently, 1-year returns of arbitrage funds are lower than liquid fund returns.

Dividend options no longer favourable. We have always taken a stand that depending on mutual fund dividends for regular income is unwise and that dividends are unpredictable. We’ve maintained that dividends from mutual funds are nothing but your own gains given back to you. With dividend from equity-oriented funds – and this includes balanced funds – now taxed, dividend and dividend reinvestment options are not viable. Though capital gains will be taxed, it happens only at the time of redemption. In the dividend option, taxes are cut every time a dividend is declared. This is true of dividend payout and dividend reinvestment. Besides, in the reinvestment option, it reduces compounding. If you have dividend reinvestment options in equity and balanced funds, it is best to switch out of them and into the growth option before 31st March 2018, if you are especially a long-term investor. You may suffer an exit load and/or short-term capital gains tax, but it would still be prudent from a longer-term tax perspective.

Keep portfolio churn to the minimum. Every redemption suffers a tax. If you keep redeeming funds each time you think it’s not doing well or it’s doing too well and you want to book profits, you will be paying tax henceforth, whether long-term and short term. Hence urge to switch funds constantly based on performance should be reduced.

Plan your redemptions. If you wish to plan your redemptions tax effectively, in future, you can consider staggering redemptions over two or three fiscals, instead of in one shot to gain a larger tax-exempt income. For example, selling (your long-term holding) in March and April of a calendar year would entail an exemption of Rs 200,000 (Rs 1 lakh each) since redemptions would fall in two fiscal years. Had you redeemed only in March, your exemption would be limited to Rs 100,000. This also holds to the logic of gradually moving out of equity as your goal approaches. Please note that we are NOT recommending this to avoid taxes now by churning your portfolio. This planning is for redeeming your money when you reach your goal.

Other key personal tax changes

	Education cess paid on taxes at 3% will be revised to 4%, and renamed health and education cess. Tax rates would then work out to 5.20%, 20.80%, and 31.20% in each slab under the new cess.
	For salaried employees, a flat Rs 40,000 will be available as standard deduction. That is, your income will be reduced by Rs 40,000 and then all applicable exemptions will come into play. Your taxable income will be calculated after all these. However, transport allowance of Rs 19,200 and medical reimbursement exemption of Rs 15,000 will be done away with. The standard deduction benefits all salaried employees, and more so for those who do not claim exemptions under transport and/or medical reimbursement.
	For senior citizens, up to Rs 50,000 per year on interest income from savings bank account and fixed deposits put together, will be exempt from taxes. Fixed deposits here include those from banks and post office accounts. TDS will also apply on income in excess of Rs 50,000. Earlier, TDS applied on FD income above Rs 10,000. FD income was also taxed in entirety; it was only savings account interest that was exempt up to Rs 10,000. With this move, instruments such as the Senior Citizens Savings Scheme become more attractive. Careful allocation between this option and debt funds can maximise tax benefits.
	For senior citizens with health insurance policies, the deduction claimable on premium paid will be increased to Rs 50,000 from the Rs 30,000 it was earlier. Similarly, expenditure on treatment of critical illnesses can be claimed up to Rs 100,000.


Other articles you may like
	FundsIndia Reviews: HDFC Top 200
	Budget 2018 impact - Why ELSS still scores over PPF
	What happened with IL&FS? What’s the debt fund impact?
	How much markets have crashed and risen





			
			Post Views: 
			1,906
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    budget. Mutual fund. tax
                

			

            	


	
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


