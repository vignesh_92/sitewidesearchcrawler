

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Recommendations: A Round Up

            		December 31, 2013 . Vidya Bala
                    

	
                    
                                    Mutual funds . Recommendations
                    
		
            	

            	Our Hits and Misses for 2012-13

As we bid goodbye to 2013, it would be incomplete if we did not take stock of how the fund recommendations and reviews we send you through the weekly e-mails fared. After all, we need to both pat ourselves a bit for the winning strokes and own up to you for the calls that went sore.

For this exercise, we started from our first actively recommended call beginning October 1, 2012 and took the cut-off for calls as June 30, 2013. Calls given between July-December 2013 were not considered as it would be too short a time frame to assess. Returns were as of December 27, 2013.

Invest Now with a FREE FundsIndia Account

Methodology and Universe

This gave us about 22 funds with ‘invest calls’, 22 funds given either as strategy calls or as reviews (suiting only specific investors) and hold calls. The rest were pure strategies and NFO reviews and therefore, not considered.

As it would be difficult to have a common thread to measure the average returns of our calls, we resorted to annualizing all the fund returns and compared it with a few common benchmarks over the same period as each of the calls. Returns were taken point to point for this purpose.

Please note that the actual returns (for recommendations less than 1 year) may not therefore be what is annualized. It is only for uniformity purpose that we have annualized them.

The benchmarks were S&P BSE Sensex and CNX Midcap for equities; the NSE Treasury Bill Index and NSE G-Sec Composite Index for debt funds.

Equity Performance Surprises

Honestly, we did not expect any fireworks from our calls. For one, since our first call, which started on October 2012, we have hardly completed a year and most of the calls have a longer tenure. Having given that convenient disclaimer, the markets too were far from kind.


2013 saw debt markets gallop at a fast pace, only to collapse in the second half. It was the reverse for equities, which remained volatile for most part, catching up in a final dash towards the end.

Perhaps because of our low expectations, the  equity fund returns did surprise us a bit. The average annualized returns of equity and balanced funds on which we had an ‘invest’ call was 12.7 per cent, as against the respective indices’ average return of 8.8%.

It is noteworthy that SIPs would have delivered more. We have considered point-to-point returns from our call date since we will have to reasonably take ownership for the timing of our call as well.

More interestingly, the returns from some of the funds ‘reviewed’ by us or those on which we had ‘hold’ calls, delivered much more – a good 29.6%, as against indices’ return of 10.9%. Clearly, this was aided by some of our reviews of international funds – specifically US funds.

Among the ‘invest’ equity calls, Religare Tax Plan, ICICI Pru Dynamic, ICICI Pru Focused Bluechip and Religare Mid N Small Cap were among the best calls; some of which we can take pride of, for specific reasons.

While ICICI Pru Focused Bluechip seems like a true winner on the charts today, at the time of our call in March, this fund had seen slippage in performance and saw its rating drop with some rating firms. Also, a change in fund management a few months before the call was also viewed with skepticism. ICICI Pru Dynamic, too, went through such a phase.

Religare Mid N Small Cap, which was incidentally the first call with which we started our weekly fund recommendations, was a fund with small asset size and therefore ignored by most, despite sound performance.

Of the mediocre performers, L&T Tax Advantage was a call we could have skipped. We were hopeful of the fund’s renewed performance after its take over by L&T. The fund was removed from our Select Funds’ list a couple of quarters ago. Others such as HDFC Mid-Cap Opportunities (given in January 2013) with returns of just 8.5% may seem lack lustre prima facie, but far outperformed the CNX Midcap performance (over the same period) of -6.2%.

Among our review calls (which are basically our way of picking interesting funds but leaving it to investors to take a call because they may not be suitable for all), US focused funds from ICICI Pru Mutual and Franklin Templeton respectively, and theme fund Franklin Build India took the honours. The last fund, was a complete dark horse play, and we are glad it worked. We intend to feature it again, for investors looking for tactical theme positions.

Lesson with our Equity Calls

We realize that many of the calls that did exceedingly well were risky ones – theme or international funds or risky mid-caps. Being a conservative bunch, they were but our review calls and we did not brave ourselves to give an outright buy on them. In other words, we picked them right and showcased them for you without telling you to buy them.

This year, we will attempt to (with enough disclaimers though) feature some choice tactical calls, which will hopefully, pep your overall portfolio returns. Otherwise, we think our equity calls may not leave much to complain about.

Debt disappoints but wait…

Now for the sober part. Our debt calls paled in comparison to our equity calls. The returns on our active ‘buy’ calls were just 4.3% as against indices’ return of 0.35%. Okay, some consolation that we fared better than the indices. Our ‘strategy’ buy call on liquid funds did a lot better with an average 8.3% return (index 8%) while our strategy buy call on high-risk ‘income funds’ was a miserable -1.2% (index fell by -2.6%).

So what went wrong? Ok, these were the calls before the July collapse; simply put, calls made at the peak and calls mostly for long tenure funds, because interest rates were falling then.

Some of our worst calls were in debt-oriented funds – Reliance MIP and Franklin India Life Stage FoF 40s Plan. Yes, the long-dated instruments in these funds caused a downfall post July, when rates started climbing up. We realized, rather late, that MIPs, in general, did not go much for shorter tenure instruments. So these funds did not change their portfolio maturity even when rates steepened.

Other than this, while the liquid fund call was timely, our strategy call in May 2013 on income funds, which was simply a call on interest rates, completely went against us  as a result of the totally unexpected liquidity tightening measures in July, followed by 2 rate hikes (funds such as Birla Sun Life Income Plus, SBI Dynamic Bond, Reliance Dynamic Bond, SBI Income and Templeton India Income Builder were featured by us in our May call).

Having said that, the return prospects for these funds look mighty high now, given the steep fall, and we would still watch over these funds for 6 more months before we consider reversing them.

The lesson from our debt portfolio is that we should rather play it safe on the duration – even for longer tenure portfolios. Income funds, in our Select Funds list now, do not have any scheme with high exposure to gilts or high average maturity. Yes, such funds may offer higher returns when rates fall but the volatility caused by high duration is something an investor’s portfolio can do without.

	Top Equity Buy Calls	Avg. Annualized Fund Returns (%)	Avg. Annualized Index Returns (%)
	Religare Tax Plan	25.17	9.77
	Religare Mid N Small Cap Fund – Growth	18.14	0.60
	ICICI Pru Dynamic	18.85	11.76
	ICICI Pru Focused Bluechip Equity	14.83	11.94
	HDFC Index Fund – Sensex Plus	15.55	18.89
	HDFC Children’s Gift Fund – Investment Plan	14.58	11.28
	Top strategic/review calls	Avg. Annualized Fund Returns (%)	Avg. Annualized Index Returns (%)
	FT India Feeder Franklin US Opportunities	57.61	9.47
	MOSt Shares Nasdaq 100 ETF	53.00	9.47
	ICICI Pru US Bluechip Equity	47.23	9.47
	L&T Global Real Assets	26.83	12.55
	Franklin Build India	25.03	22.40


Fund recommendations given between Oct.1 2012- June 30, 2013

Returns as of Dec 27, 2013

* Equity indices – BSE S&P Sensex, CNX Midcap 

That’s the round up of our calls. We hope to bring to your mail box more interesting funds  and strategies that will, hopefully, capture trends in a timely manner.

As we said earlier, watch out for our tactical calls (strictly for risk takers) in 2014. For others, we can with certainty say that 2014 will be a year to accumulate equities. For those holding on to debt, do wait for some time, as the party, when it happens, will not last long; like the New Year’s. 

Invest Now with a FREE FundsIndia Account

Other articles you may like
	FundsIndia Reviews: SBI Magnum Global
	Long is the name of the equity game
	Should you try to ‘manage’ your fund?
	The importance of retirement planning





			
			Post Views: 
			2,823
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    fund recommendations
                

			

            	


			
			6 thoughts on “FundsIndia Recommendations: A Round Up”		


		
			
			
				
					
												e m sivasnkaran says:					


					
						
							
								December 31, 2013 at 3:35 pm							
						
											


									

				
					it is praiseworthy that your recommendations had given the investors good return.please continue thein the same in the coming year.may god bless you.

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								December 31, 2013 at 10:53 pm							
						
											


									

				
					Thank you and wish you a great year ahead. tks, Vidya

				


				Reply
			




	
			
				
					
												Sumesh says:					


					
						
							
								January 1, 2014 at 12:23 pm							
						
											


									

				
					Great review Vidya…I had been wondering as to why my investment into Reliance MIP was not providing any returns recently…had not been tracking other investments too…Religare seems to be really nice , will keep a watch out for both the Tax plan and also for the Mid & Small fund

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								January 1, 2014 at 12:45 pm							
						
											


									

				
					Thanks Sumesh 🙂

				


				Reply
			




	
			
				
					
												Ram says:					


					
						
							
								January 2, 2014 at 6:29 am							
						
											


									

				
					Good one Vidya. Hopefully the long term performance of your calls keep this up. I’m a little skeptical right now on this report because the markets are at an all time high. I’m not taking away anything from your recommendations, but just calling out the fact…

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								January 4, 2014 at 2:56 pm							
						
											


									

				
					Thanks Ram. It is good for any investor to be a bit skeptical. The only thing that I would like to tell in our defence is that whether the markets are at a high or low, it helps to compare our call performance against the market. And in that we (read the right mutual funds) have scored well. thanks, Vidya

				


				Reply
			







		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


