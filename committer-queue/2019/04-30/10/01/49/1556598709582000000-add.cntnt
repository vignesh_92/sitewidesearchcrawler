

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia explains: What is a value fund?

            		July 18, 2016 . Mutual Fund Research Desk
                    

	
                    
                                    General . Mutual funds . Simply Important
                    
		
            	

            	If you’re a savvy shopper, you wait for discount seasons during the year to buy that new television, phone, fancy shoes or clothes. You get the same good performance or durability, but for a much lower price. If you look for cheaper prices for good quality while buying a phone and are willing to wait for it, you can do the same thing when you’re buying a stock. That’s a value strategy.

 Open  a FREE Account Now! 

What is it?

The principle of looking for good businesses that are trading cheap is value investing. Theoretically, a stock’s price should reflect the underlying potential in a company. A company’s fundamentals, which are its business strategy, revenue and earnings growth, its management, and so on, determines its true worth or intrinsic value. When the intrinsic value of a company is widely misjudged or not realised by the market, the stock’s price will be below this value. As and when the market realises this potential, the stock’s price will move up. A rerating of a stock (from cheap to good) can reap huge gains.

A value investor seeks stocks that are so undervalued. A value mutual fund is therefore one that follows a value strategy. Funds such as ICICI Prudential Focused Bluechip, Franklin India Bluechip, ICICI Prudential Value Discovery, PPFAS Long Term Value, and HDFC Top 200 are some examples of funds that tend towards a value strategy.

Stocks can become undervalued for several reasons – if there is an overreaction to bad news, the sector in which the company operates is in disfavour, if the company is just starting to turn around, or just plain market irrationality (it happens!). Investors use a combination of several metrics to judge whether a stock is undervalued – price metrics such as price to earnings, price to book, enterprise value, dividend yield, earnings metrics such as profit margins, return on capital, return on investment, performance metrics such as asset turnover, credit worthiness and so on.

One has to tread the value strategy with caution as a miss can result in value trap. A stock may appear cheap but for good reason. Such a stock may never move up and you may end up with an opportunity loss as your money could have been deployed elsewhere. 

How is it important?

Knowing that a fund is a value one will help you understand its performance. First, a value-based strategy will pay off only over a longer period. It takes time for the market to recognise potential and then lift the stock. 

Second, if it is a bull market, then value will well underperform. In such a market, there will be a set of favoured stocks or sectors and these run up swiftly. As a result, they will not be cheap and therefore will not be a part of a value fund’s portfolio. For the same reason, during market downtrends, a value fund will keep losses to a minimum. Since it already holds stocks that are down, they will not fall by much when markets fall.

 Open  a FREE Account Now! 

Third, value requires patience and suits moderate risk investors. If you hold a value fund, you should be willing to take temporary bouts of underperformance. For example, those such as HDFC Top 200, UTI Opportunities, ICICI Pru Focused Bluechip have all shown relative underperformance over the past year. This is because sectors and stocks these funds favoured, such as banking, engineering, or industrials were all out of market favour over 2015 and much of 2016. A preference for quality and earnings visibility by the market, which has been in place for years now, sent valuations of FMCG, pharmaceuticals, automobiles, finance companies and so on through the roof. Value funds may not own these stocks as they are seldom cheap. Consequently, they would have posted lower performance figures.

Other articles you may like
	Good funds alright but are they right for you?
	Ladies, feel liberated with a purse of your own!
	Of market crashes and recovery
	2017 – When mixing strategies paid off for our Select Funds





			
			Post Views: 
			3,528
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                                    

			

            	


			
			6 thoughts on “FundsIndia explains: What is a value fund?”		


		
			
			
				
					
												tarun says:					


					
						
							
								July 18, 2016 at 11:00 pm							
						
											


									

				
					Surprised to not see Birla Snlife Pure Value fund, l&t value fund, Templeton India growth fund, Templeton India equity income fund,,quantum long term equity fund.

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								July 19, 2016 at 10:27 pm							
						
											


									

				
					Hi Tarun,

As mentioned in the article, we were simply giving a few examples of funds that follow value-based investing. It certainly not an exhaustive list. 

Thanks,

Bhavana

				


				Reply
			
	
			
				
					
												tarun says:					


					
						
							
								July 21, 2016 at 11:06 pm							
						
											


									

				
					Hi 

Thanks for your reply. Would you want.to.consider.rating value funds as a separate category – just a suggestion.

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								July 26, 2016 at 3:28 pm							
						
											


									

				
					Hi Tarun,

I don’t think, at this point, we’d need to segregate funds based on their strategy. For one thing, none of the funds are purely value or purely growth. They are always a mix; the general leaning of the fund is towards a particular strategy. It wouldn’t be right to move them out separately. Secondly, at the end of the day, its the returns and performance that matters. The fund’s strategy is used to explain the performance and understand the potential of returns. It may not be a prudent thing to have a fund that’s actually not doing well at all, but is rated higher just because its been grouped with other value funds and additionally, in a smaller universe of funds. 

Regards,

Bhavana

				


				Reply
			










	
			
				
					
												Anant Prakash says:					


					
						
							
								July 28, 2016 at 7:52 am							
						
											


									

				
					In the blog , it was mentioned about HDFC Top 200. I was invested in it through SIP since 2008.As a layman, earlier the return seemed good, but not so in recent past. Even my FundsIndia expert suggested to switch. Was it a mistake and being invested there would have been better? And why that expert from the same Institution as of here could not suggest me to stay put with the fund citing the reasoning mentioned in this blog.

				


				Reply
			
	
			
				
					
												Bhavana Acharya says:					


					
						
							
								July 29, 2016 at 12:04 pm							
						
											


									

				
					Hello sir,

Trust your advisor has spoken to you and answered the query you have raised here. This particular blog was only an explanation of what value funds are. The funds mentioned here are examples, and not meant as an indication of what to do with a fund. That action depends on the other funds in your portfolio, your time-frame, how long you have been investing in the fund and so on. Hope your doubt is cleared now.

Thanks,

Bhavana

				


				Reply
			







		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


