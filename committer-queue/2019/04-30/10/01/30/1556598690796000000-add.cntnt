

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Equity Strategy: Why the India shining story has just begun

            		January 20, 2015 . Vidya Bala
                    

	
                    
                                    Equity Research . Fund advice . Mutual funds
                    
		
            	

            	If you are wondering if there is any upside left in the equity market after the 2014 rally and the flash rally this month till date, our answer to it is that we believe that the story may just have begun.

We had put out a quick note last week when RBI made a surprise rate cut of 25 basis points and opined that the rate cut, while obviously expected to boost debt markets, would be a more significant event for the equity markets.

This week, we’d like to elaborate on how this could be a tipping point for a more sustained turnaround in the economy and corporate earnings and what you should be doing to capitalise on this.

 Buy  24 Karat Gold Online 

Lower current account and fiscal deficit, a more stable rupee and lower inflation were all positive macro economic developments that led to the Indian equity markets outperforming with a 30% return in 2014. That is old wine now.

The rate cut – a directional shift

In 2014, while the improving macro numbers gave reason for the equity markets to cheer, the RBI clearly did not get sufficient comfort to go for a rate cut. The RBI governor had time and again spelt that once the monetary policy stance shifts, further policy actions will be consistent with such a stance. Seen from this angle, the rate cut last week, although just 25 basis points, can finally be seen as an endorsement of an easing interest rate cycle.

Even if we can expect measured cuts over several quarters from now, we think the directional shift is significant for two reasons:

One, sustained periods of lower interest rates are also periods of heightened urban consumption and growth which in turn buttress economic growth.

Two, a sustained rate cut also means lower cost of capital (cost of borrowing for companies), a key for improving the investment momentum by the private sector. As we see today, leaving out external risks, lack of investment in the private sector is the only (significant) missing link to the India growth story and lower cost of capital, together with reforms can put that part of the jigsaw puzzle in place.

So what did the markets not factor in last year? We think the markets remained uncertain about a sustained lower inflation number as the RBI kept a neutral rate stance even as inflation and deficit numbers improved. Also, the markets did not really reward the cyclical sectors as much as we thought it did (in the following section, you will see why we say this).

Hence, we think the upside coming from rate cuts were not fully factored earlier.

 Buy  24 Karat Gold Online 

What the markets factored

True, markets are not cheap. But neither are they expensive. As against the 5-year average of 15.1 times price earnings multiple, the Sensex (Bloomberg consensus) is trading at 15.3 times its one-year forward earnings. That means, it is not cheap but nor is it expensive. Compared with the 25-30 times it traded at in 2000-01 and 2007-08.

Besides, the below data from Nomura provides some interesting insights on what the markets factored in while rallying last year. Clearly, the defensive sectors (FMCG, pharma, IT) in the Sensex are much more expensive than the non-defensive (cyclical) segments. And the gap between defensive and non-defensive continue to be reasonably wide.




The equity market clearly saw the uncertainty stemming from higher inflation, higher interest rates and lower investment activity in the cyclical space and did not reward these sectors enough, last year, to narrow the gap in valuations between these 2 segments.

With the current rebound in the economy expected to largely benefit cyclical growth stocks, a narrowing of this gap would mean some more re-rating in the cyclical space.

 Buy  24 Karat Gold Online 

Uptick in activity

The above data suggests that the equity markets remained slightly skeptical about the cyclical space and rightly so since there appeared little evidence of any improvement in corporate earnings growth or investment activity by companies.

However, there are other less direct data points from the manufacturing indicators of PMI and IIP that suggest that economic activity and therefore corporate activity is indeed on the uptick. We give below some interesting data we sourced to bring out this shift.




Clearly, the increase in production of some of the key inputs used by the manufacturing sector and also an increase in import activity (signifying a possible uptick in demand for inputs by companies) all suggest that the manufacturing space may be seeing a slow rebound. This is also corroborated by an increase in the HSBC Purchasing Manager’s Index (PMI) for India.

While private investment activity could gain pace with some delays, we think an increase in spending by government (with easing deficit) and an already visible capex spending plan by public sector companies can set the ball rolling for the private companies to join the momentum. Rate cuts, could only further buttress such a shift.

The perceived risks

The question of whether FII flows will sustain is important for equity markets such as India as FIIs drive our markets. While this remains an inherent risk at all times, we see 2 mitigating factors at this juncture.

One, the clear differentiation in the premium afforded to some emerging markets will further become apparent this year. Countries that make a conscious effort in stabilising their economy with policy measures can be expected to get a premium for their equities. This trend is visible from mid 2014. India, clearly, appears to be among the top in such a list.

 Buy  24 Karat Gold Online 

Two, Asian countries such as Malaysia, Indonesia, Thailand and India which have the highest oil subsidy per person (source International Energy Agency data as of 2013), have benefitted the most from a global fall in oil prices.

Countries such as India have been prudent not to transmit the fall to consumers and have instead chosen to ease their deficit situation and improve tax collection (hike in excise duty). This further adds weight to its prudent economic measures.

The other perceived risk of a hike in Fed rates too, seems less threatening for India at this juncture for the following reason: it is the real rate differential of India with the US that typically drives foreign money flows into India.

The short-term real rate differential (3-month treasury rates) at 4.9 percentage points (as against 2 percentage points in May 2013) provides sufficient cushion for any shocks from US rate hikes. The long-term (10-year) rate differential too is at a comfortable 2.5 percentage points (negative in mid 2013). This positive real rate differential is likely to prevent any sudden exodus of capital.

What’s in it for you

To cut the long story short, we believe the equity markets could be ripe ground to start out serious long-term wealth building. While we are not advocating timing the market, we would like to ensure that you do not miss the positive signals that lay the ground for a bull market. Even as we say this, we do not rule out any intermittent corrections. Those are necessary and will keep the equity markets healthy.

For those of you who are in the game for the long term, we would recommend upping your mutual fund SIPs at this juncture, simply to ensure that you invest more now but still in a disciplined manner. For those who can take plenty of intermittent shocks and have a 10-year plus view, lump sums, to our mind, can do no harm.

Even as I say this, please ensure that you have a well-balanced asset allocation and not go all out on equity. Our Select Funds have been chosen keeping the above factors in mind and picked based on portfolios that are well poised to make good the rebound. Choose well, based on your risk appetite or seek our advisor’s help.

 Buy  24 Karat Gold Online 

Other articles you may like
	The wait for rate cut is set to continue
	Don't be afraid to get rich
	Changes to FundsIndia's Select Funds
	FundsIndia Recommends: Axis Banking & PSU Debt





			
			Post Views: 
			2,071
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    equity market. Mutual fund
                

			

            	


			
			5 thoughts on “FundsIndia Equity Strategy: Why the India shining story has just begun”		


		
			
			
				
					
												Bharani says:					


					
						
							
								January 21, 2015 at 4:21 pm							
						
											


									

				
					Very well researched article. Liked the risk part in the piece, with strong supporting data. Could’ve been better if this added the near-term prognosis for cyclical/defensive based on P/E run-up and due to rate-cut in rate-sensitive sectors. My assessment is PE would moderate over the later part of 2015, while near-term impact will be more on both sectors due to sentiments (well, I am just a novice!!

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								January 22, 2015 at 10:24 pm							
						
											


									

				
					Bharani, thanks for your comments. Your suggestions are noted. Appreciate it. Vidya

				


				Reply
			




	
			
				
					
												Rajiv Ahuja says:					


					
						
							
								February 1, 2015 at 3:26 pm							
						
											


									

				
					I know these question I am about to ask have no relation to this article. But what are views on UTI Balanced Fund & ICICI Pru Advantaged Balanced fund. I invested in these funds in July 2015. Should I exit now  ? Are these good funds ?

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								February 2, 2015 at 10:24 am							
						
											


									

				
					Hello Rajiv, Thanks you for writing to us. I request to please route any fund or portfolio specific query through your FundsIndia account (click help tab and choose advisory appointment and mail us your query), if you are a FundsIndia investor. Since this would amount to advice/opinion, we have a process to follow and track, if questions are routed through your account. thanks, Vidya

				


				Reply
			
	
			
				
					
												Rajiv Ahuja says:					


					
						
							
								February 2, 2015 at 11:10 am							
						
											


									

				
					I have summited a query ,like you have said.

				


				Reply
			










		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


