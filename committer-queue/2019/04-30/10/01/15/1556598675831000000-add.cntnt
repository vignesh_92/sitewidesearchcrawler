

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		“Closed-end funds allow fund managers to take concentrated positions”

            		November 22, 2013 . Vidya Bala
                    

	
                    
                                    Mutual funds
                    
		
            	

            	Reliance mutual recently  launched a closed-end equity fund – Reliance Close-Ended Equity Fund. This five-year closed-end fund will be managed by Shailesh Raj Bahn, who also manages one of the top performing funds, Reliance Equity Opportunities. The NFO closes on November 29.


FundsIndia.com has a Q&A with the fund house on the reason behind launching this fund and how distinct it would be from other schemes in the stable.

Excerpts:

What is the reason for launching a close-ended equity fund now? 

The last few years had been quite challenging for the equity markets due to the global slowdown and the subsequent negative impact on the Indian Economy and stock markets.

The stock markets have recovered a bit in the recent past (S&P BSE Sensex closed at 21209 on 31st Oct, 2013, one of the highest closing levels) due to the recent improvements in certain macro parameters. However, the rally is quite concentrated and skewed.

We believe such skewness and narrowness of the markets have left behind lots of good quality companies with sustainable business models & proven track records at low absolute valuations. We expect some of these ideas to do considerably well over the medium to long term.

In order to capture some of these opportunities, we are launching Reliance Close-ended Equity Fund – Series A. Close-ended format would allow the portfolio manager the flexibility to execute the strategies effectively over the chosen time frame – in this case, over 5 years.

Further, the portfolio could be constructed based on the market merits, without possibly getting impacted by external flows. Close-ended funds also allows fund managers to take concentrated positions in stocks / sectors and possibly offer portfolios that may be distinct and unique from other open-ended funds.

Indian markets have roughly seen 8-year cycles, topping the charts in 1992, 2000 and 2008. Do you think we are therefore still not at the verge of a recovery? 

The last few years have been truly exceptional where we have witnessed a few ‘black swan’ events leading to unprecedented outcome such as the mortgage crisis in the US, Sovereign debt crisis in the Europe and much of the economies, including emerging economies like India getting impacted. It may not be appropriate to draw inferences about future market movements, based on the past, particularly based on the last five years.

In our opinion, some of the factors plaguing the markets in past 5 years are likely to be different in the next 5 years:

US recovery in contrast to US recession of 2008

	The US recovery is expected to be in full swing by 2015
	Likely to aid other developed markets to get back on their feet, which, in turn, is expected to push export growth within emerging economies


Flattening of oil prices in contrast to rising oil prices 

	This would  relieve pressure on tight government finances and contain the current account deficit
	Fuel-led inflation likely to be under control
	The resultant INR stability key for stable capital inflows


Domestic reforms, such as liberalization in FDI in retail and pensions, in contrast to lack of reforms

	Key Bills passed in recent times – Land Acquisition and Rehabilitation Bill, the Pension Bill, & Companies Bill
	Other reforms on the way – Undoing the retrospective tax changes, FDI in multi-brand retail



On the whole, we may see the next five years as bringing in the third round of liberalization measures that may help the economy get back on its potential growth track.

We think this is very good time for long term portfolio construction for the following reasons:

	GDP growth estimates have been toned down to 4 – 4.5%
	Corporate profit margin has fallen to low of 6.1% (for top 418 companies, Ex Financials), which is close to low of 5.9% it touched in FY 2002
	Valuations for midcap companies are at the lows  of 2001
	Equity allocation is at all time low as investors are overweight in gold & real estate (both these sectors are showing signs of fatigue)
	G-Sec yield close to 9%, it peaked at similar level in Mid 2008 and then declined to 5%
	INR Depreciation
	Elections are around the corner – investor expectations are close to all time low
	Government is pushed to the wall (that’s when it acts)
	Twin Deficit
	Fuel price hike (no choice left)
	Policy paralysis
	Fear of rating downgrade
	India has had one of the best monsoon in last 35 years, agriculture growth set to rebound sharply – rural demand to be strong – market is ignoring this


In addition to these fundamental factors, polarized valuation offers good investment opportunity in 101 to 500 ranked companies and offers tremendous opportunity for alpha creation.

What segments/sectors in the market do you currently see opportunity?

Given the market distortions, where 2/3rd of the markets are trading way below their peaks and 1/3rd of the markets is below even their 2009 lows, we are positive on a few sectors like Engineering, Capital Goods, Auto, Auto ancillaries, Midcap IT, Midcap Pharma, Retail and Media, where good stocks are available for attractive valuations.

With interest rates not showing much signs of moving southward, is there a risk of debt and interest servicing pressures hurting corporate financials further? 

We believe we are at the fag end of a rate hike cycle and RBI would once again embark on taking pro-growth measures, which will support yield going forward and keep interest rates in the system relatively low. Some of the positives for the markets are as follows:

	Expected listing of India’s sovereign debt on leading international indices will attract $15 – 20 billion of FII flows every year into the Indian Debt market.
	RBI has already conducted three Open Market Operation (OMOs) purchases of around Rs 22,400 crore since August end. As RBI injected additional daily liquidity of around Rs 20,000 cr recently through term Repo, we do not expect OMOs immediately. But once policy rates are normalized, RBI will start OMO purchase again as liquidity is expected to remain tight (due to continuous supply in Nov and advance corporate tax outflows in Dec). OMOs would further support yields at the longer end of the curve
	The RBI has revised trajectory on Inflation (WPI around 7% & CPI around 9%). If the food prices fall substantially in winter there is a possibility of inflation surprising positively.
	Improving global macro scenario and dovish FED stance with further postponement of Quantitative Easing (QE) might increase portfolio flows into the country.
	Improving sentiments prior to elections would start reflecting on the forex as well as the equity markets and prompt additional portfolio flows into the country easing liquidity in the system.


Is Reliance Closed end Equity likely to have a mid-cap bias? 

The fund is expected to have 70-80% exposure in midcaps, considering the narrowness of the market movement where much of the broader markets have been left behind leaving lots of opportunities where good businesses are available at attractive valuations.

These midcaps would mostly be larger midcaps.  The stocks would have to fulfill the investment framework of having solid well-established businesses and other sustainable business characteristics and yet available for attractive valuations. The Remaining 20-30% would be in large-caps.

How different is this fund going to be from your open-ended diversified funds such as Reliance Equity Opportunities?

The fund would be quite different from any other open-ended fund. Following are some of the USPs of the Fund: 

	The Fund would be benchmark-agnostic. i.e., it may not mirror the benchmark. It may take significant deviation from the benchmark to invest into stocks fitting into the framework of the Fund explained in the previous question. The approach is very different from that taken by most open-ended funds, which are managed very close to the benchmark.
	The fund will focus on absolute valuations and alpha generation.
	The fund may hold significant positions in a few stocks that have solid businesses and yet are available at attractive valuations.
	The fund would endeavor to pay substantial dividends under the Dividend Payout option, depending on the opportunities and the market scenario.


Other articles you may like
	To be or not to be in the market?
	FundsIndia Reviews: Axis Mid Cap Fund
	FundsIndia Strategies: The higher-returning FD alternative
	The job of a debt fund





			
			Post Views: 
			2,209
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    Mutual fund. NFO
                

			

            	


			
			3 thoughts on ““Closed-end funds allow fund managers to take concentrated positions””		


		
			
			
				
					
												shankar says:					


					
						
							
								November 22, 2013 at 2:49 pm							
						
											


									

				
					But most of the closed end funds have never performed

				


				Reply
			

	
			
				
					
												e m sivasnkaran says:					


					
						
							
								November 22, 2013 at 4:36 pm							
						
											


									

				
					those who are interested in speculation should invest in the fund.it is not for ordinary investor.

				


				Reply
			

	
			
				
					
												ramanathan dwarakanathan says:					


					
						
							
								November 22, 2013 at 5:37 pm							
						
											


									

				
					I agree. lots of these ideas mentioned by shailesh can be done in their existing scheme as well.

				


				Reply
			




		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


