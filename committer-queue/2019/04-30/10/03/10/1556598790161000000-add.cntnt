

    
        
            
                
                        
                    

            


            
                    
                        
                            
                        
                    
                
                    
                    
                        Search for:
                        
                    
                    
                    

                

            

            
                
                    Insights

                

            
            	 Get App

Subscribe
                Login
            
            
                
                        
                            Search for:
                            
                        
                        
                

            

            
        
        
            
                
                    
                        
                            
                                Toggle navigation
                                
                                
                                
                            
                        

                        
                            	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance


                        

                    
                    
                

            

        
		
    

    

        
            
                Insights

            

            
    

    
         
        	Mutual Fund Basics
	Mutual Fund Research
	Personal Finance
	Equity Research
	General
	FI Platform
	Insurance

        

    
    

	
	
		
				
			
            	
            		FundsIndia Equity Strategies: A Portfolio for Election Euphoria and Beyond

            		March 12, 2014 . Vidya Bala
                    

	
                    
                                    Mutual funds . Recommendations
                    
		
            	

            	A little over a week into March and you see the Sensex climb swiftly by over 800 points, or up by close to 4%. While you might think that the announcement of election dates simply triggered this rally, there appears more to the rally than meets the eye.

But we are not going into a political analysis here. We simply wish to find an answer to the question – Which sectors/stocks would stand to benefit the most pre and post elections? To know this, we did a quick exercise on what has been the choice of investors and institutions in the recent rally and how different is this move.

Discerning Investment

The current rally suggests that investors appear more discerning in their choice; and that encouraged us to take cues from the market. While they did go for under-valued stocks, the top gainers suggest that stocks that are over leveraged or have other management concerns have not mostly been in the toppers’ list. However, quite a few joined the party later.

Reliance My Gold Plan – Get 6% Extra Gold

Similarly, unlike earlier rallies, when companies flocked the markets to raise money through IPOs or placements, they have stayed away this time. They clearly seem to read that investors are in no mood to pick IPOs of companies that are desperate for funds. In fact, these companies appear to rather wait for a better equity environment to offload some of their assets to deleverage their balance sheet, is in contrast to what corporates usually do in market euphoria.

The recent rally clearly appears to suggest that investors are picky in their choice of stocks and sectors.  A look at these sectors and you will see that – these sectors are under-owned, stocks in these spaces are undervalued, but not poor in ‘quality’, and there are policy initiatives that are already underway in some of these segments. Also, such initiatives cannot significantly differ, irrespective of which party rules.



Of course, there are also sectors that could directly benefit from a boost in investors’ sentiment, notwithstanding policy moves. We think some of the stocks in such sectors could continue to witness momentum, the election outcome notwithstanding (but we are not factoring a third front for this purpose), simply because the job at hand for any government would be to revive the investment cycle.

Such stocks are currently being picked from sectors such as banking and finance, energy, engineering and industrials, telecom and pockets of infrastructure, to name a few.

We dug deeper into these sectors to know what are the kinds of regulatory/policy initiatives that have been happening and what sectors could benefit simply from even a sentiment boost. Here’s what we found out:

	In the power space, there has been a gradual hike in tariffs to reduce losses of State Electricity Boards and aid their restructuring. Fuel supply uncertainty for power plants reduced after Coal India signed supply agreements.
	In the oil and gas space, rationalization of subsidy is underway, what with gradual increase in diesel prices over several months now.
	Export duty on iron ore and iron ore pellets has ensured better availability locally, keeping prices at bay for consuming industries.
	In telecom, spectrum auction and spectrum trading, irrespective of election outcome, can result in specific stocks becoming major beneficiaries.
	Continuing digitization policies in media could also boost specific stocks.
	At a more macro level, the number of coal mining projects given clearance has shot up post November 2013, after change in the Ministry’s head. Besides, a number of stuck infrastructure projects such as the power project of NTPC’s have been cleared by the Cabinet Committee on Investments.


Reliance My Gold Plan – Get 6% Extra Gold

Beneficiaries of Sentiment Boost

Besides, there are a few other segments that could be direct beneficiaries of a boost in investor sentiment:

	Currently, banks are weighed down by asset quality concerns that can be split into two – one from concerns that are more cyclical in nature – that is companies that are otherwise sound but suffering from the prolonged macro economic slowdown. Two, those weighed down by  structural issues – fundamentally weak companies that borrowed and have turned bad quickly as a result of harsh external environment. We believe while the latter (structural) will be something that banks, especially public sector banks, will learn the hard way, a boost in sentiment could well reduce the asset quality concerns for banks laden with assets that have more cyclical issues.
	Engineering and capital good stocks of quality companies could be among the biggest beneficiaries when sentiments turn.
	Banks/finance companies with focus on infrastructure lending could once again see a revival in their fortunes.
	Select consumer discretionaries (which have been neglected for a while now), which may include retail chains or automobiles, may see an uptick in their fortunes.


How to Play

Now if you are an astute stock picker, it makes sense to look for specific opportunities. But for mutual fund investors, it is evident that some fund managers have already sniffed the opportunities and are steadily tweaking their portfolio, looking beyond elections.

Given the sector-specific opportunities we have stated, you may well think that we would come up with specific sector themes to play these opportunities.

However, at this point, given that economic indicators still remain weak, we would hesitate to go with sector funds from these segments (except Franklin Build India, which we covered a few weeks ago) at this juncture. The downside, if the strategy goes off beam, could be more painful with sector funds and may not also provide time to make a quick move to other opportune sectors.

Hence, we decided to fish for funds that had diversified portfolios with focus, in recent times, on sectors/stocks that benefit from policies/improving sentiments.

We had quite a few funds but decided to have a combination of dark horse plays, value picks and of course, predominant mid-cap focus and exposure to sectors discussed above, to build a 3-fund portfolio. The funds are HDFC Top 200, Franklin India Prima and ICICI Pru Discovery (we have at various junctures, reviewed these funds). We would go with equal exposure to these funds.

Please note that we prefer this as a portfolio holding (call it the ‘Revival Story Portfolio’, if you please!) as this would give a combination of stocks across market-cap segments.

For instance, HDFC Top 200, which is our dark horse play (after underperforming for a good part in 2013), will provide sufficient large-cap focus. Its overweight position in finance and energy can buttress its performance.

Similarly, Franklin India Prima’s overweight position in the engineering space and ICICI Pru Discovery’s focus in all the sectors (finance, energy, capital goods) we just mentioned, were key considerations behind our choice. The last said fund could also be one to provide some hedge to the portfolio in case of volatility.

Beyond sectors, the quality of stocks would ultimately help not just ride a rally, but prevent too much pain if fortunes prove otherwise. Hence,the ability to withstand downturns (HDFC top 200 being an exception) was a key consideration behind the choice of the other 2 mid-cap biased funds.

Suitability

This portfolio is strictly for people looking for opportunities and willing to take risks. Also, if you are already laden with too many midcap funds, this may not fit you.

We will, of course, go with the SIP route to invest in these funds. However, if you can buy on dips in markets, you should consider such an option too, as we reckon volatility could well continue. Have a minimum of 3-year view on the portfolio and be ready to book profits in years of exceptional rallies. We will review this portfolio strategy if we feel the need for a rejig.

Reliance My Gold Plan – Get 6% Extra Gold

Other articles you may like
	FundsIndia reviews: JP Morgan JF ASEAN Equity Offshore
	REC tax-free bond offers attractive rates
	“We are aiming for equity-like returns with low volatility”
	How to choose liquid funds





			
			Post Views: 
			3,747
			
Share this:
	Click to share on WhatsApp (Opens in new window)
	Click to share on Facebook (Opens in new window)
	Click to share on Twitter (Opens in new window)
	Click to share on LinkedIn (Opens in new window)
	Click to share on Reddit (Opens in new window)
	Click to share on Google+ (Opens in new window)
	




Like this:
Like Loading...

Comments

                
                
                    
                                                Read Previous Article
                    

                    
                                                Read Next Article
                    

                

                
                 
                    Comments

                

                
                    Get FundsIndia’s articles delivered straight to your inbox!
			Enter your email address to get:

	Mutual fund recommendations from experts
	Buy, hold or sell calls for stocks
	Investment tips and tricks
	All the latest news from Fundsindia.com


Subscribe to Blog via Email

			
				Enter your email address to subscribe to this blog and receive notifications of new posts by email.


					
						
							Email Address						
						
					


					
						
						
						
						
												
					

							


			
				




		
                

                
                    2014. elections. fund recommendation. strategy
                

			

            	


			
			11 thoughts on “FundsIndia Equity Strategies: A Portfolio for Election Euphoria and Beyond”		


		
			
			
				
					
												Udaya Poojary says:					


					
						
							
								March 12, 2014 at 7:23 pm							
						
											


									

				
					Hi Vidya,

Thanks for sharing such valuable information, which is really helpful to the investor,

Here i need one point of clarification, in the last paragraph you have mentioned “Have a minimum of 3-year view on the portfolio and be ready to book profits in years of exceptional rallies”.

Talking about profit booking, i have an investment HDFC Top 200 (which i did before i joined FundsIndia), and the last SIP was in the month of August 2013. Now this fund is giving me a profit of approx Rs.5000 against the actual investment of Rs.30,000.

I had no plan of redeeming this Fund but after reading your blog, i need your suggestion on profit booking on this Fund. Should i go for profit booking or should / can i keep this Fund for another couple of years.

Please suggest.

Thanks and Regards,

Udaya Poojary

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								March 13, 2014 at 10:34 am							
						
											


									

				
					Hi Udaya, Thank you for commenting. The profit booking I mentioned here is only for the entire portfolio and also for ‘exceptional gains’…like in 2007 when many funds gained anywhere between 70-100% in a single year. Such gain basically upsets your original asset allocation, hence the need to profit book. We cannot think of any exceptional gains in the past few years. Also you have onyl in August 2013 completed SIPs. Hence wait it out. Equity investing is certainly a long-term approach. Because we have had muted markets in recent times, gains may also be extraordinary, esp. if election outcome suits the market’s fancies. Even when you book profits, you should redeploy. For istance, if your portfolio is overweight on equities, when you book profits, you should redeploy in debt funds or sometimes even within equities, in segments which just seem to be picking up; and not let it lie in savings account 🙂 thanks, Vidya

				


				Reply
			
	
			
				
					
												Udaya Poojary says:					


					
						
							
								March 13, 2014 at 10:47 am							
						
											


									

				
					Hi Vidya,

Thanks for clarifying the doubts… 🙂

				


				Reply
			







	
			
				
					
												Vivek says:					


					
						
							
								March 18, 2014 at 11:19 am							
						
											


									

				
					Hi Vidya,

I was looking for a dynamic portfolio where I can stay invested in the funds for 3-5 years. For that I have selected a few funds:

1. ICICI exp and services fund

2. SBI Magnum Midcap Fund

3. Mirae Asset Emerging Bluchip Fund/ ICICI Pru Discovery fund

The reason I am posting this question here is that the strategy you have mentioned in this article is more or less on same lines as I was planning.I have selected these funds based on track record and rating.

Now that I have read your article I am a bit confused about how to go about it.

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								March 22, 2014 at 7:58 pm							
						
											


									

				
					Hello vivek,

As mentioned in the article, the portfolio was chosen based on whether they had sufficient weights to the sectors that we expect to pick up based on the thesis we mentioned there. In this regard, ICICI Pru Export, SBI Magnum Midcap and Mirae Asset Emerging Bluechip are not overweight on the sectors we mentioned in this article. Hence, they would not fit the theme we mentioned. But separately if you wish to play the IT and pharma space then ICICI Export is a good choice. thanks, Vidya

				


				Reply
			
	
			
				
					
												Vivek says:					


					
						
							
								March 24, 2014 at 10:24 am							
						
											


									

				
					Hi Vidya,

Thanks for the clarification. 

Vivek

				


				Reply
			







	
			
				
					
												sharad sinha says:					


					
						
							
								April 1, 2014 at 11:38 pm							
						
											


									

				
					hi vidya i am investing 2000 pm via sip in can robeco tax saver(G) for this fin year i want to invest 36000 in elss.should i go for icici pru tax plan or axis long term equity for 1000? or to reduce can robeco to 1000 and add new sip of 2000 of any of these two? thanks

sharad

				


				Reply
			

	
			
				
					
												sharad sinha says:					


					
						
							
								April 1, 2014 at 11:39 pm							
						
											


									

				
					hi vidya, got your point and my question is a bit out of the context.I am investing in sbi emerging business and uti opportunities and was deciding to stop these sips and start new in franklin india smaller comps and mirae india opportunities regular .now franklin india smaller comps or Prima which one to choose ?? should i go for mirae india opportuinies regular??

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								April 9, 2014 at 3:37 pm							
						
											


									

				
					hello Sharad, If you hold an FundsIndia account, kindly route your portfolio related questions, though the Advisor support to ensure systematic and prompt feedback. To broadly answer your question: Between SBI Emergign Businesses, Franklin India Smaller Cos. and Franklin India Prima the first is most risky and the last least risky although all are in the mid and small-cap category. Similarly, between UTI and Mirae, Mirae can under perform for shorter periods but consistent over long periods while UTI is more dynamic. Both have a large-cap tilt although hold some midcaps. You may choose accordingly. thanks, Vidya

				


				Reply
			




	
			
				
					
												Kumar says:					


					
						
							
								May 17, 2014 at 1:22 am							
						
											


									

				
					Hi Vidya.  Thanks for the insightful article. Request your views on below choice.

I plan to invest 6k/ month via SIP for minimum 5 yrs & beyond.  Have selected : Idfc premier equity, Icici focus blue chip & Hdfc Balanced fund.  

Can you please suggest if thats a right fund mix. Regards.

				


				Reply
			
	
			
				
					
												Vidya Bala says:					


					
						
							
								May 21, 2014 at 11:58 am							
						
											


									

				
					Hi Shravan Kumar,

Being our investor, I have raised this as a ticket in your name and you shall receive my response through mail.

In future, request you to use the help tab where you will find a feature called ‘Advisory appointment’. Kindly send your query there and our advisors will respond to it. This will also help us track it and respond in a timely manner. The blog is more of a discussion forum.

thanks

Vidya

				


				Reply
			







		
		
							Leave a Reply					Cancel reply
				

						
				
				
				
				
			

		


		
		

		This site uses Akismet to reduce spam. Learn how your comment data is processed.




		
 
				
            
                Back to Basics

                                                 
                                  	
                                        What are the benefits of investing in mutual funds?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        What is a mutual fund and why should you invest in it?                                        
                                          FundsIndia Desk
                                      

 
                              

                                                
                                  	
                                        7 Reasons to start a SIP                                        
                                          Shweta Nichani
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: When is it a good time to invest?                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        7 reasons why you should invest in Mutual Funds                                        
                                          Aparna Hari
                                      

 
                              

                                                
                                  	
                                        FundsIndia explains: Closed-ended Fixed Income Mutual Funds                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                                
                                  	
                                        FundsIndia Explains: Why all equity funds aren’t the same                                        
                                          Mutual Fund Research Desk
                                      

 
                              

                                    
                    View all posts
                    

            

		    
 
 
Facebook


 
		     
		
	




		
			
				
			

			
				
			

			
				
			

		










        
            
				
                    Sign up with FundsIndia
                

                
                    
                        
                    
                    FundsIndia is India’s friendliest online-only investment platform. Built on robust technology, FundsIndia gives users access to mutual funds from leading fund houses in India, corporate fixed deposits, stocks from the BSE and various other investment products, all in one convenient online location. In short, FundsIndia is your one stop shop to build wealth.

                    
                        
                    
                    
                        
                    
                

                
                    About FundsIndia

                    	Media
	Partners
	Careers
	Become an Affiliate
	FAQs
	Reach us
	Privacy
	Terms & Conditions


                

                
                    Our Offerings

                    	Top Mutual Fund Schemes
	Money Mitr
	Investment Calculators
	Funds Explorer
	Mutual Fund Categories
	Knowledge Center


                    We are Social

                    	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        
	
                            
                        


                

                
                    
                        Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Please consider your specific investment requirements before choosing a fund, or designing a portfolio that suits your needs.
                    

                    
                        Wealth India Financial Services Pvt. Ltd. (with ARN code 69583) makes no warranties or representations, express or implied, on products offered through the platform. It accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services. Terms and conditions of the website are applicable.
                    

                    
                        Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.The Android robot is reproduced or modified from work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
                    

                    SEBI Reg. No. INB011468932 | ARN - 69583 

                

            

        
        
            
                
                    
                        
                                Market Risk Statement
                            
                        © 2019 FundsIndia.com
                        
                            Mutual Funds Explorer

                            
                                Axis,
                                Baroda Pioneer,
                                Birla Sun Life,
                                BNP Paribas,
                                BOI AXA,
                                Canara Robeco,
                                DSP BlackRock,
                                Edelweiss,
                                Franklin Templeton,
                                HDFC,
                                HSBC,
                                ICICI Prudential,
                                IDBI,
                                IDFC,
                                IIFL,
                                Indiabulls,
                                Invesco,
                                JM Financial,
                                Kotak Mahindra,
                                L&T,
                                LIC Nomura,
                                Mirae Asset,
                                Motilal Oswal,
                                PPFAS,
                                Principal,
                                Quantum,
                                Reliance,
                                SBI,
                                Shriram,
                                Tata,
                                Taurus,
                                UTI
                            

                            Mutual Fund Categories

                            
                                Tax Saving Funds,
                                Diversified Funds,
                                Large-cap Funds,
                                Mid & Small cap Funds,
                                International Funds,
                                Sector Funds,
                                Liquid Funds,
                                Short-term Funds,
                                Gold Funds
                            

                            BSE 30 Stocks

                            
                                Bajaj Auto Ltd,
                                Jindal Steel & Power Ltd,
                                Ntpc Ltd,
                                Maruti Suzuki India Ltd,
                                Tata Consultancy Services Ltd,
                                Tata Power Co.Ltd,
                                Oil And Natural Gas Corporation Ltd,
                                Hero Motocorp Ltd,
                                Tata Motors Ltd,
                                Itc Ltd,
                                Gail (India) Ltd,
                                Mahindra & Mahindra Ltd,
                                Icici Bank Ltd,
                                Dr.Reddys Laboratories Ltd,
                                Tata Steel Ltd,
                                Wipro Ltd,
                                State Bank Of India,
                                Sun Pharmaceutical Industries Ltd,
                                Hdfc Bank Ltd,
                                Hindalco Industries Ltd,
                                Hindustan Unilever Ltd,
                                Larsen & Toubro Ltd,
                                Infosys Ltd,
                                Housing Development Finance Corp.Ltd,
                                Coal India Ltd,
                                Bharti Airtel Ltd,
                                Bharat Heavy Electricals Ltd,
                                Cipla Ltd,
                                Reliance Industries Ltd,
                                Sesa Goa Ltd
                            

                            FUNDSINDIA.COM-WHO WE ARE AND WHAT WE DO

                            FundsIndia is India's friendliest online investment platform. Here, investors (resident Indians and NRIs) get access to a wide range of Mutual Funds, Equities from the Bombay Stock Exchange (BSE),Corporate Deposits from premium companies, and various other investment products in one convenient online location.

                            FundsIndia also offers a host of beneficial value-added services like free financial advisory services, flexible types of Systematic Investment Plans (SIPs), trigger-based investing, Portfolio-level SIPs, SIP Designer, Value-averaging Investment Plans (VIPs), instant Portfolio X-rays, and so much more that further enrich an investor's investment experience. Also, with India's most complete automated advisory service, Money Mitr, investors can get great mutual fund recommendations for lump sum and SIP-investing automagically.

                            Registering with FundsIndia takes less than two minutes, what with the Aadhaar-based eKYC system. This makes investing a paperless and hassle-free process, and FundsIndia the best investment platform in India.

                            Investing with a FundsIndia account is absolutely safe and secure. The platform is registered with entities such as the Association of Mutual Funds in India (AMFI), the Bombay Stock Exchange (BSE), the Credit Information Bureau Limited (CIBIL), the Central Depository Services Limited (CDSL), and the Central Insurance Repository Limited (CIRL).

                            FundsIndia also offers many resources that can help investors make the right investment decision before they invest online. Some of them are access to different types of investment calculators, tax calculators to know how much you need to save in tax-saving mutual funds (or ELSS) to benefit from exemptions, and so on.

                            Thus, FundsIndia is a one-stop shop for Indian investors who'd like to invest in the best mutual funds, buy shares or stocks online by opening a demat account, and who'd like to get access to the best investment plans and different ways of investing through SIPs. Every investor with FundsIndia gets a dedicated investment advisor too, making FundsIndia a full-fledged financial advisory services portal for investors who aspire to build wealth.
                            

                        

                    

                

            

        
    


